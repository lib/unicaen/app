AjaxModalDialog
===============

Cette aide de vue :

-   intercepte le clic sur un lien ayant la classe CSS `ajax-modal`,
    lance la requête correspondante en AJAX et ouvre le résultat dans
    une fenêtre modale Bootstrap 3 ;
-   simplifie la gestion d\'un éventuel formulaire présent dans cette
    fenêtre modale.

\<note warning\> Cette aide de vue \"manuelle\" est devenue caduque du
fait que le mécanisme est désormais installé systématiquement sur tous
les liens hypertextes ayant la classe CSS `ajax-modal`. Cf.
[Javascript](/develop/unicaen2/moduleunicaenunicaenapp/js).

Le mécanisme d\'événement évoqué plus bas reste néanmoins d\'actualité.
\</note\>

\<note tip\>Rappel : il est possible de spécifier la classe `ajax-modal`
sur les pages de navigation.\</note\>

\<note important\>Le composant
[ModalListener](/develop/unicaen2/moduleunicaenunicaenapp/mvc#modallistener)
(activé dans le module UnicaenApp) est indispensable au fonctionnement
de cette aide de vue.\</note\>

1/ Sans formulaire
------------------

Si l\'action à \"ouvrir\" dans la fenêtre modale ne contient aucun
formulaire, ce paragraphe est pour vous.

Exemple de vue contenant le lien à ouvrir :

``` {.php}
<a title="Modifier le service" href="/ose/service/saisie/147"
   class="ajax-modal"><span class="glyphicon glyphicon-edit"></span></a>
<?php echo $this->modalAjaxDialog('ma-div-id'); ?>
```

\<note important\>Le lien hypertexte doit posséder la classe CSS
`ajax-modal`.\</note\>

2/ Avec formulaire
------------------

Ce paragraphe explique comment cette aide de vue peut vous simplifier la
gestion d\'un formulaire s\'affichant dans la fenêtre modale.

Le clic sur le bouton \"submit\" du formulaire (i.e. bouton ayant la
classe CSS `btn-primary`) présent dans la fenêtre modale est intercepté
pour soumettre la requête en Ajax (en POST). Deux cas de figure
possibles selon le contenu (HTML) de la réponse reçue :

-   si une balise HTML ayant la classe CSS `has-error` ou `has-errors`
    ou `alert` ou `input-error` est trouvée, alors cela veut dire qu\'il
    y a des erreurs de validation du formulaire : rien ne se passe.
-   sinon, l\'événement jQuery dont le nom est spécifié par l\'attribut
    `data-event` du lien cliqué est déclenché sur le \"body\" de la
    page, donnant l\'opportunité à qui écoute cet événement de mettre à
    jour l\'interface par exemple et *notamment de fermer la fenêtre
    modale*.

\<note important\>Le lien doit fournir via l\'attribut `data-event` le
nom de l\'événement qui sera déclenché à la soumission de ce
formulaire.\</note\>

Exemple de vue contenant le lien à ouvrir :

``` {.php}
<a title="Modifier le service" href="/ose/service/saisie/147"
   class="ajax-modal"
   data-event="service-modify-message"><span class="glyphicon glyphicon-edit"></span></a>
<?php echo $this->modalAjaxDialog('ma-div-id'); ?>

<script>
    $(function() {
        // écoute de l'événement
        $("body").on("service-modify-message", function(event, data) {
            /**
             * event          : événement jQuery (cf. http://api.jquery.com/category/events/event-object/)
             *   event.type   : nom de l'événement ("save-message" dans cet exemple)
             *   event.div    : DIV correspondant à la fenêtre modale (objet jQuery)
             *   event.a      : lien hypertexte cliqué
             * data           : formulaire sérialisé en tableau à l'aide de jQuery.serializeArray()
             */
            console.log("Event received : ", event);
            console.log("With args : ", data);
            // ...
            event.div.modal('hide'); // ferme la fenêtre modale
        });
    });
</script>
```


[Bootstrap DatetimePicker](./widgets/bootstrap-datetimepicker.md)

[PopAjax](./widgets/pop-ajax.md)

[TabAjax](./widgets/tab-ajax.md)

Création d\'éléments d\'interface \"riches\" mélangeant HTML et Javascript
==========================================================================

Il est parfois nécessaire d\'adjoindre à des éléments d\'interface en
HTML des traitements en Javascript.

Il est préférable de concentrer le code Javascript au sein de widgets
Jquery (<https://jqueryui.com/widget/>) enregistrées dans des fichiers
au format `.js`{.php} dédiés.

-   Ainsi la logique objet est préservée jusqu\'au niveau de
    l\'interface, avec tous les avantages en terme de lisibilité du
    sode, réutilisabilité, etc.

<!-- -->

-   Ensuite ce code n\'est chargé qu\'une fois par le navigateur, même
    si ce dernier est amené à être exploité à plusieurs endroits de la
    page.

<!-- -->

-   De plus, ce fichier JS peut être mis en cache par le navigateur,
    d\'où une économie de bande passante et une rapidité accrue.

<!-- -->

-   Ensuite, le code javascript d\'une application peut être
    \"minifié\", c\'est-à-dire que les caractères superflus sont
    éliminés pour que le fichier soit encore plus rapide à charger et à
    analyser par le navigateur.

<!-- -->

-   Enfin, pour un développeur, il est plus simple de déboguer du code
    Javascript dans un fichier .js que directement au sein d\'une page
    Web à l\'aide des outils de développement livrés avec la plupart des
    navigateurs modernes (Firfox, Chrome, etc).

Par ailleurs, ces éléments d\'interface doivent être codés de telle
manière qu\'ils doivent fonctionner parfaitement même si plusieurs
instances sont affichés dans une même page. Autrement dit, le codage de
l\'élément doit être fait de telle manière que la structure de la page
web sous-jacente ne doit avoir aucun effet indésirable.

En voici sans plus tarder un exemple de Widget :

-   Dans ce fichier HTML, que du HTML, pas de bouts de Javascript!!
-   La classe fenetre-confirmation permet de savoir que cet élément est
    bien une fenêtre de confirmation et donc que ceci doit être un
    Widget fenetreConfirmation.
-   L\'ID est indiqué pour pouvoir la sélectionner ensuite mais il
    n\'est pas obligatoire de l\'ajouter.

``` {.html}
<div class="fenetre-confirmation" id="fen1">
    <p id="message">Ceci est un texte. L'avez-vous lu ?</p>
    <button id="oui">OUI</button>
    <button id="non">NON</button>
</div>
```

Et voici le code du widget en Javascript :

``` {.javascript}
$.widget("unicaen.fenetreConfirmation", {

    message: function (message)
    {
        if (message === undefined) {
            return this.getMessageP().html();
        } else {
            this.getMessageP().html(message);
        }
    },

    repondreOui: function ()
    {
        alert('Parfait, merci!');
        this.fermer();
    },

    repondreNon: function ()
    {
        alert('Dommage...');
    },

    fermer: function ()
    {
        this.element.hide();
    },

    _create: function ()
    {
        var that = this;

        // On initialise les callbacks
        this.getBtnOui().on('click', function () { that.repondreOui(); });
        this.getBtnNon().on('click', function () { that.repondreNon(); });
    },

    // c'est pas obligatoire mais c'est plus propre de définir des accesseurs pour les éléments du widget...
    getMessageP: function () { return this.element.find("p#message"); },
    getBtnOui: function () { return this.element.find("button#oui"); },
    getBtnNon: function () { return this.element.find("button#non"); }

});

$(function ()
{
    WidgetInitializer.add('fenetre-confirmation', 'fenetreConfirmation');
});
```

Grâce au
[WidgetInitializer](/develop/unicaen2/ModuleUnicaenUnicaenApp/js), il
n\'y a rien d\'autre à faire : le click sur \"Oui\" va fermer la fenêtre
de confirmation correspondante. Il est vrai que c\'est plus verbeux que
la méthode \"tout intégré\".\
Mais :

-   c\'est plus lisible car tout est structuré ;
-   c\'est aussi plus maintenable
-   c\'est également plus évolutif ;
-   ce type d\'architecture sera aussi valable pour des objets bien plus
    complexes ;
-   c\'est également plus puissant puisqu\'on peut piloter la fenêtre de
    confirmation à l\'aide d\'appels comme suivent :

``` {.javascript}

// affiche le message de la fenêtre
alert( $("#fen1").fenetreConfirmation("message") );

// Change le message de la fenêtre
$("#fen1").fenetreConfirmation("message", "Coucou c'est moi!!");

// Déclenche la fermeture de la fenêtre
$("#fen1").fenetreConfirmation("fermer");
```

\<WRAP center round info 60%\> Pour aller plus loin, vous pouvez aller
consulter l\'API de JQuery Widget
<http://api.jqueryui.com/jQuery.widget/>

Vous avez également de la documentation sur le sujet :
<https://learn.jquery.com/jquery-ui/widget-factory/>

\</WRAP\>

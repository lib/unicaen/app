Gestion de l\'historique
========================

Un système de gestion des données historiques est disponible dans
UnicaenApp.

En principe
-----------

L\'historique permet de savoir, pour une entité donnée :

-   Qui l\'a créée et quand (propriétés `histoCreateur`{.php} et
    `histoCreation`{.php}) ;
-   Qui l\'a modifié et quand (propriétés `histoModificateur`{.php} et
    `histoModification`{.php}) ;
-   Qui l\'a supprimé et quand (propriétés `histoDestructeur`{.php} et
    `histoDestruction`{.php}).

Les propriétés liées aux utilisateurs doivent être des
`UnicaenAuth\Entity\Db\AbstractUser`{.php}. Les propriétés liées aux
dates doivent être des `DateTime`{.php}.

\<WRAP center round important 80%\> Avec l\'historisation, il n\'y a
donc plus de suppression de données mais une simple modification.
\</WRAP\>

En pratique
-----------

Il s\'applique à une entité et fonctionne de la manière suivante :

-   L\'entité doit implémenter
    `UnicaenApp\Entity\HistoriqueAwareInterface`{.php} ;
-   Au besoin, un Trait `UnicaenApp\Entity\HistoriqueAwareTrait`{.php}
    pourra être ajouté à l\'entité pour implémenter l\'interface
    correspondante.

Si l\'entité est issue d\'une base de données et est générée par
Doctrine, c\'est pareil: l\'entité doit implémenter l\'interface
d\'historique. La table correspondante devra alors être dotée des 6
champs correspondants.

### Aide de vue

Une aide de vue permet d\'afficher des informations d\'historique (du
genre \"Dernière modification: le jj/mm à hh:mm par Utilisateur\"). Il
s\'utilise à partir d\'une vue de cette manière :

``` {.php}

echo $this->historique($entity);

```

On peut aussi lui fournir manuellement les propriétés suivantes sans lui
fournir d\'entité au besoin :

-   histoModification (DateTime)
-   histoModificateur (UnicaenApp\\Entity\\UserInterface)
-   isDeleted (boolean)

Avec Doctrine
-------------

Pour que l\'historique fonctionne, il faut :

-   renseigner les valeurs des champs d\'historique (listener) ;
-   les exploiter lorsqu\'on fait des requêtes (filtre) pour éviter que
    les données historisées ne polluent les résultats.

### Listener

Doctrine doit se charger, par l\'entremise du Listener de peupler
automatiquement les champs liés aux historiques lorsque l\'entité est
enregistrée en base.

Un Listener tout prêt est disponible : il s\'agit de
`UnicaenAuth\ORM\Event\Listeners\HistoriqueListener`{.php}. Il n\'est
cependant pas activé par défaut. Pour l\'activer, il suffit d\'ajouter à
la configuration du projet les données suivantes :

``` {.php}
<?php
return [
    'doctrine' => [
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    'UnicaenApp\HistoriqueListener',
                ],
            ],
        ],
    ],
];
```

### Filter

Le but des filtres est d\'éviter que des entités dont les données ont
été historisées ne soient collectées. Il n\'existe pas de filtre
générique. Il doit être créé sur mesure. \<WRAP center round info 80%\>
Pour information, les filtres Doctrine héritent de la classe
`Doctrine\ORM\Query\Filter\SQLFilter`{.php}. \</WRAP\> Il est également
possible (voir recommandé) de filtrer au moment où les requêtes sont
exécutées.

Validateurs
===========

EarlierThan
-----------

Permet de valider qu\'une date est antérieure (ou égale) à une autre.
Exemple:

``` {.php}
$now = new \DateTime; // instant présent
$validator = new \UnicaenApp\Validator\EarlierThan(array('max' => $now, 'inclusive' => true));
$date = \DateTime::createFromFormat('d-m-Y', '29-11-2012');
if ($validator->isValid($date) {
    // $date est antérieure ou égale à $now
}
else {
    // $date est postérieure à $now
}
```

LaterThan
---------

Permet de valider qu\'une date est postérieure (ou égale) à une autre.
Exemple:

``` {.php}
$now = new \DateTime; // instant présent
$validator = new \UnicaenApp\Validator\LaterThan(array('min' => $now, 'inclusive' => false));
$date = \DateTime::createFromFormat('d-m-Y', '29-11-2012');
if ($validator->isValid($date) {
    // $date est postérieure à $now
}
else {
    // $date est antérieure ou égale à $now
}
```

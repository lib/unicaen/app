Configuration
=============

Il s'agit ici d'adapter certaines options de configuration du module.

Configuration globale
---------------------

-   Copier/coller/renommer le fichier
    `config/unicaen-app.global.php.dist` du module vers
    `config/autoload/unicaen-app.global.php` de votre projet.
-   Adapter les options de config à votre contexte (sans modifier les
    noms des clés)...

### Fiche d'identité de l'appli

Il s'agit de renseigner nom de l'appli, description, numéro et date de
version, adresses et/ou numéros de téléphones de contact, URL des pages
externes "mentions légales" et "informatique et libertés", exemple :

``` {.php}
$settings = array(
    'app_infos' => array(
        'nom'     => "GesNum",
        'desc'    => "Gestion des services numériques",
        'version' => "1.0.0",
        'date'    => "05/09/2013",
        'contact' => array('mail' => "dsi.applications@unicaen.fr", /*'tel' => "01 02 03 04 05"*/),
        'mentionsLegales'        => "http://www.unicaen.fr/outils-portail-institutionnel/mentions-legales/",
        'informatiqueEtLibertes' => "http://www.unicaen.fr/outils-portail-institutionnel/informatique-et-libertes/",
    ),
);
```

Configuration locale
--------------------

-   Copier/coller/renommer le fichier
    `config/unicaen-app.local.php.dist` du module vers
    `config/autoload/unicaen-app.local.php` de votre projet.
-   Adapter les options de config à votre contexte (sans modifier les
    noms des clés)\...

### Annuaire LDAP

Clé `'ldap'` : il faut configurer la connexion à l'annuaire LDAP via
un compte omnipotent pour la récupération des coordonnées, affectations
administratives, rôles, etc. de l'utilisateur connecté (notamment),
exemple :

``` {.php}
    'ldap' => array(
        'connection' => array(
            'default' => array(
                'params' => array(
                    'host'                => 'localhost',
                    'port'                => 389,
                    'username'            => "uid=xxxxxxxxxxx,ou=system,dc=unicaen,dc=fr",
                    'password'            => "yyyyyyyyyyyy",
                    'baseDn'              => "ou=people,dc=unicaen,dc=fr",
                    'bindRequiresDn'      => true,
                    'accountFilterFormat' => "(&(objectClass=posixAccount)(supannAliasLogin=%s))",
                )
            )
        )
    ),
```

### Mode maintenance

Possibilité de passer l'appli en mode maintenance (indisponibilité
totale) dans la config, sauf pour certaines adresses IP.

Clé `'maintenance`' : options concernant le mode maintenance de
l'application, exemple :

``` {.php}
        /**
         * Mode maintenance (application indisponible)
         */
        'maintenance' => array(
            // activation (true: activé, false: désactivé)
            'enable' => true,
            // liste blanche des adresses IP clientes non concernées
            'white_list' => [
                ['127.0.0.1'],   // localhost
                ['10.60.11.88'], // Bertrand
            ],
        ),
```

**Important!** En mode "CLI" (ligne de commande), c'est censé
faire un exit(0) mais c'est pas sûr que ça fonctionne. Merci à celui
qui en a la possibilité de tester !

Aides de vue (view helpers)
===========================

Application
-----------

AppConnection
=============

Aide de vue générant l\'encart de connexion à l\'application.

\<note important\>En l\'occurence, cette aide de vue ne génère rien car
une application n\'utilisant que le module UnicaenApp ne fournit rien
permettant à l\'utilisateur de se connecter. Elle existe simplement dans
le but d\'être surchargée par le module UnicaenUser.\</note\>

AppInfos
========

Aide de vue permettant d\'afficher les informations concernant
l\'application (nom, description, version, etc.), informations issues de
la config du module UnicaenApp.

Exemple d\'utilisation :

``` {.php}
<h1>À propos de cette application</h1>
<?php echo $this->appInfos()->setHtmlListFormat() ?>
```

Exemple d\'utilisation avec accès aux attributs individuellement :

``` {.php}
<h1>À propos de cette application</h1>
<dl>
    <dt>Nom</dt>
    <dd><?php echo $this->appInfos()->nom; ?></dd>
    <dt>Description</dt>
    <dd><?php echo $this->appInfos()->desc; ?></dd>
    <dt>Version</dt>
    <dd><?php echo $this->appInfos()->version; ?></dd>
    <dt>Date</dt>
    <dd><?php echo $this->appInfos()->date; ?></dd>
    <dt>Contact</dt>
    <dd><?php echo implode($this->appInfos()->contact->toArray(), ', '); ?></dd>
</dl>
```

AppLink
=======

Aide de vue dessinant le nom de l\'application sous forme de lien
pointant vers la page d\'accueil, ou une ancre si l\'on s\'y trouve
déjà.

Exemple d\'utilisation :

``` {.php}
<?php echo $this->appLink(); ?>
```

Messages
--------

Messenger
=========

Aide de vue permettant de stocker une liste de messages d\'information
de différentes sévérités et de générer le code HTML pour les afficher
(affublés d\'un icône correspondant à leur sévérité).

Offre la possibilité d\'importer les messages du FlashMessenger pour les
mettre en forme de la même manière.

Exemple d\'utilisation :

``` {.php}
<?php echo $this->messenger()->addMessage("Info",       \UnicaenApp\View\Helper\Messenger::INFO)
                              ->addMessage("Ok",         \UnicaenApp\View\Helper\Messenger::OK)
                              ->addMessage("Attention",  \UnicaenApp\View\Helper\Messenger::WARNING)
                              ->addMessage("Erreur",     \UnicaenApp\View\Helper\Messenger::ERROR); ?>
```

Rendu correspondant :

``` {.html}
<p class="messenger info text-info">
    <i class="icon-info-sign"></i>
    Info
</p>
<p class="messenger success text-success">
    <i class="icon-ok-sign"></i>
    Ok
</p>
<p class="messenger warning text-warning">
    <i class="icon-warning-sign"></i>
    Attention
</p>
<p class="messenger error text-error">
    <i class="icon-exclamation-sign"></i>
    Erreur
</p>
```

Pour importer les éventuels messages présents dans le FlashMessenger
(sans les effacer pour autant), invoquez l\'aide de vue avec le premier
paramètre à `true` :

``` {.php}

<?php echo $this->messenger()
                ->addMessagesFromFlashMessenger()
                ->addMessage("Info", \UnicaenApp\View\Helper\Messenger::INFO); ?>
```

Navigation
----------

FilAriane
=========

Aide de vue générant le code HTML du \"fil d\'Ariane\" (breadcrumbs) à
partir des pages de navigation de l\'application.

Utilisation :

``` {.php}
<?php echo $this->navigation('navigation')->filAriane(); ?>
```

Exemple de rendu :

``` {.html}
<ul class="breadcrumb"><li><a href="/gesnum/" title="Page d'accueil de l'application">Accueil</a> <span class="divider">&gt;</span> </li>
    <li><a href="/gesnum/demande" title="Liste de toute les demandes">Demandes</a> <span class="divider">&gt;</span> </li>
    <li><a href="/gesnum/demande/recherche" title="Recherche de demandes">Recherche</a> <span class="divider">&gt;</span> </li>
    <li>Détaillée</li>
</ul>
```

MenuContextuel
==============

Aide de vue vouée au rendu d\'une sous-partie du menu de navigation
courant (qui tient compte des ACL), avec différents filtrages et
manipulations d\'attributs possibles.

Hérite de \\Laminas\\View\\Helper\\Navigation\\Menu.

\<note important\>Cette aide de vue ne fonctionne que si elle est en
mesure de trouver la page de navigation active. Vous devez donc veiller
à avoir une navigation bien configurée : il doit être possible de
déterminer la page de navigation correspondant à l\'URL de la page
courante.

Dans le cas contraire, un message du genre \"Page de navigation active
introuvable.\" est affiché. \</note\>

\<note\> La page de navigation active est la page de référence dont
seules les pages filles sont prises en compte par cette aide de vue.
\</note\>

Dans la suite, considérez que la variable `$mc` est valuée comme suit :

``` {.php}
$mc = $this->navigation('navigation')->menuContextuel();
```

Filtrage des pages filles
-------------------------

  ---------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------
                   Utilité                                                                                                                                                                                                                                                                                        Exemple
  withParam        Ne prendre en compte que les pages possédant le paramètre spécifié. Substitution possible de la valeur initiale du paramètre par une autre.                                                                                                                                                    `$mc->withParam('id', 12)`{.php}
  withoutParam     Que les pages ne possédant pas le paramètre spécifié.                                                                                                                                                                                                                                          `$mc->withoutParam('admin')`{.php}
  withProp         Ne prendre en compte que les pages possédant l\'attribut spécifié et ayant la valeur spécifiée éventuelle.                                                                                                                                                                                     `$mc->withProp('class', 'iconify')`{.php}
  withoutProp      Ne prendre en compte que les pages ne possédant pas l\'attribut spécifié                                                                                                                                                                                                                       `$mc->withoutProp('sitemap')`{.php}
  withTarget       Présence requise de la propriété de page \'withtarget\'. Si une cible est spécifiée, un paramètre ayant la valeur de cette cible est ajouté à chaque page. Si la cible spécifiée est un objet avec un attribut \'id\' on utilise cet attribut ; sinon on utilise sa représentation littéral.   `$mc->withTarget($entity)`{.php}
  withoutTarget    Absence requise de la propriété de page \'withtarget\'                                                                                                                                                                                                                                         `$mc->withoutTarget()`{.php}
  includeIf        Spécifie une page à inclure, uniquement si la condition spécifiée est remplie.                                                                                                                                                                                                                 `$mc->includeIf($production, 'envoyer-mail')`{.php}
  includeRouteIf   Exclut toutes les pages à l\'exception de celle spécifiée par sa route, uniquement si la condition est remplie (principe de la liste blanche).                                                                                                                                                 `$mc->includeRouteIf($condition, 'admin')`{.php}
  except           Spécifie une page à exclure par le nom de l\'action et/ou du contrôleur et/ou de ses paramètres.                                                                                                                                                                                               `$mc->except('exporter')`{.php}
  exceptIf         Spécifie une page à exclure, uniquement si la condition spécifiée est remplie.                                                                                                                                                                                                                 `$mc->exceptIf(true === $testing, 'envoyer-mail')`{.php}
  exceptRoute      Exclut une page spécifiée par sa route.                                                                                                                                                                                                                                                        `$mc->exceptRoute('admin')`{.php}
  exceptRouteIf    Exclut une page spécifiée par sa route, uniquement si la condition spécifiée est remplie..                                                                                                                                                                                                     `$mc->exceptRouteIf($condition, 'admin')`{.php}
  ---------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------

\<note\>NB: tous ces filtres sont cumulatifs (certains pouvant en
contredire d\'autres mais là c\'est votre problèmes).\</note\>

Ajout / suppression de paramètres ou d\'attributs
-------------------------------------------------

  ------------- ------------------------------------------------------------------ ------------------------------------------------------------------------
                Utilité                                                            Exemple
  addParam      Ajoute un paramètre à toutes les pages systématiquement.           `$mc->addParam('format', 'html')`{.php}
  addParams     Ajoute plusieurs paramètres à toutes les pages systématiquement.   `$mc->addParams(array('format' => 'html', 'type' => 2))`{.php}
  removeParam   Supprime un paramètre à toutes les pages systématiquement.         `$mc->removeParam('format')`{.php}
  addProp       Ajoute une propriété toutes les pages systématiquement.            `$mc->addProp('class', 'iconify modal-action')`{.php}
  addProps      Ajoute plusieurs propriétés à toutes les pages systématiquement.   `$mc->addProps(array('class' => 'iconify', 'sitemap' => false))`{.php}
  ------------- ------------------------------------------------------------------ ------------------------------------------------------------------------

Réinitialisation
----------------

Pour remettre à zero les filtres et les ajouts/suppressions de
paramètres/propriétés :

``` {.php}
$mc->reset()
```

Icônes
------

Si la classe CSS (propriété de page `class`) `iconify` est présente sur
une page ainsi que la propriété `icon`, cette aide de vue remplacera le
label du lien hypertexte par l\'icône correspondant.

Par exemple, imaginons que l\'on ait la page de navigation suivante :

``` {.php}
'afficher' => array(
    'label'  => 'Afficher cette ligne',
    'title'  => "Afficher le détails de la ligne {id}",
    'route'  => 'closerligne',
    'params' => array('action' => 'afficher'),
    'visible' => false,
    'class' => 'action-afficher',
    'icon' => 'glyphicon glyphicon-eye-open',
    'withtarget' => true,
),
```

Et ceci dans une vue :

``` {.html}
<?php echo $this->navigation('navigation')->menuContextuel()->withTarget($ligne)->addProp('class' ,'iconify'); ?>
```

Le lien hypertexte généré ressemblerait à cela :

``` {.html}
<a href="/closer/ligne/afficher/id-12"
   class="action-afficher iconify"
   title="Afficher le détails de la ligne 12"><span class="glyphicon glyphicon-eye-open"></span></a>
```

Classe CSS pour événement jQuery
--------------------------------

Cette aide de vue ajoute à chaque page de navigation une classe CSS
`event_xxx`. Cette classe est utile si vous utilisez l\'aide de vue
[AjaxModalDialog](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/ajaxmodalform).

Le nom de cette classe est construit à partir de 2 propriétés de la la
page de navigation :

-   propriété `route` si elle est présente, c\'est le nom de la route ;
-   propriété `action`, c\'est le nom de l\'action.

Selon le motif suivant : `event_[route_]action`.

Par exemple, imaginons que l\'on ait la page de navigation suivante :

``` {.php}
'afficher' => array(
    'label'  => 'Afficher cette ligne',
    'route'  => 'closerligne',
    'params' => array('action' => 'afficher'),
    'visible' => false,
    'withtarget' => true,
),
```

Et ceci dans une vue :

``` {.html}
<?php echo $this->navigation('navigation')->menuContextuel()->withTarget($ligne); ?>
```

Le lien hypertexte généré ressemblerait à cela :

``` {.html}
<a href="/closer/ligne/afficher/id-12" class="event_closerligne_afficher">Afficher cette ligne</a>
```

Exemple d\'utilisation typique
------------------------------

``` {.php}
if ($ligneId) {
    echo $this->navigation('navigation')->menuContextuel()
            ->withTarget($ligneId) // ajoutera un paramètre 'id' ayant la valeur $ligneId
            ->setUlClass('action list-inline')
            ->addParams(array('type' => $ligneType))
            ->addProp('class', 'iconify modal-action');
}
else {
    echo $this->navigation('navigation')->menuContextuel()
            ->withoutTarget()
            ->setUlClass('action')
            ->addParams(array('type' => $ligneType))
            ->addProp('class', 'modal-action');
}
```

MenuPiedDePage
==============

Aide de vue héritant de `\Laminas\View\Helper\Navigation\Menu` qui fabrique
le code HTML du menu de pied de page à partir des pages de navigation de
l\'application.

\<note important\>Seules les pages possédant la propriété `footer` à
`true` sont inclues dans le menu de pied de page.\</note\> \<note
important\>Les ACL de l\'application sont respectées par cette aide de
vue.\</note\>

Pages de navigation utilisées pour nos exemples
-----------------------------------------------

``` {.php}
namespace Application;

return array(
    ...
    'navigation' => array(
        'default' => array(
            'home' => array(
                'label'      => 'Accueil',
                'title'      => "Page d'accueil de l'application",
                'route'      => 'home',
                'order'      => -100, // make sure home is the first page
                'pages' => array(
                    'etab' => array(
                        'label'   => "Université de Caen - Basse Normandie",
                        'title'   => "Page d'accueil du site de l'Université de Caen - Basse Normandie",
                        'uri'     => 'http://www.unicaen.fr/',
                        'class'   => 'ucbn',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                    ),
                    'apropos' => array(
                        'label'   => "À propos",
                        'title'   => "À propos de cette application",
                        'route'   => 'apropos',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'contact' => array(
                        'label'   => 'Contact',
                        'title'   => "Contact concernant l'application",
                        'route'   => 'contact',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'plan' => array(
                        'label'   => 'Plan de navigation',
                        'title'   => "Plan de navigation au sein de l'application",
                        'route'   => 'plan',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'ml' => array(
                        'label'   => 'Mentions légales',
                        'title'   => "Mentions légales",
                        'uri'     => 'http://www.unicaen.fr/outils-portail-institutionnel/mentions-legales/',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'il' => array(
                        'label'   => 'Informatique et libertés',
                        'title'   => "Informatique et libertés",
                        'uri'     => 'http://www.unicaen.fr/outils-portail-institutionnel/informatique-et-libertes/',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                ),
            ),
        ),
    ),
);
```

Utilisation
-----------

``` {.php}
<?php echo $this->navigation('navigation')->menuPiedDePage(); ?>
```

Exemple de rendu correspondant :

``` {.html}
<ul class="navigation">
    <li>
        <a href="http://www.unicaen.fr/" class="ucbn" title="Page d'accueil du site de l'Université de Caen - Basse Normandie">Université de Caen - Basse Normandie</a>
    </li>
    <li>
        <a href="/gesnum/apropos" title="À propos de cette application">À propos</a>
    </li>
    <li>
        <a href="/gesnum/contact" title="Contact concernant l'application">Contact</a>
    </li>
    <li>
        <a href="/gesnum/plan" title="Plan de navigation au sein de l'application">Plan de navigation</a>
    </li>
    <li>
        <a href="http://www.unicaen.fr/outils-portail-institutionnel/mentions-legales/" title="Mentions légales">Mentions légales</a>
    </li>
    <li>
        <a href="http://www.unicaen.fr/outils-portail-institutionnel/informatique-et-libertes/" title="Informatique et libertés">Informatique et libertés</a>
    </li>
</ul>
```

MenuPrincipal
=============

Aide de vue héritant de `\Laminas\View\Helper\Navigation\Menu` qui fabrique
le code HTML du menu principal (pages de niveau 1 et 2 seulement) de
l\'application à partir des pages de navigation de l\'application.

\<note important\>Les ACL de l\'application sont respectées par cette
aide de vue.\</note\>

Pages de navigation utilisées pour nos exemples
-----------------------------------------------

Exemple :

``` {.php}
namespace Application;

return array(
    ...
    'navigation' => array(
        'default' => array(
            'home' => array(
                'pages' => array(
                    'demande' => array(
                        'label'      => 'Demandes',
                        'title'      => "Liste de toute les demandes",
                        'action'     => 'index',
                        'route'      => 'demande',
                        'resource'   => 'controller/Application\Controller\Demande',  // ici, pas d'action spécifiée
                        'pages' => array(
                            'ajouter' => array(
                                'label'      => 'Nouvelle demande',
                                'title'      => "Faire une nouvelle demande",
                                'action'     => 'ajouter',
                                'route'      => 'demande',
                                'resource'   => 'controller/Application\Controller\Demande',
                                'pages' => array(
                                    'identite' => array(
                                        'label'      => 'Identité',
                                        'title'      => "Saisie des informations concernant l'identité",
                                        'action'     => 'ajouter-identite',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                        'visible'    => false,
                                    ),
                                    'coordonnees' => array(
                                        'label'      => 'Coordonnées',
                                        'title'      => "Saisie des coordonnées",
                                        'action'     => 'ajouter-coordonnees',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                        'visible'    => false,
                                    ),
                                    'divers' => array(
                                        'label'      => 'Divers',
                                        'title'      => "Saisie des informations diverses",
                                        'action'     => 'ajouter-divers',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                        'visible'    => false,
                                    ),
                                ),
                            ),
                            'mienne' => array(
                                'label'      => 'Miennes',
                                'title'      => "Liste des demandes me concernant",
                                'action'     => 'miennes',
                                'route'      => 'demande',
                                'resource'   => 'controller/Application\Controller\Demande',
                            ),
                            'recherche' => array(
                                'label'      => 'Recherche',
                                'title'      => "Recherche de demandes",
                                'action'     => 'recherche',
                                'route'      => 'demande',
                                'resource'   => 'controller/Application\Controller\Demande',
                                'pages' => array(
                                    'simple' => array(
                                        'label'      => 'Simple',
                                        'title'      => "Recherche simple de demandes",
                                        'action'     => 'recherche',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                    ),
                                    'detaillee' => array(
                                        'label'      => 'Détaillée',
                                        'title'      => "Recherche détaillée de demandes",
                                        'action'     => 'recherche-detaillee',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
```

Le premier niveau seulement
---------------------------

Exemple :

``` {.php}
<?php echo $this->navigation('navigation')->menuPrincipal(); ?>
```

Exemple de rendu correspondant :

``` {.html}
<ul class="nav menu-principal">
    <li class="dropdown">
        <a title="Liste de toute les demandes" href="/gesnum/demande">Demandes</a>
    </li>
    <li class="active dropdown">
        <a href="/gesnum/administration/utilisateur">Administration</a>
    </li>
</ul>
```

Les deux premiers niveaux
-------------------------

Exemple :

``` {.php}
<?php echo $this->navigation('navigation')->menuPrincipal()->setMaxDepth(2); ?>
```

Exemple de rendu correspondant :

``` {.html}
<nav>
    <ul class="nav menu-principal">
        <li class="active dropdown">
            <a href="/gesnum/demande" title="Liste de toute les demandes">Demandes</a>
            <ul class="dropdown-menu" style="display: none;">
                <li class="active dropdown">
                    <a href="/gesnum/demande/ajouter" title="Faire une nouvelle demande">Nouvelle demande</a>
                </li>
                <li class="">
                    <a href="/gesnum/demande/miennes" title="Liste des demandes me concernant">Miennes</a>
                </li>
                <li class="dropdown">
                    <a href="/gesnum/demande/recherche" title="Recherche de demandes">Recherche</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="/gesnum/administration/utilisateur">Administration</a>
            <ul class="dropdown-menu" style="display: none;">
                <li class="dropdown">
                    <a href="/gesnum/administration/utilisateur">Utilisateurs</a>
                </li>
                <li class="dropdown">
                    <a href="/gesnum/administration/parametre">Paramètres</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
```

MenuSecondaire
==============

Aide de vue héritant de `\Laminas\View\Helper\Navigation\Menu` qui fabrique
le code HTML du menu secondaire de l\'application à partir des pages de
navigation de l\'application.

\<note important\>Les ACL de l\'application sont respectées par cette
aide de vue.\</note\>

Utilisation :

``` {.php}
<?php echo $this->navigation('navigation')->menuSecondaire(); ?>
```

Exemple de rendu correspondant :

``` {.html}
<ul class="menu-secondaire">
    <li>
        <a href="/gesnum/demande/ajouter" title="Faire une nouvelle demande">Nouvelle demande</a>
    </li>
    <li>
        <a href="/gesnum/demande/miennes" title="Liste des demandes me concernant">Miennes</a>
    </li>
    <li class="active">
        <a href="/gesnum/demande/recherche" title="Recherche de demandes">Recherche</a>
        <ul>
            <li>
                <a href="/gesnum/demande/recherche" title="Recherche simple de demandes">Simple</a>
            </li>
            <li class="active">
                <a href="/gesnum/demande/recherche-detaillee" title="Recherche détaillée de demandes">Détaillée</a>
            </li>
        </ul>
    </li>
</ul>
```

Plan
====

Aide de vue générant le code HTML du plan de navigation à partir des
pages de navigation de l\'application.

\<note important\>Seules les pages de navigation visibles ou possédant
la propriété `sitemap` à `true` sont inclues dans le plan.\</note\>
\<note important\>Les ACL de l\'application sont respectées par cette
aide de vue.\</note\>

Pages de navigation utilisées pour l\'exemple
---------------------------------------------

Les pages de navigation sont issues de la fusion du fichier de config du
module \"vendor/UnicaenApp\"\" :

``` {.php}
namespace UnicaenApp;

return array(
    ...
    'navigation' => array(
        'default' => array(
            'home' => array(
                'label'      => 'Accueil',
                'title'      => "Page d'accueil de l'application",
                'route'      => 'home',
                'order'      => -100, // make sure home is the first page
                'pages' => array(
                    'etab' => array(
                        'label'   => "Université de Caen - Basse Normandie",
                        'title'   => "Page d'accueil du site de l'Université de Caen - Basse Normandie",
                        'uri'     => 'http://www.unicaen.fr/',
                        'class'   => 'ucbn',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                    ),
                    'apropos' => array(
                        'label'   => "À propos",
                        'title'   => "À propos de cette application",
                        'route'   => 'apropos',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'contact' => array(
                        'label'   => 'Contact',
                        'title'   => "Contact concernant l'application",
                        'route'   => 'contact',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'plan' => array(
                        'label'   => 'Plan de navigation',
                        'title'   => "Plan de navigation au sein de l'application",
                        'route'   => 'plan',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'ml' => array(
                        'label'   => 'Mentions légales',
                        'title'   => "Mentions légales",
                        'uri'     => 'http://www.unicaen.fr/outils-portail-institutionnel/mentions-legales/',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                    'il' => array(
                        'label'   => 'Informatique et libertés',
                        'title'   => "Informatique et libertés",
                        'uri'     => 'http://www.unicaen.fr/outils-portail-institutionnel/informatique-et-libertes/',
                        'visible' => false,
                        'footer'  => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap' => true, // propriété maison pour inclure cette page dans le plan
                    ),
                ),
            ),
        ),
    ),
);
```

\... et du module \"module/Application\" :

``` {.php}
namespace Application;

return array(
    ...
    'navigation' => array(
        'default' => array(
            'home' => array(
                'pages' => array(
                    'demande' => array(
                        'label'      => 'Demandes',
                        'title'      => "Liste de toute les demandes",
                        'action'     => 'index',
                        'route'      => 'demande',
                        'resource'   => 'controller/Application\Controller\Demande',  // ici, pas d'action spécifiée
                        'pages' => array(
                            'ajouter' => array(
                                'label'      => 'Nouvelle demande',
                                'title'      => "Faire une nouvelle demande",
                                'action'     => 'ajouter',
                                'route'      => 'demande',
                                'resource'   => 'controller/Application\Controller\Demande',
                                'pages' => array(
                                    'identite' => array(
                                        'label'      => 'Identité',
                                        'title'      => "Saisie des informations concernant l'identité",
                                        'action'     => 'ajouter-identite',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                        'visible'    => false,
                                    ),
                                    'coordonnees' => array(
                                        'label'      => 'Coordonnées',
                                        'title'      => "Saisie des coordonnées",
                                        'action'     => 'ajouter-coordonnees',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                        'visible'    => false,
                                    ),
                                    'divers' => array(
                                        'label'      => 'Divers',
                                        'title'      => "Saisie des informations diverses",
                                        'action'     => 'ajouter-divers',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                        'visible'    => false,
                                    ),
                                ),
                            ),
                            'mienne' => array(
                                'label'      => 'Miennes',
                                'title'      => "Liste des demandes me concernant",
                                'action'     => 'miennes',
                                'route'      => 'demande',
                                'resource'   => 'controller/Application\Controller\Demande',
                            ),
                            'recherche' => array(
                                'label'      => 'Recherche',
                                'title'      => "Recherche de demandes",
                                'action'     => 'recherche',
                                'route'      => 'demande',
                                'resource'   => 'controller/Application\Controller\Demande',
                                'pages' => array(
                                    'simple' => array(
                                        'label'      => 'Simple',
                                        'title'      => "Recherche simple de demandes",
                                        'action'     => 'recherche',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                    ),
                                    'detaillee' => array(
                                        'label'      => 'Détaillée',
                                        'title'      => "Recherche détaillée de demandes",
                                        'action'     => 'recherche-detaillee',
                                        'route'      => 'demande',
                                        'resource'   => 'controller/Application\Controller\Demande',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
```

Utilisation
-----------

``` {.php}
<h1>Plan de navigation</h1>
<?php echo $this->navigation('navigation')->plan(); ?>
```

Exemple de rendu correspondant :

``` {.html}
<h1>Plan de navigation</h1>
<ul class="menu-footer">
    <li class="active">
        <a href="/gesnum/" title="Page d'accueil de l'application">Accueil</a>
        <ul>
            <li>
                <a href="/gesnum/demande" title="Liste de toute les demandes">Demandes</a>
                <ul>
                    <li>
                        <a href="/gesnum/demande/ajouter" title="Faire une nouvelle demande">Nouvelle demande</a>
                    </li>
                    <li>
                        <a href="/gesnum/demande/miennes" title="Liste des demandes me concernant">Miennes</a>
                    </li>
                    <li>
                        <a href="/gesnum/demande/recherche" title="Recherche de demandes">Recherche</a>
                        <ul>
                            <li>
                                <a href="/gesnum/demande/recherche" title="Recherche simple de demandes">Simple</a>
                            </li>
                            <li>
                                <a href="/gesnum/demande/recherche-detaillee" title="Recherche détaillée de demandes">Détaillée</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/gesnum/apropos" title="À propos de cette application">À propos</a>
            </li>
            <li>
                <a href="/gesnum/contact" title="Contact concernant l'application">Contact</a>
            </li>
            <li class="active">
                <a href="/gesnum/plan" title="Plan de navigation au sein de l'application">Plan de navigation</a>
            </li>
            <li>
                <a href="http://www.unicaen.fr/outils-portail-institutionnel/mentions-legales/" title="Mentions légales">Mentions légales</a>
            </li>
            <li>
                <a href="http://www.unicaen.fr/outils-portail-institutionnel/informatique-et-libertes/" title="Informatique et libertés">Informatique et libertés</a>
            </li>
            <li>
                <a href="/gesnum/administration/utilisateur">Administration</a>
                <ul>
                    <li>
                        <a href="/gesnum/administration/utilisateur">Utilisateurs</a>
                        <ul>
                            <li>
                                <a href="/gesnum/administration/utilisateur/ajouter">Nouvel utilisateur</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/gesnum/administration/parametre">Paramètres</a>
                        <ul>
                            <li>
                                <a href="/gesnum/administration/parametre/ajouter">Nouveau paramètre</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>
```

Formulaire
----------

FormControlGroup
================

Aide de vue générant le marquage HTML complet d\'un élément de
formulaire à la mode Bootsrap 3 (`div.form-group`), y compris les
messages d\'erreur de validation.

``` {.html}
<?php echo $this->form()->openTag($form->prepare()) ?>
<?php echo $this->formControlGroup($form->get('message')->setLabel("Votre message :")); ?>
<?php echo $this->formSubmit($form->get('submit')->setValue("Enregistrer")); ?>
<?php echo $this->form()->closeTag() ?>
```

En plus des éléments de formulaire standards, cette aide de vue peut
recevoir un élément de type :

-   \\UnicaenApp\\Form\\Element\\DateInfSup
-   \\UnicaenApp\\Form\\Element\\SearchAndSelect

Lorsqu\'il y a des messages d\'erreur de validation la `div` possède la
classe CSS `has-error`.

FormDateInfSup
==============

Aide de vue générant le code HTML de l\'élément de formulaire composite
\"[DateInfSup](/develop/unicaen2/moduleunicaenunicaenapp/form/dateinfsup)\",
c\'est à dire :

-   le champ de saisie de la date inférieure ainsi que son label associé
    ;
-   éventuellement, le champ de saisie éventuel de la date supérieure
    ainsi que son label associé (uniquement si la date supérieure est
    désactivée) ;
-   éventuellement, un lien permettant de vider la date supérieure (via
    jQuery).

FormErrors
==========

Génère le marquage HTML pour afficher à la fois un message d\'erreur
global de validation d\'un formulaire ainsi que la liste des erreurs de
tous les éléments de formulaires.

Exemple :

``` {.html}
<?php echo $this->formErrors($form->prepare(), "Oups!") ?>

<?php echo $this->form()->openTag($form) ?>
<?php echo $this->formControlGroup($form->get('nom')->setLabel("Votre nom :")); ?>
<?php echo $this->formControlGroup($form->get('message')->setLabel("Votre message :")); ?>
<?php echo $this->formSubmit($form->get('submit')->setValue("Enregistrer")); ?>
<?php echo $this->form()->closeTag() ?>
```

FormRowDateInfSup
=================

Aide de vue héritant de l\'aide de vue standard \"FormRow\" et générant
le code HTML **complet** de l\'élément de formulaire composite
\"[DateInfSup](/develop/unicaen2/moduleunicaenunicaenapp/form/dateinfsup)\",
c\'est à dire :

-   le label global de l\'élément composite ;
-   les champs de saisie des dates et leurs labels (délégué à l\'aide de
    vue
    \"[FormDateInfSup](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/FormDateInfSup)\")
    ;
-   les messages d\'erreurs de validation.

FormSearchAndSelect
===================

Aide de vue générant le code HTML de l\'élément de formulaire
[SearchAndSelect](/develop/unicaen2/moduleunicaenunicaenapp/form/SearchAndSelect).

Exemple :

``` {.php}
$sas = $form->get('sas')
        ->setLabel("Recherche :")
        ->setAutocompleteSource($this->url('demo', array('action' => 'rechercher')));
echo $this->formLabel($sas);
echo $this->formSearchAndSelect($sas);
```

Le marquage HTML résultant est le suivant :

``` {.html}
<label for="sas">Recherche :</label>
<input type="text" value="" name="sas[id]" class="sas" id="sas-532c128ecd5c2" style="display: none;">
<input type="text" value="" name="sas[label]" class="form-control input-sm ui-autocomplete-input" id="sas-532c128ecd5c2-autocomplete" autocomplete="off">

<style>
    ul.ui-autocomplete {
        z-index: 5000
    }
    .ui-autocomplete-loading {
        background: white url("//gest.unicaen.fr/images/ajax-loader-r.gif") right center no-repeat;
    }
</style>

<script>
    // cache l'élément contenant l'id de la sélection
    $("#sas-532c128ecd5c2").css('display', 'none');

    $(function() {
        // jQuery UI autocomplete
        var autocomp = $("#sas-532c128ecd5c2-autocomplete");
        autocomp.autocompleteUnicaen({// autocompleteUnicaen() définie dans "public/js/util.js"
            elementDomId: 'sas-532c128ecd5c2',
            source: '/ose/demo/rechercher',
            minLength: 2,
            delay: 750
        });
    });
</script>
```

Lorsque le formulaire est soumis, la valeur POSTée pour cet élément
ressemble à cela :

``` {.php}
'sas' => array(
    'id' => '8',      // identifiant unique de l'item sélectionné (issu d'un champ caché)
    'label' => 'Huit' // texte présent dans le champ de saisie
)
```

MultipageFormFieldset
=====================

Aide de vue à utiliser dans chaque vue associée aux étapes de saisie
d\'un formulaire multi-pages.

Cette aide de vue génère un titre (h2) indiquant \"Étape i sur N\", le
code HTML du fieldset de l\'étape en cours ainsi que les boutons de
navigation.

Exemple :

``` {.php}
<h1>Formulaire de contact</h1>
<?php echo $this->multipageFormFieldset(); ?>
```

\<note important\>Le fieldset courant est transmis à la vue par le
plugin de contrôleur
[MultipageFormPlugin](/develop/unicaen2/moduleunicaenunicaenapp/controllerplugins/multipageform)
via la variable de vue `fieldset`.\</note\>

MultipageFormNav
================

Aide de vue générant l\'élément de navigation (de type
`\UnicaenApp\Form\Element\MultipageFormNav`) au sein d\'un formulaire
multi-pages.

MultipageFormRecap
==================

Aide de vue à utiliser dans la vue associée à l\'étape de confirmation
de la saisie d\'un formulaire multi-pages.

Cette aide de vue génère automatiquement la liste de toutes les
informations saisies et les boutons de navigation.

Exemple :

``` {.php}
<h1>Récapitulatif et confirmation des informations saisies</h1>
<?php echo $this->multipageFormRecap(); ?>
```

\<note important\>Le formulaire complet est transmis à la vue par le
plugin de contrôleur
[MultipageFormPlugin](/develop/unicaen2/moduleunicaenunicaenapp/controllerplugins/multipageform)
via la variable de vue `form`.\</note\>

\<note important\> Cette aide de vue explore automatiquement tous les
éléments visibles de tous les fieldsets du formulaire afin de collecter
leur labels et leur valeurs et les présenter sous la forme d\'une liste
de définition (dl). Si vous souhaitez maîtriser plus finement la façon
dont sont constitués les labels ou les valeurs d\'un fieldset, faites
implémenter à la classe de ce fieldset l\'interface
`\UnicaenApp\Form\MultipageFormFieldsetInterface` pour fournir via la
méthode `getLabelsAndValues()` les labels et valeurs de tous les
éléments de ce fieldset. \</note\>

MultipageFormRow
================

Aide de vue générant chaque élément d\'un fieldset de formulaire
multi-page.

Délègue le travail à l\'aide de vue standard `FormRow` pour tous les
éléments, sauf pour ceux de type `MultipageFormNav` traitée par l\'aide
de vue
`[[develop:unicaen2:moduleunicaenunicaenapp:viewhelpers:multipageformnav|MultipageFormNav]]`.

Divers
------

ToggleDetails
=============

Aide de vue qui génère un lien permettant d\'afficher/masquer un
container.

``` {.php}

<?php echo $this->toggleDetails('div-depot-initial', true)
    ->setShowDetailsIconClass('glyphicon glyphicon-chevron-up')
    ->setHideDetailsIconClass('glyphicon glyphicon-chevron-down')
    ->setShowDetailsTitle("Parental Advisory : Explicit Content")
    ->setHideDetailsTitle("Click to hide!")
    ->setShowDetailsLabel('Show me')
    ->setHideDetailsLabel('No more!') ?>
<p style="display: none" id="div-depot-initial">
    Bla bla bla...
</p>
```

Le deuxième argument de `$this->toggleDetails()` permet d\'activer ou
désactiver le mécanisme de restauration de l\'état de visibilité du
container au chargement de la page (état mémorisé dans le localStorage
du navigateur).

\<note\>Si vous retirez le `style="display: none"` du container pour
qu\'il soit visible par défaut, l\'aide de vue s\'adapte
automatiquement.\</note\>

Aide de vue Tag
===============

Cette aide de vue sert à afficher des tags HTML de manière sécurisée, en
particulier pour prévenir le \"défaçement\" de site Web.

En voici quelques exemples d\'utilisation :

``` {.php}


echo $this->tag('h1')->text('Ti<span>tre</span>' );
// Résultat : <h1>Titre</h1>

echo $this->tag('h1')->escaped('Ti<span>tre</span>');
// Résultat : <h1>Ti&lt;span&gt;tre&lt;/span&gt;</h1>

echo $this->tag('h1')->html('Ti<span>tre</span>');
// Résultat : <h1>Ti<span>tre</span></h1>



echo $this->tag('div', [
                    'class' => 'row',
                    'data-index' => 2
                ]);
// Résultat : <div class="row" data-index="2">


echo $this->tag('h2');
echo 'Titre H2';
echo $this->tag('h2')->close();
// Résultat : <h2>Titre H2</h2>

```

Et voici un exemple plus complet avec un cas de \"défaçement\" de site
évité grâce à l\'échappement de caractères :

``` {.php}
<?php
$data = [
    [
        'term' => 'Bonjour',
        'title' => 'Bonne journée',
        'description' => 'Ceci est une politesse du matin lorsque nous nous retrouvons'
    ],
    [
        'term' => 'Bonsoir',
        'title' => 'Bonne soirée "><div style="background-color:black;font-size:90pt;color:white;position:absolute;left:0px;top:0px;width:100%;height:100%">Bouh ceci est un défaçement</div>',
        'description' => 'Ceci <b>est</b> une <i>politesse</i> du soir lorsqu\'il est temps de partir...'
    ],
];

// Bon exemple !!
echo '<dl>';
foreach( $data as $item ){
    echo $this->tag('dt', ['title' => $item['title']])->text($item['term']);
    echo $this->tag('dd')->html($item['description'], ['b']); // on laisse passer le gras ici (b)
}
echo '</dl>';

// Résultat :

?>
<dl>
<dt title="Bonne&#x20;journ&#xE9;e">Bonjour</dt>
<dd>Ceci est une politesse du matin lorsque nous nous retrouvons</dd>
<dt title="Bonne&#x20;soir&#xE9;e&#x20;&quot;&gt;&lt;div&#x20;style&#x3D;&quot;background-color&#x3A;black&#x3B;font-size&#x3A;90pt&#x3B;color&#x3A;white&#x3B;position&#x3A;absolute&#x3B;left&#x3A;0px&#x3B;top&#x3A;0px&#x3B;width&#x3A;100&#x25;&#x3B;height&#x3A;100&#x25;&quot;&gt;Bouh&#x20;ceci&#x20;est&#x20;un&#x20;d&#xE9;fa&#xE7;ement&lt;&#x2F;div&gt;">Bonsoir</dt>
<dd>Ceci <b>est</b> une politesse du soir lorsqu'il est temps de partir...</dd>
</dl>

// Et voici le mauvais exemple :


<dl>
<?php foreach( $data as $item ): ?>
    <dt title="<?php echo $item['title'] ?>"><?php echo $item['term'] ?></dt>
    <dd><?php echo $item['description'] ?></dd>
<?php endforeach ?>
</dl>

```

\<WRAP center round tip 60%\> Vous noterez que le code sécurisé n\'est
pas plus \"lourd\" à écrire que le code non sécurisé!! \</WRAP\>

Autres cas
----------

Pour finir, voici un autre exemple d\'échappement de chaîne de
caractères à utiliser pour écrire du HTML avec le Zend Framework :

``` {.php}
<?php

$texte = 'Bonne soirée "><div style="background-color:black;font-size:90pt;color:white;position:absolute;left:0px;top:0px;width:100%;height:100%">Bouh ceci est un défaçement qui a échoué</div>';

// Affichage sécurisé grâce à l'aide de vue standard "escapeHtml"
?>

<p class="nouvel-exemple">
<?php echo $this->escapeHtml($texte); ?>
</p>


```

\<WRAP center round tip 60%\> Pour aller plus loin, vous pouvez
consulter la documentation de la classe ici :
<https://ldev.unicaen.fr/doc/UnicaenApp/html/class_unicaen_app_1_1_view_1_1_helper_1_1_tag_view_helper.html>
\</WRAP\>

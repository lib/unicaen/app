Fichier \"unicaen.js\"
======================

Javascript commun à toutes les applis.

AjaxModalListener
-----------------

Classe d\'installation d\'un mécanisme d\'ouverture de fenêtre modale
Bootstrap 3 lorsqu\'un lien ayant la classe CSS \'modal-action\' est
cliqué, et de gestion de la soumission du formulaire éventuel se
trouvant dans cette fenêtre modale.

WidgetInitializer
-----------------

Le WidgetInitializer est un système qui permet de lier automatiquement
un widget JQuery à un ou plusieurs éléments.

Par exemple, si en HTML un élément INPUT doit servir de datepicker;
alors il faut faire : `$("votre_input").datepicker()`{.javascript}

Avec le WidgetInitializer ce n\'est plus nécessaire. Il suffit de donner
à votre élément la bonne classe et tout se fait automatiquement.

Par exemple, à votre élément \<input class=\"datepicker\" /\> se verra
automatiquement associer le widget DatePicker. Ceci, bien sûr, si vous
avez préalablement configuré le WidgetInitializer!!

Pour ajouter un widget au WidgetInitializer, rien de plus simple. Il
suffit de faire :

``` {.javascript}
$(function ()
{
    WidgetInitializer.add('form-advanced-multi-checkbox', 'formAdvancedMultiCheckbox');
    // exemple pris pour le formAdvancedMultiCheckbox, qui est au demerant déjà pré-renseigné
});
```

Dans certains cas (en particulier si votre code HTML est généré en
Javascript), l\'initialisation ne va pas se déclencher (cette dernière
ne se fait qu\'au chargement de la page et après avoir exécuté une
requête AJAX).

Pour que l\'initialisation de votre widget se fasse il faut faire :

``` {.javascript}
WidgetInitializer.run();
```

Ceci entraînera l\'initialisation de tous les widgets pas encore en
place.

\<WRAP center round tip 60%\> Certains widgets, en particulier ceux
issus de la bibliothèque Unicaen, sont déjà déclarés auprès du
WidgetInitializer. C\'est par exemple le cas de
[AdvancedMultiCheckBox](/develop/unicaen2/moduleunicaenunicaenapp/form/AdvancedMultiCheckBox).
Dans ce cas, il n\'y a rien d\'autre à faire pour que le widget soit
initialisé. \</WRAP\>

Refresh
-------

Il est possible de rafraichir un élément en AJAX en lui donnant une URL
qui sera chargée de lui fournir son nouveau contenu.

### Conditions de fonctionnement

Ajouter à l\'élément un attribut data-url avec comme valeur l\'url qui
fournira le nouveau contenu.

### Exécution

Pour lancer le rafarpichissement, rien de plus simple. Il suffit de
faire : `$("//sélecteur de l'objet//").refresh();`{.javascript}

Refresh peut accepter deux paramètres optionnels :

-   data qui sera un tableau JSON des paramètres à transmettre au
    serveur
-   onEnd qui est une fonction anonyme permettant d\'exécuter un code
    après le rafraichissement de l\'élément.

alertFlash
----------

Fonction JS permettant d\'afficher en bas de l\'écran un message
d\'info/alerte temporaire.

Exemple :

``` {.javascript}
// message de succès disparaissant au bout de 3 secondes
alertFlash("Demande enregistrée avec succès.", "success", 3000);

// message d'erreur disparaissant au bout de 5 secondes
alertFlash("Erreur rencontrée lors de l'enregistrement de la demande!", "error", 5000);
```

Les sévérités disponibles sont :

-   \"info\"
-   \"success\"
-   \"warning\"
-   \"error\"

Divers
------

-   Détection de réponse \"403 Unauthorized\" aux requêtes AJAX pour
    rediriger vers la page de connexion.
-   Installation d\'un lien permettant de remonter en haut de la page.
    Ce lien apparaît lorsque c\'est nécessaire.

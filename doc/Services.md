Services
========

LDAP
----

### Group

Service d\'accès aux groupes de l\'annuaire LDAP. Avec le gestionnaire de service, ce service est accessible ainsi :

``` {.php}
$service = $sm->get('ldap_group_service'); /* @var $service \UnicaenApp\Service\Ldap\Group */
```

### People

Service d\'accès aux individus de l\'annuaire LDAP. Avec le gestionnaire de service, ce service est accessible ainsi :

``` {.php}
$service = $sm->get('ldap_people_service'); /* @var $service \UnicaenApp\Service\Ldap\People */
```

### Structure

Service d\'accès aux structures de l\'annuaire LDAP. Avec le gestionnaire de service, ce service est accessible ainsi :

``` {.php}
$service = $sm->get('ldap_structure_service'); /* @var $service \UnicaenApp\Service\Ldap\Structure */
```

### HostLocalization

Le service HostLocalization a pour but de déterminer si le poste client est situé dans le réseau de l'Etablissement ou non.

En standard, seul un mécanisme de détection pour l'Université de Caen est disponible. Pour Caen, l'idée est la suivante : la
méthode inEtablissement du service renverra true si la machine de l'utilisateur est dans les cas suivants :

* En direct et en interne sans passer par le proxy (pas de redirection)
* En interne, que l'on sort puis on re-rentre en passant par le reverse proxy
* En interne, que l'on passe par le proxy en interne Sinon false.

Il est cependant possible d'ajouter votre propre mécanisme de localisation.

Attention : en mode développement, sur votre machine et avec l'appli en localhost, vous ne serez pas considéré comme sur le
réseau de l'établissement.

#### Récupération et utilisation du service

```php

// Dans une Factory. On privilégiera l'usage de l'alias HostLocalization
$hl = $container->get('HostLocalization');

// Dans votre code métier
// Récupérer $hl
/* @var $hl \UnicaenApp\HostLocalization\HostLocalizationInterface */
if ($hl->inEtablissement()){
    var_dump("Vous êtes sur le réseau de l'Unicaen");
}else{
    var_dump("Vous n'êtes pas sur le réseau de l'Unicaen");
}
```

Un Trait [HostLocalizationAwareTrait](../src/UnicaenApp/HostLocalization/HostLocalizationAwareTrait.php) est dispo avec un
getter et un setter pour pouvoir injecter proprement la dépendance dans votre code.

### Créer votre propre système de localization

Pour utiliser votre propre système de HostLocalization, il vous faut :

Créer une classe implémentant [HostLocalizationInterface](../src/UnicaenApp/HostLocalization/HostLocalizationInterface.php).
La rendre dispo comme service dans votre config du ServiceManager. Modifier l'alias de service 'HostLocalization' pour le
faire pointer sur votre propre classe.

Vous pourrez au besoin vous inspirer de [HostLocalization](../src/UnicaenApp/HostLocalization/HostLocalization.php).
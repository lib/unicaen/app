Installation
============

Cette page traite de l\'installation du module UnicaenApp au sein d\'une
application ne l\'utilisant par encore.

**Important!** Ce module est une dépendance du module
`unicaen/auth`. Si vous prévoyez d'installer ce dernier, inutile de lire cette page car
l'installation de "unicaen/app" se fera automatiquement.

-   Éditez le fichier `composer.json` se trouvant à la racine de votre
    projet et assurez-vous que le \"repository\" suivant est bien
    présent :

``` {.json}
    "repositories": [
        {
            "type": "composer",
            "url": "http://dev.unicaen.fr/packagist"
        }
    ],
```

-   Ajoutez à présent la dépendance suivante :

``` {.json}
    "require": {
        ...
        "unicaen/unicaen-app": "dev-master"
    },
    "minimum-stability": "dev"
```

-   Placez-vous à la racine de votre projet et lancez la commande
    suivante dans un shell :
```
gauthierb@bertrand-crisi:~/workspace/gesnum$ php ../composer.phar update
```

Remarque: La commande ci-dessus fonctionne seulement si le binaire
`composer.phar` se trouve dans le répertoire parent. Plus d\'infos :
<http://getcomposer.org>.

-   Créer le répertoire `data/DoctrineORMModule` à la racine de votre
    projet et l\'autoriser en écriture au serveur web en faisant exemple :
    

    gauthierb@bertrand-crisi:~/workspace/gesnum$ mkdir -p data/DoctrineORMModule
    gauthierb@bertrand-crisi:~/workspace/gesnum$ sudo chown gauthierb:www-data data/DoctrineORMModule

-   Activez les modules suivants **dans cet ordre** dans le fichier
    `config/application.config.php` de l'application :

```{.php}
    'modules' => array(
        'Application',
        'UnicaenApp', 'AssetManager',
        // ...
    ),
```

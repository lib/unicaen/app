ApplicationController
=====================

C\'est la classe du contrôleur fournissant les actions que l\'on
retrouve dans toutes nos application :

-   Affichage de la page \"À propos\"
-   Affichage de la page \"Contact\"
-   Affichage de la page \"Plan de navigation\"
-   Affichage de la page \"Mentions légales\" (ou redirection)
-   Affichage de la page \"Informatique et libertés\" (ou redirection)

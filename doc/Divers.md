Divers
======

Export CSV
----------

UnicaenApp intègre un ensemble de classes et de méthodes qui permettent
de générer facilement des contenus téléchargeables au format CSV.

Exemple d\'utilisation :

``` {.php}
public function csvAction()
{
        /* Création d'un nouveau modèle de données au format CSV. L'en-tête peut être renseignée dès l'instanciation. */
        $result = new \UnicaenApp\View\Model\CsvModel( ['header' => ['Nom','Prenom'] ]);

        /* Les données peuvent être transmises par des variables (data, header, separator, filename, delimiter)... */
        $result->setVariable('data', [
            ['Lécluse','Laurent'],
            ['Morin','Paul'],
            ['Durand','Patrick']
        ]);

        /* .. ou bien par des accesseurs. Ici on attribue un nom de fichier pour l'export. */
        $result->setFilename('test_export_csv.csv');

        /* On peut aussi ajouter au besoin des lignes avec la méthode addLine ou des tableaux avec addLines... */
        $result->addLine( [
            'Anquetil', 'Pierre'
        ] );

        return $result;
}
```

Export XML
----------

Il est possible de générer des fichiers XML à la volée et en utilisant
le modèle MVC.

Cela se passe de la manière suivante :

Dans votre action de contrôleur :

``` {.php}

        $data = ['balise1' => 'contenu1', 'balise2' => 'contenu2'];

        $viewModel = new \UnicaenApp\View\Model\XmlModel();
        $viewModel->setVariable('data', $data); // on transfère les données à la vue
        $viewModel->setFileName('mon-fichier-export.xml'); // facultatif
        return $viewModel;
```

Dans votre vue :

``` {.php}
<?php
/**
 * @var $data array
 */

echo "<body>\n";
foreach( $data as $balise => $valeur ){
    echo "\t".$this->tag($balise)->text($valeur)."\n";
}
echo '</body>';
```

\<WRAP center round tip 60%\> Dans cette vue, l\'[Aide de vue
Tag](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/tag) est
utilisée ici pour générer des tags. Ceci évite de gérer explicitement
les caractères foireux (\<, \>, &). \</WRAP\>

Résultat :

``` {.xml}
<?xml version="1.0" encoding="utf-8"?>
<body>
    <balise1>contenu1</balise1>
    <balise2>contenu2</balise2>
</body>
```

Export PDF
----------

La classe `\UnicaenApp\Exporter\Pdf` facilite la fabrication d\'un
document PDF et voici ses arguments principaux :

-   Nourrissage à partir de marquage HTML, spécifié directement sous
    forme de chaînes de caractères ou bien par l\'intermédiaire d\'un
    script de vue.
-   Format A4, A3, etc., portrait ou paysage.
-   En-tête contenant le logo de l\'UCBN (spécifié en dur, désolé!), un
    titre éventuel et un sous-titre éventuel. Possibilité de
    différencier en-tête des pages paires et en-tête des pages impaires.
-   Pied de page contenant l\'heure de génération du document, le numéro
    de page, le nombre total de pages et un titre éventuel. Possibilité
    de différencier pied des pages paires et pied des pages impaires.
-   Un texte en filigrane éventuel (watermark).
-   Spécification possible des opérations autorisées sur le document
    généré et le mot de passe éventuel permettant d\'ouvrir le document.

\<note important\>C\'est la bibliothèque mPDF
(<http://www.mpdf1.com/mpdf/>) qui motorise la génération PDF donc vous
devez ajouter un truc comme ça dans les \"require\" de votre
`composer.json` :

``` {.php}
"mpdf/mpdf": "v5.7.1"
```

\</note\>

Exemple d\'utilisation :

``` {.php}
public function pdfAction()
{
    $renderer = $this->getServiceLocator()->get('view_renderer'); /* @var $renderer \Laminas\View\Renderer\PhpRenderer */
    $exporter = new \UnicaenApp\Exporter\Pdf($renderer, 'A4', true);
    $exporter->addBodyHtml('<h1>Module UnicaenApp</h1>');
    $exporter->addBodyHtml('<h2>Export PDF</h2>', false);
    $exporter->addBodyScript('application/demo/paragraphe.phtml', false);
    $exporter->addBodyScript('application/demo/nouvelle-page.phtml', true);
    $exporter->export('export.pdf');
}
```

Util
----

La classe `\UnicaenApp\Util` regroupe quelques méthodes statiques
utiles.

### generateStringTimestamp()

Génère une chaine de caractères corespondant à la date spécifiée (ou
l\'instant présent) au format \"aaaammjj\_hhmmss\", pouvant être
utilisée dans un nom de fichier par exemple. Utilisation :

``` {.php}
$ts = \UnicaenApp\Util::generateStringTimestamp();
var_dump($ts);
// affiche '20121130_104056'
```

### topChrono()

Permet de chronométrer le temps écoulé à plusieurs endroits de votre
code. Affiche le temps écoulé depuis le dernier top du chronomètre.
Utilisation :

``` {.php}
\UnicaenApp\Util::topChrono();      // affiche 'chrono: 0'
for ($i = 0; $i < 10000000; $i++) { }
\UnicaenApp\Util::topChrono("Ici"); // affiche 'Ici: 1.2799' (par exemple)
for ($i = 0; $i < 10000000; $i++) { }
\UnicaenApp\Util::topChrono("Là");  // affiche 'Là: 1.2841' (par exemple)
```

### zip()

Compresse un fichier ou répertoire, en utilisant la classe
\"\\ZipArchive\" (l\'extension PHP \"php5\_zip\" est donc requise sur le
serveur). Utilisation :

``` {.php}
\UnicaenApp\Util::zip($sourceDir, $zipfilePath);
```

### removeFile()

Supprime un fichier ou un répertoire. Utilisation :

``` {.php}
\UnicaenApp\Util::removeFile($fileOrDirectoryPath);
```

### formattedFloat()

Formatte un nombre flottant pour l\'affichage. Utilisation :

``` {.php}
$value = 1200.61;
echo \UnicaenApp\Util::formattedFloat($value, \NumberFormatter::DECIMAL, 1);
// résultat: "1 200,6"
echo \UnicaenApp\Util::formattedFloat($value, \NumberFormatter::CURRENCY);
// résultat: "1 200,61 €"

$value = 1247.0;
echo \UnicaenApp\Util::formattedFloat($value, \NumberFormatter::DECIMAL, -1);
// résultat: "1 247"
```

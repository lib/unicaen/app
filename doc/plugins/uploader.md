Uploader
========

Tente de simplifier le dépôt de fichiers en mode AJAX, et leur téléchargement.

*Requiert le plugin jQuery "Form" (http://malsup.com/jquery/form) pour le dépôt en mode AJAX : "jquery.form.js" doit 
être dans le répertoire "public/js" de l'appli.*

Fonctionne en association avec l'aide de vue "uploader".

Le plugin et l'aide de vue manipulent chaque fichier déposé sous la forme d'une instance d'une classe  implémentant 
l'interface `\UnicaenApp\Controller\Plugin\Upload\UploadedFileInterface`.
Dans l'exemple qui suit, il s'agit de la classe `FichierEntity` dont les détails d'implémentation importent peu.


Côté contrôleur
---------------

Voici un exemple de code d'un contrôleur fournissant de quoi déposer des fichiers, lister les fichiers déposés, 
télécharger et supprimer des fichiers.

Seules les actions "ajouter" et "telecharger" font appel au plugin, les autres sont là pour la compréhension.

```php
// IndexController.php
 
/**
 * Affichage du formulaire de dépôt et des fichiers déposés.
 */
public function indexAction()
{
    return [];
}

/**
 * Dépôt de fichiers.
 */
public function ajouterAction()
{
    $result = $this->uploader()->upload();

    // Si le plugin retourne du JSON, c'est qu'il y a un problème
    if ($result instanceof \Laminas\View\Model\JsonModel) {
        return $result;
    }

    // Si le plugin retourne un tableau, l'upload a réussi.
    // Enregistrement des fichiers temporaires uploadés, par exemple :
    if (is_array($result)) {
        foreach ($result['files'] as $file) {
            $path          = $file['tmp_name'];
            $nomFichier    = $file['name'];
            $typeFichier   = $file['type'];
            $tailleFichier = $file['size'];

            $fichier = (new FichierEntity())
                    ->setType($typeFichier)
                    ->setNom($nomFichier)
                    ->setTaille($tailleFichier)
                    ->setContenu(file_get_contents($path));

            $fichier->persist();

            unlink($path);
        }
    }

    // Redirection vers l'affichage des fichiers déposés
    return $this->redirect()->toRoute('fichier/lister', [], [], true);
}

/**
 * Affichage de la liste des fichiers déposés.
 */
public function listerAction()
{
    // Recherche des fichiers déposés, par exemple :
    $fichiers = FichierEntity::findAll();
    /* @var $fichiers \UnicaenApp\Controller\Plugin\Upload\UploadedFileInterface[] */

    // NB: $fichiers doit être de type \UnicaenApp\Controller\Plugin\Upload\UploadedFileInterface[]
    return ['fichiers' => $fichiers];
}

/**
 * Téléchargement d'un fichier déposé.
 */
public function telechargerAction()
{
    // Recherche du fichier demandé, spécifié dans la requête, exemple :
    $fichiers = FichierEntity::findAll();
    $fichier = $fichiers[$this->params('name')];
    /* @var $fichier \UnicaenApp\Controller\Plugin\Upload\UploadedFileInterface */

    // Envoi du fichier au client (navigateur)
    // NB: $fichier doit être de type \UnicaenApp\Controller\Plugin\Upload\UploadedFileInterface
    $this->uploader()->download($fichier);
}

/**
 * Suppression d'un fichier déposé.
 */
public function supprimerAction()
{
    // Recherche du fichier demandé, spécifié dans la requête, exemple :
    $fichiers = FichierEntity::findAll();
    $fichier = $fichiers[$this->params('name')];

    $fichier->delete();

    // Redirection vers l'affichage des fichiers déposés
    return $this->redirect()->toRoute('fichier/lister', [], [], true);
}
```

L'action "listerAction()" répond à une requête de type AJAX envoyée au chargement de la page par 
l'aide de vue "Uploader". Par conséquent, si ce n'est pas déjà fait dans votre application, 
vous devez fournir un layout adapté à ce type de requête (i.e. un layout "allégé" ne rendant 
que le résultat de l'action).

Par exemple :

```php
// Module.php

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        [...]

        $eventManager = $application->getEventManager();

        /* Utilise un layout spécial si on est en AJAX. Valable pour TOUS les modules de l'application */
        $eventManager->getSharedManager()->attach('Laminas\Mvc\Controller\AbstractActionController', 'dispatch',
            function (MvcEvent $e) {
                $request = $e->getRequest();
                if ($request instanceof HttpRequest && $request->isXmlHttpRequest()) {
                    $e->getTarget()->layout('layout/ajax.phtml');
                }
            }
        );
    }
```

```phtml
// view/layout/ajax.phtml

<?php echo $this->content;
```


Côté vues
---------

Voici un exemple de code permettant d'afficher le formulaire de dépôt ainsi que la liste des fichiers déjà déposés (action "index" du contrôleur) :

```html
<!-- index.phtml -->

<div class="upload-container">    <!-- Cette div.upload-container est INDISPENSABLE -->
    <?php
    $urlAjouter = $this->url('fichier/ajouter', [], [], true);
    $urlLister  = $this->url('fichier/lister',  [], [], true);
    $uploader   = $this->uploader()->setUrl($urlAjouter);
    ?>
    <div class="row">
        <div class="col-md-6">
            <!-- Liste des fichiers déposés -->
            <?php echo $uploader->renderUploadedFiles($urlLister) ?>
        </div>
        <div class="col-md-6">
            <!-- Formulaire de dépôt de PJ -->
            <?php echo $uploader->renderForm() ?>
        </div>
    </div>
</div>
```

Il est possible à peu de frais d'ajouter un champ supplémentaire au formulaire de dépôt, exemple :
```php
<?php
$form = $uploader->getForm();
$form->addElement((new Hidden('annexe'))->setValue(1));
echo $uploader->renderForm();
?>
```


Et voici un exemple de code pour la vue associée à l'action "lister" :

```phtml
<!-- lister.phtml -->

<?php if (count($fichiers)): ?>

<ul>
<?php foreach ($fichiers as $fichier): ?>
    <li>
        <!-- lien de téléchargement du fichier -->
        <?php
        $paramsTelecharger = ['name' => $fichier->getNom()];
        $urlTelecharger    = $this->url('fichier/telecharger', $paramsTelecharger, [], true);
        echo $this->uploader()->renderUploadedFile($fichier, $urlTelecharger);
        ?>

        <!-- lien de suppression du fichier -->
        <?php
        $paramsSupprimer = ['name' => $fichier->getNom()];
        $urlSupprimer    = $this->url('fichier/supprimer', $paramsSupprimer, [], true);
        echo $this->uploader()->renderDeleteFile($fichier, $urlSupprimer);
        ?>
    </li>
<?php endforeach ?>
</ul>

<?php else: ?>

<p>Aucun.</p>

<?php endif;
```

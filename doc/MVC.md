MVC
===

ModalListener
-------------

C\'est un composant qui simplifie l\'ouverture de lien hypertexte dans
une fenêtre modale Bootstrap 3.

Le principe est l\'écoute de l\'événement MVC `render` pour imbriquer le
modèle de vue fourni par l\'action courante dans le modèle de vue
correspondant au contenu interne d\'une fenêtre modale
(ModalInnerViewModel).

\<note important\> Ce mécanisme est enclenché seulement si :

-   un paramètre GET ou POST `modal` valant `1` est trouvé dans la
    requête.
-   le modèle de vue fourni par l\'action courante n\'est pas déjà celui
    d\'une fenêtre modale (ModalViewModel ou ModalInnerViewModel).

\</note\>

ExceptionStrategy
-----------------

Stratégie permettant d\'afficher proprement un message d\'erreur
lorsqu\'une exception maison est levée dans une action. La vue utilisée
par cette stratégie est \'error/exception\'.

\<note important\>NB: Cette stratégie ne rentre en action que si
l\'exception lancée implémente l\'interface
`UnicaenApp\Exception\ExceptionInterface`.\</note\>

Cette stratégie est activée par défaut dans le module UnicaenApp :

``` {.php}
public function onBootstrap(EventInterface $e) /* @var $e \Laminas\Mvc\MvcEvent */
{
    /* @var $application \Laminas\Mvc\Application */
    $application = $e->getApplication();
    /* @var $services ServiceManager */
    $services    = $application->getServiceManager();

    // ...

    $eventManager = $application->getEventManager();
    $viewManager  = $services->get('view_manager');

    $exceptionStrategy = new ExceptionStrategy();
    $exceptionStrategy->setDisplayExceptions($viewManager->getExceptionStrategy()->displayExceptions());
    $exceptionStrategy->attach($eventManager);

    // ...
}
```

AjaxStrategy
------------

Stratégie permettant de na pas retourner le template en AJAX.

``` {.php}
public function onBootstrap(EventInterface $e) /* @var $e \Laminas\Mvc\MvcEvent */
{
    /* Déclare la dernière vue transmise comme terminale si on est en AJAX */
    $sharedEvents = $e->getApplication()->getEventManager()->getSharedManager();
    $sharedEvents->attach('Laminas\Mvc\Controller\AbstractActionController','dispatch',
         function($e) {
            $result = $e->getResult();
            if(is_array($result)){
                $result = new \Laminas\View\Model\ViewModel($result);
                $e->setResult($result);
            }elseif(empty($result)){
                $result = new \Laminas\View\Model\ViewModel();
                $e->setResult($result);
            }
            if ($result instanceof \Laminas\View\Model\ViewModel) {
                $result->setTerminal($e->getRequest()->isXmlHttpRequest());
            }
    });

    // ...
}
```

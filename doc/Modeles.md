Modèles
=======

Entités LDAP
------------

### Group

Classe des groupes issus de l\'annuaire LDAP. Ce sont des objets de ce
type qui sont retournés par défaut par les méthodes de recherche du
service
[Group](/develop/unicaen2/moduleunicaenunicaenapp/services#group).

### People

Classe des individus issus de l\'annuaire LDAP. Ce sont des objets de ce
type qui sont retournés par défaut par les méthodes de recherche du
service
[People](/develop/unicaen2/moduleunicaenunicaenapp/services#people).

### Structure

Classe des structures issues de l\'annuaire LDAP. Ce sont des objets de
ce type qui sont retournés par défaut par les méthodes de recherche du
service
[Structure](/develop/unicaen2/moduleunicaenunicaenapp/services#structure).

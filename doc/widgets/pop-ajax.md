
# PopAjax

PopAjax est un `popover` basé sur celui de Bootstrap et
compatible Ajax. C'est un Widget conçu avec la Widget Factory de
JQueryUI (inclus dans UnicaenApp).

Il propose :

-   de charger éventuellement son contenu en Ajax à partir d'une URL
-   de créer simplement une DIV et de le paramétrer en lui passant des
    attributs data*.
-   de pouvoir l'instancier en Javascript comme n'importe quel widget
    JQuery
-   un système d'événements permettant de fermer le popover, de
    recharger la page ou de lancer l'événement de votre choix si le
    formulaire inclus
-   de personnaliser plein de paramètres (où il s'affiche, quel message
    d'attente, etc.
-   si besoin, un popajax peut être appelé depuis un autre popajax et
    ainsi de suite sans limites.
-   quand on clique hors du popover, il se ferme tout seul.
-   un système de boite de dialogue de confirmation (avec possibilité
    d'ajouter des éléments de formulaire au besoin)

Voici ce que ça donne :
![PopAjax](popajax.png)

Exemples :
----------

```php
<!-- Le fait de préciser que le lien est de classe popajax suffit!!! data-submit-close fermera le popAjax dès qu'un formulaire sera posté -->
<a href="<?php echo $url ?>" class="pop-ajax" data-submit-close="true">Lien 1</a>

<!-- Ca marche aussi avec un bouton mais il faut lui préciser l'URL de chargement en passant par un attribut du dataset-->
<button type="button" class="pop-ajax" data-url="http://www.google.com">Mon bouton</button>

<!-- Exemple avec un événement -->
<a href="<?php echo $url ?>" class="pop-ajax" data-submit-event="mon-formulaire-enregistre">Lien 1</a>
<script>

    $("body").on("mon-formulaire-enregistre", function (event, popAjax)
    {
        console.log(popAjax); // affiche en console votre objet popajax
    });

</script>
```

En bref
-------

### Gestion des titres

Le titre du popajax peut être adapté au contenu retourné par la requête
AJAX : Si le code HTMl contient :

-   un titre h1
-   ou un élément de classe .popover-title
-   ou un élément de classe .page-header

alors cet élément sera supprimé du contenu du popover et son contenu
sera injecté dans la partie titre du popover.

Si aucun titre n'est disponible alors la zone de titre disparaît.

### Boites de dialogue de confirmation

PopAjax vous permet de réaliser simplement des boites de dialogue de
confirmation.

Exemple :

```html
<a
    class="pop-ajax"
    href="<?php echo $url ?>"
    data-content="Voulez-vous vraiement faire cela <input name='t1' value='t1' /> ?"
    data-confirm="true"
>Confirm</a>
```

Dans ce cas, le texte est posé au départ et l'URL ne sera appelée que
lorsque le bouton de confirmation sera cliqué. J'ai même ajouté un
input dont la valeur sera récupérable dans les données POST de l'action
pour montrer que c'est possible.

Il est même possible de ne pas fournir de contenu de de le charger en
AJAX si besoin. Attention dans votre action de contrôleur de bien
distinguer dans ce cas la demande (méthode GET) de l'action (méthode
POST).

Si vous ne souhaitez rien renvoyer de spécial hormis des messages
informatifs, le modèle de vue
`UnicaenApp\View\Model\MessengerViewModel`{.php} vous aidera à ne
renvoyer que les messages collectés dans votre action sans avoir à créer
de vue spécifique pour cela.

Exemple côté PHP (dans une action de contrôleur):

```php

if ($this->getRequest()->isPost()){
    try{
        $this->getVotreService()->faireVotretravail();
        $this->flashMessenger()->addSuccessMessage("Tout est OK.");
    }catch(\Exception $e){
        $this->flashMessenger()->addErrorMessage($e->getMessage());
    }
}else{
    $this->flashMessenger()->addErrorMessage('Vous devez d'abord confirmer.');
}

return new \UnicaenApp\View\Model\MessengerViewModel();

```

### Bouton de fermeture

Le clic sur tout élément ayant pour classe \"pop-ajax-hide\" engendrera
la fermeture du popAjax.

Options
-------

| Nom (dataset)   | Nom (Json)    | Type    | Valeur par défaut                                        | Description                                                                                                                                               |
|-----------------|---------------|---------|----------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| url             | url           | string  | undefined ou href (si présent)                           | URL de chargement de la page AJAX                                                                                                                         |
| content         | content       | string  | undefined                                                | contenu (en HTML). si défini alors le chargement ne se fait plus à partir de l'URL (plus d'AJAX à l'affichage)\...                                        |
| submit-event    | submitEvent   | string  | undefined                                                | nom d'un événement à transmettre. L'événement sera lancé si un formulaire est posté à l'intérieur du popover ET que le résultat ne contient aucune erreur |
| submit-close    | submitClose   | boolean | false                                                    | Détermine on doit fermer le popover après le post d'un formulaire sans retour d'erreur                                                                    |
| submit-reload   | submitReload  | boolean | false                                                    | Détermine on doit recharger toute la page après le post d'un formulaire sans retour d'erreur                                                              |
| loading-title   | loadingTitle  | string  | Chargement\...                                           | Titre lorsque le popover est en cours de chargement                                                                                                       |
| forced          | forced        | boolean | false                                                    | Permet de relancer la requête AJAX systématiquement au réaffichage de la popAjax                                                                          |
| loading-content | loadingContent | string  | <div class="loading"></div>                              | Contenu lorsque le popover est en cours de chargement                                                                                                     |
| title           | title         | string  | undefined                                                | Titre à afficher si le contenu issu de la requête ne contient aucun titre à afficher                                                                      |
| confirm         | confirm       | boolean | false                                                    | transforme le popajax en boite de dialogue de confirmation                                                                                                |
| confirm-button  | confirmButton | string  | <span class="glyphicon glyphicon-ok"></span> OK          | Texte du bouton de confirmation (si vous fournissez un texte volontairement vide alors le bouton n'apparaitra pas)                                        |
| cancel-button   | cancelButton  | string  | <span class="glyphicon glyphicon-remove"></span> Annuler | Texte du bouton d'annulation (si vous fournissez un texte volontairement vide alors le bouton n'apparaitra pas)                                           |

Événements
----------

Les événements permettent de réagir à des changements d'état. Les
closures ou les fonctions qui seront appelées se verront transmettre
deux arguments :

1.  l'événement en lui-même
2.  l'objet popajax (ce qui permet de le manipuler ou de récupérer son
    contenu (méthode getContent), etc.

Exemple d'utilisation d'événement :

```html
<!-- Ici, le widget est associé à l'élément a directement en Javascript et pas en passant par la classe popajax. Le titre est également transmis par le tableau JSON des options, de même que la closure pour l'événement show -->
<a id="popajax_1" href="<?php echo $url ?>">TEST</a>
<script>

    $(function(){

        $('#popajax_1').popAjax({title: 'Mon Titre', show: function(event, popAjax){
            console.log('show');
            console.log(event);
            console.log(popAjax);
        }});

    });

</script>

```

### Liste des événements

| Événement | Description                                                                 |
|-----------|-----------------------------------------------------------------------------|
| show      | se déclenche à l'affichage du popajax                                       |
| hide      | se déclenche à la fermeture du popajax                                      |
| change    | lorsqu'un changement de contenu est détecté                                 |
| submit    | lorsqu'un formulaire a été posté et que le retour ne comporte aucune erreur |
| error     | lorsqu'un formulaire a été posté et que le retour comporte des erreurs      |

Méthodes
--------

Popajax comporte un certain nombre de méthodes :

| Nom             | Arguments | Valeur de retour            | Description                                 |
|-----------------|-----------|-----------------------------|---------------------------------------------|
| shown           | Aucun     | boolean                     | Retourne true s'il est affiché, false sinon |
| show            | Aucun     | this                        | Affiche le popover                          |
| hide            | Aucun     | this                        | Masque le popover                           |
| getContent      | Aucun     | element Jquery ou undefined | Retourne le contenu du popover              |
| setContent      | string    | this                        | Permet de modifier le contenu du popover    |

# TabAjax

Cette aide de ue sert à dessiner des listes d'onglets avec contenu
chargé statiquement et/ou dynamiquement. Il est possible de mélanger des
onglets dynamiques et d'autres statiques.

Usage simple (dans une vue phtml):

```php

echo $this->tabajax([
    [
        'id'      => 'fiche',
        'label'   => 'Fiche',
        'content' => 'Ceci est le contenu de la fiche descriptive',
    ],
    [
        'id'    => 'détails',
        'label' => 'Détails',
        'url'   => $this->url('fiche/details', ['fiche' => 1245]),
    ],
]);

```

Ici, le premier onglet (Fiche) a un contenu statique. Le deuxième onglet
reçoit une URL, qui sera exploitée le moment venu pour charger son
contenu en AJAX. Les ID permettent de nommer les onglets pour les
exploiter ensuite en JS si besoin.\
`label`{.php} est le nom affiché sur l'onglet.\
`content`{.php} le contenu de l'onglet (si pré-chargé).

Événement "loaded"
--------------------

Il est déclenché lorsqu'un onglet AJAX a terminé son chargement :

```javascript
    $(function(){
        $('#zozo').tabAjax({
            'loaded': function( event, tab ){
                //console.log(event);
                console.log(tab);
            }
        });

    });
```

Chargement d'un onglet à la demande
------------------------------------

```javascript
    $(function(){
        $('#zozo').tabAjax('select', 'onglet-general');
    });
```
# Bootstrap DateTimePicker

Utilise le composant suivant : https://eonasdan.github.io/bootstrap-datetimepicker
pour dessiner des composants DateTime, Date ou Time.

## Installation

Attention : ce composant n'est pas activé par défaut. Il faut ajouter le code Javascript suivant
dans votre fichier `public/js/app.js` en adptant les URL débutant par `vendor/unicaen/app/` à votre contexte :

```javascript
/* DateTime Picker */
WidgetInitializer.add('bootstrap-datetimepicker', 'bootstrapDatetimepicker', function(){
    WidgetInitializer.includeJs(Url('vendor/unicaen/app/js/bootstrap-datetimepicker.min.js'));
    WidgetInitializer.includeCss(Url('vendor/unicaen/app/css/bootstrap-datetimepicker.min.css'));
});
```

## Usage

Voici un exemple de composant ajouté à un formulaire Zend :

```php
$this->add([
    'type'       => 'DateTime',
    'name'       => 'horaire-debut',
    'options'    => [
        'label'  => 'Horaire de début',
        'format' => \UnicaenApp\Util::DATETIME_FORMAT, /* Formats de DateTime au standard PHP. La conversion en format MomentJS est automatique */
        /* Placez ici d'autres options que vous souhaiteriez transmettre au widget */
    ],
]);
```

Ce code fait appel au composant standard `DateTime` du Zend Framework.
Il n'y a rien de spécifique à faire dans la vue, à part utiliser l'aide de vue `FormControlGroup` 
pour afficher le composant.

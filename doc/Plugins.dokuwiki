====== Plugins de contrôleur (controller plugins) ======

===== Confirm =====

Ce plugin forme un couple avec l'aide de vue du même nom.

Dans l'exemple qui suit, on affiche une fenêtre (modale) de demande de confirmation lorsqu'on clique sur le lien de suppression d'un contrat.

La requête de suppression du contrat est une requête AJAX (grâce au mécanisme installé sur les liens classés "ajax-modal", cf. ''AjaxModalListener'' dans "unicaen.js").

Voici la vue affichant le lien de suppression d'un contrat :

<code html index.phtml>
<?php $urlSuppression  = $this->url('contrat/supprimer', ['contrat' => $this->contrat->getId()]); ?>

<a href="<?php echo $urlSuppression ?>" class="btn btn-danger ajax-modal"
   data-event="event-contrat-suppr">Supprimer <?php echo $this->contrat ?></a>

<script>
    $("body").on("event-contrat-suppr", function(event, data) {
        event.div.modal('hide');  // ferme la fenêtre modale de demande de confirmation
        window.location.reload(); // recharge la page courante
    });
</script>
</code>

Voici le contrôleur répondant aux requêtes de suppression de contrat (route 'contrat/supprimer') :

<code php ContratController.php>
public function supprimerAction()
{
    $result = $this->confirm()->execute();

    // si un tableau est retourné par le plugin, l'opération a été confirmée
    if (is_array($result)) {
        $this->getServiceContrat()->supprimer($this->contrat);
        $this->flashMessenger()->addSuccessMessage("Suppression du contrat effectuée avec succès.");
    }

    // récupération du modèle de vue auprès du plugin et passage de variables classique
    $viewModel = $this->confirm()->getViewModel();

    $viewModel->setVariables([
        'title'   => "Suppression d'un contrat",
        'contrat' => $this->contrat,
    ]);

    return $viewModel;
}
</code>

Et voici la vue associée à l'action ''supprimerAction'' qui utilise l'aide de vue ''confirm'' :

<code php supprimer.phtml>
<h1><?php echo $this->title ?></h1>
<?php
echo $this->confirm("Confirmez-vous la suppression du contrat $this->contrat, svp ?");
</code>



===== Uploader =====

Cf. [[develop:unicaen2:moduleunicaenunicaenapp:controllerplugins:uploader|page dédiée.]]


===== LdapGroupService =====
Fournit le service d'accès aux groupes LDAP. Exemple :
<file php>
$this->ldapGroupService()->getMapper()->findOneByDn('cn=support_info,ou=groups,dc=unicaen,dc=fr');
</file>

===== LdapPeopleService =====
Fournit le service d'accès aux individus LDAP. Exemple :
<file php>
$this->ldapPeopleService()->getMapper()->findOneByUsername('gauthierb');
</file>

===== LdapStructureService =====
Fournit le service d'accès aux structures LDAP. Exemple :
<file php>
$this->ldapStructureService()->getMapper()->findOneByDnOrCodeEntite('HS_C68');
</file>

===== ModalInnerViewModel =====
Encapsule le résultat d'une vue (ViewModel) dans le marquage attendu par Bootstrap pour l'intégrer dans une fenêtre modale.

Exemple :
<file php DemoController.php>
public function modalAction()
{
    $terminal = $this->getRequest()->isXmlHttpRequest();

    $viewModel = new \Laminas\View\Model\ViewModel();
    $viewModel->setTemplate('application/demo/modal')
            ->setTerminal($terminal) // Turn off the layout for AJAX requests
            ->setVariables(array(
                'name'     => "Bertrand",
            ));

    if ($terminal) {
        return $this->modalInnerViewModel($viewModel, "Fenêtre modale", false);
    }

    return $viewModel;
}
</file>


===== PopoverInnerViewModel =====
Encapsule le résultat d'une vue (ViewModel) dans le marquage attendu par Bootstrap pour l'intégrer dans un élément "popover".

Exemple :
<file php DemoController.php>
public function popoverAction()
{
    $terminal = $this->getRequest()->isXmlHttpRequest();

    $viewModel = new \Laminas\View\Model\ViewModel();
    $viewModel->setTemplate('application/demo/popover')
            ->setTerminal($terminal) // Turn off the layout for AJAX requests
            ->setVariables(array(
                'name'     => "Laurent",
            ));

    if ($terminal) {
        return $this->popoverInnerViewModel($viewModel, "Titre du Popover", false);
    }

    return $viewModel;
}
</file>

===== MultipageFormPlugin =====

Plugin de contrôleur facilitant la mise en œuvre d'un processus de saisie en plusieurs étapes (formulaire multi-pages) :
  * les infos saisies à l'étape courante doivent être valides pour pouvoir passer à l'étape suivante
  * stockage en session des infos saisies à chaque étape
  * application du pattern [[http://fr.wikipedia.org/wiki/Post-Redirect-Get|Post-Redirect-Get]] (délégation au [[http://framework.zend.com/manual/2.0/en/modules/zend.mvc.plugins.html#the-post-redirect-get-plugin|plugin fourni par ZF2]])
  * possibilité de revenir en arrière sans perdre les infos saisies (même en cas de saisie partielle à l'étape courante)

==== Formulaire ====

L'idée est de créer un formulaire global héritant de la classe ''\UnicaenApp\Form\MultipageForm'' composé de plusieurs fieldsets, chaque fieldset correspondant à une étape de la saisie.

Exemple d'un formulaire de contact avec une saisie en 3 étapes (identité, coordonnées, divers) :

<file php ContactForm.php>
namespace Application\Form;

use UnicaenApp\Form\MultipageForm;
use Laminas\Form\Element\Csrf;
use Laminas\Form\Element\Submit;

class ContactForm extends MultipageForm
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->addFieldsetFirst(new IdentFieldset('identite'))
             ->addFieldsetNext(new CoordFieldset('coordonnees'))
             ->addFieldsetLast(new DiversFieldset('divers'))
             ->add(new Csrf('csrf'))
             ->add(new Submit('ajouter', array('label'=>"Ajouter")));
    }
}
</file>

Chaque fieldset hérite de la classe ''\Laminas\Form\Fieldset''.

Exemple du fieldset de saisie des coordonnées :

<file php CoordFieldset.php>
namespace Application\Form;

use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Fieldset;
use Laminas\InputFilter\InputFilterProviderInterface;

class CoordFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->setLabel("Coordonnées")
             ->add(new Textarea('adresse', array('label'=>"Adresse postale")))
             ->add(new Text('email', array('label'=>"Adresse mail")));
    }

    public function getInputFilterSpecification()
    {
        return array(
            'adresse' => array(
                'required' => false,
                'filters'  => array(
                    array('name' => '\Laminas\Filter\StringTrim'),
                ),
            ),
            'email' => array(
                'required' => true,
                'filters'  => array(
                    array('name' => '\Laminas\Filter\StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'=> 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array('isEmpty' => "L'adresse mail est requise"),
                        )
                    ),
                    array(
                        'name' => '\Laminas\Validator\EmailAddress',
                        'options' => array(
                            'messages' => array('emailAddressInvalidFormat' => "L'adresse mail spécifiée est invalide"),
                        ),
                    ),
                ),
            ),
        );
    }
}
</file>

<note>Implémenter l'interface ''InputFilterProviderInterface'' (méthode ''getInputFilterSpecification()'') n'est pas obligatoire mais elle permet de fournir les règles de filtrage et de validation des valeurs saisie dans le fieldset.</note>

==== Contrôleur ====

Pour fonctionner, le plugin a besoin de connaître :
  * Le formulaire global :
      * Il doit donc être fourni à **chaque** invocation du plugin : ''$this->multipageForm($this->getForm())''.
  * La table de correspondance qui associe à chacun des fieldsets du formulaire une action du contrôleur :
    * Par défaut, le nom de l'action correspondant à un fieldset est le nom de ce fieldset. Par exemple, le fieldset ''new CoordFieldset('coordonnees')'' correspondra à l'action nommée ''coordonnees'' (i.e. méthode ''coordonneesAction()'' du contrôleur).
    * Cette table de correspondance est obtenue par le plugin auprès du formulaire, vous pouvez donc spécifier une table différente en appelant la méthode ''setFieldsetActionMapping()'' du formulaire.
  * L'action du contrôleur correspondant au récapitulatif et à la demande de confirmation de la saisie complète (facultatif)
    * Par défaut, c'est '''confirmer'''.
    * Elle est obtenue par le plugin auprès du formulaire, vous pouvez donc spécifier une action différente grâce à la méthode ''setConfirmAction()'' du formulaire.
  * L'action du contrôleur correspondant à l'enregistrement de la saisie
    * Par défaut, c'est '''enregistrer'''.
    * Elle est obtenue par le plugin auprès du formulaire, vous pouvez donc spécifier une action différente grâce à la méthode ''setProcessAction()'' du formulaire.
  * L'action du contrôleur correspondant à l'abandon à tout moment de la saisie
    * Par défaut, c'est '''annuler'''.
    * Elle est obtenue par le plugin auprès du formulaire, vous pouvez donc spécifier une action différente grâce à la méthode ''setCancelAction()'' du formulaire.

<note important>
NB: il est possible de spécifier un préfixe à appliquer à **tous** les noms d'actions grâce à la méthode ''setActionPrefix()'' du formulaire.
Dans notre exemple, le préfixe '''ajouter-''' est spécifié pour pointer vers les méthodes :
  * ''ajouterIdentiteAction()'' plutôt que vers ''identiteAction()''
  * ''ajouterCoordonneesAction()'' plutôt que vers ''coordonneesAction()''
  * ''ajouterDiversAction()'' plutôt que vers ''diversAction()''
  * ''ajouterConfirmerAction()'' plutôt que vers ''confirmerAction()''
  * ''ajouterEnregistrerAction()'' plutôt que vers ''enregistrerAction()''
  * ''ajouterAnnulerAction()'' plutôt que vers ''annulerAction()''
</note>

Exemple de contrôleur pour la saisie d'une candidature :
  * l'action ''ajouter'' est le point d'entrée du processus
  * l'action ''ajouter-identite'' est la première étape
  * l'action ''ajouter-coordonnees'' est la deuxième étape
  * l'action ''ajouter-divers'' est la troisième et dernière étape
  * l'action ''ajouter-confirmer'' est chargée de récapituler les infos saisies et demander confirmation
  * l'action ''ajouter-enregistrer'' est chargée d'enregistrer les infos saisies dans une base de données par exemple
  * l'action ''ajouter-annuler'' correspond à la requête d'abandon de la saisie

<file php ContactController.php>
namespace Application\Controller;

use Application\Form\ContactForm;
use UnicaenApp\Form\MultipageForm;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Mvc\Controller\AbstractActionController;

class DemandeController extends AbstractActionController
{
    protected $form;

    public function ajouterAction()
    {
        return $this->multipageForm($this->getForm())->start(); // réinit du plugin et redirection vers la 1ère étape
    }

    public function ajouterIdentiteAction()
    {
        return $this->multipageForm($this->getForm())->process();
    }

    public function ajouterCoordonneesAction()
    {
        return $this->multipageForm($this->getForm())->process();
    }

    public function ajouterDiversAction()
    {
        return $this->multipageForm($this->getForm())->process();
    }

    public function ajouterAnnulerAction()
    {
        return $this->redirect()->toRoute('home');
    }

    public function ajouterConfirmerAction()
    {
        $response = $this->multipageForm($this->getForm())->process();
        if ($response instanceof Response) {
            return $response;
        }
        return array('form' => $this->getForm());
    }

    public function ajouterEnregistrerAction()
    {
        $data = $this->multipageForm($this->getForm())->getFormSessionData();
        // ...
        // enregistrement en base de données (par exemple)
        // ...
        return $this->redirect()->toRoute('home');
    }

    protected function getForm()
    {
        if (null === $this->form) {
            $this->form = new ContactForm('contact');
            $this->form->setActionPrefix('ajouter-');
        }
        return $this->form;
    }
}
</file>

==== Vue ====

=== Étape de saisie ===
Un script de vue par étape de saisie doit être fourni, dans lequel l'aide de vue ''MultipageFormFieldset'' doit être utilisée.
Cette aide de vue génère un titre (h2) indiquant "Étape i sur N", le code HTML du fieldset de l'étape en cours ainsi que les boutons de navigation.

Exemple :
<file php ajouter-coordonnees.phtml>
<h1>Formulaire de contact</h1>
<?php echo $this->multipageFormFieldset(); ?>
</file>

<note important>Inscrivez le même titre h1 pour toutes les étapes.</note>

=== Confirmation ===

Vous devez fournir le script de vue pour la page de récapitulatif/confirmation et utiliser l'aide de vue ''MultipageFormRecap''. Cette aide de vue génère automatiquement la liste de toutes les informations saisies et les boutons de navigation.

Exemple :
<file php ajouter-confirmer.phtml>
<h1>Formulaire de contact</h1>
<h2>Récapitulatif et confirmation des informations saisies</h2>
<?php echo $this->multipageFormRecap(); ?>
</file>

<note important>
L'aide de vue ''MultipageFormRecap'' explore automatiquement tous les éléments visibles de tous les fieldsets du formulaire afin de collecter leur labels et leur valeurs et les présenter sous la forme d'une liste de définition (dl).
Si vous souhaitez maîtriser plus finement la façon dont sont constitués les labels ou les valeurs d'un fieldset, faites implémenter à la classe de ce fieldset l'interface ''\UnicaenApp\Form\MultipageFormFieldsetInterface'' pour fournir via la méthode ''getLabelsAndValues()'' les labels et valeurs de tous les éléments de ce fieldset.

Exemple :
<file php DiversFieldset.php>
namespace Application\Form;

use Unicaen\Exception;
use UnicaenApp\Form\MultipageFormFieldsetInterface;
use Laminas\Form\Element\Text;
use Laminas\Form\Fieldset;
use Laminas\InputFilter\InputFilterProviderInterface;

class DiversFieldset extends Fieldset implements InputFilterProviderInterface, MultipageFormFieldsetInterface
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->setLabel("Divers")
                ->add(new Text('age', array('label' => "Age")))
                ->add(new Text('profession', array('label' => "Profession", 'placeholder' => "Fonctionnaire")));
    }

    ...

    public function getLabelsAndValues($data = null)
    {
        if (null === $data && !($data = $this->getValue())) {
            throw new Exception("Aucune donnée saisie disponible.");
        }
        if (array_key_exists($this->getName(), $data)) {
            $data = $data[$this->getName()];
        }

        $values = array();

        $elem = $this->get('age');
        $values[$elem->getName()]['label'] = $elem->getLabel();
        $values[$elem->getName()]['value'] = $data[$elem->getName()] ? $data[$elem->getName()] . " ans" : "Non renseigné";

        $elem = $this->get('profession');
        $values[$elem->getName()]['label'] = $elem->getLabel();
        $values[$elem->getName()]['value'] = $data[$elem->getName()] ? $data[$elem->getName()] : "Chômage";

        return $values;
    }
}
</file>
</note>

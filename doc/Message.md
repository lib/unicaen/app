Fourniture de messages selon le contexte
========================================

Configuration
-------------

``` {.php}

use UnicaenApp\Message\Specification\MessageSpecificationInterface;

class MineurSpecification implements MessageSpecificationInterface {
    public function isSatisfiedBy($age = null, array &$sentBackData = []) {
        $sentBackData['nom'] = 'Junior';
        return 0 <= $age && $age <  18;
    }
}
class MajeurSpecification implements MessageSpecificationInterface {
    public function isSatisfiedBy($age = null, array &$sentBackData = []) {
        return $age >= 18;
    }
}

return array(
    'message' => [
        'messages' => [
            [
                'id' => 'MSG_CLOSURE',
                'data' => [
                    "Vous êtes mineur(e), {nom}." => function($age) { return 0 <= $age && $age < 18; },
                    "Vous êtes majeur(e), {nom}." => function($age) { return $age >= 18; },
                    "Vous n'êtes pas né(e) !"     => true, // texte par défaut
                ],
            ],
            [
                'id' => 'MSG_CLASS',
                'data' => [
                    "Vous êtes mineur(e), {nom}." => new MineurSpecification(),
                    "Vous êtes majeur(e), {nom}." => new MajeurSpecification(),
                    "Vous n'êtes pas né(e) !"     => true,
                ],
            ],
            [
                'id' => 'MSG_OTHER',
                'data' => [
                    "Vous êtes mineur(e), {nom}." => 'mineur',
                    "Vous êtes majeur(e), {nom}." => 'majeur',
                    "Vous n'êtes pas né(e) !"     => true,
                ],
            ],
        ]
    ],
    ...
];
```

Dans un contrôleur (par exemple)
--------------------------------

``` {.php}
class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        /** @var \UnicaenApp\Message\MessageService $service */
        $service = $this->getServiceLocator()->get('MessageService');
        $service->setContext(15); // contexte global

        var_dump(
            $service->render('MSG_CLOSURE', ['nom'=>"Beber"], 19), // context = 19 (remplace 15)
            $service->render('MSG_CLASS'), // context = 15
            $service->render('MSG_OTHER', ['nom'=>"Bobby"], 'majeur'), // context = 'majeur' (égale la valeur de la spécification)
            $service->render('MSG_OTHER') // context = 15
        );

        return new ViewModel();
    }
}
```

Affiche :

    string 'Vous êtes majeur(e), Beber.' (length=28)
    string 'Vous êtes mineur(e), Junior.' (length=29)
    string 'Vous êtes majeur(e), Bobby.' (length=28)
    string 'Vous n'êtes pas né(e) !' (length=25)

Dans une vue
------------

``` {.php}
<p><?php echo $this->message()->render('MSG_CLOSURE', ['nom'=>"Beber"], 19); ?></p>
<p><?php echo $this->message()->render('MSG_CLASS'); ?></p>
<p><?php echo $this->message()->render('MSG_OTHER', ['nom'=>"Bobby"], 'majeur'); ?></p>
<p><?php echo $this->message()->render('MSG_OTHER'); ?></p>
```

Donne

``` {.html}
<p>Vous êtes majeur(e), Beber.</p>
<p>Vous êtes mineur(e), Junior.</p>
<p>Vous êtes majeur(e), Bobby.</p>
<p>Vous n'êtes pas né(e) !</p>
```

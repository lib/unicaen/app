#!/usr/bin/env bash

pandoc -f dokuwiki -t markdown Installation.dokuwiki > Installation.md
pandoc -f dokuwiki -t markdown Configuration.dokuwiki > Configuration.md
pandoc -f dokuwiki -t markdown Services.dokuwiki > Services.md
pandoc -f dokuwiki -t markdown Modeles.dokuwiki > Modeles.md
pandoc -f dokuwiki -t markdown Plugins.dokuwiki > Plugins.md
pandoc -f dokuwiki -t markdown ViewHelpers.dokuwiki > ViewHelpers.md
pandoc -f dokuwiki -t markdown Controleurs.dokuwiki > Controleurs.md
pandoc -f dokuwiki -t markdown Formulaires.dokuwiki > Formulaires.md
pandoc -f dokuwiki -t markdown Filtres.dokuwiki > Filtres.md
pandoc -f dokuwiki -t markdown Validateurs.dokuwiki > Validateurs.md
pandoc -f dokuwiki -t markdown MVC.dokuwiki > MVC.md
pandoc -f dokuwiki -t markdown Message.dokuwiki > Message.md
pandoc -f dokuwiki -t markdown Divers.dokuwiki > Divers.md
pandoc -f dokuwiki -t markdown Historique.dokuwiki > Historique.md
pandoc -f dokuwiki -t markdown ElementsPageRiches.dokuwiki > ElementsPageRiches.md
pandoc -f dokuwiki -t markdown Javascript.dokuwiki > Javascript.md
pandoc -f dokuwiki -t markdown Widgets.dokuwiki > Widgets.md

Filtres
=======

BytesFormatter
--------------

Convertit et formatte un nombre d'octets en ko, Mo, Go ou To.

Exemples :

``` {.php}
$f = new \UnicaenApp\Filter\BytesFormatter();
$result = $f->filter(765);   // retourne "765 o"
$result = $f->filter(1024);  // retourne "1 ko"
$result = $f->filter(79057); // retourne "77,2 ko"
```

Possibilité de spécifier le nombre voulu de chiffres après la virgule
(qui est 1 par défaut) :

``` {.php}
$f->setPrecision(2);
```

Formulaires
===========

Formulaires
-----------

MultipageForm
=============

Classe mère des formulaire multi-pages (saisie en plusieurs étapes).

Cf. documentation du plugin de contrôleur
[MultipageFormPlugin](./Plugins.md#multipageformplugin).

Éléments de formulaire
----------------------

DateInfSup
==========

Elément de formulaire permettant de choisir une date inférieure (date de
début) et une date supérieure éventuelle (date de fin).

Le filtre d\'entrée (input filter)
\"[DateInfSupInputFilter](/develop/unicaen2/moduleunicaenunicaenapp/form/dateinfsupinputfilter)\"
permet de valider la saisie de cet élément.

Exemple :

``` {.php}
class CaractEvtFieldset extends \Laminas\Form\Fieldset implements \Laminas\InputFilter\InputFilterProviderInterface
{
    const DATETIME_FORMAT_PHP   = 'd-m-Y H:i';
    const DATETIME_FORMAT_HUMAN = 'jj-mm-aaaa hh:mm';

    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->setLabel("Caractéristiques de l'événement")
             ->add(new \UnicaenApp\Form\Element\DateInfSup('dates', array('label' => "Début et fin éventuelle")))
             ->add(new \Laminas\Form\Element\Text('duree_prevue', array('label' => _("Durée prévue"))))
             ->add(new \Laminas\Form\Element\Textarea('serv_affectes', array('label' => _("Services numériques affectés principalement"))))
             ->add(new \Laminas\Form\Element\Textarea('actions', array('label' => _("Moyens et actions mis en oeuvre"))));
    }

    public function getInputFilterSpecification()
    {
        $dateInfSupInputFilter = new \UnicaenApp\Form\Element\DateInfSupInputFilter();
        $dateInfSupInputFilter->setDateInfRequired()
                              ->setDateSupRequired();

        return array(
            'dates' => $dateInfSupInputFilter,
            'serv_affectes' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name'=> 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array('isEmpty' => _("Ce champ est obligatoire")),
                        )
                    ),
                ),
            ),
            'actions' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name'=> 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array('isEmpty' => _("Ce champ est obligatoire")),
                        )
                    ),
                ),
            ),
        );
    }
}
```

\<note\> L\'aide de vue (view helper) qui est utilisée pour générer le
code HTML complet de cet élément (labels, champs et erreurs) est
\"[FormRowDateInfSup](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/FormRowDateInfSup)\"
(qui elle-même fait appel à l\'aide de vue
\"[FormDateInfSup](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/FormDateInfSup)\").
\</note\>

MultipageFormNavElement
================

Élément composite de navigation au sein d\'un formulaire multipage
(formulaire en plusieurs étapes)
[MultipageForm](/develop/unicaen2/moduleunicaenunicaenapp/form/MultipageForm).

Les boutons présents dépendent du contexte (de l\'étape courante) et du
paramétrage :

-   \"Précédent\"
-   \"Suivant\"
-   \"Terminer\"
-   \"Confirmer et enregistrer\"
-   \"Annuler\"

Cette aide de vue n\'a pas vraiment vocation à être utilisée directement
: elle est utilisée en interne par l\'aide de vue
[MultipageFormRow](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/multipageformrow).

SearchAndSelect
===============

Élément de formulaire permettant de rechercher puis sélectionner quelque
chose dans une source de données.

Cet élément composite encapsule en fait 2 éléments :

-   `label` : texte saisi dans un champ texte ;
-   `id` : identifiant unique de la chose *sélectionnée* (ex: le
    persopass/etupass pour un individu recherché dans l\'annuaire LDAP)
    dans un champ caché.

NB: Il faut utiliser l\'aide de vue
[FormSearchAndSelect](/develop/unicaen2/moduleunicaenunicaenapp/viewhelpers/formsearchandselect)
pour dessiner cet élément.

Lorsqu\'un texte est saisi, une requête GET Ajax est exécutée et à
charge au développeur de réaliser la recherche dans l\'action
correspondante. L\'URL de l\'action réalisant la recherche doit être
spécifiée comme suit :

``` {.php}
$sas = new SearchAndSelect('sas');
$sas->setAutocompleteSource('/ose/demo/rechercher');
```

La requête AJAX inclut le paramètre GET `term` ayant la valeur du texte
à rechercher. L\'action doit répondre à la requête AJAX un tableau JSON
au format précis. Exemple :

``` {.php}
public function rechercherAction()
{
    if (($term = $this->params()->fromQuery('term'))) {
        // réponse bidon
        $result = array();
        for ($index = 0; $index < 15; $index++) {
            // mise en forme attendue par l'aide de vue FormSearchAndSelect
            $result[] = array(
                'id'    => $index,        // identifiant unique de l'item
                'label' => uniqid($term), // libellé de l'item
                'extra' => "Bla bla",     // infos complémentaires (facultatives) sur l'item
            );
        }
        return new \Laminas\View\Model\JsonModel($result);
    }
    exit;
}
```

AdvancedMultiCheckBox
=====================

Description
-----------

Ceci est une aide de vue pour élément de formulaire. Il accepte les
éléments de type `Laminas\Form\Element\MultiCheckbox`{.php}.

-   Il ajoute deux boutons au multicheckbox habituel : un pour tout
    sélectionner et un pour ne rien sélectionner.
-   En outre, il encapsule les items dans une liste dont la hauteur est
    définissable au besoin. Ainsi pas de risque d\'avoir une liste
    interminable à l\'écran.

\<WRAP center round info 60%\> Ce composant est codé sous forme de
Widget JQuery (<https://jqueryui.com/widget/>).\
\
De plus, il exploite le
[WidgetInitializer](/develop/unicaen2/ModuleUnicaenUnicaenApp/js) donc
inutile de l\'initialiser, ça se fait automatiquement. \</WRAP\>

Exemple
-------

Dans la vue, il s\'exploite comme suit :

``` {.php}
$this->formControlGroup(
    $form->get('votre-multicheckbox'),
    $this->formAdvancedMultiCheckbox()->setHeight('20em')
);
```

Là, la hauteur a été définie directement depuis le code PHP.

Mais il est également possible de définir la hauteur de votre
AdvancedMultiCheckBox en Javascript.

Exemples d\'utilisation :

``` {.javascript}

// Ici la hauteur est limitée à 20em
$("votre_selecteur_de_div").formAdvancedMultiCheckbox("height", "20em");

// Là on sélectionne tous les items
$("votre_selecteur_de_div").formAdvancedMultiCheckbox("selectAll");

```

Liste des propriétés
--------------------

-   `height`{.php} : définit la hauteur maximale de votre widget
-   `overflow`{.php} : gère l\'overflow du widget

Ces propriétés sont également utilisables directement en PHP via le
ViewHelper.

Liste des méthodes
------------------

-   `selectAll`{.php} : sélectionne tous les items de la liste
-   `selectNone`{.php} : désélectionne tous les items de la liste
-   `getSelectAllBtn`{.php} : retourne le bouton qui permet de tout
    sélectionner
-   `getSelectNoneBtn`{.php} : retorune le bouton qui permet de tout
    désélectionner

Filtres d\'entrée (input filters)
---------------------------------

DateInfSupInputFilter
=====================

Filtre d\'entrée associé à l\'élément de formulaire \"DateInfSup\" à la
saisie d\'une date inférieure (date de début) et une date supérieure
éventuelle (date de fin).

Cf. exemple sur la page consacrée à l\'élément de formulaire
\"[DateInfSup](/develop/unicaen2/moduleunicaenunicaenapp/form/dateinfsup)\".

<?php

namespace UnicaenApp;

use Laminas\ServiceManager\Factory\InvokableFactory;
use UnicaenApp\Controller\CacheControllerFactory;
use UnicaenApp\DeveloperTools\MessageCollectorService;
use UnicaenApp\DeveloperTools\MessageCollectorServiceFactory;
use UnicaenApp\Form\Element\SearchAndSelect2;
use UnicaenApp\Form\View\Helper\FormControlGroup;
use UnicaenApp\Form\View\Helper\FormControlGroupFactory;
use UnicaenApp\Form\View\Helper\FormControlText;
use UnicaenApp\Form\View\Helper\FormElementCollection;
use UnicaenApp\Form\View\Helper\FormElementRow;
use UnicaenApp\Form\View\Helper\FormSearchAndSelect2;
use UnicaenApp\HostLocalization\HostLocalization;
use UnicaenApp\HostLocalization\HostLocalizationFactory;
use UnicaenApp\Message\View\Helper\MessageHelper;
use UnicaenApp\Message\View\Helper\MessageHelperFactory;
use UnicaenApp\Mvc\RedirectResponse;
use UnicaenApp\Mvc\RedirectResponseFactory;
use UnicaenApp\ORM\Query\Functions\Chr;
use UnicaenApp\ORM\Query\Functions\Coalesce;
use UnicaenApp\ORM\Query\Functions\CompriseEntre;
use UnicaenApp\ORM\Query\Functions\PasHistorise;
use UnicaenApp\ORM\Query\Functions\Replace;
use UnicaenApp\ServiceManager\ServiceLocatorAwareInitializer;
use UnicaenApp\View\Helper\AppInfos;
use UnicaenApp\View\Helper\AppInfosFactory;
use UnicaenApp\View\Helper\AppLink;
use UnicaenApp\View\Helper\AppLinkFactory;
use UnicaenApp\View\Helper\HeadLink;
use UnicaenApp\View\Helper\HeadLinkFactory;
use UnicaenApp\View\Helper\HeadScript;
use UnicaenApp\View\Helper\HeadScriptFactory;
use UnicaenApp\View\Helper\InlineScript;
use UnicaenApp\View\Helper\InlineScriptFactory;
use UnicaenApp\View\Helper\MessageCollectorHelper;
use UnicaenApp\View\Helper\MessageCollectorHelperFactory;
use UnicaenApp\View\Helper\Messenger;
use UnicaenApp\View\Helper\MessengerFactory;
use UnicaenApp\View\Helper\QueryParams;
use UnicaenApp\View\Helper\QueryParamsHelperFactory;
use UnicaenApp\View\Helper\Upload\UploaderHelper;
use UnicaenApp\View\Helper\Upload\UploaderHelperFactory;
use UnicaenApp\View\Helper\UserProfileSelect;
use UnicaenApp\View\Helper\UserProfileSelectFactory;

return [
    /**
     * Paramétrage pour autorisation ou non de la connexion à une app de l'exterieur de l'établissement.
     */
    'hostlocalization' => [
        'activated' => false,
        'proxies' => [
            //xxx.xx.xx.xxx
        ],
        'reverse-proxies' => [
            //xxx.xx.xx.xxx
        ],
        'masque-ip' => '',
    ],
    'router'          => [
        'routes' => [
            // Base “route”, which describes the base match needed, the root of the tree
            'home'             => [
                // The Literal route is for doing exact matching of the URI path
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
            ],
            // A propos
            'apropos'          => [
                'type'     => 'Literal',
                'options'  => [
                    'route'    => '/apropos',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'apropos',
                    ],
                ],
                'priority' => 9999,
            ],
            // Contact
            'contact'          => [
                'type'     => 'Literal',
                'options'  => [
                    'route'    => '/contact',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'contact',
                    ],
                ],
                'priority' => 9999,
            ],
            // Plan de navigation
            'plan'             => [
                'type'     => 'Literal',
                'options'  => [
                    'route'    => '/plan',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'plan',
                    ],
                ],
                'priority' => 9999,
            ],
            // Mentions légales
            'mentions-legales' => [
                'type'     => 'Literal',
                'options'  => [
                    'route'    => '/mentions-legales',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'mentions-legales',
                    ],
                ],
                'priority' => 9999,
            ],
            // Informatique et libertés
            'il'               => [
                'type'     => 'Literal',
                'options'  => [
                    'route'    => '/informatique-et-libertes',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'informatique-et-libertes',
                    ],
                ],
                'priority' => 9999,
            ],
            // Rafraîchissement de la session
            'refresh-session'  => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/refresh-session',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'refresh-session',
                    ],
                ],
            ],
            //            // Rendu du menu secondaire
            //            'menu-secondaire' => [
            //                'type'          => 'Literal',
            //                'options'       => [
            //                    'route'       => '/menu-secondaire',
            //                    'defaults'    => [
            //                        'controller' => 'UnicaenApp\Controller\Application',
            //                        'action'     => 'menu-secondaire',
            //                    ],
            //                ],
            //            ],
            // Test d'envoi de mail par l'appli
            'maintenance'      => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/maintenance',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'maintenance',
                    ],
                ],
            ],
            // Icônes fournis
            'icons'          => [
                'type'     => 'Literal',
                'options'  => [
                    'route'    => '/icons',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Application',
                        'action'     => 'icons',
                    ],
                ],
            ],
            'cache'            => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/cache',
                    'defaults' => [
                        'controller' => 'UnicaenApp\Controller\Cache',
                    ],
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'js'  => [
                        'type'          => 'Segment',
                        'options'       => [
                            'route'    => '/js[/:version]',
                            'defaults' => [
                                'action' => 'js',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                    'css' => [
                        'type'          => 'Segment',
                        'options'       => [
                            'route'    => '/css[/:version]',
                            'defaults' => [
                                'action' => 'css',
                            ],
                        ],
                        'may_terminate' => true,
                    ],
                ],
            ],
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application'      => [
                // The Literal route is for doing exact matching of the URI path
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/application',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ],
                ],
                // Hints to the router that no other segments will follow it
                'may_terminate' => true,
                // Additional routes that stem from the base “route” (i.e., build from it)
                'child_routes'  => [
                    'default' => [
                        // A Segment route allows matching any segment of a URI path
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults'    => [
//                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories'          => [
            'translator'                  => 'Laminas\I18n\Translator\TranslatorServiceFactory',
            'navigation'                  => 'Laminas\Navigation\Service\DefaultNavigationFactory',
            // service de gestion de la session
            'Laminas\Session\SessionManager' => 'UnicaenApp\Session\SessionManagerFactory',
            // service d'accès aux options de config de ce module
            'unicaen-app_module_options'  => 'UnicaenApp\Options\ModuleOptionsFactory',
            // mapper d'accès aux individus de l'annuaire LDAP
            'ldap_people_mapper'          => 'UnicaenApp\Mapper\Ldap\PeopleFactory',
            // mapper d'accès aux groupes de l'annuaire LDAP
            'ldap_group_mapper'           => 'UnicaenApp\Mapper\Ldap\GroupFactory',
            // mapper d'accès aux structures de l'annuaire LDAP
            'ldap_structure_mapper'       => 'UnicaenApp\Mapper\Ldap\StructureFactory',
            // service de manipulation des individus de l'annuaire LDAP
            'ldap_people_service'         => 'UnicaenApp\Service\Ldap\PeopleFactory',
            // service de manipulation des groupes de l'annuaire LDAP
            'ldap_group_service'          => 'UnicaenApp\Service\Ldap\GroupFactory',
            // service de manipulation des structures de l'annuaire LDAP
            'ldap_structure_service'      => 'UnicaenApp\Service\Ldap\StructureFactory',

            // Gestion des CSV
            'ViewCsvRenderer'             => 'UnicaenApp\Mvc\Service\ViewCsvRendererFactory',
            'ViewCsvStrategy'             => 'UnicaenApp\Mvc\Service\ViewCsvStrategyFactory',

            // Gestion des exports XML
            'ViewXmlRenderer'             => 'UnicaenApp\Mvc\Service\ViewXmlRendererFactory',
            'ViewXmlStrategy'             => 'UnicaenApp\Mvc\Service\ViewXmlStrategyFactory',

            'MessageCollector' => 'UnicaenApp\Service\MessageCollectorFactory',

            'MessageConfig'     => 'UnicaenApp\Message\MessageConfigFactory',
            'MessageRepository' => 'UnicaenApp\Message\MessageRepositoryFactory',
            'MessageService'    => 'UnicaenApp\Message\MessageServiceFactory',

            HostLocalization::class => HostLocalizationFactory::class,
            RedirectResponse::class => RedirectResponseFactory::class,

            MessageCollectorService::class => MessageCollectorServiceFactory::class,
        ],
        'invokables'         => [
        ],
        'abstract_factories' => [
//            'UnicaenApp\Service\Doctrine\MultipleDbAbstractFactory',
        ],
        'initializers'       => [
            ServiceLocatorAwareInitializer::class,
            'UnicaenApp\Service\EntityManagerAwareInitializer',
        ],
        'aliases'            => [
            'HostLocalization' => HostLocalization::class,
        ],
    ],
    'form_elements'   => [
        'invokables'   => [
            'UploadForm' => 'UnicaenApp\Controller\Plugin\Upload\UploadForm',
            SearchAndSelect2::class => InvokableFactory::class,
        ],
        'initializers' => [
            'UnicaenApp\Service\EntityManagerAwareInitializer',
        ],
    ],
    'hydrators'       => [
        'initializers' => [
            'UnicaenApp\Service\EntityManagerAwareInitializer',
        ],
    ],
    'view_helpers'    => [
        'shared'       => [
            'formControlGroup' => false,
        ],
        'aliases'      => [
            'appInfos'          => AppInfos::class,
            'appLink'           => AppLink::class,
            'userProfileSelect' => UserProfileSelect::class,
            'Message'           => MessageHelper::class,
            'message'           => MessageHelper::class,
            'messenger'         => Messenger::class,
            'queryParams'       => QueryParams::class,
            'formControlGroup'  => FormControlGroup::class,
            'headLink'          => HeadLink::class,
            'headScript'        => HeadScript::class,
            'inlineScript'      => InlineScript::class,
            'messageCollector'  => MessageCollectorHelper::class,
            'uploader'          => UploaderHelper::class,
            'Uploader'          => UploaderHelper::class,
        ],
        'factories'    => [
            AppInfos::class               => AppInfosFactory::class,
            AppLink::class                => AppLinkFactory::class,
            UserProfileSelect::class      => UserProfileSelectFactory::class,
            MessageHelper::class          => MessageHelperFactory::class,
            Messenger::class              => MessengerFactory::class,
            QueryParams::class            => QueryParamsHelperFactory::class,
            FormControlGroup::class       => FormControlGroupFactory::class,
            HeadLink::class               => HeadLinkFactory::class,
            HeadScript::class             => HeadScriptFactory::class,
            InlineScript::class           => InlineScriptFactory::class,
            MessageCollectorHelper::class => MessageCollectorHelperFactory::class,
            UploaderHelper::class         => UploaderHelperFactory::class,
        ],
        'invokables'   => [
            'appConnection'             => 'UnicaenApp\View\Helper\AppConnection',
            'modalAjaxDialog'           => 'UnicaenApp\View\Helper\ModalAjaxDialog',
            'confirm'                   => 'UnicaenApp\View\Helper\ConfirmHelper',
            'toggleDetails'             => 'UnicaenApp\View\Helper\ToggleDetails',
            'multipageFormFieldset'     => 'UnicaenApp\Form\View\Helper\MultipageFormFieldset',
            'multipageFormNav'          => 'UnicaenApp\Form\View\Helper\MultipageFormNav',
            'multipageFormRow'          => 'UnicaenApp\Form\View\Helper\MultipageFormRow',
            'multipageFormRecap'        => 'UnicaenApp\Form\View\Helper\MultipageFormRecap',
            'formDate'                  => 'UnicaenApp\Form\View\Helper\FormDate',
            'formDateTime'              => Form\View\Helper\FormDateTime::class,
            'formDateInfSup'            => 'UnicaenApp\Form\View\Helper\FormDateInfSup',
            'formElementCollection'     => FormElementCollection::class,
            'formRowDateInfSup'         => 'UnicaenApp\Form\View\Helper\FormRowDateInfSup',
            'formSearchAndSelect'       => 'UnicaenApp\Form\View\Helper\FormSearchAndSelect',
            'formSearchAndSelect2'      => FormSearchAndSelect2::class,
            'formLdapPeople'            => 'UnicaenApp\Form\View\Helper\FormLdapPeople',
            'formErrors'                => 'UnicaenApp\Form\View\Helper\FormErrors',
            'form'                      => 'UnicaenApp\Form\View\Helper\Form',
            'formAdvancedMultiCheckbox' => 'UnicaenApp\Form\View\Helper\FormAdvancedMultiCheckbox',
            'historique'                => 'UnicaenApp\View\Helper\HistoriqueViewHelper',
            'tabajax'                   => 'UnicaenApp\View\Helper\TabAjax\TabAjaxViewHelper',
            'tag'                       => 'UnicaenApp\View\Helper\TagViewHelper',
        ],
        'initializers' => [
            'UnicaenApp\Service\EntityManagerAwareInitializer',
            ServiceLocatorAwareInitializer::class,
        ],
    ],
    'translator'      => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '/%s/Zend_Captcha.php',
            ],
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '/%s/Zend_Validate.php',
            ],
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'controllers'     => [
        'invokables'   => [
            'UnicaenApp\Controller\Application' => 'UnicaenApp\Controller\ApplicationController',
        ],
        'initializers' => [
            'UnicaenApp\Service\EntityManagerAwareInitializer',
        ],
        'factories'    => [
            'UnicaenApp\Controller\Cache'    => CacheControllerFactory::class,
        ],
    ],

    'doctrine' => [
        'driver' => [
//            'orm_app_driver' => [
//                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
//                'cache' => 'array',
//                'paths' => [
//                    __DIR__ . '/../src/UnicaenApp/Entity/Db',
//                ],
//            ],
            'orm_default'    => [
                'class'   => 'Doctrine\ORM\Mapping\Driver\DriverChain',
//                'drivers' => [
//                    'UnicaenApp\Entity\Db' => 'orm_app_driver',
//                ],
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'string_functions' => [
                    'compriseEntre' => CompriseEntre::class,
                    'pasHistorise' => PasHistorise::class,
                    'replace' => Replace::class,
                    'chr' => Chr::class,
                    'coalesce' => Coalesce::class,
                ],
            ],
        ],
    ],

    'view_manager' => [
        // RouteNotFoundStrategy configuration
        'display_not_found_reason' => false, // display 404 reason in template
        'not_found_template'       => 'error/404', // e.g. '404'
        // ExceptionStrategy configuration
        'display_exceptions'       => false,
        'exception_template'       => 'error/index',
        // Doctype with which to seed the Doctype helper
        'doctype'                  => 'HTML5',
        // TemplateMapResolver configuration
        // template/path pairs
        'template_map'             => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404'     => __DIR__ . '/../view/error/404.phtml',
            'error/index'   => __DIR__ . '/../view/error/index.phtml',
            //            'unicaen-app/application/apropos'                  => __DIR__ . '/../view/application/apropos.phtml',
            //            'unicaen-app/application/contact'                  => __DIR__ . '/../view/application/contact.phtml',
            //            'unicaen-app/application/plan'                     => __DIR__ . '/../view/application/plan.phtml',
            //            'unicaen-app/application/mentions-legales'         => __DIR__ . '/../view/application/mentions-legales.phtml',
            //            'unicaen-app/application/informatique-et-libertes' => __DIR__ . '/../view/application/informatique-et-libertes.phtml',
            'laminas-developer-tools/toolbar/messages' => __DIR__ . '/../view/laminas-developer-tools/toolbar/messages.phtml',
        ],
        // TemplatePathStack configuration
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
        // Layout template name
        'layout'                   => 'layout/layout', // e.g., 'layout/layout'
        // Additional strategies to attach
        'strategies'               => [
            'ViewJsonStrategy', // register JSON renderer strategy
            'ViewCsvStrategy', // register CSV renderer strategy
            //            'ViewXmlStrategy', // register XML renderer strategy
            //            'ViewFeedStrategy', // register Feed renderer strategy
        ],
    ],
    // All navigation-related configuration is collected in the 'navigation' key
    'navigation'   => [
        // The DefaultNavigationFactory we configured uses 'default' as the sitemap key
        'default' => [
            // And finally, here is where we define our page hierarchy
            'home' => [
                'label' => _("Accueil"),
                'title' => _("Page d'accueil de l'application"),
                'route' => 'home',
                'order' => -100, // make sure home is the first page
                'pages' => [
                    'etab'                     => [
                        'label'    => _("Université de Caen Normandie"),
                        'title'    => _("Page d'accueil du site de l'Université de Caen Normandie"),
                        'uri'      => 'http://www.unicaen.fr/',
                        'class'    => 'ucbn',
                        'visible'  => false,
                        'footer'   => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'resource' => 'controller/UnicaenApp\Controller\Application:etab', // ACL (cf. module BjyAuthorize)
                        'order'    => 1000,
                    ],
                    'apropos'                  => [
                        'label'    => _("À propos"),
                        'title'    => _("À propos de cette application"),
                        'route'    => 'apropos',
                        'class'    => 'apropos',
                        'visible'  => false,
                        'footer'   => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap'  => true, // propriété maison pour inclure cette page dans le plan
                        'resource' => 'controller/UnicaenApp\Controller\Application:apropos',
                        'order'    => 1001,
                    ],
                    'contact'                  => [
                        'label'    => _("Contact"),
                        'title'    => _("Contact concernant l'application"),
                        'route'    => 'contact',
                        'class'    => 'contact',
                        'visible'  => false,
                        'footer'   => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap'  => true, // propriété maison pour inclure cette page dans le plan
                        'resource' => 'controller/UnicaenApp\Controller\Application:contact',
                        'order'    => 1002,
                    ],
                    'plan'                     => [
                        'label'    => _("Plan de navigation"),
                        'title'    => _("Plan de navigation au sein de l'application"),
                        'route'    => 'plan',
                        'class'    => 'plan',
                        'visible'  => false,
                        'footer'   => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap'  => true, // propriété maison pour inclure cette page dans le plan
                        'resource' => 'controller/UnicaenApp\Controller\Application:plan',
                        'order'    => 1003,
                    ],
                    'mentions-legales'         => [
                        'label'    => _("Mentions légales"),
                        'title'    => _("Mentions légales"),
                        'uri'      => 'http://www.unicaen.fr/acces-direct/mentions-legales/',
                        'class'    => 'ml',
                        'visible'  => false,
                        'footer'   => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap'  => true, // propriété maison pour inclure cette page dans le plan
                        'resource' => 'controller/UnicaenApp\Controller\Application:mentions-legales',
                        'order'    => 1004,
                    ],
                    'informatique-et-libertes' => [
                        'label'    => _("Vie privée"),
                        'title'    => _("Vie privée"),
                        'uri'      => 'http://www.unicaen.fr/vie-privee/',
                        'class'    => 'il',
                        'visible'  => false,
                        'footer'   => true, // propriété maison pour inclure cette page dans le menu de pied de page
                        'sitemap'  => true, // propriété maison pour inclure cette page dans le plan
                        'resource' => 'controller/UnicaenApp\Controller\Application:informatique-et-libertes',
                        'order'    => 1005,
                    ],
                ],
            ],
        ],
    ],

    /**
     * NB: Lors d'un `composer install` fait par une appli requérant le module "unicaen/app", le répertoire
     * `public/unicaen` de ce module doit être copié dans le répertoire "public/" de l'appli en question grâce
     * à la "post install command" suivante :
     *
     * "scripts": {
     *      "post-install-cmd": [
     *          "cp -r vendor/unicaen/app/public/unicaen public/"
     *      ]
     * }
     *
     * Les chemins ci-dessous sont donc relatifs au dossier racine de l'appli.
     */
    'public_files' => [
        'head_scripts'          => [
            '015_jquery'   => '/unicaen/app/vendor/jquery-3.6.0.min.js',
            '020_jqueryui' => '/unicaen/app/vendor/jquery-ui-1.12.1/jquery-ui.min.js',
            '040_bootstrap' => '/unicaen/app/vendor/bootstrap-5.0.2/js/bootstrap.bundle.min.js',
        ],
        'inline_scripts'        => [
            '020_app'         => '/js/app.js',
            '030_util'        => '/unicaen/app/js/util.js',
            '040_unicaen'     => '/unicaen/app/js/unicaen.js',
            '050_jquery_form' => '/unicaen/app/vendor/jquery.form.min.js' // pour l'uploader Unicaen uniquement!!,
        ],
        'stylesheets'           => [
            '010_jquery-ui'           => '/unicaen/app/vendor/jquery-ui-1.12.1/jquery-ui.min.css',
            '020_jquery-ui-structure' => '/unicaen/app/vendor/jquery-ui-1.12.1/jquery-ui.structure.min.css',
            '030_jquery-ui-theme'     => '/unicaen/app/vendor/jquery-ui-1.12.1/jquery-ui.theme.min.css',
            '040_bootstrap'           => '/unicaen/app/vendor/bootstrap-5.0.2/css/bootstrap.min.css',
            '060_unicaen'             => '/unicaen/app/css/unicaen.css',
            '065_unicaen-icon'        => '/unicaen/app/css/unicaen-icon.css',
            '070_app'                 => '/css/app.css',
        ],
        'printable_stylesheets' => [
        ],
        'cache_enabled'         => false,
    ],

    'bjyauthorize' => [
        'guards' => [
            'BjyAuthorize\Guard\Controller' => [
                [
                    'controller' => 'UnicaenApp\Controller\Application',
                    'action'     => ['maintenance', 'icons'],
                    'roles'      => [],
                ],
                [
                    'controller' => 'UnicaenApp\Controller\Cache',
                    'action'     => ['js', 'css', 'printable-css'],
                    'roles'      => [],
                ],
            ],
        ],
    ],

    // intégration à la Laminas Developer Toolbar
    'laminas-developer-tools' => [
        'profiler' => [
            'collectors' => [
                DeveloperTools\MessageCollectorService::class => DeveloperTools\MessageCollectorService::class,
            ],
        ],
        'toolbar'  => [
            'entries' => [
                DeveloperTools\MessageCollectorService::class => 'laminas-developer-tools/toolbar/messages',
            ],
        ],
    ],
];

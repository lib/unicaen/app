<?php
/**
 * Created by PhpStorm.
 * User: gauthierb
 * Date: 22/07/15
 * Time: 15:38
 */

namespace UnicaenAppTest\Message\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenApp\Message\View\Helper\MessageHelperFactory;

class MessageHelperFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $helperPluginManager;

    /**
     * @var ContainerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $serviceLocator;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $messageService;

    protected function setUp()
    {
        $this->messageService = $this->getMockBuilder('UnicaenApp\Message\MessageService')
            ->disableOriginalConstructor()
            ->getMock();

        $this->serviceLocator      = $this->createMock(ContainerInterface::class);
        $this->helperPluginManager = $this->getMockForHelperPluginManager($this->serviceLocator);
    }

    private function getMockForHelperPluginManager($serviceLocator)
    {
        $helperPluginManager = $this->getMockBuilder('Laminas\View\HelperPluginManager')
            ->disableOriginalConstructor()
            ->setMethods(['getServiceLocator'])
            ->getMock();
        $helperPluginManager
            ->method('getServiceLocator')
            ->willReturn($serviceLocator);

        return $helperPluginManager;
    }

    public function testCreatingServiceAsksServiceLocatorForMessageService()
    {
        $this->serviceLocator
            ->expects($this->once())
            ->method('get')
            ->with('MessageService')
            ->willReturn($this->messageService);

        $factory = new MessageHelperFactory();
        $factory->__invoke($this->serviceLocator, '');
    }

    public function testCreatingServiceReturnsMessageHelper()
    {
        $this->serviceLocator
            ->method('get')
            ->with('MessageService')
            ->willReturn($this->messageService);

        $factory = new MessageHelperFactory();
        $helper = $factory->__invoke($this->serviceLocator, '');
        $this->assertInstanceOf('UnicaenApp\Message\View\Helper\MessageHelper', $helper);
    }
}

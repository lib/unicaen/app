<?php
/**
 * Created by PhpStorm.
 * User: gauthierb
 * Date: 22/07/15
 * Time: 14:06
 */

namespace UnicaenAppTest\Message;

use UnicaenApp\Message\MessageServiceFactory;

class MessageServiceFactoryTest extends \PHPUnit\Framework\TestCase
{
    private $serviceLocator;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $messageRepository;

    protected function setUp()
    {
        $this->serviceLocator = $this->getMockForAbstractClass('Laminas\ServiceManager\ServiceLocatorInterface');

        $this->givenThatServiceLocatorWillReturnMessageRepository();
    }

    public function testCanCreateService()
    {
        $factory = new MessageServiceFactory();
        $service = $factory->__invoke($this->serviceLocator, '');
        $this->assertInstanceOf('UnicaenApp\Message\MessageService', $service);
    }

    private function givenThatServiceLocatorWillReturnMessageRepository()
    {
        $this->messageRepository = $this->getMockBuilder('UnicaenApp\Message\MessageRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->serviceLocator
            ->method('get')
            ->with('MessageRepository')
            ->willReturn($this->messageRepository);

        return $this;
    }
}

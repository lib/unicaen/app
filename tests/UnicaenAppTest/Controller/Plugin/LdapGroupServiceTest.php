<?php
namespace UnicaenAppTest\Controller\Plugin;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Controller\Plugin\LdapGroupService;

/**
 * Tests du plugin.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see LdapGroupService
 */
class LdapGroupServiceTest extends TestCase
{
    public function testInvokingPluginReturnsService()
    {
        $service = $this->createMock('UnicaenApp\Service\Ldap\Group');
        $plugin = new LdapGroupService($service);
        $this->assertSame($service, $plugin() /* équivalent à $plugin->__invoke() */ );
    }
}
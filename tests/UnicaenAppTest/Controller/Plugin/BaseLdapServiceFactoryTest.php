<?php

namespace UnicaenAppTest\Controller\Plugin;

/**
 * Description of BaseLdapServiceFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
abstract class BaseLdapServiceFactoryTest extends BaseServiceFactoryTest
{
    protected $ldapServiceName;
    protected $ldapServiceClass;
    
    public function testCanCreateService()
    {
        $this->serviceManager->expects($this->once())
                ->method('get')
                ->with($this->ldapServiceName)
                ->will($this->returnValue($this->createMock($this->ldapServiceClass)));

        $this->assertInstanceOf($this->serviceClass, $this->factory->__invoke($this->serviceManager, ''));
    }
}
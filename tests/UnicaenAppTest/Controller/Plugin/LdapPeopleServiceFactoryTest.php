<?php

namespace UnicaenAppTest\Controller\Plugin;

/**
 * Description of LdapPeopleServiceFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapPeopleServiceFactoryTest extends BaseLdapServiceFactoryTest
{
    protected $ldapServiceName  = 'ldap_people_service';
    protected $ldapServiceClass = 'UnicaenApp\Service\Ldap\People';
    protected $factoryClass     = 'UnicaenApp\Controller\Plugin\LdapPeopleServiceFactory';
    protected $serviceClass     = 'UnicaenApp\Controller\Plugin\LdapPeopleService';
}
<?php
namespace UnicaenAppTest\Controller\Plugin;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Controller\Plugin\LdapStructureService;

/**
 * Tests du plugin.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see LdapStructureService
 */
class LdapStructureServiceTest extends TestCase
{
    public function testInvokingPluginReturnsService()
    {
        $service = $this->createMock('UnicaenApp\Service\Ldap\Structure');
        $plugin = new LdapStructureService($service);
        $this->assertSame($service, $plugin() /* équivalent à $plugin->__invoke() */ );
    }
}
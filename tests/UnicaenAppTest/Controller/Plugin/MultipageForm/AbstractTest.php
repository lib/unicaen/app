<?php
namespace UnicaenAppTest\Controller\Plugin\MultipageForm;

use Psr\Container\ContainerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use UnicaenApp\Controller\Plugin\MultipageFormPlugin;
use UnicaenApp\Form\Element\MultipageFormNavElement;
use UnicaenAppTest\Controller\Plugin\TestAsset\ContactController;
use UnicaenAppTest\Form\TestAsset\ContactMultipageForm;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\PluginManager;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Router\SimpleRouteStack;
use Laminas\Session\Container;
use Laminas\Stdlib\Parameters;

/**
 * 
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see    MultipageFormPlugin
 * @see ContactController
 */
abstract class AbstractTest extends TestCase
{
    protected static $stepFieldsets = array(
        // stepIndex => fieldsetName
        1 => ContactMultipageForm::FIELDSET_1_NAME,
        2 => ContactMultipageForm::FIELDSET_2_NAME,
        3 => ContactMultipageForm::FIELDSET_3_NAME,
    );
    /**
     * @var ContactController
     */
    public $controller;

    /**
     * @var MvcEvent
     */
    public $event;

    /**
     * @var RouteMatch
     */
    public $routeMatch;

    /**
     * @var Request
     */
    public $request;

    /**
     * @var Response
     */
    public $response;

    /**
     * @var SimpleRouteStack
     */
    public $router;

    /**
     * @var ContainerInterface|MockObject
     */
    protected $container;

    /**
     * @var MultipageFormPlugin
     */
    protected $plugin;

    /**
     * @var ContactMultipageForm
     */
    protected $form;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $options = array(
            'route'    => '/contact/:action',
            'defaults' => array(
                'controller' => 'UnicaenAppTest\Controller\Plugin\TestAsset\ContactController',
            ),
        );
        $this->router = new SimpleRouteStack();
        $this->router->addRoute('contact', \Laminas\Router\Http\Segment::factory($options));

        $this->controller = new ContactController();
        $this->routeMatch = new RouteMatch(array('controller' => 'contact', 'action'     => 'index'));
        $this->request    = new Request();
        $this->response   = new Response();
        $this->event      = new MvcEvent();

        $this->event->setRequest($this->request);
        $this->event->setResponse($this->response);
        $this->event->setRouteMatch($this->routeMatch);
        $this->event->setRouter($this->router);

        $this->routeMatch->setMatchedRouteName('contact');

        $this->container = $this->createMock(ContainerInterface::class);

        $this->plugin = new MultipageFormPlugin();
        $this->plugin->setSessionContainer(new Container(uniqid()));
        $this->plugin->setController($this->controller);

        $pluginManager = new PluginManager($this->container);
        $pluginManager->setService('multipageForm', $this->plugin);

        $this->controller->setEvent($this->event);
        $this->controller->setPluginManager($pluginManager);

//        $this->plugin = $this->controller->plugin('multipageForm');
    }

    protected function dispatchGetRequestOnStep($fieldsetName)
    {
        $this->request = new Request();
        $this->routeMatch->setParam('action', 'ajouter-' . $fieldsetName);
        return $this->controller->dispatch($this->request, $this->response);
    }

    protected function dispatchGetRequestOnAction($actionName)
    {
        $this->request = new Request();
        $this->routeMatch->setParam('action', $actionName);
        return $this->controller->dispatch($this->request, $this->response);
    }

    protected function dispatchPostRequestOnStep($fieldsetName, $validPostData = true, $submitName = MultipageFormNavElement::NEXT)
    {
        $post = $this->getForm()->createSamplePostDataForFieldset($fieldsetName, $validPostData, $submitName);
        $this->request = new Request();
        $this->request->setMethod(Request::METHOD_POST)
                ->setPost(new Parameters($post));
        $this->routeMatch->setParam('action', 'ajouter-' . $fieldsetName);
        return $this->controller->dispatch($this->request, $this->response);
    }

    protected function dispatchPostRequestOnAction($actionName, $submitName = MultipageFormNavElement::NEXT)
    {
        $post = array(MultipageFormNavElement::NAME => array($submitName => $submitName));
        $this->request = new Request();
        $this->request->setMethod(Request::METHOD_POST)
                ->setPost(new Parameters($post));
        $this->routeMatch->setParam('action', $actionName);
        return $this->controller->dispatch($this->request, $this->response);
    }

    protected function assertIsRedirectResponse($result, $stepIndex)
    {
        $redirectUris = array(
            1 => '/contact/ajouter-identite',
            2 => '/contact/ajouter-adresse',
            3 => '/contact/ajouter-message',
        );
        /** @var Response $result */
        $this->assertInstanceOf(Response::class, $result);
        $this->assertTrue($result->isRedirect());
        $this->assertTrue($result->getHeaders()->has('Location'));
        $this->assertEquals($redirectUris[$stepIndex], $result->getHeaders()->get('Location')->getUri());
    }

    protected function assertIsRedirectResponseToCancelAction($result)
    {
        /** @var Response $result */
        $this->assertInstanceOf(Response::class, $result);
        $this->assertTrue($result->isRedirect());
        $this->assertTrue($result->getHeaders()->has('Location'));
        $this->assertEquals('/contact/ajouter-annuler', $result->getHeaders()->get('Location')->getUri());
    }

    protected function assertIsRedirectResponseToConfirmAction($result)
    {
        /** @var Response $result */
        $this->assertInstanceOf(Response::class, $result);
        $this->assertTrue($result->isRedirect());
        $this->assertTrue($result->getHeaders()->has('Location'));
        $this->assertEquals('/contact/ajouter-confirmer', $result->getHeaders()->get('Location')->getUri());
    }

    protected function assertIsRedirectResponseToFinalAction($result)
    {
        /** @var Response $result */
        $this->assertInstanceOf(Response::class, $result);
        $this->assertTrue($result->isRedirect());
        $this->assertTrue($result->getHeaders()->has('Location'));
        $this->assertEquals('/contact/ajouter-enregistrer', $result->getHeaders()->get('Location')->getUri());
    }

    protected function assertIsArrayResponse($result, $stepIndex, $withForm = false)
    {
        $this->assertIsArray($result);
        $this->assertArrayHasKey('stepCount', $result);
        $this->assertArrayHasKey('stepIndex', $result);
        $this->assertEquals(3, $result['stepCount']);
        $this->assertEquals($stepIndex, $result['stepIndex']);
        if ($withForm) {
            $this->assertArrayHasKey('form', $result);
            $this->assertInstanceOf('Laminas\Form\Form', $fs = $result['form']); /* @var $fs Form */
        }
        else {
            $classes = array(
                1 => '\UnicaenAppTest\Form\TestAsset\IdentiteFieldset',
                2 => '\UnicaenAppTest\Form\TestAsset\AdresseFieldset',
                3 => '\UnicaenAppTest\Form\TestAsset\MessageFieldset',
            );
            $this->assertArrayHasKey('fieldset', $result);
            $this->assertInstanceOf($classes[$stepIndex], $fs = $result['fieldset']); /* @var $fs Fieldset */
        }
    }

    /**
     * @return ContactMultipageForm
     */
    protected function getForm()
    {
        return $this->controller->getForm();
    }
}
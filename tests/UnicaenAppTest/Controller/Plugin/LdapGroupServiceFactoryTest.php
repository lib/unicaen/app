<?php

namespace UnicaenAppTest\Controller\Plugin;

/**
 * Description of LdapGroupServiceFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapGroupServiceFactoryTest extends BaseLdapServiceFactoryTest
{
    protected $ldapServiceName  = 'ldap_group_service';
    protected $ldapServiceClass = 'UnicaenApp\Service\Ldap\Group';
    protected $factoryClass     = 'UnicaenApp\Controller\Plugin\LdapGroupServiceFactory';
    protected $serviceClass     = 'UnicaenApp\Controller\Plugin\LdapGroupService';
}
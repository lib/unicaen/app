<?php
namespace UnicaenAppTest\Controller\Plugin;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Controller\Plugin\LdapPeopleService;

/**
 * Tests du plugin.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see LdapPeopleService
 */
class LdapPeopleServiceTest extends TestCase
{
    public function testInvokingPluginReturnsService()
    {
        $service = $this->createMock('UnicaenApp\Service\Ldap\People');
        $plugin = new LdapPeopleService($service);
        $this->assertSame($service, $plugin() /* équivalent à $plugin->__invoke() */ );
    }
}
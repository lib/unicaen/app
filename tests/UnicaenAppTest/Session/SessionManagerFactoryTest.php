<?php

namespace UnicaenAppTest\Session;

use UnicaenApp\Options\ModuleOptions;
use UnicaenApp\Session\SessionManagerFactory;
use UnicaenAppTest\BaseServiceFactoryTest;
use Laminas\Session\Container;
use Laminas\Session\SessionManager;

/**
 * Description of ModuleOptionsFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ModuleOptionsFactoryTest extends BaseServiceFactoryTest
{
    /**
     * @var SessionManagerFactory
     */
    protected $factory;

    protected $factoryClass = SessionManagerFactory::class;
    protected $serviceClass = SessionManager::class;

    public function testCanCreateService()
    {
        $appInfos = [
            'nom' => "Mon application",
        ];

        $moduleOptions = $this->createMock(ModuleOptions::class);
        $moduleOptions
            ->expects($this->once())
            ->method('getAppInfos')
            ->willReturn($appInfos);

        $this->serviceManager
            ->expects($this->once())
            ->method('get')
            ->with('unicaen-app_module_options')
            ->willReturn($moduleOptions);

        $service = $this->factory->__invoke($this->serviceManager);
        /* @var $service \Laminas\Session\SessionManager */

        $this->assertInstanceOf($this->serviceClass, $service);
        $this->assertNotEmpty($service->getValidatorChain()->getListeners('session.validate'));
        $this->assertEquals($service->getConfig()->getName(), md5($appInfos['nom']));
    }
}
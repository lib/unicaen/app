<?php
namespace UnicaenAppTest\Mapper\Ldap\TestAsset;

/**
 * Données de test représentant des résultats de recherche de groupe LDAP.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class Group
{
    /**
     * @var array
     */
    static public $data1 = array(
        'dn'                  => array('cn=admin_reseau,ou=groups,dc=unicaen,dc=fr'),
        'cn'                  => array('admin_reseau'),
        'member'              => array('uid=p00000478,ou=people,dc=unicaen,dc=fr', 'uid=p00001888,ou=people,dc=unicaen,dc=fr', 'uid=p00015593,ou=people,dc=unicaen,dc=fr'),
        'description'         => array("Administrateurs réseau de la DSI"),
        'supanngroupedatefin' => array('999912310000Z'),
    );
    
    /**
     * @var array
     */
    static public $data2 = array(
        'dn'                  => array('cn=dept_info,ou=groups,dc=unicaen,dc=fr'),
        'cn'                  => array('dept_info'),
        'member'              => array('uid=p00000478,ou=people,dc=unicaen,dc=fr', 'uid=p00001888,ou=people,dc=unicaen,dc=fr', 'uid=p00015593,ou=people,dc=unicaen,dc=fr'),
        'description'         => array("Département Informatique"),
        'supanngroupedatefin' => array('999912310000Z'),
    );
    
    /**
     * 'supannGroupeDateFin' au 14/01/2013
     * @var array
     */
    static public $data3 = array(
        'dn'                  => array('cn=consult_pandemie,ou=groups,dc=unicaen,dc=fr'),
        'cn'                  => array('consult_pandemie'),
        'member'              => array('uid=p00000478,ou=people,dc=unicaen,dc=fr', 'uid=p00001888,ou=people,dc=unicaen,dc=fr', 'uid=p00015593,ou=people,dc=unicaen,dc=fr'),
        'description'         => array("Correspondants Consultation Pandémie"),
        'supanngroupedatefin' => array('201301140000Z'),
    );
    
    /**
     * 'supannGroupeDateFin' au 14/01/2013
     * @var array
     */
    static public $data4 = array(
        'dn'                  => array('cn=groupe-fini,ou=groups,dc=unicaen,dc=fr'),
        'cn'                  => array('groupe-fini'),
        'member'              => array('uid=p00000478,ou=people,dc=unicaen,dc=fr', 'uid=p00001888,ou=people,dc=unicaen,dc=fr', 'uid=p00015593,ou=people,dc=unicaen,dc=fr'),
        'description'         => array("Groupe dont la date de fin est passée"),
        'supanngroupedatefin' => array('201301140000Z'),
    );
}
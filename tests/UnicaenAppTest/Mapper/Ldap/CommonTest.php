<?php

namespace UnicaenAppTest\Mapper\Ldap;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Mapper\Ldap\AbstractMapper;
use Laminas\Ldap\Ldap;

/**
 * Description of CommonTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
abstract class CommonTest extends TestCase
{
    /**
     * @var string
     */
    protected $entityClassName;
    
    /**
     * @var AbstractMapper 
     */
    protected $mapper;
    
    /**
     * @var Ldap|\PHPUnit\Framework\MockObject\MockObject Mock object
     */
    protected $ldap;

    /**
     * @var array
     */
    protected $config;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->ldap = $this->createMock('Laminas\Ldap\Ldap'/*, array('searchEntries')*/);
        $this->config = [];
    }
}

<?php

namespace UnicaenAppTest\Mapper\Ldap;

use PHPUnit\Framework\Constraint_IsAnything;
use PHPUnit\Framework\Constraint_StringContains;
use UnicaenApp\Entity\Ldap\Group as EntityGroup;
use UnicaenApp\Entity\Ldap\Structure as EntityStructure;
use UnicaenApp\Exception;
use UnicaenApp\Mapper\Ldap\People;
use UnicaenApp\Mapper\Ldap\Structure as MapperStructure;
use UnicaenAppTest\Entity\Ldap\TestAsset\Group as TestAssetEntityGroup;
use UnicaenAppTest\Entity\Ldap\TestAsset\Structure as TestAssetEntityStructure;
use UnicaenAppTest\Mapper\Ldap\TestAsset\People as TestAssetPeople;
use UnicaenAppTest\Mapper\Ldap\TestAsset\Structure as TestAssetStructure;

/**
 * Classe de test du mapper LDAP des individus.
 *
 * @property People $mapper
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class PeopleTest extends CommonTest
{
    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->entityClassName = '\UnicaenApp\Entity\Ldap\People';
        $this->mapper = new People($this->ldap);
    }

    public function testGettingStructureMapperReturnsDefaultOne()
    {
        $mapper = new People($this->ldap);
        $structureMapper = $mapper->getMapperStructure();
        $this->assertInstanceOf('UnicaenApp\Mapper\Ldap\Structure', $structureMapper);
        $this->assertSame($mapper->getLdap(), $structureMapper->getLdap());
        $this->assertSame($mapper->getConfig(), $structureMapper->getConfig());
    }

    public function testSettingStructureMapperPropagatesLdapObjectToIt()
    {
        $this->mapper->setMapperStructure(new MapperStructure($this->ldap));
        $this->assertSame($this->mapper->getLdap(), $this->mapper->getMapperStructure()->getLdap());
        $this->assertSame($this->mapper->getConfig(), $this->mapper->getMapperStructure()->getConfig());
    }

    public function testFindOneByUidReturnsEntity()
    {
        $this->mapper->setConfig([
            'filters' => [
                'UID_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1]));

        $uid = 'peu-importe';
        $result = $this->mapper->findOneByUid($uid);
        $this->assertInstanceOf($this->entityClassName, $result);
        $this->assertEquals(TestAssetPeople::$data1['dn'][0], $result->getDn());
        $this->assertFalse($result->estDesactive());
    }

    public function testFindOneByUidReturnsNullWhenNoEntryFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'UID_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $uid = 'unexisting';
        $result = $this->mapper->findOneByUid($uid);
        $this->assertNull($result);
    }

    public function testFindOneByUidTriesDeactivatedBranchAndReturnsEntity()
    {
        $this->mapper->setConfig([
            'filters' => [
                'UID_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], [TestAssetPeople::$dataDeactivated]));

        $uid = 'peu-importe';
        $result = $this->mapper->findOneByUid($uid, true);
        $this->assertInstanceOf($this->entityClassName, $result);
        $this->assertTrue($result->estDesactive());
    }

    public function testFindOneByUidTriesDeactivatedBranchAndReturnsNullWhenNoEntryFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'UID_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], []));

        $uid = 'unexisting';
        $result = $this->mapper->findOneByUid($uid, true);
        $this->assertNull($result);
    }

    public function testFindByNoIndividuReturnsEntity()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NO_INDIVIDU_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1]));

        $noIndividu = '390';
        $result = $this->mapper->findOneByNoIndividu($noIndividu);
        $this->assertInstanceOf($this->entityClassName, $result);
        $this->assertFalse($result->estDesactive());
    }

    public function testFindByNoIndividuTriesDeactivatedBranchAndReturnsEntity()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NO_INDIVIDU_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], [TestAssetPeople::$dataDeactivated]));

        $noIndividu = '390';
        $result = $this->mapper->findOneByNoIndividu($noIndividu, true);
        $this->assertInstanceOf($this->entityClassName, $result);
        $this->assertTrue($result->estDesactive());
    }

    public function testFindByNoIndividuReturnsNullWhenNoEntryFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NO_INDIVIDU_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $noIndividu = 'unexisting';
        $result = $this->mapper->findOneByNoIndividu($noIndividu);
        $this->assertNull($result);
    }

    public function testFindByNoIndividuTriesDeactivatedBranchAndReturnsNullWhenNoEntryFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NO_INDIVIDU_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], []));

        $noIndividu = 'unexisting';
        $result = $this->mapper->findOneByNoIndividu($noIndividu, true);
        $this->assertNull($result);
    }

    public function testFindByUsernameReturnsEntity()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1]));

        $username = 'login';
        $result = $this->mapper->findOneByUsername($username);
        $this->assertInstanceOf($this->entityClassName, $result);
        $this->assertFalse($result->estDesactive());
    }

    public function testFindByUsernameTriesDeactivatedBranchAndReturnsEntity()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], [TestAssetPeople::$dataDeactivated]));

        $username = 'login';
        $result = $this->mapper->findOneByUsername($username, true);
        $this->assertInstanceOf($this->entityClassName, $result);
        $this->assertTrue($result->estDesactive());
    }

    public function testFindByUsernameReturnsNullWhenNoEntryFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $username = 'unexisting';
        $result = $this->mapper->findOneByUsername($username);
        $this->assertNull($result);
    }

    public function testFindByUsernameTriesDeactivatedBranchAndReturnsNullWhenNoEntryFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], []));

        $username = 'unexisting';
        $result = $this->mapper->findOneByUsername($username);
        $this->assertNull($result);
    }

    public function testFindByNameReturnsEntitiesArray()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NAME_FILTER'            => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $name = 'Nom*';
        $result = $this->mapper->findAllByName($name);
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindByNameTriesDeactivatedBranchAndReturnsEntitiesArray()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NAME_FILTER'            => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([TestAssetPeople::$data1, TestAssetPeople::$data2], [TestAssetPeople::$dataDeactivated]));

        $name = 'Nom*';
        $result = $this->mapper->findAllByName($name, null, null, true);
        $this->assertIsArray($result);
        $this->assertCount(3, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindByNameReturnsEntitiesArrayUsingSpecifiedKey()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NAME_FILTER'            => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $name = 'Nom*';
        $result = $this->mapper->findAllByName($name, 'mail');
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertArrayHasKey(TestAssetPeople::$data1['mail'][0], $result);
        $this->assertArrayHasKey(TestAssetPeople::$data2['mail'][0], $result);
    }

    public function testFindByNameAppendsSpecifiedFilter()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NAME_FILTER' => 'xxx',
//                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $name = 'Nom*';
        $filterUtilisateur = '(sexe=M)';

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->with(new Constraint_StringContains($filterUtilisateur) /* peu importe les arguments suivants */)
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $this->mapper->findAllByName($name, null, $filterUtilisateur);
    }

    public function testFindByNameOrUsernameReturnsEntitiesArray()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_OR_NAME_FILTER'   => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $name = 'gauthier';
        $result = $this->mapper->findAllByNameOrUsername($name);
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindByNameOrUsernameTriesDeactivatedBranchAndReturnsEntitiesArray()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_OR_NAME_FILTER'   => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([TestAssetPeople::$data1, TestAssetPeople::$data2], [TestAssetPeople::$dataDeactivated]));

        $name = 'gauthier';
        $result = $this->mapper->findAllByNameOrUsername($name, null, null, true);
        $this->assertIsArray($result);
        $this->assertCount(3, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindByNameOrUsernameReturnsEntitiesArrayUsingSpecifiedKey()
    {
        $this->mapper->setConfig([
            'filters' => [
                'NAME_FILTER'            => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $name = 'gauthier';
        $result = $this->mapper->findAllByName($name, 'mail');
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertArrayHasKey(TestAssetPeople::$data1['mail'][0], $result);
        $this->assertArrayHasKey(TestAssetPeople::$data2['mail'][0], $result);
    }

    public function testFindByNameOrUsernameAppendsSpecifiedFilter()
    {
        $this->mapper->setConfig([
            'filters' => [
                'LOGIN_OR_NAME_FILTER'   => 'xxx',
                'UTILISATEUR_STD_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
            ],
        ]);

        $name = 'gauthier';
        $filterUtilisateur = '(sexe=M)';

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->with(new Constraint_StringContains($filterUtilisateur) /* peu importe les arguments suivants */)
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $this->mapper->findAllByNameOrUsername($name, null, '(sexe=M)');
    }

    public function provideStructureCode()
    {
        return [
            'format-harpege' => ['C68'],
            'format-supann'  => ['HS_C68'],
        ];
    }

    /**
     * @dataProvider provideStructureCode
     * @param string $code
     */
    public function testFindAllByAffectationSpecifiedAsStringReturnsEntitiesArray($code)
    {
        $this->mapper->setConfig([
            'filters' => [
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
                'AFFECTATION_CSTRUCT_FILTER'   => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([TestAssetStructure::$data1], [TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $result = $this->mapper->findAllByAffectation($code);
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    /**
     * @expectedException Exception
     */
    public function testFindAllByAffectationSpecifiedAsStringThrowsExceptionWhenStructureNotFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
                'AFFECTATION_CSTRUCT_FILTER'   => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->exactly(2))
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $code = 'XXXXX';
        $result = $this->mapper->findAllByAffectation($code);
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindAllByAffectationSpecifiedAsEntityReturnsEntitiesArray()
    {
        $this->mapper->setConfig([
            'filters' => [
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
                'AFFECTATION_CSTRUCT_FILTER'   => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1]));

        $object = new EntityStructure(TestAssetEntityStructure::$data1);
        $result = $this->mapper->findAllByAffectation($object);
        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindAllByAffectationSpecifiedAsEntityTriesDeactivatedBranchAndReturnsEntitiesArray()
    {
        $this->mapper->setConfig([
            'filters' => [
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
                'AFFECTATION_CSTRUCT_FILTER'   => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN'            => 'xxx',
                'UTILISATEURS_DESACTIVES_BASE_DN' => 'xxx',
                'STRUCTURES_BASE_DN'              => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->any())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([TestAssetPeople::$data1, TestAssetPeople::$data2], [TestAssetPeople::$dataDeactivated]));

        $object = new EntityStructure(TestAssetEntityStructure::$data1);
        $result = $this->mapper->findAllByAffectation($object, null, [], true);
        $this->assertIsArray($result);
        $this->assertCount(3, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
    }

    public function testFindAllByAffectationReturnsEntitiesArrayUsingSpecifiedKey()
    {
        $this->mapper->setConfig([
            'filters' => [
                'AFFECTATION_CSTRUCT_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $object = new EntityStructure(TestAssetEntityStructure::$data1);
        $result = $this->mapper->findAllByAffectation($object, 'mail');
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertArrayHasKey(TestAssetPeople::$data1['mail'][0], $result);
        $this->assertArrayHasKey(TestAssetPeople::$data2['mail'][0], $result);
    }

    public function testFindAllByAffectationCompletesAttributesList()
    {
        $this->mapper->setConfig([
            'filters' => [
                'AFFECTATION_CSTRUCT_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $anything = new Constraint_IsAnything();
        $expectedAttributes = ['dn', 'mail', 'cn'];
        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->with($anything, $anything, $anything, $expectedAttributes /* peu importe les arguments suivants */)
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $object = new EntityStructure(TestAssetEntityStructure::$data1);
        $attributes = ['dn'];
        $this->mapper->findAllByAffectation($object, 'mail', $attributes);
    }

    public function provideGroup()
    {
        return [
            'string-group' => ['cn=support_info,ou=groups,dc=unicaen,dc=fr'],
            'entity-group' => [new EntityGroup(TestAssetEntityGroup::$data1)],
        ];
    }

    /**
     * @dataProvider provideGroup
     * @param string|EntityGroup $group
     */
    public function testFindAllByMembershipReturnsEntitiesArray($group)
    {
        $this->mapper->setConfig([
            'filters' => [
                'MEMBERSHIP_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $result = $this->mapper->findAllByMembership($group);
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertContainsOnly('string', array_keys($result));
    }

    /**
     * @dataProvider provideGroup
     * @param string|EntityGroup $group
     */
    public function testFindAllByMembershipReturnsEmptyArrayWhenNoEntryFound($group)
    {
        $this->mapper->setConfig([
            'filters' => [
                'MEMBERSHIP_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $result = $this->mapper->findAllByMembership($group);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function provideGroupAndStructure()
    {
        $stringGroup = 'cn=support_info,ou=groups,dc=unicaen,dc=fr';
        $stringStructure = 'supannCodeEntite=HS_C68,ou=structures,dc=unicaen,dc=fr';
        $entityGroup = new EntityGroup(TestAssetEntityGroup::$data1);
        $entityStructure = new EntityStructure(TestAssetEntityStructure::$data1);

        return [
            'string-group_string-structure' => [$stringGroup, $stringStructure],
            'string-group_entity-structure' => [$stringGroup, $entityStructure],
            'entity-group_string-structure' => [$entityGroup, $stringStructure],
            'entity-group_entity-structure' => [$entityGroup, $entityStructure],
        ];
    }

    /**
     * @dataProvider provideGroupAndStructure
     * @param string|EntityGroup     $group
     * @param string|EntityStructure $structure
     */
    public function testFindAllByMembershipForStructureReturnsEntitiesArray($group, $structure)
    {
        $this->mapper->setConfig([
            'filters' => [
                'MEMBERSHIP_FILTER'           => 'xxx',
                'FILTER_STRUCTURE_DN'         => 'xxx',
                'AFFECTATION_ORG_UNIT_FILTER' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        if (is_object($structure)) {
            $this->ldap
                ->expects($this->once())
                ->method('searchEntries')
                ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));
        } else {
            $this->ldap
                ->expects($this->exactly(2))
                ->method('searchEntries')
                ->will($this->onConsecutiveCalls([TestAssetStructure::$data1], [TestAssetPeople::$data1, TestAssetPeople::$data2]));
        }

        $result = $this->mapper->findAllByMembership($group, $structure);
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertContainsOnly('string', array_keys($result));
    }

    /**
     * @expectedException \Exception
     */
    public function testFindAllByMembershipForStructureThrowsExceptionWhenStructureNotFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'MEMBERSHIP_FILTER'           => 'xxx',
                'FILTER_STRUCTURE_DN'         => 'xxx',
                'AFFECTATION_ORG_UNIT_FILTER' => 'xxx',
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'xxx',
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $group = 'peu-importe';
        $structure = 'peu-importe';
        $this->mapper->findAllByMembership($group, $structure);
    }

    public function testFindAllByMembershipForStructureAppendsAffectationFilter()
    {
        $this->config = [
            'filters' => [
                'MEMBERSHIP_FILTER'           => '(peu importe)',
                'AFFECTATION_ORG_UNIT_FILTER' => '(filter)',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $group = new EntityGroup(TestAssetEntityGroup::$data1);
        $structure = new EntityStructure(TestAssetEntityStructure::$data1);
        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->with('(&(peu importe)(filter))')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $this->mapper->findAllByMembership($group, $structure);
    }

    public function testFindAllByMembershipForStructureRecursivelyClimbsUpHierarchy()
    {
        $this->config = [
            'filters' => [
                'MEMBERSHIP_FILTER'            => '(peu importe)',
                'AFFECTATION_ORG_UNIT_FILTER'  => '(peu importe)',
                'FILTER_STRUCTURE_CODE_ENTITE' => '(peu importe)',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'peu importe',
                'STRUCTURES_BASE_DN'   => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $group = new EntityGroup(TestAssetEntityGroup::$data1);
        $structure = 'peu-importe'; // structure de niveau 2
        $this->ldap
            ->expects($this->exactly(4))
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls(
                [TestAssetStructure::$data2], // résultat de la recherche de la structure de niveau 2
                [], // aucun membre trouvé affecté à la structure de niveau 2
                [TestAssetStructure::$data1], // résultat de la recherche de la structure mère (niveau 1)
                [TestAssetPeople::$data1, TestAssetPeople::$data2])); // 2 membres trouvés affectés à la structure mère

        $result = $this->mapper->findAllByMembership($group, $structure, true);
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
    }

    public function testFindAllByRoleReturnsEntitiesArray()
    {
        $this->config = [
            'filters' => [
                'ROLE_FILTER' => '(peu importe)',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $role = 'T84';
        $result = $this->mapper->findAllByRole($role);
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertContainsOnly('string', array_keys($result));
    }

    public function testFindAllByRoleReturnsEmptyArrayWhenNoEntryFound()
    {
        $this->config = [
            'filters' => [
                'ROLE_FILTER' => '(peu importe)',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $role = 'T84';
        $result = $this->mapper->findAllByRole($role);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function provideStructure()
    {
        return [
            'string-structure'  => ['supannCodeEntite=HS_C68,ou=structures,dc=unicaen,dc=fr'],
            'entity-structure'  => [new EntityStructure(TestAssetEntityStructure::$data1)],
            'string-structures' => ['supannCodeEntite=HS_C68,ou=structures,dc=unicaen,dc=fr', 'supannCodeEntite=HS_G72,ou=structures,dc=unicaen,dc=fr'],
            'entity-structures' => [new EntityStructure(TestAssetEntityStructure::$data1), new EntityStructure(TestAssetEntityStructure::$data2)],
        ];
    }

    /**
     * @dataProvider provideStructure
     * @param string|EntityStructure $structure
     */
    public function testFindAllByRoleForStructureReturnsEntitiesArray($structure)
    {
        $this->config = [
            'filters' => [
                'FILTER_STRUCTURE_DN' => '(peu importe)',
                'ROLE_FILTER'         => '(peu importe)',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'peu importe',
                'STRUCTURES_BASE_DN'   => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        if (is_object($structure)) {
            $this->ldap
                ->expects($this->once())
                ->method('searchEntries')
                ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));
        } else {
            $this->ldap
                ->expects($this->exactly(2))
                ->method('searchEntries')
                ->will($this->onConsecutiveCalls([TestAssetStructure::$data1], [TestAssetPeople::$data1, TestAssetPeople::$data2]));
        }

        $role = 'T84';
        $result = $this->mapper->findAllByRole($role, $structure);
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertContainsOnly('string', array_keys($result));
    }

    /**
     * @expectedException \Exception
     */
    public function testFindAllByRoleForStructureThrowsExceptionWhenStructureNotFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
            ],
            'dn'      => [
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls([], [TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $role = 'peu-importe';
        $structure = 'peu-importe';
        $this->mapper->findAllByRole($role, $structure);
    }

    public function testFindAllByRoleForStructureAppendsAffectationFilter()
    {
        $this->config = [
            'filters' => [
                'ROLE_FILTER' => '(role filter)',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'ou=people,dc=unicaen,dc=fr',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $role = 'T84';
        $structure = new EntityStructure(TestAssetEntityStructure::$data1);
        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->with('(role filter)')
            ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));

        $this->mapper->findAllByRole($role, $structure);
    }

    public function testFindAllByRoleForStructureRecursivelyClimbsUpHierarchy()
    {
        $this->config = [
            'filters' => [
                'ROLE_FILTER'                  => 'peu importe',
                'FILTER_STRUCTURE_CODE_ENTITE' => 'peu importe',
            ],
            'dn'      => [
                'UTILISATEURS_BASE_DN' => 'peu importe',
                'STRUCTURES_BASE_DN'   => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $structure = 'peu-importe'; // structure de niveau 2
        $this->ldap
            ->expects($this->exactly(4))
            ->method('searchEntries')
            ->will($this->onConsecutiveCalls(
                [TestAssetStructure::$data2], // résultat de la recherche de la structure de niveau 2
                [], // aucun membre trouvé affecté à la structure de niveau 2
                [TestAssetStructure::$data1], // résultat de la recherche de la structure mère (niveau 1)
                [TestAssetPeople::$data1, TestAssetPeople::$data2])); // 2 membres trouvés affectés à la structure mère

        $role = 'T84';
        $result = $this->mapper->findAllByRole($role, $structure, true);
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
    }

    /**
     * @dataProvider provideStructure
     * @param string|EntityStructure $structure
     */
    public function testFindAllTeachersByStructureReturnsEntitiesArray($structure)
    {
        $this->config = [
            'filters' => [
                'FILTER_STRUCTURE_DN' => 'peu importe',
                'PROF_STRUCTURE'      => 'peu importe',
            ],
            'dn'      => [
                'STRUCTURES_BASE_DN'   => 'peu importe',
                'UTILISATEURS_BASE_DN' => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        if (is_object($structure)) {
            $this->ldap
                ->expects($this->once())
                ->method('searchEntries')
                ->will($this->returnValue([TestAssetPeople::$data1, TestAssetPeople::$data2]));
        } else {
            $this->ldap
                ->expects($this->exactly(2))
                ->method('searchEntries')
                ->will($this->onConsecutiveCalls([TestAssetStructure::$data1], [TestAssetPeople::$data1, TestAssetPeople::$data2]));
        }

        $result = $this->mapper->findAllTeachersByStructure($structure);
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertContainsOnlyInstancesOf($this->entityClassName, $result);
        $this->assertContainsOnly('string', array_keys($result));
    }

    public function testFindAllTeachersByStructureReturnsEmptyArrayWhenNoEntryFound()
    {
        $this->config = [
            'filters' => [
                'FILTER_STRUCTURE_DN' => 'peu importe',
                'PROF_STRUCTURE'      => 'peu importe',
            ],
            'dn'      => [
                'STRUCTURES_BASE_DN'   => 'peu importe',
                'UTILISATEURS_BASE_DN' => 'peu importe',
            ],
        ];
        $this->mapper->setConfig($this->config);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $structure = new EntityStructure(TestAssetEntityStructure::$data1);
        $result = $this->mapper->findAllTeachersByStructure($structure);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    /**
     * @expectedException \Exception
     */
    public function testFindAllTeachersByStructureThrowsExceptionWhenStructureNotFound()
    {
        $this->mapper->setConfig([
            'filters' => [
                'FILTER_STRUCTURE_CODE_ENTITE' => 'xxx',
            ],
            'dn'      => [
                'STRUCTURES_BASE_DN'   => 'xxx',
            ],
        ]);

        $this->ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->will($this->returnValue([]));

        $structure = 'peu-importe';
        $this->mapper->findAllTeachersByStructure($structure);
    }

    /**
     * @dataProvider provideStructure
     * @param string|EntityStructure $structure
     */
    public function testCreatingFilterForAffectation($structure)
    {
        $this->config = [
            'filters' => [
                'AFFECTATION_CSTRUCT_FILTER' => 'xxxx',
            ],
        ];

        $peopleMapper = new People(null, $this->config);
        $filter = $peopleMapper->createFilterForAffectation($structure);
        $this->assertIsString($filter);
        $this->assertNotEmpty($filter);
    }
}

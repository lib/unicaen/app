<?php
namespace UnicaenAppTest\Form\TestAsset;

use UnicaenApp\Form\MultipageFormFieldsetInterface;
use Laminas\Form\Element\MultiCheckbox;
use Laminas\Form\Element\Text;
use Laminas\Form\Fieldset;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 * 
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class IdentiteFieldset extends Fieldset implements InputFilterProviderInterface, MultipageFormFieldsetInterface
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
 
        $this->setLabel("Vous")
             ->add(new Text('nom', array('label' => "Nom")))
             ->add(new Text('prenom', array('label' => "Prénom")))
             ->add(new MultiCheckbox('civ', array('label'=>"Civilité", 'value_options' => array('Melle'=>'Melle', 'Mme'=>'Mme', 'M'=>'M'))));
    }
 
    public function getInputFilterSpecification()
    {
        return array(
            'nom' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name'=> 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array('isEmpty' => "Vous devez renseigner votre nom"),
                        )
                    ),
                ),
            ),
        );
    }
    
    /**
     * Retourne les labels ainsi que les valeurs des éléments de ce fieldset.
     *
     * @return array 'element_name' => array('label' => Label de l'élément, 'value' => Valeur saisie au format texte)
     */
    public function getLabelsAndValues()
    {
        $result = array();
        $result['nom'] = array(
            'label' => "Nom",
            'value' => $this->get('nom')->getValue() ?: "Non spécifié"
        );
        $result['prenom'] = array(
            'label' => "Prénom",
            'value' => $this->get('prenom')->getValue() ?: "Non spécifié"
        );
        $result['civ'] = array(
            'label' => "Civilité",
            'value' => $this->get('civ')->getValue() ?: "???"
        );
        return $result;
    }
}

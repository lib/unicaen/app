<?php
namespace UnicaenAppTest\Form\TestAsset;

use Laminas\Form\Element\Text;
use Laminas\Form\Fieldset;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 * 
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AdresseFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
 
        $this->setLabel("Votre adresse")
             ->add(new Text('email', array('label'=>"Email")));
    }
 
    public function getInputFilterSpecification()
    {
        return array(
            'email' => array(
                'required' => true,
                'filters'  => array(
                    array('name' => '\Laminas\Filter\StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'=> 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array('isEmpty' => "Vous devez renseigner votre adresse mail"),
                        )
                    ),
                    array(
                        'name' => '\Laminas\Validator\EmailAddress',
                        'options' => array(
                            'messages' => array('emailAddressInvalidFormat' => "L'adresse mail spécifiée est invalide"),
                        ),
                    ),
                ),
            ),
        );
    }
}

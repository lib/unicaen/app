<?php
namespace UnicaenAppTest\Form\View\Helper;

/**
 * Description of AbstractTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
abstract class AbstractTest extends \UnicaenAppTest\View\Helper\AbstractTest
{
    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();
        
        $this->files = __DIR__ . '/_files';
    }
}
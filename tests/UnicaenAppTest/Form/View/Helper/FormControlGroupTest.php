<?php
namespace UnicaenAppTest\Form\View\Helper;

use UnicaenApp\Form\View\Helper\FormControlGroup;
use UnicaenApp\Form\Element\DateInfSup;
use UnicaenApp\Form\Element\SearchAndSelect;
use Laminas\Form\Element\Text;

/**
 * Description of FormControlGroupTest
 *
 * @property FormControlGroup $helper Description
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class FormControlGroupTest extends AbstractTest
{
    protected $helperClass = 'UnicaenApp\Form\View\Helper\FormControlGroup';
    protected $delegateHelperClass;
    protected $renderer;

    public function setUp()
    {
        parent::setUp();
        
        $helpers = array(
            'formDateInfSup'      => $formDateInfSup      = $this->createMock('UnicaenApp\Form\View\Helper\FormDateInfSup'/*, array('__invoke')*/),
            'formSearchAndSelect' => $formSearchAndSelect = $this->createMock('UnicaenApp\Form\View\Helper\FormSearchAndSelect'/*, array('__invoke')*/),
            'formElement'         => $formElement         = $this->createMock('Laminas\Form\View\Helper\FormElement'/*, array('__invoke')*/),
            'formLabel'           => $formLabel           = $this->createMock('Laminas\Form\View\Helper\FormLabel'/*, array('__invoke')*/),
            'formElementErrors'   => $formElementErrors   = $this->createMock('Laminas\Form\View\Helper\FormElementErrors'/*, array('__invoke')*/),
        );
        foreach ($helpers as $name => $helper) {
            $helper
                    ->expects($this->any())
                    ->method('__invoke')
                    ->will($this->returnValue("$name markup"));
        }
        
        $renderer = $this->createMock('Laminas\View\Renderer\PhpRenderer'/*, array('plugin')*/);
        $map = array(
            array('formDateInfSup', null, $formDateInfSup),
            array('formSearchAndSelect',  null, $formSearchAndSelect),
            array('formElement',  null, $formElement),
            array('formLabel',  null, $formLabel),
            array('formElementErrors',  null, $formElementErrors),
        );
        $renderer
                ->expects($this->any())
                ->method('plugin')
                ->will($this->returnValueMap($map));
        
        $this->helper->setView($renderer);
    }
    
    public function testRenderingEntryPoints()
    {
        $element = new Text('text');
        $markup = $this->helper->render($element);
        $this->assertEquals($markup, $this->helper->__invoke($element));
    }
    
    public function testCanDelegateFullRenderingToDateInfSupHelper()
    {
        $element = new DateInfSup('dates');
        $expected = <<<'EOD'
formDateInfSup markup
EOD;
        $this->assertEquals($expected, $this->helper->render($element));
    }
    
    public function testCanDelegatePartialRenderingToSearchAndSelectHelper()
    {
        $element = new SearchAndSelect('person');
        $expected = <<<'EOD'
<div class="form-group">formSearchAndSelect markup</div>
EOD;
        $this->assertEquals($expected, $this->helper->render($element));
    }
    
    public function testCanRenderDefault()
    {
        $element = new Text('text');
        $this->assertTrue($this->helper->getIncludeLabel());
        $this->assertFalse($this->helper->getAddClearButton());
        $expected = <<<'EOD'
<div class="form-group">formElement markup</div>
EOD;
        $this->assertEquals($expected, $this->helper->render($element));
    }
    
    public function testCanRenderWithoutLabel()
    {
        $this->helper->setIncludeLabel(false);
        $element = new Text('text');
        $this->assertFalse($this->helper->getIncludeLabel());
        $expected = <<<'EOD'
<div class="form-group">formElement markup</div>
EOD;
        $this->assertEquals($expected, $this->helper->render($element));
    }
    
    public function testCanRenderWithClearButton()
    {
        $this->helper->setAddClearButton(true);
        $element = new Text('text');
        $this->assertTrue($this->helper->getAddClearButton());
        $expected = <<<'EOD'
<div class="form-group input-group"><div class="input-group">formElement markup<span class="input-group-btn"><button class="btn btn-secondary btn-sm" type="button" title="Vider" onclick="$(this).siblings(':input').val(null).focus();"><span class="icon iconly icon-remove"></span></button></span></div><span class="input-group-btn"><button class="btn btn-secondary btn-sm" type="button" title="Vider" onclick="$(this).siblings(':input').val(null).focus();"><span class="icon icon-supprimer"></span></button></span></div>
EOD;
        $this->assertEquals($expected, $this->helper->render($element));
    }
    
    public function testCanRenderErrors()
    {
        $element = new Text('text');
        $element->setMessages(array("Erroooor!"));
        $expected = <<<'EOD'
<div class="form-group has-error">formElement markupformElementErrors markup</div>
EOD;
        $this->assertEquals($expected, $this->helper->render($element));
    }
}
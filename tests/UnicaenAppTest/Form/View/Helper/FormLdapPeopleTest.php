<?php
namespace UnicaenAppTest\Form\View\Helper;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Form\Element\LdapPeople;
use UnicaenApp\Form\View\Helper\FormLdapPeople;
use Laminas\Form\Form;
use Laminas\Form\View\HelperConfig;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Description of FormLdapPeople
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class FormLdapPeopleTest extends TestCase
{
    protected $helper;
    protected $renderer;

    public function setUp()
    {
        $this->helper = new FormLdapPeople();

        $this->renderer = new PhpRenderer;
        $helpers = $this->renderer->getHelperPluginManager();
        $config = new HelperConfig();
        $config->configureServiceManager($helpers);

        $this->helper->setView($this->renderer);
    }
    
    public function testCanSetAutocompleteSource()
    {
        $autocompleteSource = '/autocomplete/source';
        $this->helper->setAutocompleteSource($autocompleteSource);
        $this->assertEquals($autocompleteSource, $this->helper->getAutocompleteSource());
    }
    
    public function testCanSetAutocompleteMinLength()
    {
        $autocompleteMinLength = 2;
        $this->helper->setAutocompleteMinLength($autocompleteMinLength);
        $this->assertEquals($autocompleteMinLength, $this->helper->getAutocompleteMinLength());
    }
    
    public function testCanSetSpinnerSource()
    {
        $spinnerSource = '/spinner/source.gif';
        $this->helper->setSpinnerSource($spinnerSource);
        $this->assertEquals($spinnerSource, $this->helper->getSpinnerSource());
    }
    
    public function testInvokingWithNoArgReturnsPluginItSelf()
    {
        $helper = $this->helper;
        $this->assertSame($helper, $helper());
    }
    
    /**
     * @expectedException Laminas\Form\Exception\InvalidElementException
     */
    public function testInvokingWithInvalidElementThrowsException()
    {
        $helper = $this->helper;
        $form = new Form('form');
        $helper($form);
    }
    
    public function testInvokingWithElementReturnsString()
    {
        $helper = $this->helper;
        $form = new LdapPeople('form');
        $markup = $helper($form);
        $this->assertIsString($markup);
    }
    
    /**
     * @expectedException Laminas\Form\Exception\InvalidElementException
     */
    public function testRenderingInvalidElementThrowsException()
    {
        $form = new Form('form');
        $this->helper->render($form);
    }
    
    public function testRenderingElementWithoutDomIdCreatesOne()
    {
        $elem = new LdapPeople('elem');
        $this->helper->render($elem);
        $this->assertNotNull($elem->getAttribute('id'));
    }
    
    public function testRenderingTransformsElementNameIntoAnArrayWithIdKey()
    {
        $elem = new LdapPeople('elem');
        $name = $elem->getName();
        $this->helper->render($elem);
        $this->assertEquals($name . "[id]", $elem->getName());
    }
    
    public function testRenderingGeneratesAutocompleteInput()
    {
        $elem = new LdapPeople('elem');
        $elem->setAttribute('id', 'test-id');
        $markup = $this->helper->render($elem);
        $expected = <<<'EOD'
<input name="elem&#x5B;id&#x5D;" id="test-id" class="sas" type="text" value=""><input type="text" name="elem&#x5B;label&#x5D;" id="test-id-autocomplete" class="form-control&#x20;form-control-sm" value=""><style>
    ul.ui-autocomplete {
        z-index: 5000
    }
    .ui-autocomplete-loading {
        background: white url("//gest.unicaen.fr/images/ajax-loader-r.gif") right center no-repeat;
    }
</style>
<script>// cache l'élément contenant l'id de la sélection
$("#test-id").css('display', 'none');

$(function() {
    // jQuery UI autocomplete
    var autocomp = $("#test-id-autocomplete");
    autocomp.autocompleteUnicaen({ // autocompleteUnicaen() définie dans "public/js/util.js"
        elementDomId: 'test-id',
        source: '',
        minLength: 2,
        delay: 750
    });
});</script>

EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRendersJavascript()
    {
        $elem = new LdapPeople('elem');
        $elem->setAttribute('id', 'test-id');
        $markup = $this->helper->render($elem);
        $this->assertContains('<script', $markup);
        $this->assertContains('</script>', $markup);
    }
    
    /**
     * @expectedException \UnicaenApp\Exception\LogicException
     */
    public function testGettingJavascriptThrowsExceptionIfNoElementSet()
    {
        $this->helper->getJavascript();
    }
}
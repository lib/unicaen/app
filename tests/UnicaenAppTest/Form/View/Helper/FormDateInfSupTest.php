<?php
namespace UnicaenAppTest\Form\View\Helper;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Form\Element\DateInfSup;
use UnicaenApp\Form\View\Helper\FormDateInfSup;
use Laminas\Form\View\HelperConfig;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Description of FormDateInfSupTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class FormDateInfSupTest extends TestCase
{
    protected $helper;
    protected $renderer;

    public function setUp()
    {
        $this->helper = new FormDateInfSup();

        $this->renderer = new PhpRenderer;
        $helpers = $this->renderer->getHelperPluginManager();
        $config = new HelperConfig();
        $config->configureServiceManager($helpers);

        $helpers->get('basePath')->setBasePath('/peu-importe');

        $this->helper->setView($this->renderer);
    }
    
    public function testInvokingWithNoArgReturnsPluginItSelf()
    {
        $helper = $this->helper;
        $this->assertSame($helper, $helper());
    }
    
    public function testInvokingWithElementReturnsString()
    {
        $helper = $this->helper;
        $element = new DateInfSup('elem');
        $markup = $helper($element);
        $this->assertIsString($markup);
    }
    
    public function testElementDateSupOptionsArePropagatedToHelper()
    {
        $helper = $this->helper;
        $element = new DateInfSup('elem');
        
        foreach (array(true, false) as $value) {
            $element->setDateSupActivated($value)
                    ->getInputFilter()->setDateSupRequired($value);
            $helper($element);
            $this->assertEquals(
                    $element->getDateSupActivated(), 
                    $this->readAttribute($helper, 'dateSupActivated'));
            $this->assertEquals(
                    $element->getInputFilter()->getDateSupRequired(), 
                    $this->readAttribute($helper, 'dateSupRequired')); 
        }
    }
    
    /**
     * @expectedException \UnicaenApp\Exception\LogicException
     */
    public function testGettingJavascriptThrowsExceptionIfNoElementSet()
    {
        $this->helper->getJavascript();
    }
    
    public function testRenderingJavascript()
    {
        $element = new DateInfSup('elem');
        
        // render JS
        $this->helper->setRenderJs(true);
        $markup = $this->helper->render($element);
        $this->assertContains($this->helper->getJavascript(), $markup);
        
        // do NOT render JS
        $this->helper->setRenderJs(false);
        $headScriptHelper = $this->createMock('Laminas\View\Helper\InlineScript'/*, array('__call')*/);
        $this->renderer->getHelperPluginManager()->setAllowOverride(true);
        $this->renderer->getHelperPluginManager()->setService('inlineScript', $headScriptHelper);
        $headScriptHelper
            ->expects($this->atLeastOnce())
            ->method('__call')
            ->with('appendScript');
        $this->helper->render($element);
    }
    
    public function testRenderingGeneratesDivForEachDate()
    {
        $element = new DateInfSup('elem');
        
        $this->helper->setRenderJs(false);
        
        $element->setDateSupActivated(false);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<div class="input-dateinf .*">.+</div>#s', $markup);
        // NB: avec l'option "s", le métacaractère point (.) remplace n'importe quel caractère, y compris les nouvelles lignes
        
        $element->setDateSupActivated(true);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<div class="input-dateinf .*">.+</div>\s*<div class="input-datesup .*">.+</div>#s', $markup);
        // NB: avec l'option "s", le métacaractère point (.) remplace n'importe quel caractère, y compris les nouvelles lignes
    }
    
    public function testRenderingElementWithErrorsGeneratesDivWithErrorClass()
    {
        $element = new DateInfSup('elem');
        $element->setMessages(array('inf' => 'Erreur 1', 'sup' => 'Erreur 2'));
        
        $this->helper->setRenderJs(false);
        
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<div class="input-dateinf.* error">.+</div>\s*<div class="input-datesup.* error">.+</div>#s', $markup);
    }
    
    public function testRenderingElementWithErrorsAppendsErrorClassToInput()
    {
        $element = new DateInfSup('elem');
        $element->setMessages(array('inf' => 'Erreur 1', 'sup' => 'Erreur 2'));
        
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;".* class=".*input-error".*>#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;".* class=".*input-error".*>#', $markup);
    }
    
    public function testRenderingGeneratesLabelAndInputForEachDate()
    {
        $element = new DateInfSup('elem');
        $element->setDateInfLabel('Date min')->setDateInf('01/01/2013');
        
        $this->helper->setRenderJs(false);
        
        $element->setDateSupActivated(false);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<label for="dateinfsup-inf-text-[a-zA-Z0-9]+">Date min</label>#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;".* id="dateinfsup-inf-text-[a-zA-Z0-9]+" class="input-dateinf&\#x20;required\s*" value="01&\#x2F;01&\#x2F;2013">#', $markup);
        
        $element->setDateSupActivated(true)->setDateSupLabel('Date max')->setDateSup('31/12/2013');
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<label for="dateinfsup-sup-text-[a-zA-Z0-9]+">Date max</label>#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;".* id="dateinfsup-sup-text-[a-zA-Z0-9]+" class="input-datesup&\#x20;required\s*" value="31&\#x2F;12&\#x2F;2013">#', $markup);
    }
    
    public function testRenderingGeneratesTrashLinkOnlyWhenMaxDateIsNotRequired()
    {
        $element = new DateInfSup('elem');
        
        $element->getInputFilter()->setDateSupRequired(false);
        $markup = $this->helper->render($element);
        $this->assertRegexp('`<a id="dateinfsup-clear-sup-[a-zA-Z0-9]+" href="#" title="Vider">Vider</a>`', $markup);
        
        $element->getInputFilter()->setDateSupRequired(true);
        $markup = $this->helper->render($element);
        $this->assertNotRegexp('`<a id="dateinfsup-clear-sup" href="#" title="Vider">Vider</a>`', $markup);
    }
    
    public function testRenderingWithSameInputAttributesForBothDates()
    {
        $element = new DateInfSup('elem');
        $element->setDateInfLabel('Date min')->setDateInf(null)
                ->setDateSupActivated(true)->setDateSupLabel('Date max')->setDateInf(null);

        $element->setAttribute('class', 'common-input-class')
                ->setAttribute('title', 'common-input-title');
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;" title="common-input-title".* class="input-dateinf&\#x20;required&\#x20;common-input-class" value="">#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;" title="common-input-title".* class="input-datesup&\#x20;required&\#x20;common-input-class" value="">#', $markup);
    }
    
    public function testRenderingWithDifferentInputAttributesForDates()
    {
        $element = new DateInfSup('elem');
        $element->setDateInfLabel('Date min')->setDateInf(null)
                ->setDateSupActivated(true)->setDateSupLabel('Date max')->setDateInf(null);

        $element->setAttribute('class', array('min-input-class', 'max-input-class'))
                ->setAttribute('title', array('min-input-title', 'max-input-title'));
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;" title="min-input-title".* class="input-dateinf&\#x20;required&\#x20;min-input-class" value="">#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;" title="max-input-title".* class="input-datesup&\#x20;required&\#x20;max-input-class" value="">#', $markup);
    }
    
    public function testRenderingWithSameLabelAttributesForBothDates()
    {
        $element = new DateInfSup('elem');
        $element->setDateInfLabel('Date min')
                ->setDateSupActivated(true)->setDateSupLabel('Date max');

        $attributes = array(
            'title' => 'Common label title', 
            'class' => 'common-label-class'
        );
        $element->setLabelAttributes($attributes);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<label title="Common&\#x20;label&\#x20;title" class="common-label-class" for="dateinfsup-inf-text-[a-zA-Z0-9]+">Date min</label>#', $markup);
        $this->assertRegexp('#<label title="Common&\#x20;label&\#x20;title" class="common-label-class" for="dateinfsup-sup-text-[a-zA-Z0-9]+">Date max</label>#', $markup);
    }
    
    public function testRenderingWithDifferentLabelAttributesForDates()
    {
        $element = new DateInfSup('elem');
        $element->setDateInfLabel('Date min')
                ->setDateSupActivated(true)->setDateSupLabel('Date max');

        $this->helper->setRenderJs(false);
        
        $attributes = array(
            'title' => array('Min label title', 'Max label title'),
            'class' => array('min-label-class', 'max-label-class')
        );
        $element->setLabelAttributes($attributes);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<label title="Min&\#x20;label&\#x20;title" class="min-label-class" for="dateinfsup-inf-text-[a-zA-Z0-9]+">Date min</label>#', $markup);
        $this->assertRegexp('#<label title="Max&\#x20;label&\#x20;title" class="max-label-class" for="dateinfsup-sup-text-[a-zA-Z0-9]+">Date max</label>#', $markup);
    }
    
    public function testRenderingIncludingTimeOrNot()
    {
        $element = new DateInfSup('elem');
        $element->setDateInf('01/06/2013')
                ->setDateSup('21/06/2013');

        $element->getDateInf()->setTime(10, 14, 20);
        $element->getDateSup()->setTime(10, 17, 20);

        $element->setIncludeTime(true);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;".* value="01&\#x2F;06&\#x2F;2013&\#x20;&\#x20;&\#x7C;&\#x20;&\#x20;10&\#x3A;14">#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;".* value="21&\#x2F;06&\#x2F;2013&\#x20;&\#x20;&\#x7C;&\#x20;&\#x20;10&\#x3A;17">#', $markup);

        $element->setIncludeTime(false);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;".* value="01&\#x2F;06&\#x2F;2013">#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;".* value="21&\#x2F;06&\#x2F;2013">#', $markup);
    }
    
    public function testRenderingWithHelperReadonlyOptionsActivated()
    {
        $element = new DateInfSup('elem');
        
        $this->helper->setDateInfReadonly(true)
                     ->setDateSupReadonly(true);
        $markup = $this->helper->render($element);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;inf&\#x5D;".* readonly="readonly".*>#', $markup);
        $this->assertRegexp('#<input type="text" name="elem&\#x5B;sup&\#x5D;".* readonly="readonly".*>#', $markup);
    }
}
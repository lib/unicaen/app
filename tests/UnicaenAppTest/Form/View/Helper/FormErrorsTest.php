<?php
namespace UnicaenAppTest\Form\View\Helper;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Form\View\Helper\FormErrors;
use Laminas\Form\View\HelperConfig;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Description of FormErrorsTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class FormErrorsTest extends TestCase
{
    protected $helper;
    protected $renderer;

    public function setUp()
    {
        $this->helper = new FormErrors();

        $this->renderer = new PhpRenderer;
        $helpers = $this->renderer->getHelperPluginManager();
        $config = new HelperConfig();
        $config->configureServiceManager($helpers);

        $this->helper->setView($this->renderer);
    }
    
    public function testInvokingWithNoArgReturnsPluginItSelf()
    {
        $helper = $this->helper;
        $this->assertSame($helper, $helper());
    }
    
    public function testInvokingWithElementReturnsString()
    {
        $helper = $this->helper;
        $form = new \Laminas\Form\Form('form');
        $markup = $helper($form);
        $this->assertIsString($markup);
    }
    
    public function testInvokingSpecifyingMessageSetsMessageOption()
    {
        $helper = $this->helper;
        $form = new \Laminas\Form\Form('form');
        $helper($form, $message = 'Oups!');
        $this->assertEquals($this->helper->getMessage(), $message);
    }
    
    public function testRenderingWithFormWithoutErrorsReturnsEmptyString()
    {
        $helper = $this->helper;
        $form = new \Laminas\Form\Form('form');
        $markup = $helper($form);
        $this->assertEquals('', $markup);
    }
    
    public function testRenderingFormErrorsGeneratesDivContainer()
    {
        $form = new \Laminas\Form\Form('form');
        $form->add($elem = new \Laminas\Form\Element\Text('text'));
        $elem->setMessages(array('Erreur de saisie sur ce champ'));
        
        $markup = $this->helper->render($form);
        $this->assertRegexp('#<div class="alert alert-danger">.*</div>#s', $markup);
        // NB: avec l'option "s", le métacaractère point (.) remplace n'importe quel caractère, y compris les nouvelles lignes
    }
    
    public function testRenderingFormErrorsGeneratesHeaderBeforeErrorsList()
    {
        $form = new \Laminas\Form\Form('form');
        $form->add($elem = new \Laminas\Form\Element\Text('text'));
        $elem->setMessages(array('Erreur de saisie sur ce champ'));
        
        $markup = $this->helper->render($form);
        $this->assertRegexp('#<strong>Attention!</strong>\s*<ul>#', $markup);
    }
    
    public function testCanTranslate()
    {
        $mockTranslator = $this->createMock('Laminas\I18n\Translator\Translator');
        $mockTranslator->expects($this->any())
                       ->method('translate')
                       ->will($this->returnValue('translated content'));
        
        $form = new \Laminas\Form\Form('form');
        $form->add($elem = new \Laminas\Form\Element\Text('text'));
        $elem->setMessages(array('Erreur de saisie sur ce champ'));
        
        $this->helper->setTranslator($mockTranslator);
        $this->assertTrue($this->helper->hasTranslator());
        
        $this->helper->render($form);
    }
    
    public function testRenderingFormErrorsGeneratesErrorsList()
    {
        $form = new \Laminas\Form\Form('form');
        $form->add($elem1 = new \Laminas\Form\Element\Text('text'));
        $form->add($elem2 = new \Laminas\Form\Element\Checkbox('agree'));
        $elem1->setMessages(array($msg1 = 'Erreur 1', $msg2 = 'Erreur 2'));
        $elem2->setMessages(array($msg3 = 'Erreur 3'));
        
        $markup = $this->helper->render($form);
        $this->assertRegexp('#<ul>\s*<li>Erreur 1</li>\s*<li>Erreur 2</li>\s*<li>Erreur 3</li>\s*</ul>#s', $markup);
        // NB: avec l'option "s", le métacaractère point (.) remplace n'importe quel caractère, y compris les nouvelles lignes
    }
}
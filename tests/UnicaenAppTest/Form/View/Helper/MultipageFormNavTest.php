<?php

namespace UnicaenAppTest\Form\View\Helper;

use UnicaenApp\Form\Element\MultipageFormNavElement as MultipageFormNavElement;
use UnicaenApp\Form\View\Helper\MultipageFormNav;
use UnicaenAppTest\View\Helper\TestAsset\ArrayTranslatorLoader;
use Laminas\Form\View\HelperConfig;
use Laminas\I18n\Translator\Translator;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Description of MultipageFormNavTest
 *
 * @property MultipageFormNav $helper Description
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MultipageFormNavTest extends AbstractTest
{
    protected $helperClass = 'UnicaenApp\Form\View\Helper\MultipageFormNav';
    protected $renderer;
    protected $element;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        
        $this->renderer = new PhpRenderer;
        $helpers = $this->renderer->getHelperPluginManager();
        $config = new HelperConfig();
        $config->configureServiceManager($helpers);
        
        $this->helper->setView($this->renderer);
        
        $this->element = new MultipageFormNavElement('nav');
    }
    
    public function testInvokingReturnsSelf()
    {
        $this->assertSame($this->helper, $this->helper->__invoke($this->element));
    }
    
    public function testRenderingDefault()
    {
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
        
        // traduction
        $this->helper->setTranslator($this->_getTranslator());
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Go&#x20;to&#x20;next&#x20;step" class="multipage-nav&#x20;next" value="">Next &gt;</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Cancel&#x20;it&#x20;all" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Cancel</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesPreviousButton()
    {
        $this->element->setPreviousEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<button type="submit" name="nav&#x5B;_previous&#x5D;" title="Revenir&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;pr&#xE9;c&#xE9;dente" class="multipage-nav&#x20;previous" style="float&#x3A;&#x20;left&#x3B;" value="">&lt; Précédent</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesNextButton()
    {
        $this->element->setNextEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesNextButtonBeforePreviousOne()
    {
        // NB: ordre permettant de valider avec la touche "Entrée"
        $this->element->setPreviousEnabled(true)
                      ->setNextEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<button type="submit" name="nav&#x5B;_previous&#x5D;" title="Revenir&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;pr&#xE9;c&#xE9;dente" class="multipage-nav&#x20;previous" style="float&#x3A;&#x20;left&#x3B;" value="">&lt; Précédent</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesSubmitButton()
    {
        $this->element->setSubmitEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<button type="submit" name="nav&#x5B;_submit&#x5D;" title="Terminer&#x20;la&#x20;saisie" class="multipage-nav&#x20;submit" value="">Terminer</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesCancelButton()
    {
        $this->element->setCancelEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesConfirmButton()
    {
        $this->element->setConfirmEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_confirm&#x5D;" title="Confirmer&#x20;et&#x20;enregistrer&#x20;la&#x20;saisie" class="multipage-nav&#x20;confirm" value="">Confirmer et enregistrer</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingGeneratesSubmitButtonBeforeNextOne()
    {
        // NB: ordre permettant de valider (terminer) avec la touche "Entrée"
        $this->element->setPreviousEnabled(true)
                      ->setSubmitEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<button type="submit" name="nav&#x5B;_submit&#x5D;" title="Terminer&#x20;la&#x20;saisie" class="multipage-nav&#x20;submit" value="">Terminer</button> 
<button type="submit" name="nav&#x5B;_previous&#x5D;" title="Revenir&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;pr&#xE9;c&#xE9;dente" class="multipage-nav&#x20;previous" style="float&#x3A;&#x20;left&#x3B;" value="">&lt; Précédent</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }
    
    public function testRenderingDoesNotGenerateConfirmButtonIfSubmitOneIs()
    {
        $this->element->setSubmitEnabled(true)
                      ->setConfirmEnabled(true);
        $markup = $this->helper->__invoke($this->element)->__toString();
        $expected = <<<'EOD'
<button type="submit" name="nav&#x5B;_next&#x5D;" title="Passer&#x20;&#xE0;&#x20;l&#x27;&#xE9;tape&#x20;suivante" class="multipage-nav&#x20;next" value="">Suivant &gt;</button> 
<button type="submit" name="nav&#x5B;_submit&#x5D;" title="Terminer&#x20;la&#x20;saisie" class="multipage-nav&#x20;submit" value="">Terminer</button> 
<div class="clearer"></div> 
<button type="submit" name="nav&#x5B;_cancel&#x5D;" title="Abandonner&#x20;d&#xE9;finitivement&#x20;la&#x20;saisie" class="multipage-nav&#x20;cancel" onclick="askConfirmation&#x28;this&#x29;" value="">Annuler</button>
EOD;
        $this->assertEquals($expected, $markup);
    }

    /**
     * Returns translator
     *
     * @return Translator
     */
    protected function _getTranslator()
    {
        $loader = new ArrayTranslatorLoader();
        $loader->translations = array(
            "Passer à l'étape suivante"           => "Go to next step",
            "Suivant >"                           => "Next >",
            "Abandonner définitivement la saisie" => "Cancel it all",
            "Annuler"                             => "Cancel",
            "Terminer"                            => "Finish",
            "Confirmer et enregistrer"            => "Confirm and save",
        );

        $translator = new Translator();
        $translator->getPluginManager()->setService('default', $loader);
        $translator->addTranslationFile('default', null);
        
        return $translator;
    }
}
<?php
namespace UnicaenAppTest\Form\Element;

use PHPUnit\Framework\TestCase;
use UnicaenApp\Form\Element\MultipageFormNavElement;

/**
 * Description of MultipageFormNavTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MultipageFormNavTest extends TestCase
{
    protected $element;
    
    protected function setUp()
    {
        $this->element = new MultipageFormNavElement('elem');
    }
    
    public function testConstructorByDefautSetsDefaultName()
    {
        $element = new MultipageFormNavElement();
        $this->assertEquals(MultipageFormNavElement::NAME, $element->getName());
    }
    
    public function testConstructorInitializesPropertiesWithDefaultValues()
    {
        $this->assertFalse($this->element->isPreviousEnabled());
        $this->assertTrue($this->element->isNextEnabled());
        $this->assertFalse($this->element->isSubmitEnabled());
        $this->assertTrue($this->element->isCancelEnabled());
        $this->assertFalse($this->element->isConfirmEnabled());
    }
}
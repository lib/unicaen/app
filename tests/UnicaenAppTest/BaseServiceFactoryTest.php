<?php

namespace UnicaenAppTest;

use Psr\Container\ContainerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
abstract class BaseServiceFactoryTest extends TestCase
{
    protected $factory;
    
    /**
     * @var string
     */
    protected $factoryClass;
    
    /**
     * @var mixed
     */
    protected $serviceClass;
    
    /**
     * @var ContainerInterface|MockObject
     */
    protected $serviceManager;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     * @throws \ReflectionException
     */
    protected function setUp()
    {
        $this->factory        = new $this->factoryClass();
        $this->serviceManager = $this->getMockForAbstractClass('Laminas\ServiceManager\ServiceLocatorInterface', array('get'));
    }
}
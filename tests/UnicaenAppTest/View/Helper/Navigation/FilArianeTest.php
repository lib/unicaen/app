<?php
namespace UnicaenAppTest\View\Helper\Navigation;

use PHPUnit\Framework\MockObject\MockObject;
use UnicaenApp\View\Helper\Navigation\FilAriane;
use Laminas\Navigation\Navigation;
use Laminas\View\Helper\Navigation\Breadcrumbs;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Renderer\RendererInterface;
use ZfcUser\Options\ModuleOptions;

/**
 * Description of FilArianeTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see \UnicaenApp\View\Helper\Navigation\FilAriane
 */
class FilArianeTest extends AbstractTest
{
    /** @var FilAriane */
    protected $helper;

    protected $navigation  = 'fil-ariane/navigation.php';
    protected $routes      = 'fil-ariane/routes.php';
    protected $helperClass = FilAriane::class;

    public function testConstructorInitializesProperties()
    {
        $this->assertTrue($this->helper->getRenderInvisible());
        $this->assertEquals(0, $this->helper->getMinDepth());
    }
    
    public function testRendering()
    {
        /** @var FilAriane|MockObject $helper */
        $helper = $this->getMockBuilder(FilAriane::class)->setMethods(['parentRender'])-> getMock();
        $helper
            ->expects($this->once())
            ->method('parentRender')
            ->with('Navigation')
            ->willReturn('breadcrumbs parent renderering');

        $markup = $helper->render('Navigation');

        $this->assertEquals('<ul class="breadcrumb"><li>breadcrumbs parent renderering</li></ul>', $markup);
    }

    public function testRenderingWhenEmpty()
    {
        /** @var FilAriane|MockObject $helper */
        $helper = $this->getMockBuilder(FilAriane::class)->setMethods(['parentRender'])-> getMock();
        $helper
            ->expects($this->once())
            ->method('parentRender')
            ->with('Navigation')
            ->willReturn('');

        $markup = $helper->render('Navigation');

        $this->assertEquals('', $markup);
    }
}
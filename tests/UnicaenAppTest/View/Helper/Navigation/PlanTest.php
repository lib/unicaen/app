<?php
namespace UnicaenAppTest\View\Helper\Navigation;

use UnicaenApp\View\Helper\Navigation\Plan;

/**
 * Description of PlanTest
 *
 * @property Plan $helper Description
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class PlanTest extends AbstractTest
{
    protected $navigation  = 'plan/navigation.php';
    protected $routes      = 'plan/routes.php';
    protected $helperClass = 'UnicaenApp\View\Helper\Navigation\Plan';
    
    public function testCanRenderMenuFromServiceAlias()
    {
        $markup = $this->helper->render('Navigation');
        $expected = <<<'EOD'
<ul class="menu-footer">
    <li>
        <a href="home">Accueil</a>
        <ul>
            <li>
                <a href="http&#x3A;&#x2F;&#x2F;www.unicaen.fr&#x2F;">UCBN</a>
            </li>
            <li>
                <a href="home&#x2F;apropos">À propos</a>
            </li>
        </ul>
    </li>
</ul>
EOD;
        $this->assertEquals($expected, $markup);
    }
}
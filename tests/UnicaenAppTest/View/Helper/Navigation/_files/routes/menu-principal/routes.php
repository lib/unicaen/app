<?php
return array(
    'home' => array(
        'type' => 'Literal',
        'options' => array(
            'route'    => '/',
        ),
    ),
    'apropos' => array(
        'type' => 'Literal',
        'options' => array(
            'route'    => '/apropos',
        ),
    ),
    'contact' => array(
        'type'    => 'Literal',
        'options' => array(
            'route'    => '/contact',
        ),
        'may_terminate' => true,
        'child_routes' => array(
            'ajouter' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/ajouter',
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'identite' => array(
                        'type' => 'Laminas\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/identite[/:source[/:branch]]',
                        ),
                    ),
                    'adresse' => array(
                        'type' => 'Laminas\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/adresse[/:type]',
                        ),
                    ),
                    'message' => array(
                        'type' => 'Laminas\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/message[/:id]',
                        ),
                    ),
                ),
            ),
            'supprimer' => array(
                'type' => 'Laminas\Router\Http\Segment',
                'options' => array(
                    'route'    => '/supprimer/:id',
                ),
            ),
            'modifier' => array(
                'type' => 'Laminas\Router\Http\Segment',
                'options' => array(
                    'route'    => '/modifier/:id',
                ),
            ),
            'envoyer' => array(
                'type' => 'Laminas\Router\Http\Segment',
                'options' => array(
                    'route'    => '/envoyer/:id',
                ),
            ),
        ),
    ),
);
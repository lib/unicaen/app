<?php
return array(
    'home' => array(
        'label'      => "Accueil",
        'uri'        => 'home',
        'pages' => array(
            'etab' => array(
                'id'       => 'etab',
                'label'    => "UCBN",
                'uri'      => 'http://www.unicaen.fr/',
                'visible'  => true,
                'footer'   => true,
            ),
            'apropos' => array(
                'id'       => 'apropos',
                'label'    => "À propos",
                'uri'      => 'home/apropos',
                'visible'  => false,
                'active'   => true,
                'footer'   => true,
            ),
            'other' => array(
                'id'       => 'other',
                'label'    => "Something different",
                'uri'      => 'home/other',
                'visible'  => false,
                // NB: propriété 'footer' absente
            ),
        ),
    ),
);
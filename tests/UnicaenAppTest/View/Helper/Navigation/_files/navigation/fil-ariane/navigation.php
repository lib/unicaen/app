<?php
return array(
    'home' => array(
        'label' => "Accueil",
        'uri'   => 'home',
        'pages' => array(
            'etab' => array(
                'label'   => "Université de Caen Normandie",
                'uri'     => 'http://www.unicaen.fr/',
                'visible' => false,
            ),
            'apropos' => array(
                'label'    => "À propos",
                'uri'      => 'home/apropos',
                'visible'  => false,
                'active'   => true,
            ),
        ),
    ),
);
<?php

namespace UnicaenAppTest\View\Helper;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Description of AbstractTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
abstract class AbstractTest extends TestCase
{
    protected $files;
    protected $helperClass;
    protected $helper;

    /**
     * @var PhpRenderer|MockObject
     */
    protected $renderer;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->files = __DIR__ . '/_files';

        $this->renderer = $this->createMock(PhpRenderer::class);

        $this->helper = new $this->helperClass();
        $this->helper->setView($this->renderer);
    }

    /**
     * Returns the content of the expected $file
     * 
     * @param string $file
     * @return string
     */
    protected function getExpected($file)
    {
        return file_get_contents($this->files . '/expected/' . $file);
    }
}
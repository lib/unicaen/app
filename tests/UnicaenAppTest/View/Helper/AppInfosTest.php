<?php
namespace UnicaenAppTest\View\Helper;

use stdClass;
use UnicaenApp\Exception\LogicException;
use UnicaenApp\View\Helper\AppInfos;
use Laminas\Config\Config;
use Laminas\I18n\Translator\Translator;
use Laminas\View\Helper\HtmlList;

/**
 * Description of AppInfosTest
 *
 * @property AppInfos $helper Description
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AppInfosTest extends AbstractTest
{
    protected $helperClass = 'UnicaenApp\View\Helper\AppInfos';
    
    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->helper->setTranslator(new Translator());
    }
    
    public function testReturnsSelfWhenInvoked()
    {
        $this->assertSame($this->helper, $this->helper->__invoke());
    }
    
    public function testCanGetDefaultConfig()
    {
        $this->assertInstanceOf('Laminas\Config\Config', $config = $this->helper->getConfig());
        $this->assertEmpty($config->toArray());
    }
    
    public function getConfig()
    {
        $array = [
            'nom'              => "Nom de l'application",
            'version'          => "1.3.0",
            'not-allowed-prop' => 'foo'
        ];

        return [
            [$array],
            [new Config($array)],
        ];
    }

    /**
     * @dataProvider getConfig
     * @param array|Config $config
     */
    public function testCanSetConfig($config)
    {
        $this->helper->setConfig($config);
        $this->assertInstanceOf('Laminas\Config\Config', $this->helper->getConfig());
    }
    
    /**
     * @dataProvider getConfig
     * @param array|Config $config
     */
    public function testConstructorAcceptsConfig($config)
    {
        $helper = new AppInfos($config);
        $this->assertInstanceOf('Laminas\Config\Config', $helper->getConfig());
    }
    
    /**
     * @dataProvider getConfig
     * @param array|Config $config
     */
    public function testCanAccessConfigAttribute($config)
    {
        $helper = new AppInfos($config);
        $this->assertInstanceOf('Laminas\Config\Config', $config = $helper->getConfig());
        $this->assertEquals("Nom de l'application", $helper->nom);
        $this->assertEquals("1.3.0", $helper->version);
    }
    
    /**
     * @dataProvider getConfig
     * @param array|Config $config
     */
    public function testFiltersConfig($config)
    {
        $helper = new AppInfos($config);
        $this->assertInstanceOf('\Laminas\Config\Config', $config = $helper->getConfig());
        $this->assertNull($config->get('foo'));
    }
    
    public function getInvalidConfig()
    {
        return [
            [null],
            ["Hello"],
            [12],
            [new stdClass()],
        ];
    }
    
    /**
     * @dataProvider getInvalidConfig
     * @expectedException LogicException
     * @expectedExceptionMessage invalide
     */
    public function testThrowsExceptionWhenSettingInvalidConfig($config)
    {
        $this->helper->setConfig($config);
    }
    
    public function getEmptyConfig()
    {
        return [
            [[]],
            [new Config([])],
        ];
    }
    
    /**
     * @dataProvider getEmptyConfig
     * @expectedException LogicException
     * @expectedExceptionMessage vide
     * @param array|Config $config
     */
    public function testThrowsExceptionWhenSettingEmptyConfig($config)
    {
        $this->helper->setConfig($config);
    }

    public function testCanGenerateCorrectHTMLFormatMarkupIncludingContact()
    {
        $includeContact = true;

        $htmlListHelper = $this->createMock(HtmlList::class);
        $htmlListHelper
            ->expects($this->once())
            ->method('__invoke')
            ->willReturn('htmlList HTML markup');

        $this->renderer
            ->expects($this->once())
            ->method('plugin')
            ->with('htmlList')
            ->willReturn($htmlListHelper);

        $this->helper
            ->setConfig(['peu', 'importe', 'la', 'config'])
            ->setHtmlListFormat(true)
            ->setIncludeContact($includeContact);
        $markup = $this->helper->__toString();
        $this->assertEquals('htmlList HTML markup', $markup);
    }

    public function testCanGenerateCorrectHTMLFormatMarkupNotIncludingContact()
    {
        $includeContact = false;

        $htmlListHelper = $this->createMock(HtmlList::class);
        $htmlListHelper
            ->expects($this->once())
            ->method('__invoke')
            ->willReturn('htmlList HTML markup');

        $this->renderer
            ->expects($this->once())
            ->method('plugin')
            ->with('htmlList')
            ->willReturn($htmlListHelper);

        $this->helper
            ->setConfig(['peu', 'importe', 'la', 'config'])
            ->setHtmlListFormat(true)
            ->setIncludeContact($includeContact);
        $markup = $this->helper->__toString();
        $this->assertEquals('htmlList HTML markup', $markup);
    }

    public function getConfigAndExpectedScript()
    {
        return [
            [
                'text-contact-aucun' => [
                    'nom'     => "Mon application",
                    'desc'    => "Magnifique appli!",
                    'version' => '1.3.0',
                    'date'    => '01/07/2013',
                ],
                'includeContact' => true,
                'script' => 'app-infos/text/contact-aucun.phtml',
            ],
            [
                'text-contact-vide' => [
                    'nom'     => "Mon application",
                    'desc'    => "Magnifique appli!",
                    'version' => '1.3.0',
                    'date'    => '01/07/2013',
                    'contact' => null,
                ],
                'includeContact' => true,
                'script' => 'app-infos/text/contact-aucun.phtml',
            ],
            [
                'text-contact-exclu' => [
                    'nom'     => "Mon application",
                    'desc'    => "Magnifique appli!",
                    'version' => '1.3.0',
                    'date'    => '01/07/2013',
                    'contact' => 'e.mail@domain.fr',
                ],
                'includeContact' => false,
                'script' => 'app-infos/text/contact-exclu.phtml',
            ],
            [
                'text-contact-simple' => [
                    'nom'     => "Mon application",
                    'desc'    => "Magnifique appli!",
                    'version' => '1.3.0',
                    'date'    => '01/07/2013',
                    'contact' => 'e.mail@domain.fr',
                ],
                'includeContact' => true,
                'script' => 'app-infos/text/contact-simple.phtml',
            ],
            [
                'text-contact-multi-array' => [
                    'nom'     => "Mon application",
                    'desc'    => "Magnifique appli!",
                    'version' => '1.3.0',
                    'date'    => '01/07/2013',
                    'contact' => ['e.mail@domain.fr', '01 02 03 04 05'],
                ],
                'includeContact' => true,
                'script' => 'app-infos/text/contact-multi.phtml',
            ],
            [
                'text-contact-multi-config' => [
                    'nom'     => "Mon application",
                    'desc'    => "Magnifique appli!",
                    'version' => '1.3.0',
                    'date'    => '01/07/2013',
                    'contact' => new Config(['e.mail@domain.fr', '01 02 03 04 05']),
                ],
                'includeContact' => true,
                'script' => 'app-infos/text/contact-multi.phtml',
            ],
        ];
    }
    
    /**
     * @dataProvider getConfigAndExpectedScript
     * @param array $config
     * @param boolean $includeContact
     * @param string $expectedScript
     */
    public function testCanGenerateCorrectTextFormatMarkup($config, $includeContact, $expectedScript)
    {
        $this->helper->setConfig($config)
                     ->setHtmlListFormat(false)
                     ->setIncludeContact($includeContact);

        $markup = $this->helper->__toString();

        $this->assertEquals($this->getExpected($expectedScript), $markup);
    }
}
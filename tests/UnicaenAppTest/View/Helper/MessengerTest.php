<?php

namespace UnicaenAppTest\View\Helper;

use PHPUnit\Framework\MockObject\MockObject;
use UnicaenApp\View\Helper\Messenger;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger as FlashMessengerPlugin;

/**
 * Description of MessengerText
 *
 * @property Messenger $helper Description
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MessengerText extends AbstractTest
{
    protected $helperClass = Messenger::class;

    /**
     * @var FlashMessengerPlugin|MockObject
     */
    protected $flashMessengerPlugin;
    
    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->flashMessengerPlugin = $this->createMock(FlashMessengerPlugin::class);

        $this->helper->setPluginFlashMessenger($this->flashMessengerPlugin);
    }
    
    public function testReturnsSelfWhenInvoked()
    {
        $this->assertSame($this->helper, $this->helper->__invoke());
    }
    
    public function getMessages()
    {
        $info    = "Information message";
        $success = "Success message";
        $error   = "Error message";
        
        return [
            'aa' => [
                'severity' => null,
                'messages' => [
                    'info'    => $info,
                    'success' => $success,
                    'danger'   => $error
                ],
                'expected' => [
                    'info'    => [
                        0/*priority*/ => $info,
                    ],
                    'success' => [
                        0/*priority*/ => $success,
                    ],
                    'danger'  => [
                        0/*priority*/ => $error,
                    ]
                ],
            ],
            'namespace' => [
                'severity' => null,
                'messages' => [
                    'these/info' => $info,
                    'success'    => $success,
                    'danger'     => $error,
                ],
                'expected' => [
                    'these/info'    => [
                        0/*priority*/ => $info,
                    ],
                    'success' => [
                        0/*priority*/ => $success,
                    ],
                    'danger'  => [
                        0/*priority*/ => $error,
                    ]
                ],
            ],
            'bb' => [
                'severity' => null, 
                'messages' => $messages = [
                    'info'    => [
                        $info,
                        $info . ' n°2',
                    ],
                    'success' => [
                        $success
                    ],
                    'danger'  => [
                        $error
                    ]
                ],
                'expected' => [
                    'info'    => [
                        0/*priority*/ => [
                            $info,
                            $info . ' n°2',
                        ],
                    ],
                    'success' => [
                        0/*priority*/ => [
                            $success
                        ],
                    ],
                    'danger'  => [
                        0/*priority*/ => [
                            $error
                        ],
                    ],
                ],
            ],
            'cc' => [
                'severity' => 'info', 
                'messages' => $info,
                'expected' => [
                    0/*priority*/ => $info
                ],
            ],
            'dd' => [
                'severity' => 'info', 
                'messages' => [
                    $info,
                    $info . ' n°2',
                ],
                'expected' => [
                    0/*priority*/ => $info,
                    1/*priority*/ => $info . ' n°2'
                ],
            ],
            'ee' => [
                'severity' => 'info', 
                'messages' => ['info' => [$info, $info . ' n°2']],
                'expected' => [
                    0/*priority*/ => [
                        $info,
                        $info . ' n°2'
                    ]
                ],
            ],
            'ff' => [
                'severity' => 'info', 
                'messages' => ['info' => $info],
                'expected' => [
                    0/*priority*/ => $info
                ],
            ],
            'gg' => [
                'severity' => 'success', 
                'messages' => ['success' => $success],
                'expected' => [
                    0/*priority*/ => $success
                ],
            ],
            'hh' => [
                'severity' => 'danger', 
                'messages' => ['danger' => $error],
                'expected' => [
                    0/*priority*/ => $error
                ],
            ],
        ];
    }

    /**
     * @dataProvider getMessages
     * @param string $severity
     * @param array $messages
     * @param array $expected
     */
    public function testGetMessagesFormatIsCorrect($severity, $messages, $expected)
    {
        $this->helper->setMessages($messages);
        $this->assertTrue($this->helper->hasMessages($severity));
        $this->assertEquals($expected, $this->helper->getMessages($severity));
    }

    public function testSettingUniqueMessageReplaceExistingMessages()
    {
        $this->helper->setMessages([
            'info'  => "Information message.",
            'danger' => "Error message."
        ]);

        $this->helper->setMessage($message = "Unique information message.", 'info');
        $this->assertEquals([
            'info' => [
                $message,
            ],
        ], $this->helper->getMessages());
    }

    public function testCanAddMessage()
    {
        $this->helper->setMessages([
            'info'  => "Information message.",
            'danger' => "Error message."
        ]);

        $this->helper->addMessage("Success message.", 'success');
        $expected = [
            'info'    => ["Information message."],
            'success' => ["Success message."],
            'danger'   => ["Error message."]
        ];
        $this->assertEquals($expected, $this->helper->getMessages());

        $this->helper->addMessage("Another information message.");
        $expected = [
            'info'    => ["Information message.", "Another information message."],
            'success' => ["Success message."],
            'danger'   => ["Error message."]
        ];
        $this->assertEquals($expected, $this->helper->getMessages());
    }

    public function testCanClearMessages()
    {
        $this->helper->setMessages([
            'info'    => "Information message.",
            'success' => "Success message.",
            'danger'   => "Error message."
        ]);

        $this->helper->clearMessages('info');
        $expected = [
            'info'    => [],
            'success' => ["Success message."],
            'danger'   => ["Error message."]
        ];
        $this->assertEquals($expected, $this->helper->getMessages());

        $this->helper->clearMessages();
        $this->assertEmpty($this->helper->getMessages());
    }

    public function testCanImportMessagesFromFlashMessenger()
    {
        $this->flashMessengerPlugin
            ->expects($this->any())
            ->method('getMessagesFromNamespace')
            ->willReturnMap([
                ['success', "Success message from FlashMessenger."],
                ['info', "Information message from FlashMessenger."],
                ['error', "Error message from FlashMessenger."],
                ['warning', "Warning message from FlashMessenger."],
            ]);

        $this->helper->setMessages([
            'info'    => "Information message.",
            'success' => "Success message.",
            'danger'  => "Error message.",
            'warning' => "Warning message.",
        ]);

        // appends
        $this->helper->addMessagesFromFlashMessengerWithNoNamespace();
        $expected = [
            'info'    => [
                "Information message.",
                "Information message from FlashMessenger."
            ],
            'success' => [
                "Success message.",
                "Success message from FlashMessenger."
            ],
            'danger'  => [
                "Error message.",
                "Error message from FlashMessenger."
            ],
            'warning'  => [
                "Warning message.",
                "Warning message from FlashMessenger."
            ]
        ];
        $this->assertEquals($expected, $this->helper->getMessages());

        // replaces
        $this->helper->setMessagesFromFlashMessengerWithNoNamespace();
        $expected = [
            'info'    => [
                "Information message from FlashMessenger."
            ],
            'success' => [
                "Success message from FlashMessenger."
            ],
            'danger'  => [
                "Error message from FlashMessenger."
            ],
            'warning'  => [
                "Warning message from FlashMessenger."
            ]
        ];
        $this->assertEquals($expected, $this->helper->getMessages());
    }

    public function testCanImportMessagesFromFlashMessengerWithPreciseNamespace()
    {
        $namespace = 'these/info';

        $this->flashMessengerPlugin
            ->expects($this->exactly(2))
            ->method('getMessagesFromNamespace')
            ->with($namespace)
            ->willReturn(["Information message from FlashMessenger namespace."]);

        $this->helper->setMessages([
            'info'    => "Information message.",
            'success' => "Success message.",
            'danger'   => "Error message."
        ]);

        /**
         * appends
         */
        $this->helper->addMessagesFromFlashMessengerWithNamespace($namespace);
        $expected = [
            'info'    => [
                "Information message.",
                "Information message from FlashMessenger namespace."
            ],
            'success' => [
                "Success message.",
            ],
            'danger'  => [
                "Error message.",
            ]
        ];
        $this->assertEquals($expected, $this->helper->getMessages());

        // replaces
        $this->helper->setMessagesFromFlashMessengerWithNamespace($namespace);
        $expected = [
            'info'    => [
                "Information message from FlashMessenger namespace."
            ],
        ];
        $this->assertEquals($expected, $this->helper->getMessages());
    }

    public function testCanImportMessagesFromFlashMessengerWithWildcardedNamespace()
    {
        $wildcardedNamespace = 'these/*';

        $this->flashMessengerPlugin
            ->expects($this->exactly(8))
            ->method('getMessagesFromNamespace')
            ->will($this->returnValueMap([
                ['these/danger',  ["Message from FlashMessenger namespace these/danger"]],
                ['these/success', ["Message from FlashMessenger namespace these/success"]],
                ['these/info',    ["Message from FlashMessenger namespace these/info"]],
                ['these/warning', ["Message from FlashMessenger namespace these/warning"]],
            ]));

        $this->helper->setMessages([
            'info'    => "Information message.",
            'success' => "Success message.",
            'danger'   => "Error message."
        ]);

        /**
         * appends
         */
        $this->helper->addMessagesFromFlashMessengerWithNamespace($wildcardedNamespace);
        $expected = [
            'danger'  => [
                "Error message.",
                "Message from FlashMessenger namespace these/danger"
            ],
            'success' => [
                "Success message.",
                "Message from FlashMessenger namespace these/success"
            ],
            'info'    => [
                "Information message.",
                "Message from FlashMessenger namespace these/info"
            ],
            'warning'  => [
                "Message from FlashMessenger namespace these/warning"
            ],
        ];
        $this->assertEquals($expected, $this->helper->getMessages());

        // replaces
        $this->helper->setMessagesFromFlashMessengerWithNamespace($wildcardedNamespace);
        $expected = [
            'danger'  => [
                "Message from FlashMessenger namespace these/danger"
            ],
            'success' => [
                "Message from FlashMessenger namespace these/success"
            ],
            'info'    => [
                "Message from FlashMessenger namespace these/info"
            ],
            'warning'  => [
                "Message from FlashMessenger namespace these/warning"
            ],
        ];
        $this->assertEquals($expected, $this->helper->getMessages());
    }

    public function testCanImportCurrentMessagesFromFlashMessenger()
    {
        $this->flashMessengerPlugin
            ->expects($this->any())
            ->method('getCurrentMessagesFromNamespace')
            ->willReturnMap([
                ['info', "Information message from FlashMessenger."],
                ['success', "Success message from FlashMessenger."],
                ['error', "Error message from FlashMessenger."],
                ['warning', "Warning message from FlashMessenger."],
            ]);

        $this->helper->setMessages([
            'info'    => "Information message.",
            'success' => "Success message.",
            'danger'  => "Error message.",
            'warning' => "Warning message.",
        ]);

        /**
         * appends
         */
        $this->helper->addCurrentMessagesFromFlashMessengerWithNoNamespace();
        $expected = [
            'info'    => [
                "Information message.",
                "Information message from FlashMessenger."
            ],
            'success' => [
                "Success message.",
                "Success message from FlashMessenger."
            ],
            'danger'  => [
                "Error message.",
                "Error message from FlashMessenger."
            ],
            'warning' => [
                "Warning message.",
                "Warning message from FlashMessenger."
            ],
        ];
        $this->assertEquals($expected, $this->helper->getMessages());

        // replaces
        $this->helper->setCurrentMessagesFromFlashMessengerWithNoNamespace();
        $expected = [
            'info'    => [
                "Information message from FlashMessenger."
            ],
            'success' => [
                "Success message from FlashMessenger."
            ],
            'danger'  => [
                "Error message from FlashMessenger."
            ],
            'warning' => [
                "Warning message from FlashMessenger."
            ],
        ];
        $this->assertEquals($expected, $this->helper->getMessages());
    }

    public function testRenderingReturnsEmptyStringIfNoMessageSpecified()
    {
        $this->assertEquals('', "" . $this->helper);
    }

    public function getMessagesAndExpectedScript()
    {
        return array(
            'one-info' => array(
                "Information message.",
                'messenger/one-info.phtml'
            ),
            'two-errors' => array(
                array(
                    'danger' => array("Error message.", "Another error message.")),
                'messenger/two-errors.phtml'
            ),
            'all' => array(
                array(
                    'info'    => "Information message.",
                    'success' => "Success message.",
                    'danger'   => "Error message."),
                'messenger/all.phtml'
            ),
        );
    }

    /**
     * @dataProvider getMessagesAndExpectedScript
     * @param array|string $messages
     * @param string $expectedScript
     */
    public function testRenderingReturnsCorrectMarkup($messages, $expectedScript)
    {
        $this->helper->setMessages($messages);
        $markup = (string) $this->helper;
        $this->assertEquals($this->getExpected($expectedScript), $markup);
    }

    public function test_rendering_is_correct_with_Imported_Messages_From_FlashMessenger_namespace()
    {
        $namespace = 'these/danger';

        $this->flashMessengerPlugin
            ->expects($this->once())
            ->method('getMessagesFromNamespace')
            ->with($namespace)
            ->willReturn(["Error message from FlashMessenger namespace."]);

        $this->helper->setMessages([
            'danger' => "Error message."
        ]);

        $this->helper->addMessagesFromFlashMessengerWithNamespace($namespace);

        $markup = (string) $this->helper;
        $this->assertEquals($this->getExpected('messenger/imported_from_fm_namspace.phtml'), $markup);
    }

    public function testGettingTemplateForUnknownSeverityReturnsInfoSeverityTemplate()
    {
        $this->assertEquals(
            $this->helper->getTemplate('info'),
            $this->helper->getTemplate('unknown')
        );
    }

    public function testCanCustomizeMarkupTemplate()
    {
        $this->helper->addMessage("Information message.")
                     ->setContainerInnerTemplate('<em>%s</em>');
        $markup = (string) $this->helper;
        $this->assertEquals($this->getExpected('messenger/custom.phtml'), $markup);
    }
}

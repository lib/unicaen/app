<?php
// Application config
$appConfig = array();//include __DIR__ . '/../../../../../config/application.config.php';

// Test config
$testConfig = array(
    'modules' => array(
        'Laminas\Cache',
        'Laminas\Filter',
        'Laminas\Form',
        'Laminas\Hydrator',
        'Laminas\I18n',
        'Laminas\InputFilter',
        'Laminas\Mvc\Console',
        'Laminas\Mvc\I18n',
        'Laminas\Mvc\Plugin\FilePrg',
        'Laminas\Mvc\Plugin\FlashMessenger',
        'Laminas\Mvc\Plugin\Identity',
        'Laminas\Mvc\Plugin\Prg',
        'Laminas\Navigation',
        'Laminas\Paginator',
        'Laminas\Router',
        'Laminas\Session',
        'Laminas\Validator',

        'UnicaenApp'
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            __DIR__ . '/autoload/{,*.}{global,local}.php',
        ),
    ),
);

return \Laminas\Stdlib\ArrayUtils::merge($appConfig, $testConfig);

# UnicaenApp

Ce module :

Est une bibliothèque qui fournit des classes de base, utilisables hors contexte web (entités, validateurs, exceptions, etc.)

C'est aussi le module de base (fondations) de toutes nos applications web qui :

- fournit les infos par défaut concernant l'application (nom, description, version, etc.)
- fournit le contrôleur et les vues pour les pages à propos (URL “/apropos”), contacts (“/contact”), plan de navigation (“/plan”), mentions légales (“/mentions-legales”), informatique et libertés (“/informatique-et-libertes”).
- configure les routes par défaut /application/:controller/:action pointant sur les contrôleurs du module “Application” de l'application.
- configure le composant multilingue et fournit les traductions des textes présent dans ce module.
-  fournit les vues associées aux pages d'erreur (standard et “404”).
- fournit le canevas de page (layout).
- spécifie le “doctype” HTML par défaut (HTML5).
- désactive l'affichage détaillé des erreurs rencontrées (notamment les exceptions).
- crée la page de navigation “Accueil” (niveau 0) et ses pages de navigation filles (niveau 1) “UCBN”, à propos, contacts, plan de navigation, mentions légales, informatique et libertés.


## Documentation

- [Installation](./doc/Installation.md)
- [Configuration](./doc/Configuration.md)
- [Services](./doc/Services.md)
- [Modèles](./doc/Modeles.md)
- [Plugins de contrôleur (controller plugins)](./doc/Plugins.md)
- [Aides de vue (view helpers)](./doc/ViewHelpers.md)
- [Contrôleurs](./doc/Controleurs.md)
- [Formulaires](./doc/Formulaires.md)
- [Filtres](./doc/Filtres.md)
- [Validateurs](./doc/Validateurs.md)
- [MVC](./doc/MVC.md)
- [Message](./doc/Message.md)
- [Divers](./doc/Divers.md)
- [Historique](./doc/Historique.md)
- [Eléments de page riches (mélangeant HTML et JS)](./doc/ElementsPageRiches.md)
- [Javascript](./doc/Javascript.md)
- [Widgets](./doc/widgets.md)

CHANGELOG
=========

7.1.0 (24/01/2025)
------------------

- Suppression de la dépendance à laminas/log



7.0.0
-----
- [Fix] cosmétique : mise en conformité su searchAndSelect qui s'affichait en -sm contraitement au formControlGroup
- Retrait du mouchard 
- Retrait du mailer

6.1.7
-----
- Retour de la possitibilité d'utiliser 'CURRENT_USER' dans les adresses de redirection pour rediriger les mails vers l'utilisateur connecté.

6.1.6
-----
- MailerService : si le 'from' du message à envoyer est vide, il est comblé avec celui par défaut.

6.1.5
-----
- Suppression de UnicaenApp\Process\ProcessResult* et UnicaenApp\Service\SQL\RunSQL* (extraction vers une nouvelle bibliothèque unicaen/sql).

6.1.4
-----
- Changement explode on NULL

6.1.3
-----
- Retrait de la dépendance à laminas-dependency-plugin

6.1.1
-----
- PHP8.0 réautorisé

6.1.0
-----
- Compatibilité PHP8.2

6.0.15
------
- Doctrine ORM : support de la fonction coalesce(), y compris dans un order by.
- AjaxModalListener (unicaen.js) : une erreur explicite est émise si le formulaire n'a pas l'attribut requis 'action'.
- AjaxModalListener (unicaen.js) : ajout de la classe CSS 'loading' sur le 'body' lors du clic sur le lien puis retrait lorsque la modale est ouverte.
- AjaxModalListener (unicaen.js) : déclenchement d'un événement lors du clic sur le lien et d'un autre lorsque la modale est ouverte
  (noms à transmettre via les attributs 'data-event-modal-loading' et 'data-event-modal-opened' du lien).
- [FIX] FormElementCollection : corrections complémentaires pour que SearchAndSelect fonctionne.

6.0.14
------
- [FIX] FormElementCollection : forçage d'id pour éviter les doubons provoqué par le clonage (SearchAndSelect ne le supporte pas) !
- [FIX] FormElementCollection : correction d'un auto-sabotage !

6.0.13
------

- Autocomplete JS : mise en subrillance de tous les mots recherchés dans les résultats de la recherche.

6.0.12
------

- MultipageForm : ajout du passage de la variable formulaire à chaque vue d'étape, permettant d'appeler dans la vue la méthode prepare() indispensable.

6.0.11
-----

- Suppression du ConsoleController utilisé par une seule appli pour lancer une ligne SQL.
- Suppression de la dépendance avec unicaen/console.

6.0.10
-----

- [Fix] FormElementCollection : correction des 'Warning: Undefined variable'.

6.0.9
-----

- MultipageForm : ajout aux données en session du fieldset dont les données viennent d'être postées (clé '_fieldset').

6.0.8
-----

- [Fix] MultipageForm::add() ne prévoyait pas la fourniture de l'élément au format array.

6.0.7
-----

- [Fix] Remise d'équerre du master et de la 6.x

6.0.6
-----
- [Fix] Msg d'erreur correctement affichés avec $messenger->addCurrentMessagesFromFlashMessengerWithNamespace('error');

6.0.5
-----
- [Fix] config Doctrine pointant vers un répertoire inexistant commentée


6.0.3
-----
- Version 6 requise de unicaen/console (plutôt que dev-main). 

6.0.2
-----
- Exporter PDF : correction du constructeur pour compatibilité avec la version de mPDF requise.
- FormElementCollection : correction (trop simple ?) du bug dans la suppression d'un item de la collection.

6.0.1
-----
- FormElementCollection->render : définition rendue compatible avec le parent

5.1.7
-----
- Nouvel élément de formulaire Collection et aide de vue associée FormElementCollection (adaptés d'Octopus).

5.1.6
-----
- Nouveau collecteur dans la barre LaminasDevTools : messages produits par \UnicaenApp\Service\MessageCollector.   

5.1.5
-----
- Nouvel élément de formulaire SearchAndSelect2 (basé sur le widget js Select2).

5.1.4
-----
-

5.1.3
-----
-

5.1.2
-----
-

5.1.1
-----
- Suppression du fix pour bootstrap-select-1.14.0-beta2 donc nécessaité de passer à bootstrap-select-1.14.0-beta3 dans vos applis.

5.1.0
-----
- Exporteur PDF : nouvelles méthodes set(Footer|Header)ScriptToDefault() et set(Footer|Header)ScriptToNone().

5.0.1
-----
- [FIX] FormControlGroup : prise en compte de la classe CSS des labels de checkboxes. 

5.0.0
-----
- Migration vers Bootstrap 5 (front-end).

4.0.3
-----
- Amélioration du RunSQLProcess : petite désadhérance avec Oracle, nouveaux attributs dans RunSQLResult, correction d'un warning.

4.0.2 (15/12/2021)
------------------
- [FIX] uploader cassé : FormElementManagerV3Polyfill est deprecated et inutilisé, remplacé par FormElementManager

4.0.1 (10/12/2021)
------------------
- PHP 7.4 & MPDF 8.0 requis

4.0.0
-----
- Passage de Zend à Laminas


3.1.23
------
- Suppression de la SessionManagerFactory jamais utilisée car mal configurée.

3.1.22
------
- Le composant TabAjax ajoute les urls dans les attributs data-url des tab-panes

3.1.21
------
- Dépendance composer supprimée : zfcampus/zf-asset-manager.
- Dépendance composer assouplie : possibilité de passer à mPDF v8.

3.1.20
------
- Ajout du support de 2 fonctions SQL oracle/postgres : replace() et chr()

3.1.19
------


3.1.18
------


3.1.17
------


3.1.16
------


3.1.15
------
- Refactoring de HostLocalizationUnicaen pour éviter d'avoir les adresses des proxies et reverse proxies en dur dans le code. Passage en fichier de config pour que chacun puisse configurer dans son application.
- Aide de vue FormControlGroup : utilisation des aides de vue ZF standards pour les éléments de formulaire ZF Date et Time
- Support des fonctions Doctrine compriseEntre() et pasHistorise()
- [FIX] Modification du répertoire de cache de mPDF car tentatives de supprimer les vieux fichiers ne lui appartenant pas
- [FIX] Correction de Util::truncatedString boguée

3.1.14
------
- Nouvelle ligne de commande 'run-sql-query' pour exécuter une requête SQL.
- Correction de ConsoleControllerFactory non migrée en ZF3.

3.1.13
------
- AjaxPopover : possibilité de désactiver l'interception du submit ajax en ajoutant la classe 'disable-ajax-submit' sur le formulaire.

3.1.12
-------------------
- Aide de vue Messenger : collecte des messages dans le namespace tel quel (fonctionne aussi si le namespace est une sévérité)

3.1.11 (18/06/2020)
-------------------
Correction d'un bug au niveau de l'intranavigatuer : le sélecteur no-intranavigation fonctionne maintenant pour les formulaires

3.1.10 (17/06/2020)
-------------------
Amélioration du TabAjax : possibilité de fournir un tab par défaut et il se charge tout seul en AJAX si son contenu n'était pas initialisé.

3.1.9 (17/06/2020)
------------------
Correction de bug : ne pas prendre ne compte l'option label_options pour le Bootstrap DateTimePicker

3.1.8 (16/06/2020)
-------------------
Correction de bug pour éviter qu'un paramétrage de FormControlgroup n'influe sur d'autres éléments de formulaires

3.1.7 (28/05/2020)
------------------
Génération de PDF : fusion de toutes les variables dans les scripts de création des header et footer.

3.1.6 (09/04/2020)
------------------
Formulaires multipages : améliorations et modifications (rupture de compatibilité probable).

3.0.0 (17/09/2019)
------------------
Première version officielle sous ZF3.

3.0.1 (19/09/2019)
------------------
- Corrections
  - Vue error/index.phtml affichant les erreurs : prise en compte aussi des erreurs de type Error!

3.1.4 (23/03/2020)
------------------
- Correction
  - Bug corrigé sur le validateur d'INSEE qui ne passait pas avec les corses
  
3.1.5 (24/03/2020)
------------------
- MultipageFormPlugin : possibilité d'omettre le formulaire lors de l'invoke
- MultipageForm : simplification des clés dans la liste des fieldsets ; possibilité de retirer un fieldset ; ajout de l'élément de navigation dans prepare().

<?php
/**
 * Created by PhpStorm.
 * User: gauthierb
 * Date: 16/07/15
 * Time: 10:45
 */

namespace UnicaenApp\Message;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class MessageRepositoryFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $messageConfig = $container->get('MessageConfig'); /** @var MessageConfig $messageConfig */
        $messages      = Message::createInstancesFromConfig($messageConfig->getMessagesConfig());

        $service = new MessageRepository($messages);

        return $service;
    }
}
<?php

namespace UnicaenApp\Message;

interface MessageServiceAwareInterface
{
    public function setMessageService(MessageService $service);
}
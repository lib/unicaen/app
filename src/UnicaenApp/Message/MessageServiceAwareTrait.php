<?php

namespace UnicaenApp\Message;

trait MessageServiceAwareTrait
{
    /**
     * @var MessageService
     */
    protected $messageService;

    /**
     * @param MessageService $service
     */
    public function setMessageService(MessageService $service)
    {
        $this->messageService = $service;
    }
}
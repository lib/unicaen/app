<?php
/**
 * Created by PhpStorm.
 * User: gauthierb
 * Date: 22/07/15
 * Time: 11:38
 */

namespace UnicaenApp\Message\Exception;


use UnicaenApp\Exception\RuntimeException;

class SatisfiedSpecificationNotFoundException extends RuntimeException
{

}
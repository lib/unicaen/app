<?php
/**
 * Created by PhpStorm.
 * User: gauthierb
 * Date: 16/07/15
 * Time: 10:45
 */

namespace UnicaenApp\Message;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class MessageConfigFactory implements FactoryInterface
{
    private $serviceLocator;
    private $messageConfigNormalizer;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $this->serviceLocator = $container;

        $appConfig = $container->get('Config');
        $config    = isset($appConfig['message']) ? $appConfig['message'] : [];

        $normalizer = $this->getMessageConfigNormalizer()->setConfig($config);

        return MessageConfig::create($normalizer);
    }

    /**
     * @return MessageConfigNormalizer
     */
    public function getMessageConfigNormalizer()
    {
        if (null === $this->messageConfigNormalizer) {
            $this->messageConfigNormalizer = new MessageConfigNormalizer($this->serviceLocator);
        }
        return $this->messageConfigNormalizer;
    }

    /**
     * @param MessageConfigNormalizer $messageConfigNormalizer
     * @return $this
     */
    public function setMessageConfigNormalizer(MessageConfigNormalizer $messageConfigNormalizer)
    {
        $this->messageConfigNormalizer = $messageConfigNormalizer;
        return $this;
    }


}

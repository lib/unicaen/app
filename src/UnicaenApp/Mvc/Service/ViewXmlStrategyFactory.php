<?php

namespace UnicaenApp\Mvc\Service;

use Psr\Container\ContainerInterface;
use UnicaenApp\View\Renderer\XmlRenderer;
use UnicaenApp\View\Strategy\XmlStrategy;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class ViewXmlStrategyFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var XmlRenderer $xmlRenderer */
        $xmlRenderer = $container->get('ViewXmlRenderer');
        $xmlStrategy = new XmlStrategy($xmlRenderer);

        return $xmlStrategy;
    }
}

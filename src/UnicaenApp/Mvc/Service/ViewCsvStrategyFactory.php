<?php

namespace UnicaenApp\Mvc\Service;

use Psr\Container\ContainerInterface;
use UnicaenApp\View\Strategy\CsvStrategy;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class ViewCsvStrategyFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $csvRenderer = $container->get('ViewCsvRenderer');
        $csvStrategy = new CsvStrategy($csvRenderer);

        return $csvStrategy;
    }
}

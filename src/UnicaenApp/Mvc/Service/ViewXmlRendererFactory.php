<?php

namespace UnicaenApp\Mvc\Service;

use Psr\Container\ContainerInterface;
use UnicaenApp\View\Renderer\XmlRenderer;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class ViewXmlRendererFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $xmlRenderer = $container->get('ViewHelperManager')->getRenderer();

        return $xmlRenderer;
    }
}

<?php

namespace UnicaenApp\Mvc;

use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Mvc\Application;
use Laminas\Router\Exception;
use Laminas\Router\RouteInterface;

/**
 * Buils a redirect response based on the current routing and parameters
 */
class RedirectResponse
{
    /** @var RouteInterface  */
    private $router;

    /** @var Application */
    private $application;

    /**
     * @param Application $application
     * @param RouteInterface $router
     */
    public function __construct(Application $application, RouteInterface $router)
    {
        $this->router = $router;
        $this->application = $application;
    }

    /**
     * @param string $defaultRedirect
     * @return Response
     */
    public function __invoke(string $defaultRedirect): Response
    {
        $redirect = $this->getRedirect($defaultRedirect);

        /** @var Response $response */
        $response = $this->application->getResponse();
        $response->getHeaders()->addHeaderLine('Location', $redirect);
        $response->setStatusCode(302);

        return $response;
    }

    /**
     * @param string $defaultRedirect
     * @return string|null
     */
    protected function getRedirect(string $defaultRedirect): ?string
    {
        $redirect = $this->getRedirectFromRequest() ?: $defaultRedirect;

        if ($this->routeExists($redirect)) {
            return $this->router->assemble([], ['name' => $redirect]);
        }

        return $redirect;
    }

    /**
     * @return string
     */
    private function getRedirectFromRequest(): ?string
    {
        /** @var Request $request */
        $request  = $this->application->getRequest();

        if ($redirectFromQuery = $request->getQuery('redirect')) {
            return $redirectFromQuery;
        }
        if ($redirectFromPost = $request->getPost('redirect')) {
            return $redirectFromPost;
        }

        return null;
    }

    /**
     * @param string $route
     * @return bool
     */
    private function routeExists(string $route): bool
    {
        try {
            $this->router->assemble([], ['name' => $route]);
        } catch (Exception\RuntimeException $e) {
            return false;
        }
        return true;
    }
}

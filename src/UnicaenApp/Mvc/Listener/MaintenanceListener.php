<?php

namespace UnicaenApp\Mvc\Listener;

use Laminas\EventManager\EventManagerInterface;
use Laminas\EventManager\ListenerAggregateInterface;
use Laminas\EventManager\ListenerAggregateTrait;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\Http\RouteMatch;
use Laminas\Stdlib\ArrayUtils;

class MaintenanceListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var array
     */
    private $defaultConfig = [
        // activation (TRUE: activé, FALSE: désactivé)
        'enable' => false,
        // message à afficher
        'message' => "L'application est temporairement indisponible pour des raisons de maintenance, veuillez nous excuser pour la gêne occasionnée.",
        // le mode console est-il aussi concerné (TRUE: oui, FALSE: non)
        'include_cli' => false,
        // liste blanche des adresses IP clientes à laisser passer
        'white_list' => [
            // Formats possibles : [ REMOTE_ADDR ] ou [ REMOTE_ADDR, HTTP_X_FORWARDED_FOR ]
            // exemples :
            // ['127.0.0.1'], // localhost
            // ['172.17.0.1'], // Docker container
            // ['195.220.135.97', '194.199.107.33'], // Via proxy
        ],
    ];

    /**
     * @var array
     */
    private $config = [];

    /**
     * MaintenanceListener constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = ArrayUtils::merge($this->defaultConfig, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->attach(MvcEvent::EVENT_ROUTE, [$this, 'beforeRoute']);
        $events->attach(MvcEvent::EVENT_RENDER, [$this, 'beforeRendering']);
    }

    /**
     * @param MvcEvent $e
     */
    public function beforeRoute(MvcEvent $e)
    {
        if (! $this->isMaintenanceEnabled()) {
            return;
        }
        if ($this->isMaintenanceBypassed()) {
            return;
        }

        // mode maintenance en mode console = exit
        if (php_sapi_name() === 'cli') {
            exit(0);
        }

        $routeMatch = new RouteMatch([
            'controller' => 'UnicaenApp\Controller\Application',
            'action'     => 'maintenance',
        ], strlen('/maintenance'));
        $routeMatch->setMatchedRouteName('maintenance');

        $e->setRouteMatch($routeMatch);
    }

    /**
     * @param MvcEvent $e
     */
    public function beforeRendering(MvcEvent $e)
    {
        if (! $this->isMaintenanceEnabled()) {
            return;
        }

        $e->getViewModel()->setVariable('message', $this->config['message']);
    }

    /**
     * Indique si le mode maintenance est activé dans la config.
     *
     * @return bool
     */
    private function isMaintenanceEnabled()
    {
        return in_array(
            $this->config['enable'],
            [1, true, '1']
        );
    }

    /**
     * Détermine si le mode maintenance doit être outrepassé.
     *
     * @return bool
     */
    private function isMaintenanceBypassed()
    {
        // traitement particulier pour le mode console.
        if (php_sapi_name() === 'cli') {
            return ! $this->isCliIncluded();
        }

        $whiteList = $this->config['white_list'];

        foreach ($whiteList as $ip) {
            $bypassed = $ip[0] === $_SERVER['REMOTE_ADDR'];
            if ($bypassed && isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $bypassed = isset($ip[1]) && $ip[1] === $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            if ($bypassed) {
                return true;
            }
        }

        return false;
    }

    /**
     * Indique si le mode console est aussi concerné.
     *
     * @return bool
     */
    private function isCliIncluded()
    {
        return in_array(
            $this->config['include_cli'],
            [1, true, '1']
        );
    }
}
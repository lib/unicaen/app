<?php

namespace UnicaenApp\Mvc;

use Psr\Container\ContainerInterface;
use UnicaenApp\Mvc\RedirectResponse;
use Laminas\Mvc\Application;
use Laminas\Router\RouteInterface;

class RedirectResponseFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RedirectResponse
    {
        /* @var RouteInterface $router */
        $router = $container->get('Router');

        /* @var Application $application */
        $application = $container->get('Application');

        return new RedirectResponse($application, $router);
    }
}

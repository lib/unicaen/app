<?php

namespace UnicaenApp\Session;

use Psr\Container\ContainerInterface;
use UnicaenApp\Options\ModuleOptions;
use Laminas\Session\Config\SessionConfig;
use Laminas\Session\SessionManager;
use Laminas\Session\Validator\HttpUserAgent;
use Laminas\Session\Validator\RemoteAddr;

/**
 * Description of SessionManager
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class SessionManagerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-app_module_options');
        $appInfos = $moduleOptions->getAppInfos();

        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions([
            'name' => md5($appInfos['nom']),
        ]);

        $sessionManager = new SessionManager($sessionConfig);

        $chain = $sessionManager->getValidatorChain();
        $chain->attach('session.validate', array(new RemoteAddr(), 'isValid'));
        $chain->attach('session.validate', array(new HttpUserAgent(), 'isValid'));

        return $sessionManager;
    }
}
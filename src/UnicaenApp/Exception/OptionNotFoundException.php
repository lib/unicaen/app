<?php

namespace UnicaenApp\Exception;

/**
 * 
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class OptionNotFoundException extends LogicException
{
    
}
<?php

namespace UnicaenApp\Exception;

/**
 * Exception correspondant à un simple message d'avertissement/erreur ne nécessitant 
 * jamais l'affichage de sa trace complète.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MessageException extends RuntimeException
{
    
}
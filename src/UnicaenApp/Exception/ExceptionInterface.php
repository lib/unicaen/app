<?php

namespace UnicaenApp\Exception;

/**
 * Interface commune à toutes les exceptions levées dans une application Unicaen.
 * 
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
interface ExceptionInterface
{
    
}
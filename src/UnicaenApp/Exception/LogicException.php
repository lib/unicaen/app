<?php

namespace UnicaenApp\Exception;

/**
 * Exception qui représente les erreurs dans la logique du programme d'une application Unicaen.
 * Ce type d'exceptions doit faire directement l'objet d'une correction de votre code. 
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LogicException extends \LogicException implements ExceptionInterface
{
    
}
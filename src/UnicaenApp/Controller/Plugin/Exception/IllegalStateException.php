<?php
namespace UnicaenApp\Controller\Plugin\Exception;

use UnicaenApp\Exception\LogicException;

/**
 * Description of IllegalStateException
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class IllegalStateException extends LogicException
{
    
}

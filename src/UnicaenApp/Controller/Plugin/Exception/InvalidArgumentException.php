<?php
namespace UnicaenApp\Controller\Plugin\Exception;

/**
 * Description of InvalidArgumentException
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class InvalidArgumentException extends \Laminas\Mvc\Exception\InvalidArgumentException
{
    
}

<?php

namespace UnicaenApp\Controller\Plugin;

use UnicaenApp\Controller\Plugin\Exception\IllegalStateException;
use UnicaenApp\Form\Fieldset\MultipageFormNavFieldset;
use UnicaenApp\Form\MultipageForm;
use Laminas\Form\Fieldset;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Laminas\Mvc\MvcEvent;
use Laminas\Mvc\Plugin\Prg\PostRedirectGet;
use Laminas\Router\Http\RouteMatch;
use Laminas\Session\Container;

/**
 * Plugin de contrôleur facilitant la mise en oeuvre d'un formulaire multi-pages.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MultipageFormPlugin extends AbstractPlugin
{
    /**
     * Navigation action constants
     */
    const ACTION_PREFIX   = 'prefix';
    const ACTION_NEXT     = 'next';
    const ACTION_PREVIOUS = 'previous';
    const ACTION_SUBMIT   = 'submit';
    const ACTION_CANCEL   = 'cancel';
    const ACTION_CONFIRM  = 'confirm';

    const NAV      = MultipageFormNavFieldset::NAME;
    const PREFIX   = MultipageFormNavFieldset::PREFIX_IN_NAME;
    const NEXT     = MultipageFormNavFieldset::NAME_NEXT;
    const PREVIOUS = MultipageFormNavFieldset::NAME_PREVIOUS;
    const SUBMIT   = MultipageFormNavFieldset::NAME_SUBMIT;
    const CANCEL   = MultipageFormNavFieldset::NAME_CANCEL;
    const CONFIRM  = MultipageFormNavFieldset::NAME_CONFIRM;

    const FIELDSET  = '_fieldset';

    /**
     * Navigation element names
     *
     * @var array
     */
    protected $navigationElements = [
        self::ACTION_PREFIX   => self::PREFIX,
        self::ACTION_NEXT     => self::NEXT,
        self::ACTION_PREVIOUS => self::PREVIOUS,
        self::ACTION_SUBMIT   => self::SUBMIT,
        self::ACTION_CANCEL   => self::CANCEL,
        self::ACTION_CONFIRM  => self::CONFIRM,
    ];

    /**
     * Session storage object
     *
     * @var Container
     */
    protected $sessionContainer;

    /**
     * The complete form instance
     *
     * @var MultipageForm
     */
    protected $form;

    /**
     * The mapping of fieldset name to controller action
     *
     * @var array
     */
    protected $fieldsetActions = [];

    /**
     * The order in which the fieldsets appear
     *
     * @var array
     */
    protected $fieldsetOrder = [];

    /**
     * The action that will be used for confirming the whole form values
     *
     * @var string
     */
    protected $confirmAction;

    /**
     * The action that will be used for processing the form
     *
     * @var string
     */
    protected $processAction;

    /**
     * The action for canceling the form
     *
     * @var string
     */
    protected $cancelAction;

    /**
     * Post Redirect Get pattern activation
     *
     * @var bool 
     */
    protected $usePostRedirectGet = false;
    
    /**
     * Response
     *
     * @var array|bool|Response
     */
    protected $prgResult;

    /**
     * @var bool
     */
    protected $reuseRequestQueryParams = false;

    /**
     * Point d'entrée.
     *
     * @param MultipageForm|null $form Formulaire multi-pages
     * @return self
     */
    public function __invoke(?MultipageForm $form = null)
    {
        if ($this->sessionContainer === null) {
            $this->sessionContainer = new Container('MultipageForm_' . get_class($this->getController()));
        }

        if ($form !== null) {
            $this->setForm($form);
        }

        return $this;
    }

    /**
     * Return response to start process.
     * 
     * @return Response
     */
    public function start()
    {
        if (null === $this->form) {
            throw new IllegalStateException('No form instance set.');
        }
        
        $this->clearSession();

        // action correspondant au fieldset de la 1ere étape
        $fieldsetName = $this->fieldsetOrder[0];
        $action = $this->fieldsetActions[$fieldsetName];
        
        return $this->redirect($action);
    }

    /**
     * Point d'entrée.
     * 
     * @return array|Response
     * @throws IllegalStateException
     */
    public function process()
    {
        if (null === $this->form) {
            throw new IllegalStateException('No form instance set.');
        }

        $action = $this->getControllerAction();
        $fieldsetNameForAction = $this->getFieldsetNameForAction($action);
        $activeFieldsetName = $this->getSessionActiveFieldsetName();

        if ($fieldsetNameForAction && $this->isFieldset($activeFieldsetName)) {

            // Si le fieldset demandé (d'après l'action de la requête) n'est pas le dernier fieldset actif,
            // on redirige vers l'action correspondant à ce dernier...
            if ($activeFieldsetName !== $fieldsetNameForAction) {
                return $this->redirect($this->fieldsetActions[$activeFieldsetName]);
            }

            return $this->handle();
        }

        //
        // Autres cas : redirection vers la première étape.
        //
        return $this->start();
    }

    /**
     * Handle the form
     *
     * @return array|Response
     */
    protected function handle()
    {
        // post redirect get pattern if required
        if ($this->usesPostRedirectGet()) {
            $prg = $this->getController()->plugin('prg'); /* @var $prg PostRedirectGet */
            $result = $prg();
            if ($result instanceof Response) {
                // returned a response to redirect us
                return $result;
            }
            $this->prgResult = $result;
        }

        $submittedAction = $this->getSubmittedAction();

        //
        // Aucun bouton Submit n'a été utilisé.
        //
        if ($submittedAction === false) {

            // Cas où l'action de la requête correspond au fieldset de récap/confirmation
            $controllerAction = $this->getControllerAction();
            if ($controllerAction === $this->confirmAction) {
                return $this->handleConfirmStep();
            }

            $currentFieldset = $this->getCurrentFieldset();
            $currentFieldset->populateValues($this->getSessionValues($currentFieldset->getName()));

            return [
                'form'      => $this->form,
                'fieldset'  => $currentFieldset,
                'stepIndex' => $this->getCurrentStepIndex(),
                'stepCount' => $this->getStepCount()];
        }

        //
        // Un bouton Submit a été utilisé.
        //
        return $this->handleSubmittedAction();
    }

    /**
     * Handle the confirmation step
     *
     * @return array|Response
     */
    protected function handleConfirmStep()
    {
        if (!$this->isValidForm()) {
            // Formulaire complet non valide : redirection vers la dernière étape valide
            $targetAction = $this->getLastValidAction();

            return $this->redirect($targetAction);
        }

        $form = $this->form;
        $data = $this->getFormSessionData();
        if (array_key_exists(self::FIELDSET, $data)) {
            unset($data[self::FIELDSET]);
        }
        $form->populateValues($data);

        $currentFieldset = $this->getCurrentFieldset();

        return [
            'form'      => $form,
            'fieldset'  => $currentFieldset,
            'stepIndex' => $this->getCurrentStepIndex(),
            'stepCount' => $this->getStepCount()
        ];
    }

    /**
     * @return array|Response
     */
    protected function handleSubmittedAction()
    {
        $submittedAction = $this->getSubmittedAction();
        $currentFieldset = $this->getCurrentFieldset();

        $postData = $this->getPostDataForCurrentFieldset();
        $currentFieldset->setValue($postData);
        $currentFieldset->populateValues($postData);

        $valid = $this->isCurrentFieldsetValid();
        $this->setSessionValues($currentFieldset, $valid);
        $this->setSessionSubmitAction($submittedAction);

        switch ($submittedAction) {

            // previous
            case $this->navigationElements[self::ACTION_PREVIOUS]:

                $position = array_search($currentFieldset->getName(), $this->fieldsetOrder);
                if ($position <= 0) {
                    $fieldset = $this->fieldsetOrder[0];
                } else {
                    $fieldset = $this->fieldsetOrder[$position - 1];
                }

                $this->setSessionActiveFieldsetName($fieldset);

                $targetAction = $this->fieldsetActions[$fieldset];

                return $this->redirect($targetAction);
                break;

            // submit
            case $this->navigationElements[self::ACTION_SUBMIT]:

                if (!$valid) {
                    return [
                        'form'      => $this->form,
                        'fieldset'  => $currentFieldset,
                        'stepIndex' => $this->getCurrentStepIndex(),
                        'stepCount' => $this->getStepCount()];
                }

                if ($this->form->hasConfirmFieldset()) {
                    $targetAction = $this->confirmAction;
                } else {
                    $targetAction = $this->processAction;
                }

                return $this->redirect($targetAction);
                break;

            case $this->navigationElements[self::ACTION_CONFIRM]:

                return $this->redirect($this->processAction);
                break;

            // cancel
            case $this->navigationElements[self::ACTION_CANCEL]:

                return $this->cancel();
                break;

            // next or other
            case $this->navigationElements[self::ACTION_NEXT]:
            default:

                if (!$valid) {
                    return [
                        'form'      => $this->form,
                        'fieldset'  => $currentFieldset,
                        'stepIndex' => $this->getCurrentStepIndex(),
                        'stepCount' => $this->getStepCount()];
                }

                $position = intval(array_search($currentFieldset->getName(), $this->fieldsetOrder));

                $fieldsetCount = count($this->fieldsetOrder);
                if ($position === $fieldsetCount - 1) {
                    $fieldset = $this->fieldsetOrder[$fieldsetCount - 1];
                } else {
                    $fieldset = $this->fieldsetOrder[$position + 1];
                }

                $this->setSessionActiveFieldsetName($fieldset);

                $targetAction = $this->fieldsetActions[$fieldset];

                return $this->redirect($targetAction);
                break;
        }
    }
    
    /**
     * Cancel multipage form process.
     * 
     * @return Response
     */
    public function cancel()
    {
        $this->clearSession();

        $target = $this->cancelAction;

        if (!$target) {
            if (isset($this->fieldsetOrder[0])) {
                $fieldset = $this->fieldsetOrder[0]; // first step fieldset
                $target = $this->fieldsetActions[$fieldset];
                $this->setSessionActiveFieldsetName($fieldset);
            }
        }

        return $this->redirect($target);
    }

    /**
     * @return string
     */
    protected function getControllerAction()
    {
        /** @var MvcEvent $event */
        $event = $this->getController()->getEvent();
        /** @var RouteMatch $routeMatch */
        $routeMatch = $event->getRouteMatch();

        return $routeMatch->getParam('action');
    }

    /**
     * Determine if a form has been validated
     *
     * @param string $fieldsetName
     * @return boolean
     */
    protected function isValidFieldset($fieldsetName)
    {
        // Loop through the fieldset => action mapping
        foreach ($this->fieldsetActions as $name => $action) {
            
            // If this loop hasn't found an invalid action yet, and the currentAction and action match
            // we can assume this is the currently active fieldset.
            if ($fieldsetName === $name) {
                break;
            }

            // If the provided fieldset isn't complete yet, we're too far.
            // This means that the provided action is invalid.
            if (!$this->isCompleteFieldset($name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine if all fieldsets have been validated
     *
     * @return boolean
     */
    protected function isValidForm()
    {
        foreach ($this->fieldsetActions as $name => $action) {
            if (!$this->isValidFieldset($name)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Retrieve current last valid action
     *
     * @return string
     */
    protected function getLastValidAction()
    {
        $action = null;
        $fieldsetActions = $this->fieldsetActions;

        // ignore le fieldset de récap/confirmation
        if (array_key_exists(MultipageForm::FIELDSET_NAME_CONFIRM, $fieldsetActions)) {
            unset ($fieldsetActions[MultipageForm::FIELDSET_NAME_CONFIRM]);
        }

        foreach ($fieldsetActions as $fieldsetName => $action) {
            // If the provided form isn't complete yet, we're too far.
            // This means that the provided action is invalid.
            if (!$this->isCompleteFieldset($fieldsetName)) {
                break;
            }
        }

        if ($action === null) {
            throw new IllegalStateException('Fieldset -> action mapping is empty');
        }

        return $action;
    }

    /**
     * Determine if a fieldset has been submitted and successfully validated
     *
     * @param string $fieldsetName
     * @return mixed
     */
    protected function isCompleteFieldset($fieldsetName)
    {
        // Retrieve the validation state from the session
        return
            isset($this->sessionContainer->valid[$fieldsetName]) &&
            $this->sessionContainer->valid[$fieldsetName];
    }

    /**
     * Set the action used for confirming the complete form
     *
     * @param string $action
     * @return self
     */
    public function setConfirmAction($action)
    {
        $this->confirmAction = $action;
        return $this;
    }

    /**
     * Set the action used for processing the complete form
     *
     * @param string $action
     * @return self
     */
    public function setProcessAction($action)
    {
        $this->processAction = $action;
        return $this;
    }

    /**
     * Set a custom cancel action
     *
     * @param string $action
     * @return self
     */
    public function setCancelAction($action)
    {
        $this->cancelAction = $action;
        return $this;
    }

    /**
     * Set sequence of actions.
     *
     * @param array $fieldsetActionMapping fieldset name => action name
     * @return self
     */
    protected function setFieldsetActionMapping(array $fieldsetActionMapping = [])
    {
        $fieldsetActions = [];
        $fieldsetOrder   = [];

        foreach ($fieldsetActionMapping as $key => $value) {
            $fieldsetActions[$key] = $value;
            $fieldsetOrder[] = $key;
        }

        $this->fieldsetActions = $fieldsetActions;
        $this->fieldsetOrder   = $fieldsetOrder;
        
        // Reset the session if this is the first time the forms/actions are mapped
        if (null === $this->sessionContainer->valid || !array_key_exists($fieldsetOrder[0], $this->sessionContainer->valid)) {
            $this->clearSession();
        }

        return $this;
    }

    /**
     * Set values for an action
     *
     * @param Fieldset $fieldset
     * @param boolean $valid
     * @return self
     */
    protected function setSessionValues(Fieldset $fieldset, $valid = false)
    {
        $fieldsetName = $fieldset->getName();
        
        // Get the form values and their element names
        $formValues = $fieldset->getValue();
        $formKeys = array_keys($formValues);
        
        // Loop through the element names to see if there are action elements (default prefixed with _)
        foreach ($formKeys as $key) {
            // If an element with the action prefix is found, remove it from the array.
            if (strpos($key, $this->navigationElements[self::ACTION_PREFIX]) === 0) {
                // We don't want to store actions in the session.
                unset($formValues[$key]);
            }
        }
        
        // Elaborate setup to write some values to the session arrays
        // This is needed to work around a bug/feature in PHP 5.2.0 iirc
        $validForms = $this->sessionContainer->valid;
        $sessionFormValues = $this->sessionContainer->value;
        
        $validForms[$fieldsetName] = (bool) $valid;
        $sessionFormValues[$fieldsetName] = /*array_key_exists($fieldsetName, $formValues) ? $formValues[$fieldsetName] :*/ $formValues;
        $sessionFormValues[self::FIELDSET] = $fieldsetName;

        // Write the validation state and form values to the session
        $this->sessionContainer->valid = $validForms;
        $this->sessionContainer->value = $sessionFormValues;

        return $this;
    }

    /**
     * @param string $submitAction
     */
    private function setSessionSubmitAction($submitAction)
    {
        $this->sessionContainer->action = $submitAction;
    }

    /**
     * Retrieve fieldset values from the session
     *
     * @param string $fieldsetName
     * @return mixed
     */
    protected function getSessionValues($fieldsetName)
    {
        return isset($this->sessionContainer->value[$fieldsetName]) ?
                $this->sessionContainer->value[$fieldsetName] :
                [];
    }

    /**
     * Set the form instance
     *
     * @param MultipageForm $form
     * @return self
     */
    public function setForm(MultipageForm $form)
    {
        $this->form = $form;

        // Check if we have some fieldsets
        if (!($fieldsets = $this->form->getFieldsets())) {
            throw new IllegalStateException('The form needs to have fieldsets.');
        }
        // Loop through all the fieldsets and check if they all have a name
        foreach ($fieldsets as $fieldset) { /* @var $fieldset Fieldset */
            if (!($fieldset->getName())) {
                throw new IllegalStateException('A fieldset needs to have a name.');
            }
        }
        
        $this->setFieldsetActionMapping($this->form->getFieldsetActionMapping());

        if (!$this->confirmAction) {
            $this->setConfirmAction($this->form->getConfirmAction());
        }
        if (!$this->processAction) {
            $this->setProcessAction($this->form->getProcessAction());
        }
        if (!$this->cancelAction) {
            $this->setCancelAction($this->form->getCancelAction());
        }

        return $this;
    }

    /**
     * Retourne l'instance du formulaire complet.
     *
     * @return MultipageForm
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Retourne le fieldset courant.
     *
     * @return Fieldset
     */
    public function getCurrentFieldset()
    {
        $fieldsetName = $this->getSessionActiveFieldsetName();

        return $this->getFieldset($fieldsetName);
    }

    /**
     * Get a fieldset by name.
     *
     * @param string $fieldsetName
     * @return Fieldset
     */
    protected function getFieldset($fieldsetName)
    {
        if (null === $this->form) {
            throw new IllegalStateException('No form instance set.');
        }

        $fieldsets = $this->form->getFieldsets();

        if (! isset($fieldsets[$fieldsetName])) {
            throw new IllegalStateException("Aucun fieldset trouvé avec ce nom : '$fieldsetName'");
        }

        return $fieldsets[$fieldsetName];
    }

    /**
     * Get all data from the fieldsets from the session.
     * Including the fieldset's name where we come from.
     *
     * @return array
     */
    public function getFormSessionData()
    {
        $formData = [];

        foreach ($this->fieldsetActions as $fieldsetName => $action) {
            $formData[$fieldsetName] = $this->getSessionValues($fieldsetName);
        }

        // Name of the fieldset where we come from.
        $formData[self::FIELDSET] = $this->sessionContainer->value[self::FIELDSET] ?? null;

        return $formData;
    }

    /**
     * Retourne les données POSTées.
     *
     * @return array
     */
    protected function getPostData()
    {
        if ($this->usesPostRedirectGet() && is_array($this->prgResult)) {
            return $this->prgResult;
        }

        /** @var Request $request */
        $request = $this->getController()->getRequest();
        if ($request->isPost()) {
            return $request->getPost()->toArray();
        }

        return [];
    }

    /**
     * Retourne les données POSTées concernant le fieldset spécifié.
     *
     * @param Fieldset $fieldset
     * @return array
     */
    protected function getPostDataForFieldset(Fieldset $fieldset)
    {
        $postData = $this->getPostData();
        if (empty($postData)) {
            return [];
        }

        $currentFieldsetPostData = isset($postData[$fieldset->getName()]) ?
            $postData[$fieldset->getName()] :
            null;

        // NB: $currentFieldsetPostData doit être un tableau, sinon il s'agit de la valeur d'un champ ayant le même
        // 'name' que le fieldset auquel il appartient.
        if ($currentFieldsetPostData && is_array($currentFieldsetPostData)) {
            $postData = $currentFieldsetPostData;
        }

        $fieldsets = $fieldset->getFieldsets();
        $elements = $fieldset->getElements();
        $postDataForFieldset = array_merge(
            array_intersect_key($postData, $fieldsets),
            array_intersect_key($postData, $elements)
        );

        // Les boutons d'action peuvent être au sein d'un élément composite '_nav',
        // on extrait donc le bouton utilisé.
        if (isset($postData[self::NAV])) {
            $postDataForFieldset += $postData[self::NAV];
            unset($postDataForFieldset[self::NAV]);
        }

        return $postDataForFieldset;
    }

    /**
     * Retourne les données POSTées concernant le fieldset courant.
     *
     * @return array
     */
    protected function getPostDataForCurrentFieldset()
    {
        $currentFieldset = $this->getCurrentFieldset();

        return $this->getPostDataForFieldset($currentFieldset);
    }

    /**
     * Spécifie que les paramètres GET de la requête courante doivent être conservés
     * lors de la prochaine redirection.
     *
     * @param bool $reuseRequestQueryParams
     * @return $this
     */
    public function setReuseRequestQueryParams($reuseRequestQueryParams = true)
    {
        $this->reuseRequestQueryParams = $reuseRequestQueryParams;

        return $this;
    }

    /**
     * Use the redirector plugin to navigate the controller
     *
     * @param string $action
     * @return Response
     */
    protected function redirect($action)
    {
        /** @var MvcEvent $event */
        $event = $this->getController()->getEvent();
        $routeMatch = $event->getRouteMatch();
        $controller = $routeMatch->getParam('controller');

        /**
         * Attention! 
         * 
         * On peut se retrouver avec plusieurs clés 'Location' dans le header de la réponse HTTP, 
         * ce qui pose problème dans les tests unitaires car Response::getHeaders()->get('Location')->getUri()
         * ne renvoit que la première 'Location' du header.
         * 
         * 1/ On ne fait pas appel à $this->getController()->plugin('redirect'), mais on
         * instancie le plugin Redirect à la main.
         * 
         * 2/ On remet à zéro le header de la réponse systématiquement.
         */
        
        $event->getResponse()->getHeaders()->clearHeaders();
        
        $redirector = new \Laminas\Mvc\Controller\Plugin\Redirect();
        $redirector->setController($this->getController());
        $params = ['action' => $action, 'controller' => $controller];
        $options = $this->reuseRequestQueryParams ? ['query' => $this->getController()->params()->fromQuery()] : [];
        $response = $redirector->toRoute($routeMatch->getMatchedRouteName(), $params, $options, true);
        $response->setStatusCode(303);
        
        return $response;
    }

    /**
     * Get the action used to submit the form
     *
     * @return string|bool
     */
    protected function getSubmittedAction()
    {
        $postData = $this->getPostDataForCurrentFieldset();

        if (!empty($postData)) {
            $submittedAction = null;
            $formDataKeys = array_keys($postData);
            foreach ($formDataKeys as $key) {
                if (strpos($key, $this->navigationElements[self::ACTION_PREFIX]) === 0) {
                    $submittedAction = $key;
                    break;
                }
            }
            if ($submittedAction && in_array($submittedAction, $this->navigationElements)) {
                return $submittedAction;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function isCurrentFieldsetValid()
    {
        $postData = $this->getPostData();

        $currentFieldset = $this->getCurrentFieldset();

        $this->form->setValidationGroup([$currentFieldset->getName()]);
        $this->form->setData($postData);

        return $this->form->isValid();
    }

    /**
     * Return the current step index.
     *
     * @return int 1 <= index <= step count
     */
    protected function getCurrentStepIndex()
    {
        $indexes = array_keys($this->fieldsetOrder, $this->getSessionActiveFieldsetName());

        return $indexes ? 1 + $indexes[0] : 0;
    }

    /**
     * Return the number of steps.
     *
     * @return int
     */
    protected function getStepCount()
    {
        return count($this->fieldsetActions);
    }

    /**
     * Retourne le nom du fieldset correspondant à l'action spécifiée, s'il existe.
     *
     * @param string $action Nom de l'action
     * @return string|null le nom du fieldset d'étape s'il existe ; `null` sinon
     */
    protected function getFieldsetNameForAction($action)
    {
        return array_search($action, $this->fieldsetActions) ?: null;
    }

    /**
     * Check if the action is an action for this form.
     *
     * @param string $fieldsetName
     * @return boolean
     */
    protected function isFieldset($fieldsetName)
    {
        return array_key_exists($fieldsetName, $this->fieldsetActions);
    }

    /**
     * Get the active fieldset name
     *
     * @return string
     */
    protected function getSessionActiveFieldsetName()
    {
        return $this->sessionContainer->active;
    }

    /**
     * Set the active fieldset name
     *
     * @param string $fieldsetName
     * @return self
     */
    protected function setSessionActiveFieldsetName($fieldsetName)
    {
        $this->sessionContainer->active = $fieldsetName;

        return $this;
    }

    /**
     * Reset all session data
     *
     * @return self
     */
    public function clearSession()
    {
        // Create two brand new arrays
        $valid = [];
        $value = [];
        
        // Loop through the formnames, so we can reset their session data
        foreach ($this->fieldsetOrder as $name) {
            $valid[$name] = false;
            $value[$name] = [];
        }

        // Write all default variables to the session
        $this->sessionContainer->valid  = $valid;
        $this->sessionContainer->value  = $value;
        $this->sessionContainer->active = $this->fieldsetOrder[0];
        $this->sessionContainer->action = '';

        return $this;
    }

    /**
     * Returns session persistance container
     * 
     * @return Container
     */
    public function getSessionContainer()
    {
        return $this->sessionContainer;
    }

    /**
     * Sets session persistance container
     * 
     * @param Container $container
     * @return self
     */
    public function setSessionContainer(Container $container)
    {
        $this->sessionContainer = $container;

        return $this;
    }
    
    /**
     * Return Post-Redirect-Get pattern activation flag
     * 
     * @return bool
     */
    public function usesPostRedirectGet()
    {
        return $this->usePostRedirectGet;
    }
    
    /**
     * Set Post-Redirect-Get pattern activation flag
     * 
     * @param bool $usePostRedirectGet
     * @return self
     */
    public function setUsePostRedirectGet($usePostRedirectGet = true)
    {
        $this->usePostRedirectGet = $usePostRedirectGet;

        return $this;
    }
}
<?php
namespace UnicaenApp\Controller\Plugin;

use UnicaenApp\Traits\MessageAwareInterface;
use UnicaenApp\Traits\MessageAwareTrait;
use UnicaenApp\View\Helper\Messenger as MessengerViewHelper;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Aide de vue permettant de stocker une liste de messages d'information de différentes sévérités 
 * et de générer le code HTML pour les afficher (affublés d'un icône correspondant à leur sévérité). 
 * 
 * Possibilité d'importer les messages du FlashMessenger pour les mettre en forme de la même manière.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MessengerPlugin extends AbstractPlugin implements MessageAwareInterface
{
    use MessageAwareTrait;
    
    /**
     * @var MessengerViewHelper
     */
    protected $messengerViewHelper;

    /**
     * @param MessengerViewHelper $messengerViewHelper
     */
    public function setMessengerViewHelper(MessengerViewHelper $messengerViewHelper)
    {
        $this->messengerViewHelper = $messengerViewHelper;
    }

    /**
     * Helper entry point.
     *
     * @return MessengerViewHelper
     */
    public function __invoke()
    {
        return $this->messengerViewHelper;
    }
}
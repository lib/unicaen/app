<?php

namespace UnicaenApp\Controller\Plugin;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of LdapPeopleServiceFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapPeopleServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $pluginManager)
    {
        return $this->__invoke($pluginManager, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new LdapPeopleService($container->get('ldap_people_service'));
    }
}
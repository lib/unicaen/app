<?php

namespace UnicaenApp\Controller\Plugin;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of LdapStructureServiceFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapStructureServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $pluginManager)
    {
        return $this->__invoke($pluginManager, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new LdapStructureService($container->get('ldap_structure_service'));
    }
}
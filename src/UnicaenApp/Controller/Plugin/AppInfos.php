<?php

namespace UnicaenApp\Controller\Plugin;

use DateTime;
use Traversable;
use UnicaenApp\Exception\LogicException;
use Laminas\Config\Config;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Plugin de contrôleur fournissant les infos sur l'application (nom, description, version, etc.)
 * 
 * @author Unicaen
 */
class AppInfos extends AbstractPlugin
{
    /**
     * @var Config|Traversable|array
     */
    protected $config;

    /**
     * @var array
     */
    protected $validOptions = [
        'nom',
        'desc',
        'version',
        'date',
        'contact'
    ];
    
    /**
     * Constructeur.
     * 
     * @param Config|Traversable|array $config
     */
    public function __construct($config = null)
    {
        if ($config) {
            $this->setConfig($config);
        }
    }

    /**
     * Point d'entrée.
     * 
     * @return AppInfos
     */
    public function __invoke()
    {
        return $this;
    }
    
    /**
     * Spécifie les informations concernant l'application.
     * 
     * @param Config|Traversable|array $config
     * @return AppInfos
     */
    public function setConfig($config)
    {
        $this->config = $this->prepareConfig($config);

        return $this;
    }

    /**
     * Retourne les informations concernant l'application.
     * 
     * @return Config
     */
    private function getConfig()
    {
        if (null === $this->config) {
            $this->config = new Config([]);
        }

        return $this->config;
    }

    /**
     * Vérifie la validité des informations fournies.
     * 
     * @param Config|Traversable|array $config
     * @return Config
     */
    private function prepareConfig($config)
    {
        if ($config instanceof Config) {
            $config = $config->toArray();
        }
        else if (!is_array($config)) {
            throw new LogicException("La configuration spécifiée est invalide.");
        }
        if (!$config) {
            throw new LogicException("La configuration spécifiée est vide.");
        }

        $valid = [];
        foreach ($config as $key => $value) {
            if (in_array($key, $this->validOptions)) {
                $valid[$key] = $value;
            }
        }

        return new Config($valid);
    }

    /**
     * Retourne le nom de l'application.
     * 
     * @return string
     */
    public function getNom()
    {
        return $this->getConfig()->get('nom');
    }

    /**
     * Retourne la description ou sous-titre de l'application.
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->getConfig()->get('desc');
    }

    /**
     * Retourne la version de l'application.
     * 
     * @return string Exemple: 1.1.0
     */
    public function getVersion()
    {
        return $this->getConfig()->get('version');
    }

    /**
     * Retourne la date de version de l'application.
     * 
     * @return DateTime
     */
    public function getDate()
    {
        return DateTime::createFromFormat('d/m/Y', $this->getConfig()->get('date'));
    }

    /**
     * Retourne les coordonnées de contact, éventuellement filtrées par type.
     *
     * @param string $key
     * @return array Retourne un tableau quoiqu'il arrive, exemples:
     *               ['mail' => "adresse@unicaen.fr", 'tel' => "01 02 03 04 05"] si $key === null
     *               ["adresse@unicaen.fr"] si $key === 'mail'
     *               [] si $key === 'fax'
     */
    public function getContact($key = null)
    {
        $contacts = $this->getConfig()->get('contact');
        $contacts = $contacts ? $contacts->toArray() : [];

        if (null === $key) {
            return $contacts;
        }

        return isset($contacts[$key]) ? (array)$contacts[$key] : [];
    }
}
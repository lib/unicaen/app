<?php

namespace UnicaenApp\Controller\Plugin\Upload;

use Psr\Container\ContainerInterface;
use Laminas\Form\FormElementManager;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class UploaderPluginFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $helperPluginManager)
    {
        return $this->__invoke($helperPluginManager, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var FormElementManager $fem */
        $fem = $container->get('FormElementManager');

        $plugin = new UploaderPlugin();
        $plugin->setFormElementManager($fem);

        return $plugin;
    }
}
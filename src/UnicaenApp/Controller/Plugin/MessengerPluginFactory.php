<?php

namespace UnicaenApp\Controller\Plugin;

use Psr\Container\ContainerInterface;
use UnicaenApp\View\Helper\Messenger;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MessengerPluginFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $helperPluginManager)
    {
        return $this->__invoke($helperPluginManager, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var Messenger $messengerViewHelper */
        $messengerViewHelper = $container->get('ViewHelperManager')->get('messenger');

        $plugin = new MessengerPlugin();
        $plugin->setMessengerViewHelper($messengerViewHelper);

        return $plugin;
    }
}
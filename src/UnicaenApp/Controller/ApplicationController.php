<?php

namespace UnicaenApp\Controller;

use UnicaenApp\Controller\Plugin\AppInfos;
use Laminas\Mvc\Controller\AbstractActionController;

/**
 * Contrôleur fournissant les actions de base d'une application.
 *
 * @method AppInfos appInfos()
 * @author Unicaen
 */
class ApplicationController extends AbstractActionController
{
    public function aproposAction()
    {
    }

    public function contactAction()
    {
    }

    public function planAction()
    {
    }

    public function refreshSessionAction()
    {
        exit;
    }

    public function maintenanceAction()
    {
        $this->layout('layout/maintenance');

        return false;
    }

    public function iconsAction()
    {
        $cssFilepath = __DIR__ . '/../../../public/unicaen/app/css/unicaen-icon.css';
        $cssContent = file_get_contents($cssFilepath);

        preg_match_all('/\.icon\.icon-(.+):before/', $cssContent, $matches);

        return [
            'names' => $matches[1],
        ];
    }
}
<?php

namespace UnicaenApp\Controller;

use Psr\Container\ContainerInterface;

class CacheControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CacheController
     */
    public function __invoke(ContainerInterface $container)
    {
        $controller = new CacheController();

        $controller->setConfig($container->get('config'));

        return $controller;
    }
}
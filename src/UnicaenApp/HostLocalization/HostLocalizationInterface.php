<?php

namespace UnicaenApp\HostLocalization;

interface HostLocalizationInterface
{
    /**
     * Renvoie true si le'utilisateur utilise une machine connectée au réseau de l'établissement
     * ou bien si la machine est hors réseau
     *
     * @return bool
     */
    public function inEtablissement(): bool;
}
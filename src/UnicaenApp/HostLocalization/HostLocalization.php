<?php

namespace UnicaenApp\HostLocalization;


class HostLocalization implements HostLocalizationInterface
{

    protected $activated;

    protected $proxies;

    protected $reverseProxies;

    protected $masqueIp;



    /**
     * Renvoie true si le'utilisateur utilise une machine connectée au réseau de l'établissement
     * ou bien si la machine est hors réseau
     *
     * @return bool
     */

    public function __construct($activated, $masqueIp, $proxies, $reverseProxies)
    {
        $this->activated      = $activated;
        $this->proxies        = $proxies;
        $this->reverseProxies = $reverseProxies;
        $this->masqueIp       = $masqueIp;
    }



    /**
     * Retourne selon les règles définies en config si on est dans l'établissement ou à l'extérieur.
     *
     * @return bool
     */
    public function inEtablissement(): bool
    {
        if (!$this->activated || empty($this->masqueIp)) {
            return true;
        }
        $ip        = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $forwarded = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';

        // Si on est en direct et en interne sans passer par le proxy (pas de redirection)
        if (0 === strpos($ip, $this->masqueIp) && $forwarded === '') return true;

        // Si on est en interne, que l'on sort puis on re-rentre en passant par le reverse proxy
        if (in_array($ip, $this->reverseProxies) && 0 === strpos($forwarded, $this->masqueIp)) return true;

        // Si on est en interne, que l'on passe par le proxy en interne
        if (in_array($ip, $this->proxies) && 0 === strpos($forwarded, $this->masqueIp)) return true;

        // Sinon, on vient de l'extérieur
        return false;
    }
}
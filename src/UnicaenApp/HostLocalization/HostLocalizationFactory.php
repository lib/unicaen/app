<?php

namespace UnicaenApp\HostLocalization;

use Psr\Container\ContainerInterface;

class HostLocalizationFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config         = $container->get('config');
        $activated      = $config['unicaen-app']['hostlocalization']['activated'];
        $proxies        = $config['unicaen-app']['hostlocalization']['proxies'];
        $reverseProxies = $config['unicaen-app']['hostlocalization']['reverse-proxies'];
        $masqueIp       = $config['unicaen-app']['hostlocalization']['masque-ip'];

        $service = new HostLocalization($activated, $masqueIp, $proxies, $reverseProxies);


        return $service;
    }
}
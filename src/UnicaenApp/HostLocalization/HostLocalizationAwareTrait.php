<?php

namespace UnicaenApp\HostLocalization;


/**
 * Description of HostLocalizationAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait HostLocalizationAwareTrait
{
    /**
     * @var HostLocalizationInterface
     */
    private $hostLocalization;



    /**
     * @param HostLocalizationInterface $hostLocalization
     *
     * @return self
     */
    public function setHostLocalization(HostLocalizationInterface $hostLocalization)
    {
        $this->hostLocalization = $hostLocalization;

        return $this;
    }



    /**
     * @return HostLocalizationInterface
     */
    public function getHostLocalization(): HostLocalizationInterface
    {
        return $this->hostLocalization;
    }
}
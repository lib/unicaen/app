<?php

namespace UnicaenApp\Filter;

use Laminas\Filter\AbstractFilter;
use UnicaenApp\Form\Element\SearchAndSelect;

class SearchAndSelectFilter extends AbstractFilter
{
    /**
     * @inheritDoc
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        if (!array_key_exists(SearchAndSelect::ID_ELEMENT_NAME, $value) || $value[SearchAndSelect::ID_ELEMENT_NAME] === '') {
            return null;
        }

        return $value;
    }
}
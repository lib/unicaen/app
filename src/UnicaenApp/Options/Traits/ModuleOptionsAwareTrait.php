<?php

namespace UnicaenApp\Options\Traits;

use UnicaenApp\Options\ModuleOptions;
use RuntimeException;

/**
 * Description of ModuleOptionsAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait ModuleOptionsAwareTrait
{
    /**
     * @var ModuleOptions
     */
    private $moduleOptions;



    /**
     * @param ModuleOptions $moduleOptions
     *
     * @return self
     */
    public function setModuleOptions(ModuleOptions $moduleOptions)
    {
        $this->moduleOptions = $moduleOptions;

        return $this;
    }



    /**
     * @return ModuleOptions
     * @throws RuntimeException
     */
    public function getModuleOptions()
    {
        return $this->moduleOptions;
    }
}
<?php
namespace UnicaenApp\Form\View\Helper;

use Application\View\Renderer\PhpRenderer;
use UnicaenApp\Form\Fieldset\MultipageFormConfirmFieldset;
use UnicaenApp\Form\MultipageForm;
use Laminas\Form\Fieldset;
use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Exception\InvalidArgumentException;
use Laminas\View\Resolver\AggregateResolver;
use Laminas\View\Resolver\TemplatePathStack;

/**
 * Aide de vue générant le code HTML du récapitulatif des données saisies dans un formulaire multi-pages.
 *
 * NB: L'élément de navigation n'est pas généré.
 *
 * @property PhpRenderer $view
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MultipageFormRecap extends AbstractHelper
{
    /**
     * @var MultipageForm
     */
    protected $form;

    /**
     * @var string
     */
    protected $partial = 'multipage-form-recap';

    /**
     * @param string $partial
     * @return MultipageFormRecap
     */
    public function setPartial(string $partial): MultipageFormRecap
    {
        $this->partial = $partial;

        return $this;
    }

    /**
     * Point d'entrée.
     *
     * @param MultipageForm|null $form
     * @return self
     */
    public function __invoke(?MultipageForm $form = null)
    {
        if ($form === null) {
            if (! $this->view->get('form')) {
                throw new InvalidArgumentException("Aucun formulaire trouvé dans la vue.");
            }
            $form = $this->view->get('form');
        }
        if (!$form instanceof MultipageForm) {
            throw new InvalidArgumentException("Le formulaire spécifié n'est pas du type attendu.");
        }
        if (!count($form->getFieldsets())) {
            throw new InvalidArgumentException("Le formulaire spécifié ne possède aucun fieldset.");
        }

        $this->form = $form;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Génère le code HTML.
     *
     * @return string
     */
    public function render(): string
    {
        $data = [];
        foreach($this->form->getFieldsets() as $fieldset) { /* @var Fieldset $fieldset */
            if ($fieldset instanceof MultipageFormConfirmFieldset) {
                continue;
            }
            $labelsAndValues = $this->form->getLabelsAndValues($fieldset);
            $data[$fieldset->getName()] = [
                'fieldset' => $fieldset,
                'labelsAndValues' => $labelsAndValues,
            ];
        }

        $variables = [
            'data' => $data,
        ];

        /** @var AggregateResolver $resolver */
        $resolver = $this->view->resolver();
        $resolver->attach(new TemplatePathStack(['script_paths' => [__DIR__ . '/partial']]));

        return $this->view->partial($this->partial, $variables);
    }
}
<?php

namespace UnicaenApp\Form\View\Helper;

use Laminas\ServiceManager\ServiceLocatorInterface as ContainerInterface;

/**
 * Description of FormControlGroupFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class FormControlGroupFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return FormControlGroup
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $vh = new FormControlGroup();

        return $vh;
    }
}
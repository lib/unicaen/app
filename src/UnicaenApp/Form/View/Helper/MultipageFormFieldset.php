<?php

namespace UnicaenApp\Form\View\Helper;

use Application\View\Renderer\PhpRenderer;
use UnicaenApp\Form\Fieldset\MultipageFormConfirmFieldset;
use UnicaenApp\Form\Fieldset\MultipageFormNavFieldset;
use UnicaenApp\Form\MultipageForm;
use Laminas\Form\Element;
use Laminas\Form\Fieldset;
use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Exception\InvalidArgumentException;

/**
 * Aide de vue générant le code HTML d'un fieldset de formulaire multi-pages.
 *
 * @property PhpRenderer $view
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MultipageFormFieldset extends AbstractHelper
{
    /**
     * @var Fieldset
     */
    private $fieldset;

    /**
     * @var string
     */
    private $partial;

    /**
     * Point d'entrée.
     *
     * @param Fieldset|null $fieldset
     * @return self
     */
    public function __invoke(?Fieldset $fieldset = null)
    {
        if ($fieldset === null) {
            if (! $this->view->get('fieldset')) {
                throw new InvalidArgumentException("Aucun fieldset trouvé dans la vue.");
            }
            $fieldset = $this->view->get('fieldset');

            if (!$fieldset instanceof Fieldset) {
                throw new InvalidArgumentException("Le fieldset spécifié n'est pas valide.");
            }
        }

        $this->fieldset = $fieldset;

        return $this;
    }

    /**
     * Spécifie le fichier "partial" à utiliser pour générer le code HTML du fieldset.
     *
     * @param string $partial
     * @return self
     */
    public function setPartial($partial)
    {
        $this->partial = $partial;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Génère le code HTML du fieldset à l'intérieur d'une balise "form".
     *
     * @return string
     */
    public function render(): string
    {
        return
            $this->renderTitle() . PHP_EOL . PHP_EOL .
            $this->renderContent();
    }

    /**
     * @return string
     */
    protected function renderTitle()
    {
        $step  = $this->translate("Étape");
        $on    = $this->translate("sur");

        $stepIndex = $this->view->get('stepIndex');
        $stepCount = $this->view->get('stepCount');

        $title = "$step $stepIndex $on $stepCount";

        if ($this->fieldset instanceof MultipageFormConfirmFieldset) {
            $title .= " : " . $this->translate("récapitulatif et confirmation");
        }

        return "<h2>$title</h2>";
    }

    /**
     * @return string
     */
    protected function renderContent()
    {
        // Passe éventuellement la main à l'aide de vue spécialisée
        if ($this->fieldset instanceof MultipageFormConfirmFieldset) {
            $multipageFormRecapHelper = $this->view->multipageFormRecap();
            $multipageFormRecapHelper->setPartial($this->partial);

            $markup = $multipageFormRecapHelper->render();
            $class = 'multipage-form-recap';
        }
        else {
            $markup = $this->renderFieldset();
            $class = '';
        }

        $template = <<<EOS
<form method="post" class="%s">
    %s
    %s
</form>
EOS;
        return sprintf($template,
            $class,
            $markup,
            $this->renderNavigation());
    }

    /**
     * @return MultipageFormNav
     */
    protected function renderNavigation()
    {
        $navElement = $this->getNavigationElement();

        return $this->view->multipageFormNav($navElement);
    }

    /**
     * Génère le code HTML du fieldset.
     * Si un partial est fourni, il est utilisé.
     *
     * @return string
     */
    private function renderFieldset()
    {
        if ($this->partial) {
            return $this->view->partial($this->partial);
        }

        $template = <<<EOS
<fieldset>
    <legend>%s</legend>
    %s
</fieldset>
EOS;

        /** @var Element $element */
        $html = '';
        foreach ($this->fieldset->getIterator() as $element) {
            if ($element instanceof MultipageFormNavFieldset) {
                // La navigation est générée ailleurs.
                continue;
            }
            $html .= $this->view->multipageFormRow($element);
        }

        $legend = $this->translate($this->fieldset->getLabel());

        return sprintf($template, $legend, $html);
    }

    /**
     * @return MultipageFormNavFieldset
     */
    protected function getNavigationElement()
    {
        /** @var MultipageFormNavFieldset $element */
        $element = $this->fieldset->get(MultipageForm::FIELDSET_NAME_NAV);

        return $element;
    }

    /**
     * @param string $text
     * @return string
     */
    protected function translate(string $text)
    {
        if (($translator = $this->getTranslator()) !== null) {
            return $translator->translate($text, $this->getTranslatorTextDomain());
        }

        return $text;
    }

    /**
     * Retourne l'aide de vue de génération du code HTML de la navigation.
     *
     * @return MultipageFormNav
     */
    public function multipageFormNav()
    {
        /** @var MultipageFormNav $helper */
        $helper = $this->view->plugin('multipageFormNav');

        return $helper;
    }
}
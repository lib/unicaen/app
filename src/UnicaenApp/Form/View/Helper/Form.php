<?php

namespace UnicaenApp\Form\View\Helper;

use Laminas\Form\Element;
use Laminas\Form\FieldsetInterface;
use Laminas\Form\FormInterface;
use Laminas\Form\View\Helper\Form as ZVHForm;

class Form extends ZVHForm {

    /**
     * Render a form from the provided $form,
     *
     * @param  FormInterface $form
     * @return string
     */
    public function render(FormInterface $form): string
    {
        if (method_exists($form, 'prepare')) {
            $form->prepare();
        }

        $formContent = '';

        foreach ($form as $element) {
            if ($element instanceof FieldsetInterface) {
                $formContent .= $this->getView()->formCollection($element);
            } elseif ($element instanceof Element\Submit || $element instanceof Element\Button){
                $formContent.= $this->getView()->formRow($element);
            }else {
                $formContent.= $this->getView()->formControlGroup($element);
            }
        }

        return $this->openTag($form) . $formContent . $this->closeTag();
    }

}
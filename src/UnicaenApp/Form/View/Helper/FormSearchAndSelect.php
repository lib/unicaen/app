<?php
namespace UnicaenApp\Form\View\Helper;

use Laminas\Form\Element\Text;
use Laminas\Form\ElementInterface;
use Laminas\Form\Exception\InvalidElementException;
use Laminas\Form\View\Helper\FormText;
use UnicaenApp\Form\Element\SearchAndSelect;

/**
 * Aide de vue générant le code HTML de l'élément de formulaire {@see \UnicaenApp\Form\Element\SearchAndSelect}.
 */
class FormSearchAndSelect extends FormText
{
    protected SearchAndSelect $element;
    protected Text $autocompleteElement;
    protected ?string $autocompleteSource = null;
    protected int $autocompleteMinLength = 2;
    protected string $spinnerSource = "//gest.unicaen.fr/images/ajax-loader-r.gif";
    
    public function __invoke(ElementInterface $element = null)
    {
        if (! $element) {
            return $this;
        }

        return $this->render($element);
    }

    public function render(ElementInterface $element): string
    {
        if (!$element instanceof SearchAndSelect) {
            throw new InvalidElementException("L'élément spécifié n'est pas du type attendu.");
        }
        
        $this->element = $element;
        $name          = $this->element->getName();
        $id            = SearchAndSelect::ID_ELEMENT_NAME;
        $label         = SearchAndSelect::LABEL_ELEMENT_NAME;
        
        if (!$this->element->getAttribute('id')) {
            $this->element->setAttribute('id', uniqid('sas-'));
        }
        
        $this->element->setAttribute('class', 'sas');
        
        // L'élément est multivalué : 
        //   'id'    => identifiant unique (ex: login de la personne), 
        //   'label' => libellé affiché (ex: nom complet de la personne)
        $this->element->setName($name . "[$id]");

        $elementDomId      = $this->element->getAttribute('id');
        $autocompleteDomId = $elementDomId . '-autocomplete';

        $this->autocompleteElement = new Text();
        $this->autocompleteElement
            ->setAttributes($this->element->getAttributes())
            ->setName($name . "[$label]")
            ->setAttribute('id', $autocompleteDomId)
            ->setAttribute('class', 'form-control')
            ->setValue($this->element->getValueLabel() ?: $this->element->getValue());

        $this->element->setAttribute('style', $this->element->getAttribute('style') . ' display:none;');

        $markup = parent::render($this->element);

        $markup .= $this->view->formText($this->autocompleteElement);
        
        $markup .= <<<EOS
<style>
    ul.ui-autocomplete {
        z-index: 5000
    }
    .ui-autocomplete-loading {
        background: white url("{$this->getSpinnerSource()}") right center no-repeat;
    }
</style>
EOS;

        $markup .= PHP_EOL . '<script>' . $this->getJavascript() . '</script>' . PHP_EOL;
        
        return $markup;
    }

    protected function getJavascript(): string
    {
        $elementDomId      = $this->element->getAttribute('id');
        $autocompleteDomId = $this->autocompleteElement->getAttribute('id');
        
        return <<<EOT
$(function() {
    // jQuery UI autocomplete
    var autocomp = $("#{$autocompleteDomId}");
    autocomp.autocompleteUnicaen({ // autocompleteUnicaen() définie dans "public/unicaen/app/js/unicaen.js"
        elementDomId: '$elementDomId',
        source: '{$this->getAutocompleteSource()}',
        minLength: {$this->getAutocompleteMinLength()},
        delay: 750
    });
});
EOT;
    }
    
    public function setAutocompleteSource(string $autocompleteSource): self
    {
        $this->autocompleteSource = $autocompleteSource;
        return $this;
    }

    public function getAutocompleteSource(): string
    {
        if (null !== $this->autocompleteSource) {
            return $this->autocompleteSource;
        }
        return $this->element->getAutocompleteSource();
    }

    public function getAutocompleteMinLength(): int
    {
        return $this->autocompleteMinLength;
    }

    public function setAutocompleteMinLength(int $autocompleteMinLength): self
    {
        $this->autocompleteMinLength = $autocompleteMinLength;
        return $this;
    }

    public function getSpinnerSource(): string
    {
        return $this->spinnerSource;
    }

    public function setSpinnerSource(string $spinnerSource): self
    {
        $this->spinnerSource = $spinnerSource;
        return $this;
    }
}
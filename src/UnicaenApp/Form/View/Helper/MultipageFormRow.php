<?php
namespace UnicaenApp\Form\View\Helper;

use Application\View\Renderer\PhpRenderer;
use UnicaenApp\Form\Element\DateInfSup;
use UnicaenApp\Form\Fieldset\MultipageFormNavFieldset;
use Laminas\Form\Element;
use Laminas\Form\ElementInterface;
use Laminas\Form\View\Helper\FormRow;

/**
 * Aide de vue générant le code HTML de chaque élément d'un fieldset de formulaire multi-page.
 *
 * Si un élément de navigation est rencontré, la génération est déléguée à l'aide de vue {@see MultipageFormNav}.
 *
 * @property PhpRenderer $view
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MultipageFormRow extends FormRow
{
    /**
     * @var Element
     */
    protected $element;

    /**
     * @inheritDoc
     */
    public function __invoke(ElementInterface $element = null, $labelPosition = null, $renderErrors = null, $partial = null)
    {
        $this->element = $element;

        return parent::__invoke($element, $labelPosition, $renderErrors, $partial);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render($this->element);
    }

    /**
     * Utility form helper that renders a label (if it exists), an element and errors
     *
     * @param ElementInterface $element
     * @param null|string $labelPosition
     * @return string
     */
    public function render(ElementInterface $element, $labelPosition = null): string
    {
        if ($element instanceof MultipageFormNavFieldset) {
            return $this->view->multipageFormNav($element);
        }
        if ($element instanceof DateInfSup) {
            return $this->view->formRowDateInfSup($element);
        }

        return
            '<div class="form-group">' . PHP_EOL .
            parent::render($element). PHP_EOL  .
            '</div>' . PHP_EOL ;
    }
}
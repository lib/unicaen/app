<?php

namespace UnicaenApp\Form\View\Helper;

use Application\View\Renderer\PhpRenderer;
use UnicaenApp\Form\Fieldset\MultipageFormNavFieldset;
use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Resolver\AggregateResolver;
use Laminas\View\Resolver\TemplatePathStack;

/**
 * Aide de vue générant le code HTML d'un fieldset de navigation au sein d'un formulaire multipage.
 *
 * @property PhpRenderer $view
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MultipageFormNav extends AbstractHelper
{
    protected $partial = 'multipage-form-nav';

    /**
     * @var MultipageFormNavFieldset
     */
    protected $navigationFieldset;

    /**
     * Spécifie la vue à utiliser pour la génération HTML des éléments de navigation.
     *
     * @param string $partial
     * @return MultipageFormNav
     */
    public function setPartial(string $partial): self
    {
        $this->partial = $partial;

        return $this;
    }

    /**
     * Point d'entrée.
     *
     * @param MultipageFormNavFieldset|null $fieldset
     * @return self
     */
    public function __invoke(?MultipageFormNavFieldset $fieldset = null) : self
    {
        $this->navigationFieldset = $fieldset;

        /** @var AggregateResolver $resolver */
        $resolver = $this->view->resolver();
        $resolver->attach(new TemplatePathStack(['script_paths' => [__DIR__ . '/partial']]));

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->render($this->navigationFieldset);
    }

    /**
     * Génère le code HTML.
     *
     * @param MultipageFormNavFieldset $element
     * @return string
     */
    protected function render(MultipageFormNavFieldset $element) : string
    {
        $buttonMap = [];
        $buttonMap['nextButton'] = $element->getNextButton();
        $buttonMap['prevButton'] = $element->getPreviousButton();
        $buttonMap['cancelButton'] = $element->getCancelButton();
        $buttonMap['submitButton'] = $element->getSubmitButton();
        $buttonMap['confirmButton'] = $element->getConfirmButton();

        $buttonEnabling = [];
        $buttonEnabling['nextButton'] = $element->isNextEnabled();
        $buttonEnabling['prevButton'] = $element->isPreviousEnabled();
        $buttonEnabling['submitButton'] = $element->isSubmitEnabled();
        $buttonEnabling['cancelButton'] = $element->isCancelEnabled();
        $buttonEnabling['confirmButton'] = $element->isConfirmEnabled();

        $buttonOrder = [];
        $buttonOrder[0] = 'nextButton';
        $buttonOrder[1] = 'prevButton';
        $buttonOrder[2] = 'submitButton';
        $buttonOrder[3] = 'cancelButton';
        if ($element->isConfirmEnabled()) {
            $buttonOrder[0] = 'confirmButton';
        }
        // si les boutons "Terminer" et "Précédent" sont présents tous les 2, on les intervertit
        // pour pouvoir valider (terminer) avec la touche "Entrée"
        if ($element->isPreviousEnabled() && $element->isSubmitEnabled()) {
            $buttonOrder[1] = 'submitButton';
            $buttonOrder[2] = 'prevButton';
        }

        return $this->view->partial($this->partial, compact('buttonMap', 'buttonEnabling', 'buttonOrder'));
    }
}

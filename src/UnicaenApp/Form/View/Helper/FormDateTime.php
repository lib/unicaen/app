<?php

namespace UnicaenApp\Form\View\Helper;

use UnicaenApp\Exception\LogicException;
use UnicaenApp\Form\Element\Date;
use UnicaenApp\Util;
use UnicaenApp\View\Helper\TagViewHelper;
use Laminas\Form\Element\DateTime;
use Laminas\Form\ElementInterface;
use UnicaenApp\Form\Element\DateTime as UnicaenDateTime;

/**
 * Class FormDateTime
 *
 * @package UnicaenApp\Form\View\Helper
 */
class FormDateTime extends \Laminas\Form\View\Helper\FormDateTime
{
    /**
     * @var DateTime
     */
    private $element;



    /**
     * Render a form <input> element from the provided $element
     *
     * @param  ElementInterface $element
     *
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element): string
    {
        $this->setElement($element);

        // On force le format à date s'il n'a pas été déjà défini!!
        if (!$this->getFormat('format')){
            $this->setFormat(Util::DATE_FORMAT);
        }

        $inputAttrs                = $element->getAttributes();
        $inputAttrs['name']        = $element->getName();
        $inputAttrs['type']        = $this->getType($element);
        $inputAttrs['value']       = $this->getStringValue();
        //$inputAttrs['placeholder'] = $this->convertPHPToHumanFormat($this->getFormat());

        if ($inputAttrs['type'] == 'datetime'){
            $inputAttrs['type'] = 'datetime-local';
        }

        if (! $element->getAttribute('id')) {
            $inputAttrs['id'] = uniqid('bdtpicker_');
        }

        $value = $element->getValue();

        $divAttrs = [
            'class'         => 'input-group',
            'data-input-id' => $inputAttrs['id'],
        ];

        /** @var $tag TagViewHelper */
        $tag = $this->getView()->tag();

        return $tag('div', $divAttrs)->html(
            $tag('input', $inputAttrs)->openClose(true)
            . $tag('span', ['class' => 'input-group-text'])->html(
                $tag('span', ['class' => 'icon iconly icon-calendrier'])
            )
        );
    }



    private function setElement(DateTime $element)
    {
        $name = $element->getName();
        if ($name === null || $name === '') {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $this->element = $element;

        return $this;
    }



    private function getStringValue()
    {
        $value = $this->element->getValue(false);
        $format = $this->getFormat();

        if ($value instanceof \DateTime){
            return $value->format($format);
        }else{
            return $value;
        }
    }



    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->element->getOption('format');
    }



    /**
     * @param string $format
     *
     * @return FormDateTime
     */
    public function setFormat($format)
    {
        $this->element->setOption('format', $format);

        return $this;
    }

}
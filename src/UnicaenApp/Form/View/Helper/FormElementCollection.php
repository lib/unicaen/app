<?php
namespace UnicaenApp\Form\View\Helper;

use Laminas\Form\Element\Collection as CollectionElement;
use Laminas\Form\ElementInterface;
use Laminas\Form\FieldsetInterface;
use Laminas\Form\LabelAwareInterface;
use Laminas\Form\View\Helper\FormCollection;
use UnicaenApp\Form\Element\Collection;

/**
 * Aide de vue dessinant une collection d'éléments.
 *
 * Ajout par rapport à celle de base :
 * - boutons d'ajout et de suppression d'élément à la collection
 * - respect du nombre mini et maxi d'éléments dans la collection
 * - styles inline :-/
 *
 * @see \UnicaenApp\Form\Element\Collection
 * @author Unicaen
 */
class FormElementCollection extends FormCollection
{
    /**
     * This is the default wrapper that the collection is wrapped into
     *
     * @var string
     */
    protected $wrapper = '<fieldset%4$s>%2$s%1$s%3$s%5$s</fieldset>%6$s%7$s';

    /**
     * The name of the default view helper that is used to render sub elements.
     *
     * @var string
     */
    protected $defaultElementHelper = 'formControlGroup';

    /**
     * Nombre d'éléments à afficher.
     */
    protected int $countElements;


    /**
     * Render a collection by iterating through all fieldsets and elements.
     */
    public function render(ElementInterface $element): string
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        $index = 0;
        $markup = '';
        $linkMarkup = '';
        $javascript = null;
        $css = null;
        $templateMarkup = '';
        $elementHelper = $this->getElementHelper();
        assert(is_callable($elementHelper));
        $fieldsetHelper = $this->getFieldsetHelper();
        assert(is_callable($fieldsetHelper));

        if ($element instanceof CollectionElement && $element->shouldCreateTemplate()) {
            $this->countElements = $element->getCount();
            $linkMarkup = $this->getAddLink();
            $javascript = $this->getJavascript($element);
            $css = $this->getCss($element);
            $element->getTemplateElement()->setAttribute('data-index', '__index__');
            $templateMarkup = $this->renderTemplate($element);
        }
        elseif ($element instanceof FieldsetInterface) {
            $linkMarkup = $this->getDelLink();
        }

        foreach ($element->getIterator() as $elementOrFieldset) {
            if ($elementOrFieldset instanceof FieldsetInterface) {
                $elementOrFieldset->setAttribute('data-index', $index);
                $fieldsetHtml = $fieldsetHelper($elementOrFieldset, $this->shouldWrap());
                $fieldsetHtml = str_replace('__index__', $index, $fieldsetHtml);
                $index++;
                $markup .= $fieldsetHtml;
            } elseif ($elementOrFieldset instanceof ElementInterface) {
                /**
                 * NB : la valeur d'un id doit être unique dans un DOM. Or, en cas de collection autorisant l'ajout d'élément
                 * (par clonage), on garantit l'unicité en concaténant le pattern '__index__', pattern qui est remplacé/incrémenté
                 * par le moteur js de cette aide de vue (cf. plus bas).
                 * Par ex, {@see \UnicaenApp\Form\Element\SearchAndSelect} ne fonctionne plus dans une collection en cas de doublon d'id.
                 */
                $id = $elementOrFieldset->getAttribute('id') ?: uniqid('element-');
                $elementOrFieldset->setAttribute('id', $id . '-__index__');
                $markup .= $elementHelper($elementOrFieldset);
            }
        }

        // Every collection is wrapped by a fieldset if needed
        if ($this->shouldWrap) {
            $attributes = $element->getAttributes();
            unset($attributes['name']);
            $attributesString = $attributes ? ' ' . $this->createAttributesString($attributes) : '';

            $label = $element->getLabel();
            $legend = '';

            if (!empty($label)) {
                if (null !== ($translator = $this->getTranslator())) {
                    $label = $translator->translate(
                        $label,
                        $this->getTranslatorTextDomain()
                    );
                }

                if (!$element instanceof LabelAwareInterface || !$element->getLabelOption('disable_html_escape')) {
                    $escapeHtmlHelper = $this->getEscapeHtmlHelper();
                    $label = $escapeHtmlHelper($label);
                }

                $legend = sprintf(
                    $this->labelWrapper,
                    $label
                );
            }

            $markup = sprintf(
                $this->wrapper,
                $markup,
                $legend,
                $templateMarkup,
                $attributesString,
                $linkMarkup,
                $css,
                $javascript,
            );
        } else {
            $markup .= $templateMarkup . $linkMarkup;
        }

        return $markup;
    }

    protected function getDelLink(): string
    {
        return '<div class="collection-delete-link">' .
                    '<a class="btn btn-link" title="Supprimer cet élément de la collection">' .
                        '<i class="fas fa-times" aria-hidden="true"></i>' .
                    '</a>' .
                '</div>';
    }

    protected function getAddLink(): string
    {
        return '<div class="collection-add-link">' .
                    '<a class="btn btn-link" title="Ajouter un élément à la collection">' .
                        '<i class="fas fa-plus" aria-hidden="true"></i> Ajouter' .
                    '</a>' .
                '</div>';
    }

    protected function getCss(Collection $container): string
    {
        $fieldsetId = $container->getAttribute('id');

        $css = <<<EOT
<style>
    #{$fieldsetId} {
        margin-bottom: 15px;
    }
    
    #{$fieldsetId} div.input-group {
        position: relative;
        float: left;
        margin: 0 10px 0 0;
    }
    
    #{$fieldsetId} div.collection-delete-link {
        position: relative;
        float: right;
    }
    
    #{$fieldsetId} div.collection-delete-link > .btn {
        padding: 6px 0;
    }
    
    #{$fieldsetId} div.collection-add-link {
        margin-bottom: 15px;
    }
</style>
EOT;
        return $css;
    }

    protected function getJavascript(Collection $container): string
    {
        $maxEnabled = ($container->getMaxElements()) ? 'true' : 'false';
        $minEnabled = ($container->getMinElements()) ? 'true' : 'false';

        $javascript = <<<EOT
$('#{$container->getAttribute('id')}').on('click', '.collection-add-link > a', function() {
    var container	 = $('#{$container->getAttribute('id')}');
    var currentCount = $(container).children('fieldset').length;
    var lastFieldset = $(container).children('fieldset').last();
    var index = currentCount === 0 ? currentCount : parseInt(lastFieldset.attr('data-index')) + 1;
    
    var templateSpan = $(container).children('span[data-template]');
    var template = templateSpan.data('template');
    template = template.replace(/__index__/g, index);

    templateSpan.before(template);

    if({$maxEnabled}) {
        if(currentCount+1 >= {$container->getMaxElements()}) {
            $('.collection-add-link').remove();
        }
    }
});

$('#{$container->getAttribute('id')}').on('click', '.collection-delete-link' , function() {
    var container = $('#{$container->getAttribute('id')}');
    var item	  = $(this).closest('fieldset');
    var curPos    = item.index();
    var curInd    = item.attr('data-index');

    // suppression du fieldset lié à l'élément
    item.remove();

    var currentCount = $(container).children('fieldset').length;
//  FONCTIONNE PAS :    
//    if(currentCount > 0) {
//        var index = (curInd < {$this->countElements}) ? {$this->countElements} : curInd;
//        $(container).children('fieldset').each(function(i, el) {
//            if(i >= curPos) {
//                $(el)
//                    .attr('data-index', '' + index)
//                    .find('select,input').attr('name', function () {
//                        return this.name.replace(/\[\d+\](\[.*\])$/, '['+index+']$1');
//                    });
//                index++;
//            }
//        });
//    }
// CORRECTION (PLUS SIMPLE, TROP ?) :
//    $(container).children('fieldset').each(function(i, el) {
//        $(el)
//            .attr('data-index', '' + i)
//            .find('select,input').attr('name', function () {
//                return this.name.replace(/\[\d+\](\[.*\])$/, '['+i+']$1');
//            });
//    });

    if({$maxEnabled}) {
        if(currentCount+1 == {$container->getMaxElements()}) {
            $(container).append('{$this->getAddLink()}');
        }
    }
});

$().ready(function updateButtons()
{
    var container = $('#{$container->getAttribute('id')}');
    var buttons   = $(container).find('.collection-delete-link');
    var currentCount = $(container).children('fieldset').length;

    if({$maxEnabled}) {
        if(currentCount >= {$container->getMaxElements()}) {
            $('.collection-add-link').remove();
        }
    }

    if($minEnabled) {
        buttons.each(function(i, el) {
            if(i<={$container->getMinElements()}-1) {
                $(el).remove();
            }
        });
    }
});
EOT;

//        foreach($container->getAutocomplete() as $name => $options) {
//            $javascript .= <<<EOT
//$('#{$container->getAttribute('id')}').on('keyup', '.{$container->getAttribute('id')}-autocomplete', function() {
//    $(this).autocompleteOctopus({
//        source: '{$options['source']}',
//        minLength: {$options['min-length']},
//        delay: {$options['delay']}
//    });
//});
//
//
//EOT;
//        }

        return '<script type="text/javascript">' . $javascript . '</script>';
    }
}

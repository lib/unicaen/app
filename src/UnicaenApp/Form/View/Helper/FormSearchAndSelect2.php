<?php

namespace UnicaenApp\Form\View\Helper;

use Laminas\Form\ElementInterface;
use Laminas\Form\Exception\InvalidElementException;
use Laminas\Form\View\Helper\FormSelect;
use UnicaenApp\Exception\LogicException;
use UnicaenApp\Form\Element\SearchAndSelect2;

/**
 * Aide de vue dédiée à l'élément {@see \UnicaenApp\Form\Element\SearchAndSelect}.
 * Génère un <select> sur lequel est installé le widget "Select2" (https://select2.org).
 *
 * @property \Application\View\Renderer\PhpRenderer $view
 *
 * @author Unicaen
 * @see \UnicaenApp\Form\Element\SearchAndSelect
 */
class FormSearchAndSelect2 extends FormSelect
{
    protected SearchAndSelect2 $element;
    protected ?string $autocompleteSource = null;
    protected int $autocompleteMinLength = 2;

    public function setAutocompleteSource(string $autocompleteSource): self
    {
        $this->autocompleteSource = $autocompleteSource;

        return $this;
    }

    public function setAutocompleteMinLength($autocompleteMinLength): self
    {
        $this->autocompleteMinLength = $autocompleteMinLength;

        return $this;
    }

    public function __invoke(ElementInterface $element = null)
    {
        if ($element && !$element instanceof SearchAndSelect2) {
            throw new InvalidElementException("L'élément spécifié n'est pas du type attendu.");
        }

        $this->element = $element;

        return parent::__invoke($element);
    }

    /**
     * @param SearchAndSelect2 $element
     * @return string
     */
    public function render(ElementInterface $element): string
    {
        if (!$element instanceof SearchAndSelect2) {
            throw new InvalidElementException("L'élément spécifié n'est pas du type attendu.");
        }

        $this->element = $element;

        if (!$this->element->getAttribute('id')) {
            $this->element->setAttribute('id', uniqid('sas-'));
        }

//        $this->element->setAttribute('class', 'sas');

//        $element = new Select();
//        $element
//            ->setAttributes($this->element->getAttributes())
//            ->setName($this->element->getName())
//            ->setAttribute('multiple', $this->element->isMultiple())
//            ->setAttribute('id', $this->element->getAttribute('id'))
//            ->setAttribute('class', 'sas form-control form-control-sm');
//
//        $element->setValueOptions($this->element->getValueOptions());
//        $element->setValue($this->element->getValueIds());

        $markup = $this->view->formSelect($this->element);

        $markup .= PHP_EOL . '<script>' . $this->getJavascript() . '</script>' . PHP_EOL;

        return $markup;
    }

    public function getJavascript(): string
    {
        if (!$this->element) {
            throw new LogicException("Aucun élément spécifié, appelez render() auparavant.");
        }

        $elementDomId = $this->element->getAttribute('id');
        $autocompleteMinLength = $this->autocompleteMinLength;
        $autocompleteSource = $this->autocompleteSource ?: $this->element->getAutocompleteSource();
        $placeholder = $this->element->getAttribute('placeholder');
        $separator = SearchAndSelect2::SEPARATOR;

        return <<<EOT
$(function() {
    $("#$elementDomId").select2({
        allowClear: true,
        minimumInputLength: $autocompleteMinLength,
        placeholder: "$placeholder",
        ajax: {
            url: '$autocompleteSource',
            processResults: function (data) {
                data.forEach(function(item) {
                    item.id = item.id + '$separator' + item.label; // concat de l'id et du label
                });
                //console.log(data);
                return { results: data };
            },
            dataType: 'json',
            delay: 500
        },
        templateResult: function(item) {
            if (!item.id) {
                return item.text;
            }
            if (item.extra && item.extra.trim()) {
                return $('<span>' + item.text + ' <span class="badge bg-secondary">' + item.extra + '</span></span>');
            } else {
                return $('<span>' + item.text + '</span>');
            }
        }
    });
});
EOT;
    }
}
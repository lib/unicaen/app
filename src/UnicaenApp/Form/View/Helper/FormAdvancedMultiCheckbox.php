<?php

namespace UnicaenApp\Form\View\Helper;

use Laminas\Form\View\Helper\FormMultiCheckbox;
use Laminas\Form\ElementInterface;
use Laminas\Form\Exception;
use Laminas\Stdlib\ArrayUtils;

/**
 * Class FormAdvancedMultiCheckbox
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 *
 */
class FormAdvancedMultiCheckbox extends FormMultiCheckbox
{

    /**
     * hauteur de la liste
     *
     * @var string
     */
    protected $height = 'auto';

    /**
     * @var string
     */
    protected $overflow = 'auto';



    /**
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }



    /**
     * @param string $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }



    /**
     * @return string
     */
    public function getOverflow()
    {
        return $this->overflow;
    }



    /**
     * @param string $overflow
     */
    public function setOverflow($overflow)
    {
        $this->overflow = $overflow;
        return $this;
    }



    /**
     * Render a form <select> element from the provided $element
     *
     * @param  ElementInterface $element
     * @throws Exception\InvalidArgumentException
     * @throws Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element): string
    {
        $template = '
        <div class="form-advanced-multi-checkbox form-control-sm form-control" style="height:auto">
            <div id="items" style="max-height:'.$this->height.';overflow:'.$this->overflow.'">
                %s
            </div>
            <a class="btn btn-secondary btn-sm select-all" role="button"><span class="icon icon-oui"></span> Tout sélectionner</a>
            <a class="btn btn-secondary btn-sm select-none" role="button"><span class="icon icon-non"></span> Ne rien sélectionner</a>
        </div>
        ';
        return sprintf($template, parent::render( $element ));
    }
}

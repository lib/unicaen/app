<?php

namespace UnicaenApp\Form;

use UnicaenApp\Form\View\Helper\MultipageFormRecap;

/**
 * Interface à implémenter par les fieldsets membres d'un formulaire multipage et désirant fournir eux-mêmes
 * les labels et valeurs de ses éléments, dans le format attendu par l'aide de vue {@see MultipageFormRecap}.
 *
 * @author bertrand.gauthier@unicaen.fr
 */
interface MultipageFormFieldsetInterface extends \Laminas\Form\FieldsetInterface
{
    /**
     * Retourne les labels ainsi que les valeurs des éléments d'un fieldset, dans le format
     * attendu par l'aide de vue {@see MultipageFormRecap}.
     *
     * @return array 'element_name' => array('label' => Label de l'élément, 'value' => Valeur saisie au format texte)
     */
    public function getLabelsAndValues();
    
}

<?php

namespace UnicaenApp\Form\Fieldset;

use Laminas\Form\Element\Submit;
use Laminas\Form\Fieldset;

/**
 * Fieldset de navigation au sein d'un formulaire multi-page.
 *
 * Boutons présents selon la situation : "Précédent", "Suivant", "Terminer", "Confirmer", "Annuler".
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MultipageFormNavFieldset extends Fieldset
{
    const NAME     = '_nav';

    const PREFIX_IN_NAME   = '_';

    const NAME_NEXT     = '_next';
    const NAME_PREVIOUS = '_previous';
    const NAME_SUBMIT   = '_submit';
    const NAME_CANCEL   = '_cancel';
    const NAME_CONFIRM  = '_confirm';

    /**
     * @var boolean
     */
    private $previousEnabled = false;
    /**
     * @var boolean
     */
    private $nextEnabled = true;
    /**
     * @var boolean
     */
    private $submitEnabled = false;
    /**
     * @var boolean
     */
    private $cancelEnabled = true;
    /**
     * @var boolean
     */
    private $confirmEnabled = false;

    /**
     * {@inheritDoc}
     */
    private function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $this
            ->setPreviousEnabled(false)
            ->setNextEnabled(true)
            ->setSubmitEnabled(false)
            ->setCancelEnabled(true)
            ->setConfirmEnabled(false);

        $this->init();
    }

    /**
     * @param array $options
     * @return self
     */
    static public function create(array $options = []): self
    {
        return new static(static::NAME, $options);
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->add($this->createNextButton());
        $this->add($this->createPreviousButton());
        $this->add($this->createCancelButton());
        $this->add($this->createSubmitButton());
        $this->add($this->createConfirmButton());
    }

    /**
     * @return Submit
     */
    protected function createNextButton() : Submit
    {
        $label    = ("Suivant >");
        $title    = ("Passer à l'étape suivante");

        $submit = new Submit(
            self::NAME_NEXT,
            ['label' => $label]
        );
        $submit->setValue($label);
        $submit->setAttributes([
            'title' => $title,
            'class' => 'multipage-nav next']);

        return $submit;
    }

    /**
     * @return Submit
     */
    public function getNextButton() : Submit
    {
        /** @var Submit $submit */
        $submit = $this->get(self::NAME_NEXT);

        return $submit;
    }

    /**
     * @return Submit
     */
    protected function createPreviousButton() : Submit
    {
        $label    = ("< Précédent");
        $title    = ("Revenir à l'étape précédente");

        $submit = new Submit(
            self::NAME_PREVIOUS,
            ['label' => $label]
        );
        $submit->setValue($label);
        $submit->setAttributes([
            'title' => $title,
            'class' => 'multipage-nav previous', 'style'=>'float: left;']);

        return $submit;
    }

    /**
     * @return Submit
     */
    public function getPreviousButton() : Submit
    {
        /** @var Submit $submit */
        $submit = $this->get(self::NAME_PREVIOUS);

        return $submit;
    }

    /**
     * @return Submit
     */
    protected function createCancelButton() : Submit
    {
        $label  = ("Annuler");
        $title  = ("Abandonner définitivement la saisie");

        $submit = new Submit(
            self::NAME_CANCEL,
            ['label' => $label]
        );
        $submit->setValue($label);
        $submit->setAttributes([
            'title'   => $title,
            'class'   => 'multipage-nav cancel',
            'onclick' => 'askConfirmation(this)',
        ]);

        return $submit;
    }

    /**
     * @return Submit
     */
    public function getCancelButton() : Submit
    {
        /** @var Submit $submit */
        $submit = $this->get(self::NAME_CANCEL);

        return $submit;
    }

    /**
     * @return Submit
     */
    protected function createSubmitButton() : Submit
    {
        $label  = ("Terminer");
        $title  = ("Terminer la saisie");

        $submit = new Submit(
            self::NAME_SUBMIT,
            ['label' => $label]
        );
        $submit->setValue($label);
        $submit->setAttributes([
            'title' => $title,
            'class' => 'multipage-nav submit']);

        return $submit;
    }

    /**
     * @return Submit
     */
    public function getSubmitButton() : Submit
    {
        /** @var Submit $submit */
        $submit = $this->get(self::NAME_SUBMIT);

        return $submit;
    }

    /**
     * @return Submit
     */
    protected function createConfirmButton() : Submit
    {
        $label = ("Confirmer et enregistrer");
        $title = ("Confirmer et enregistrer la saisie");

        $submit = new Submit(
            self::NAME_CONFIRM,
            ['label' => $label]
        );
        $submit->setValue($label);
        $submit->setAttributes([
            'title' => $title,
            'class' => 'multipage-nav confirm']);

        return $submit;
    }

    /**
     * @return Submit
     */
    public function getConfirmButton() : Submit
    {
        /** @var Submit $submit */
        $submit = $this->get(self::NAME_CONFIRM);

        return $submit;
    }

    /**
     * Active ou non le bouton "Précédent".
     *
     * @param boolean $value
     * @return self
     */
    public function setPreviousEnabled($value = true)
    {
        $this->previousEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Suivant".
     *
     * @param boolean $value
     * @return self
     */
    public function setNextEnabled($value = true)
    {
        $this->nextEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Terminer".
     *
     * @param boolean $value
     * @return self
     */
    public function setSubmitEnabled($value = true)
    {
        $this->submitEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Annuler".
     *
     * @param boolean $value
     * @return self
     */
    public function setCancelEnabled($value = true)
    {
        $this->cancelEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Confirmer et enregistrer".
     *
     * @param boolean $value
     * @return self
     */
    public function setConfirmEnabled($value = true)
    {
        $this->confirmEnabled = $value;

        return $this;
    }

    /**
     * Indique si le bouton "Précédent" est autorisé ou non.
     *
     * @return boolean
     */
    public function isPreviousEnabled()
    {
        return $this->previousEnabled;
    }

    /**
     * Indique si le bouton "Suivant" est autorisé ou non.
     *
     * @return boolean
     */
    public function isNextEnabled()
    {
        return $this->nextEnabled;
    }

    /**
     * Indique si le bouton "Terminer" est autorisé ou non.
     *
     * @return boolean
     */
    public function isSubmitEnabled()
    {
        return $this->submitEnabled;
    }

    /**
     * Indique si le bouton "Annuler" est autorisé ou non.
     *
     * @return boolean
     */
    public function isCancelEnabled()
    {
        return $this->cancelEnabled;
    }

    /**
     * Indique si le bouton "Confirmer" est autorisé ou non.
     *
     * @return boolean
     */
    public function isConfirmEnabled()
    {
        return $this->confirmEnabled;
    }
}
<?php

namespace UnicaenApp\Form\Fieldset;

use Laminas\Form\Fieldset;

/**
 * Classe des fieldsets de récapitulatif / confirmation de saisie dans un formulaire multipage.
 *
 * @author Unicaen
 */
class MultipageFormConfirmFieldset extends Fieldset
{

}
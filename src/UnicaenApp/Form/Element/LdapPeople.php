<?php
namespace UnicaenApp\Form\Element;

/**
 * Elément de formulaire permettant de rechercher/sélectionner 
 * une seule personne issue de l'annuaire LDAP.
 * 
 * Deux attributs sont encapsulés : 
 * - le "label" : texte saisi dans un champ texte ;
 * - l' "id" : identifiant unique de la personne *sélectionnée* (typiquement le persopass/etupass)
 *   dans un champ caché.
 *
 * NB: Il faut utiliser l'aide de vue 'FormLdapPeople' pour dessiner cet élément.
 * 
 * @see \UnicaenApp\Form\View\Helper\FormLdapPeople
 * @author <bertrand.gauthier@unicaen.fr>
 */
class LdapPeople extends SearchAndSelect
{
    
}
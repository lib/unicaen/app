<?php

namespace UnicaenApp\Form\Element;

use InvalidArgumentException;
use Laminas\Form\Element\Select;

/**
 * Elément de formulaire permettant de sélectionner un ou plusieurs items recherchés dans une source de
 * données distante (via ajax).
 *
 * NB: Il faut utiliser l'aide de vue 'FormSearchAndSelect2' pour dessiner cet élément.
 *
 * @see \UnicaenApp\Form\View\Helper\FormSearchAndSelect
 * @author Unicaen
 */
class SearchAndSelect2 extends Select
{
    const SEPARATOR = '|';

    protected bool $selectionRequired = false;
    protected ?string $autocompleteSource = null;

    /**
     * Spécifie la source de données dans laquelle est effectuée la recherche.
     */
    public function setAutocompleteSource(string $autocompleteSource): self
    {
        $this->autocompleteSource = $autocompleteSource;

        return $this;
    }

    /**
     * Retourne la source de données dans laquelle est effectuée la recherche.
     */
    public function getAutocompleteSource(): ?string
    {
        return $this->autocompleteSource;
    }

    public function setValue($value): self
    {
        if ($value) {
            if (!is_string($value)) {
                throw new InvalidArgumentException(
                    "Cet élément de formulaire n'accepte que les chaînes de caractères"
                );
            }
            if (!str_contains($value, $sep = self::SEPARATOR)) {
                throw new InvalidArgumentException(
                    "Cet élément de formulaire n'accepte que les valeurs de la forme 'id{$sep}label'"
                );
            }
        }

        $valueOptions = $this->extractValueOptionsFromValue($value);
        $this->setValueOptions($valueOptions);

        return parent::setValue($value);
    }

//    public function getValue()
//    {
//        if ($this->isMultiple()) {
//            return array_keys($this->getValueOptions());
//        } else {
//            return key($this->getValueOptions()) ?: null;
//        }
//    }

//    /**
//     * @return string|int|array
//     */
//    public function getValueIds()
//    {
//        if ($this->isMultiple()) {
//            return array_keys($this->getValueOptions());
//        } else {
//            return key($this->getValueOptions()) ?: null;
//        }
//    }

    protected function extractValueOptionsFromValue($value): array
    {
        if (!$value) {
            return [];
        }

        $valueOptions = [];
        if ($this->isMultiple()) {
            foreach ($value as $item) {
                $valueOptions[$item] = self::extractLabelFromValue($item);
            }
        } else {
            $valueOptions[$value] = self::extractLabelFromValue($value);
        }

        return $valueOptions;
    }

    static public function createValueFromIdAndLabel($id, string $label): string
    {
        return implode(self::SEPARATOR, [$id, $label]);
    }

    static public function extractIdFromValue(string $value): string
    {
        return explode(self::SEPARATOR, $value)[0];
    }

    static public function extractLabelFromValue(string $value): string
    {
        return explode(self::SEPARATOR, $value)[1];
    }
}
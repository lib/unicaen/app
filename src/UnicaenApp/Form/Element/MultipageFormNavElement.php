<?php

namespace UnicaenApp\Form\Element;

use Laminas\Form\Element;
use Laminas\Form\Element\Submit;

/**
 * Élément composite de navigation au sein d'un formulaire multi-page.
 * Boutons présents selon le contexte : "Précédent", "Suivant", "Terminer", "Annuler".
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 * @deprecated Remplacé par {@see MultipageFormNavFieldset}
 */
class MultipageFormNavElement extends Element
{
    const NAME     = '_nav';

    const PREFIX   = '_';

    const NEXT     = '_next';
    const PREVIOUS = '_previous';
    const SUBMIT   = '_submit';
    const CANCEL   = '_cancel';
    const CONFIRM  = '_confirm';

    /**
     * @var Submit
     */
    protected $nextButton;
    /**
     * @var Submit
     */
    protected $previousButton;
    /**
     * @var Submit
     */
    protected $cancelButton;
    /**
     * @var Submit
     */
    protected $submitButton;
    /**
     * @var Submit
     */
    protected $confirmButton;

    /**
     * @var boolean
     */
    private $previousEnabled = false;
    /**
     * @var boolean
     */
    private $nextEnabled = true;
    /**
     * @var boolean
     */
    private $submitEnabled = false;
    /**
     * @var boolean
     */
    private $cancelEnabled = true;
    /**
     * @var boolean
     */
    private $confirmEnabled = false;

    /**
     * {@inheritDoc}
     */
    private function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

//        $this->getNextButton();
        $this->getPreviousButton();
//        $this->getCancelButton();
//        $this->getSubmitButton();
//        $this->getConfirmButton();

        $this
            ->setPreviousEnabled(false)
            ->setNextEnabled(true)
            ->setSubmitEnabled(false)
            ->setCancelEnabled(true)
            ->setConfirmEnabled(false);
    }

    /**
     * @param array $options
     * @return self
     */
    static public function create(array $options = []): self
    {
        return new static(static::NAME, $options);
    }

    /**
     * @param MultipageFormNavElement $prototype
     * @return self
     */
    static public function createFromPrototype(MultipageFormNavElement $prototype)
    {
        $instance = clone $prototype;

        $instance->setAttributes($prototype->getAttributes());
        $instance->setOptions($prototype->getOptions());
        $instance->setLabelOptions($prototype->getLabelOptions());
        $instance->setValue($prototype->getValue());
        $instance->setLabel($prototype->getLabel());

        $instance
            ->setPreviousEnabled(false)
            ->setNextEnabled(true)
            ->setSubmitEnabled(false)
            ->setCancelEnabled(true)
            ->setConfirmEnabled(false);

        return $instance;
    }

    /**
     * @param MultipageFormNavElement $prototype
     * @return self
     */
    static public function createFromPrototypePrec(MultipageFormNavElement $prototype)
    {
        $instance = static::create();

        $instance->setAttributes($prototype->getAttributes());
        $instance->setOptions($prototype->getOptions());
        $instance->setLabelOptions($prototype->getLabelOptions());
        $instance->setValue($prototype->getValue());
        $instance->setLabel($prototype->getLabel());

        return $instance;
    }

    /**
     * @return Submit
     */
    public function getNextButton() : Submit
    {
        if ($this->nextButton === null) {
            $name = $this->getName();

            $label    = ("Suivant >");
            $title    = ("Passer à l'étape suivante");

            $this->nextButton = new Submit(
                $name . '[' . MultipageFormNavElement::NEXT . ']',
                ['label' => $label]
            );
            $this->nextButton->setValue($label);
            $this->nextButton->setAttributes([
                'title' => $title,
                'class' => 'multipage-nav next']);
        }

        return $this->nextButton;
    }

    /**
     * @return Submit
     */
    public function getPreviousButton() : Submit
    {
        if ($this->previousButton === null) {
            $name = $this->getName();

            $label    = ("< Précédent");
            $title    = ("Revenir à l'étape précédente");

            $this->previousButton = new Submit(
                $name . '[' . MultipageFormNavElement::PREVIOUS . ']',
                ['label' => $label]
            );
            $this->previousButton->setValue($label);
            $this->previousButton->setAttributes([
                'title' => $title,
                'class' => 'multipage-nav previous', 'style'=>'float: left;']);
        }

        return $this->previousButton;
    }

    /**
     * @return Submit
     */
    public function getCancelButton() : Submit
    {
        if ($this->cancelButton === null) {
            $name = $this->getName();

            $label  = ("Annuler");
            $title  = ("Abandonner définitivement la saisie");

            $this->cancelButton = new Submit(
                $name . '[' . MultipageFormNavElement::CANCEL . ']',
                ['label' => $label]
            );
            $this->cancelButton->setValue($label);
            $this->cancelButton->setAttributes([
                'title'   => $title,
                'class'   => 'multipage-nav cancel',
                'onclick' => 'askConfirmation(this)',
            ]);
        }

        return $this->cancelButton;
    }

    /**
     * @return Submit
     */
    public function getSubmitButton() : Submit
    {
        if ($this->submitButton === null) {
            $name = $this->getName();

            $label  = ("Terminer");
            $title  = ("Terminer la saisie");

            $this->submitButton = new Submit(
                $name . '[' . MultipageFormNavElement::SUBMIT . ']',
                ['label' => $label]
            );
            $this->submitButton->setValue($label);
            $this->submitButton->setAttributes([
                'title' => $title,
                'class' => 'multipage-nav submit']);
        }

        return $this->submitButton;
    }

    /**
     * @return Submit
     */
    public function getConfirmButton() : Submit
    {
        if ($this->confirmButton === null) {
            $name = $this->getName();

            $label = ("Confirmer et enregistrer");
            $title = ("Confirmer et enregistrer la saisie");

            $this->confirmButton = new Submit(
                $name . '[' . MultipageFormNavElement::CONFIRM . ']',
                ['label' => $label]
            );
            $this->confirmButton->setValue($label);
            $this->confirmButton->setAttributes([
                'title' => $title,
                'class' => 'multipage-nav confirm']);
        }

        return $this->confirmButton;
    }

    /**
     * Active ou non le bouton "Précédent".
     *
     * @param boolean $value
     * @return MultipageFormNavElement
     */
    public function setPreviousEnabled($value = true)
    {
        $this->previousEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Suivant".
     *
     * @param boolean $value
     * @return MultipageFormNavElement
     */
    public function setNextEnabled($value = true)
    {
        $this->nextEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Terminer".
     *
     * @param boolean $value
     * @return MultipageFormNavElement
     */
    public function setSubmitEnabled($value = true)
    {
        $this->submitEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Annuler".
     *
     * @param boolean $value
     * @return MultipageFormNavElement
     */
    public function setCancelEnabled($value = true)
    {
        $this->cancelEnabled = $value;

        return $this;
    }
    
    /**
     * Active ou non le bouton "Confirmer et enregistrer".
     *
     * @param boolean $value
     * @return MultipageFormNavElement
     */
    public function setConfirmEnabled($value = true)
    {
        $this->confirmEnabled = $value;

        return $this;
    }

    /**
     * Indique si le bouton "Précédent" est autorisé ou non.
     *
     * @return boolean
     */
    public function isPreviousEnabled()
    {
        return $this->previousEnabled;
    }
    
    /**
     * Indique si le bouton "Suivant" est autorisé ou non.
     *
     * @return boolean
     */
    public function isNextEnabled()
    {
        return $this->nextEnabled;
    }
    
    /**
     * Indique si le bouton "Terminer" est autorisé ou non.
     *
     * @return boolean
     */
    public function isSubmitEnabled()
    {
        return $this->submitEnabled;
    }
    
    /**
     * Indique si le bouton "Annuler" est autorisé ou non.
     *
     * @return boolean
     */
    public function isCancelEnabled()
    {
        return $this->cancelEnabled;
    }
    
    /**
     * Indique si le bouton "Confirmer" est autorisé ou non.
     *
     * @return boolean
     */
    public function isConfirmEnabled()
    {
        return $this->confirmEnabled;
    }
}
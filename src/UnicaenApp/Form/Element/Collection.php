<?php

namespace UnicaenApp\Form\Element;

use Laminas\Form\Element\Collection as ZendCollection;
use Laminas\Form\Element\Text;
use Laminas\Form\ElementInterface;
use Laminas\Form\FieldsetInterface;

/**
 * Collection d'éléments :
 * - possibilité d'ajouter un autocomplete sur un (et un seul) élément
 * - possibilité de limiter le nombre d'éléments
 *
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Collection extends ZendCollection
{
    const AUTOCOMPLETE_MIN_LENGTH = 2;
    const AUTOCOMPLETE_DELAY = 750;


    /**
     * Liste des autocomplete
     * Format : ['nom_element' => ['source' => , 'min-length' => , 'delay' => ]]
     *
     * @var array
     */
    protected $autocomplete = [];

    /**
     * Nombre d'éléments minimum de la collection
     * 0 = pas de limitation
     *
     * @var int
     */
    protected $minElements = 0;

    /**
     * Nombre d'éléments maximum de la collection
     * 0 = pas de limitation
     *
     * @var int
     */
    protected $maxElements = 0;


    /**
     * @param  null|int|string $name Optional name for the element
     * @param  array $options Optional options for the element
     */
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute('id', uniqid('collection'));
    }


    /**
     * @return array
     */
    public function getAutocomplete()
    {
        return $this->autocomplete;
    }


    /**
     * Ajoute un autocomplete sur un élément
     *
     * @param string $elementName
     * @param string $source
     * @param int $minLength
     * @param int $delay
     */
    public function addAutocomplete($elementName, $source, $minLength = null, $delay = null)
    {
        $targetElement = $this->getTargetElement();

        if ($targetElement instanceof FieldsetInterface) {
            $elements = array_keys($targetElement->getElements());
            if (!in_array($elementName, $elements)) {
                throw new \RuntimeException(sprintf("L'élément '%s' associé à l'autocomplete n'existe pas.", $elementName));
            }

            $element = $targetElement->get($elementName);
        } elseif ($targetElement instanceof ElementInterface) {
            if ($targetElement->getName() !== $elementName) {
                throw new \RuntimeException(sprintf("L'élément '%s' associé à l'autocomplete n'existe pas.", $elementName));
            }

            $element = $targetElement;
        }

        if (!$element instanceof Text) {
            throw new \RuntimeException("L'autocomplete doit être associé à un élément de type 'text'.");
        }

        $element->setAttribute('class', sprintf('%s-autocomplete', $this->getAttribute('id')));

        $this->autocomplete[$elementName] = [
            'source' => $source,
            'min-length' => $minLength ?: self::AUTOCOMPLETE_MIN_LENGTH,
            'delay' => $delay ?: self::AUTOCOMPLETE_DELAY
        ];
    }

    /**
     *
     * @return int
     */
    function getMinElements()
    {
        return $this->minElements;
    }

    /**
     *
     * @param int $minElements
     *
     * @return self
     */
    function setMinElements($minElements)
    {
        $this->minElements = $minElements;

        return $this;
    }

    /**
     *
     * @return int
     */
    function getMaxElements()
    {
        return $this->maxElements;
    }

    /**
     *
     * @param int $maxElements
     *
     * @return self
     */
    function setMaxElements($maxElements)
    {
        $this->maxElements = $maxElements;

        return $this;
    }


}

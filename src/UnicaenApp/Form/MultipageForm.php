<?php

namespace UnicaenApp\Form;

use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;
use Laminas\Form\ElementInterface;
use UnicaenApp\Exception\LogicException;
use UnicaenApp\Form\Element\SearchAndSelect;
use UnicaenApp\Form\Fieldset\MultipageFormConfirmFieldset;
use UnicaenApp\Form\Fieldset\MultipageFormNavFieldset;
use UnicaenApp\Form\View\Helper\MultipageFormRecap;
use Laminas\Form\Element;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\MultiCheckbox;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Fieldset;
use Laminas\Form\Form;

/**
 * Classe mère des formulaires multi-pages (saisie en plusieurs étapes).
 *
 * @author bertrand.gauthier@unicaen.fr
 */
abstract class MultipageForm extends Form
{
    /**
     * Nom du fieldset d'étape de récapitulatif / confirmation
     */
    const FIELDSET_NAME_CONFIRM = 'confirmer';

    /**
     * Nom du fieldset de navigation
     */
    const FIELDSET_NAME_NAV = MultipageFormNavFieldset::NAME;
    
    /**
     * Nom par défaut pour l'action de contrôleur gérant la demande de confirmation
     */
    const ACTION_CONFIRM = 'confirmer';
    /**
     * Nom par défaut pour l'action de contrôleur gérant la demande d'enregistrement
     */
    const ACTION_PROCESS = 'enregistrer';
    /**
     * Nom par défaut pour l'action de contrôleur gérant la demande d'annulation
     */
    const ACTION_CANCEL  = 'annuler';

    /**
     * Liste des noms des fieldsets ajoutés.
     * @var array
     */
    protected $fieldsetNames = [];
    
    /**
     * Préfixe appliqué aux noms des actions de contrôleur.
     * Par défaut: null.
     * @var string
     */
    protected $actionPrefix;

    /**
     * Mapping <nom du fieldset> => <nom de l'action du contrôleur>
     * Par défaut, le nom de l'action est égale au nom du fieldset.
     * @var array 
     */
    protected $fieldsetActionMapping;

    /**
     * Nom de l'action de contrôleur gérant la demande de confirmation
     * de la saisie du formulaire multi-pages.
     * @var array 
     */
    protected $confirmAction;

    /**
     * Nom de l'action de contrôleur gérant la demande d'enregistrement
     * de la saisie du formulaire multi-pages.
     * @var string
     */
    protected $processAction;

    /**
     * Nom de l'action de contrôleur gérant la demande d'annulation
     * de la saisie du formulaire multi-pages.
     * @var string
     */
    protected $cancelAction;

    /**
     * @var MultipageFormNavFieldset
     */
    protected $navigationFieldsetPrototype;

    /**
     * {@inheritdoc}
     */
    public function add($elementOrFieldset, array $flags = [])
    {
        if (is_array($elementOrFieldset) || ($elementOrFieldset instanceof Traversable && ! $elementOrFieldset instanceof ElementInterface)) {
            $factory = $this->getFormFactory();
            $elementOrFieldset = $factory->create($elementOrFieldset);
        }

        // traitement particulier pour les fieldsets
        if ($elementOrFieldset instanceof Fieldset) {
            return $this->_addFieldset($elementOrFieldset, $flags);
        }
        
        return parent::add($elementOrFieldset, $flags);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($elementOrFieldset)
    {
        if (! $this->has($elementOrFieldset)) {
            return $this;
        }

        $elementOrFieldset = $this->get($elementOrFieldset);

        // traitement particulier pour les fieldsets
        if ($elementOrFieldset instanceof Fieldset) {
            return $this->_removeFieldset($elementOrFieldset);
        }

        return parent::remove($elementOrFieldset);
    }

    /**
     * {@inheritdoc}
     */
    public function prepare()
    {
        $this->addNavigationToFieldsets();

        return parent::prepare();
    }

    /**
     * Indique si ce formulaire multipage a été "préparé", i.e. si {@see prepare()}.
     *
     * @return bool
     */
    public function isPrepared()
    {
        return $this->isPrepared;
    }

    /**
     * Ajoute un fieldset d'étape à ce formulaire.
     *
     * @param Fieldset $fieldset
     * @param array $flags
     * @return self
     */
    protected function _addFieldset(Fieldset $fieldset, array $flags = [])
    {
        $name = $fieldset->getName();

        if (in_array($name, $this->fieldsetNames)) {
            throw new LogicException("Ce fieldset a déjà été ajouté.");
        }

        $this->fieldsetNames[] = $name;

        // ajout du fieldset au formulaire
        parent::add($fieldset, $flags);

        return $this;
    }

    /**
     * Retire un fieldset d'étape de ce formulaire.
     *
     * @param Fieldset $fieldset
     * @return self
     */
    protected function _removeFieldset(Fieldset $fieldset)
    {
        $name = $fieldset->getName();
        $key = array_search($name, $this->fieldsetNames);
        if ($key === false) {
            throw new LogicException("Le fieldset '$name' spécifié n'a pas été trouvé dans le formulaire.");
        }

        unset($this->fieldsetNames[$key]);

        // renumérotation des clés
        $this->fieldsetNames = array_values($this->fieldsetNames);

        // retrait du fieldset du formulaire
        parent::remove($name);

        return $this;
    }

    /**
     * Ajoute le fieldset d'étape de récapitulatif / confirmation.
     */
    protected function addConfirmFieldset()
    {
        $this->add(new MultipageFormConfirmFieldset(self::FIELDSET_NAME_CONFIRM));
    }

    /**
     * Indique si ce formulaire possède le fieldset de récapitulatif / confirmation.
     *
     * @return bool
     */
    public function hasConfirmFieldset()
    {
        return $this->has(self::FIELDSET_NAME_CONFIRM);
    }

    /**
     * Retourne le fieldset d'étape de récapitulatif / confirmation.
     *
     * @return MultipageFormConfirmFieldset
     */
    public function getConfirmFieldset()
    {
        /** @var MultipageFormConfirmFieldset $fieldset */
        $fieldset = $this->get(self::FIELDSET_NAME_CONFIRM);

        return $fieldset;
    }

    /**
     * Ajout du fieldset de navigation à chaque fieldset du formulaire.
     */
    protected function addNavigationToFieldsets()
    {
        $fieldsetCount = count($this->fieldsetNames);

        foreach ($this->fieldsetNames as $key => $fieldsetName) {

            /** @var Fieldset $fieldset */
            $fieldset = $this->get($fieldsetName);

            $navigationFieldset = $this->createNavigationFieldset();
            $navigationFieldset->setCancelEnabled(true);

            if ($key === 0) {
                // premier fieldset
                $navigationFieldset
                    ->setPreviousEnabled(false)
                    ->setNextEnabled(true)
                    ->setSubmitEnabled(false)
                    ->setConfirmEnabled(false);
            } elseif ($key === $fieldsetCount - 1) {
                $navigationFieldset
                    ->setPreviousEnabled(true)
                    ->setNextEnabled(false);
                if ($fieldset instanceof MultipageFormConfirmFieldset) {
                    // fieldset de récapitulatif / confirmation
                    $navigationFieldset
                        ->setSubmitEnabled(false)
                        ->setConfirmEnabled(true);
                } else {
                    $navigationFieldset
                        ->setSubmitEnabled(true)
                        ->setConfirmEnabled(false);
                }
            } else {
                // autre fieldset
                $navigationFieldset
                    ->setPreviousEnabled(true)
                    ->setNextEnabled(true)
                    ->setSubmitEnabled(false)
                    ->setConfirmEnabled(false);
            }

            $fieldset->add($navigationFieldset);
        }
    }

    /**
     * @param MultipageFormNavFieldset $navigationFieldsetPrototype
     * @return self
     */
    public function setNavigationFieldsetPrototype(MultipageFormNavFieldset $navigationFieldsetPrototype): self
    {
        $this->navigationFieldsetPrototype = $navigationFieldsetPrototype;

        return $this;
    }

    /**
     * @return MultipageFormNavFieldset
     */
    public function createNavigationFieldset()
    {
        // Un prototype peut avoir été fourni, auquel cas on l'utilise.
        if ($this->navigationFieldsetPrototype !== null) {
            return clone $this->navigationFieldsetPrototype;
        }

        return MultipageFormNavFieldset::create();
    }
    
    /**
     * Retourne le préfixe appliqué aux noms des actions de contrôleur.
     * 
     * @return string
     */
    public function getActionPrefix()
    {
        return $this->actionPrefix;
    }

    /**
     * Retourne le tableau de correspondance [Name du fieldset => Nom de l'action de controller].
     * Par défaut, le nom de l'action est égale au nom du fieldset.
     * 
     * @return array [Name du fieldset => Nom de l'action de controller]
     */
    public function getFieldsetActionMapping()
    {
        if (empty($this->fieldsetActionMapping)) {
            $this->fieldsetActionMapping = $this->computeFieldsetActionMapping();
        }

        return $this->fieldsetActionMapping;
    }

    /**
     * Construit le tableau de correspondance [Name du fieldset => Nom de l'action de controller].
     */
    protected function computeFieldsetActionMapping()
    {
        $confirmMapping = [];
        $mapping = [];
        foreach ($this->getFieldsets() as $fieldset) {
            $name = $fieldset->getName();
            $action = $this->prefixedAction($name);
            if ($name === self::FIELDSET_NAME_CONFIRM) {
                $confirmMapping = [$name => $action];
                continue;
            }
            $mapping[$name] = $action;
        }

        if (!empty($confirmMapping)) {
            $mapping = array_merge($mapping, $confirmMapping);
        }

        return $mapping;
    }

    /**
     * Ajoute le prefixe courant éventuel au nom de l'action spécifié.
     * 
     * @param string $action
     * @return string
     */
    protected function prefixedAction($action)
    {
        if ($this->getActionPrefix() && strpos($action, $this->getActionPrefix()) !== 0) {
            $action = $this->getActionPrefix() . $action;
        }
        return $action;
    }
    
    /**
     * Retourne le nom de l'action de contrôleur gérant la demande de confirmation
     * des informations saisies dans le formulaire multi-pages.
     * Par défaut: 'confirmer'.
     * 
     * @return string
     */
    public function getConfirmAction()
    {
        if (null === $this->confirmAction) {
            $this->confirmAction = self::ACTION_CONFIRM;
        }
        return $this->prefixedAction($this->confirmAction);
    }

    /**
     * Retourne le nom de l'action de contrôleur gérant la demande d'enregistrement
     * de la saisie du formulaire multi-pages.
     * Par défaut: 'enregistrer'.
     * 
     * @return string
     */
    public function getProcessAction()
    {
        if (null === $this->processAction) {
            $this->processAction = self::ACTION_PROCESS;
        }
        return $this->prefixedAction($this->processAction);
    }

    /**
     * Retourne le nom de l'action de contrôleur gérant la demande d'annulation
     * de la saisie du formulaire multi-pages.
     * Par défaut: 'annuler'.
     * 
     * @return string
     */
    public function getCancelAction()
    {
        if (null === $this->cancelAction) {
            $this->cancelAction = self::ACTION_CANCEL;
        }
        return $this->prefixedAction($this->cancelAction);
    }

    /**
     * Spécifie le préfixe appliqué aux noms des actions de contrôleur.
     * 
     * @param string $actionPrefix Ex: 'ajouter-'
     * @return self
     */
    public function setActionPrefix($actionPrefix)
    {
        $this->actionPrefix = $actionPrefix;
        return $this;
    }
    
    /**
     * Spécifie le nom de l'action de contrôleur (sans préfixe) gérant la demande de confirmation
     * de la saisie du formulaire multi-pages.
     * 
     * @param string $confirmAction Nom de l'action sans préfixe
     * @return self
     */
    public function setConfirmAction($confirmAction)
    {
        $this->confirmAction = $confirmAction;
        return $this;
    }

    /**
     * Spécifie le nom de l'action de contrôleur (sans préfixe) gérant la demande d'enregistrement
     * de la saisie du formulaire multi-pages.
     * 
     * @param string $processAction Nom de l'action sans préfixe
     * @return self
     */
    public function setProcessAction($processAction)
    {
        $this->processAction = $processAction;
        return $this;
    }

    /**
     * Spécifie le nom de l'action de contrôleur (sans préfixe) gérant la demande d'annulation
     * de la saisie du formulaire multi-pages.
     * 
     * @param string $cancelAction Nom de l'action sans préfixe
     * @return self
     */
    public function setCancelAction($cancelAction)
    {
        $this->cancelAction = $cancelAction;
        return $this;
    }

    /**
     * Retourne pour chaque élément d'un fieldset son label et sa valeur, dans le format attendu par l'aide de vue
     * {@see MultipageFormRecap} générant un récapitulatif des données saisies.
     *
     * @param Fieldset $fieldset
     * @return array 'element_name' => array('label' => Label de l'élément, 'value' => Valeur saisie au format texte)
     */
    public function getLabelsAndValues(Fieldset $fieldset)
    {
        if ($fieldset instanceof MultipageFormFieldsetInterface) {
            return $fieldset->getLabelsAndValues();
        }

        $values = [];
        foreach ($fieldset->getIterator() as $e) {
            if ($e instanceof Element\Hidden) {
                continue;
            }
            if ($e instanceof MultipageFormNavFieldset) {
                continue;
            }

            $label = $e->getLabel();
            $value = $this->extractElementValueToString($e);

            $values[$e->getName()]['label'] = $label;
            $values[$e->getName()]['value'] = $value ?: "Non renseigné(e)";
        }

        return $values;
    }

    /**
     * Méthode chargée de retourner une ou plusieurs chaînes de caractères représentant la valeur courante
     * de l'élément spécifié.
     *
     * @param Element $element
     * @return string|array|null
     */
    protected function extractElementValueToString(Element $element)
    {
        $value = $element->getValue();

        if ($element instanceof SearchAndSelect) {
            return $element->getValueLabel() ?: $element->getValueId();
        }
        elseif ($element instanceof DoctrineObjectSelect) {
            foreach ($element->getValueOptions() as $key => $optionArray) {
                if (array_key_exists('value', $optionArray) && $optionArray['value'] == $value) {
                    if (array_key_exists('label', $optionArray)) {
                        return $optionArray['label'];
                    }
                }
            }
        }
        elseif ($element instanceof Checkbox) {
            return $value ? "Oui" : "Non";
        }
        elseif ($element instanceof MultiCheckbox || $element instanceof Radio || $element instanceof Select) {
            $options = $element->getValueOptions();
            $valueToString = array_map(function($v) use ($options) {
                return $options[$v] ?? $v;
            }, (array) $value);
            if (count($valueToString) === 1) {
                $valueToString = current($valueToString);
            }
            if (is_array($valueToString) && array_key_exists('label', $valueToString)) {
                $valueToString = $valueToString['label'];
            }
            return $valueToString;
        }
        elseif ($element instanceof Fieldset) {
            $parts = [];
            foreach ($element->getIterator() as $e) {
                /** @var Element $e */
                $subLabel = $e->getLabel();
                $parts[] = trim($subLabel, ' :') . ' : ' . $this->extractElementValueToString($e);
            }
            return $parts; // pour un fieldset, il y a 1 chaîne de caractères par élément
        }

        return (string) $value;
    }
}

<?php
namespace UnicaenApp\Service\Ldap;

use Psr\Container\ContainerInterface;
use Laminas\Ldap\Ldap;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * 
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class GroupFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('unicaen-app_module_options')->getLdap();
//        if (!$config) {
//            throw new \UnicaenApp\Exception(
//                    "Impossible de créer le service d'accès aux structures " .
//                    "car aucune info de connexion à l'annuaire LDAP n'a été fournie (option 'ldap_connection_infos').");
//        }
        $options = isset($config['connection']['default']['params']) ? $config['connection']['default']['params'] : array();

        return new Group(new Ldap($options));
    }
}

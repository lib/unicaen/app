<?php

namespace UnicaenApp\Service;

use UnicaenApp\Exception\RuntimeException;

/**
 * Interface contractualisant l'accès au service MessageCollector.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
interface MessageCollectorAwareInterface
{
    /**
     * Spécifie le service MessageCollector.
     *
     * @param MessageCollector $messageCollector
     * @return self
     */
    public function setServiceMessageCollector(MessageCollector $messageCollector = null);

    /**
     * Retourne le service MessageCollector.
     *
     * @return MessageCollector
     * @throws RuntimeException
     */
    public function getServiceMessageCollector();
}
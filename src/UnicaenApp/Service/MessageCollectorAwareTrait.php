<?php

namespace UnicaenApp\Service;

use UnicaenApp\Exception\RuntimeException;

/**
 * Trait facilitant l'accès au service MessageCollector.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
trait MessageCollectorAwareTrait
{
    /**
     * @var MessageCollector
     */
    private $messageCollector;

    /**
     * Spécifie le service MessageCollector.
     *
     * @param MessageCollector $messageCollector
     * @return self
     */
    public function setServiceMessageCollector(MessageCollector $messageCollector = null)
    {
        $this->messageCollector = $messageCollector;
        
        return $this;
    }

    /**
     * Retourne le service MessageCollector.
     *
     * @return MessageCollector
     * @throws RuntimeException
     */
    public function getServiceMessageCollector()
    {
        return $this->messageCollector;
    }
}
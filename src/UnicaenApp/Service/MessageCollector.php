<?php

namespace UnicaenApp\Service;

use UnicaenApp\ServiceManager\ServiceLocatorAwareInterface;
use UnicaenApp\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Description of MessageCollector
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MessageCollector implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    use \UnicaenApp\Traits\MessageAwareTrait;
}
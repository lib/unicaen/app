<?php

namespace UnicaenApp\Service;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\InitializerInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Transmet à une instance le gestionnaire d'entité Doctrine, ssi sa classe implémente l'interface qui va bien.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see EntityManagerAwareInterface
 */
class EntityManagerAwareInitializer implements InitializerInterface
{
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        $this->__invoke($serviceLocator, $instance);
    }

    public function __invoke(ContainerInterface $container, $instance)
    {
        if ($instance instanceof EntityManagerAwareInterface) {
            $instance->setEntityManager($container->get('doctrine.entitymanager.orm_default'));
        }
    }
}

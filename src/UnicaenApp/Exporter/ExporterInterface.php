<?php
namespace UnicaenApp\Exporter;

/**
 * 
 *
 * @author bertrand.gauthier@unicaen.fr
 */
interface ExporterInterface
{
    
    /**
     *
     * @return \Laminas\View\Renderer\PhpRenderer
     */
    public function getRenderer();
    
    /**
     *
     * @param string $filename
     */
    public function export($filename = null);
}
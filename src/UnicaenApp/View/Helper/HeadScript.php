<?php

namespace UnicaenApp\View\Helper;

class HeadScript extends \Laminas\View\Helper\InlineScript
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @param array $config
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * Return InlineScript object
     *
     * Returns InlineScript helper object; optionally, allows specifying a
     * script or script file to include.
     *
     * @param  string $mode      Script or file
     * @param  string $spec      Script/url
     * @param  string $placement Append, prepend, or set
     * @param  array  $attrs     Array of script attributes
     * @param  string $type      Script type and/or array of script attributes
     * @return InlineScript
     */
    public function __invoke(
        $mode = self::FILE,
        $spec = null,
        $placement = 'APPEND',
        array $attrs = array(),
        $type = 'text/javascript'
    ) {
        $this->appendConfigScripts();
        return parent::__invoke($mode, $spec, $placement, $attrs, $type);
    }



    protected function appendConfigScripts()
    {
        $config = $this->config;

        $version = isset($config['unicaen-app']['app_infos']['version']) ? '?v='.$config['unicaen-app']['app_infos']['version'] : '';

        $publicFiles = isset( $config['public_files'] ) ? $config['public_files'] : [];
        $jsFiles  = isset($publicFiles['head_scripts' ]) ? $publicFiles['head_scripts' ] : [];
        ksort($jsFiles);

        $offset = 0;
        foreach( $jsFiles as $jsFile ){
            if ($jsFile){
                $offset++;
                if (0 === strpos($jsFile,'//') || 0 === strpos($jsFile,'http://') || 0 === strpos($jsFile,'https://')){
                    $url = $jsFile;
                }else{
                    $url = $this->getView()->basePath($jsFile);
                    if ($version) $url .= '?v=' . $version;
                }
                $this->offsetSetFile($offset, $url, 'text/javascript');
            }
        }
    }
}
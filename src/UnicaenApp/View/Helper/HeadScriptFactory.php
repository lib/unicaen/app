<?php

namespace UnicaenApp\View\Helper;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class HeadScriptFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $helper = new HeadScript();

        $helper->setConfig($container->get('config'));

        return $helper;
    }
}
<?php

namespace UnicaenApp\View\Helper;

use Laminas\View\Helper\Placeholder\Container\AbstractContainer;

class HeadLink extends \Laminas\View\Helper\HeadLink
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @param array $config
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(array $attributes = null, $placement = AbstractContainer::APPEND)
    {
        $this->appendConfigStyles();
        return parent::__invoke($attributes, $placement);
    }



    /**
     * Ajoute des fichiers CSS décrits dans la config
     *
     * @return $this
     */
    protected function appendConfigStyles()
    {
        $config = $this->config;

        $cacheEnabled = isset($config['public_files']['cache_enabled']) ? (boolean)$config['public_files']['cache_enabled'] : false;
        $version = isset($config['unicaen-app']['app_infos']['version']) ? $config['unicaen-app']['app_infos']['version'] : '';

        $publicFiles = isset( $config['public_files'] ) ? $config['public_files'] : [];
        $cssFiles  = isset($publicFiles['stylesheets' ]) ? $publicFiles['stylesheets' ] : [];
        ksort($cssFiles);

        $offset = 0;
        foreach( $cssFiles as $cssFile ){
            if ($cssFile) {
                $offset++;
                if (0 === strpos($cssFile, '//') || 0 === strpos($cssFile, 'http://') || 0 === strpos($cssFile, 'https://')) {
                    $this->offsetSetStylesheet($offset, $cssFile, 'all');
                } elseif (!$cacheEnabled) {
                    $url = $this->getView()->basePath($cssFile);
                    if ($version) $url .= '?v=' . $version;
                    $this->offsetSetStylesheet($offset, $url, 'all');
                }
            }
        }
        if ($cacheEnabled){
            $this->offsetSetStylesheet(999,$this->getView()->url('cache/css', ['version' => $version]), 'all');
        }

        return $this;
    }
}
<?php

namespace UnicaenApp\View\Helper;

use UnicaenApp\Service\MessageCollector;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue donnant accès au collecteur de messages.
 * 
 * @see \UnicaenApp\Service\MessageCollector
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class MessageCollectorHelper extends AbstractHelper
{
    /**
     * @var string
     */
    protected $severity;

    /**
     * @var MessageCollector
     */
    protected $messageCollectorService;

    /**
     * @var Messenger
     */
    protected $messengerHelper;

    /**
     * @param MessageCollector $messageCollectorService
     */
    public function setMessageCollectorService(MessageCollector $messageCollectorService)
    {
        $this->messageCollectorService = $messageCollectorService;
    }

    /**
     * Point d'entrée.
     *
     * @param string $severity
     * @return MessageCollectorHelper
     */
    public function __invoke($severity = null)
    {
        return $this;
    }

    /**
     * @param string $severity
     * @return string Code HTML
     */
    public function render($severity = null)
    {
        $severity = $severity ?: $this->severity;

        if (! $this->messageCollectorService->hasMessages($severity)) {
            return '';
        }

        $messages = $this->messageCollectorService->getMessages($severity);
        $keys = array_fill(0, count($messages), $severity);

        $messenger = $this->getMessengerHelper();
        $messenger->setMessages(array_combine($keys, $messages));

        return (string) $messenger;
    }

    /**
     * Retourne (l'instance de) l'aide de vue utilisée pour afficher les messages.
     * Utile pour ajouter une classe CSS par exemple.
     *
     * @return Messenger
     */
    public function getMessengerHelper()
    {
        if ($this->messengerHelper == null) {
            $view = $this->getView();
            /** @var PhpRenderer $view */
            $this->messengerHelper = clone $view->plugin('messenger');
            /* @var $messenger Messenger */
        }

        return $this->messengerHelper;
    }

    /**
     * Retourne le code HTML généré par cette aide de vue.
     *
     * NB: c'est la sévérité courante qui est utilisée.
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->render($this->severity);
    }
}
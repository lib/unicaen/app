<?php

namespace UnicaenApp\View\Helper;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use UnicaenApp\Util;

/**
 * Description of AppLinkFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AppLinkFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $sl     = $container;
        $router = Util::isConsole() ? 'HttpRouter' : 'Router';
        $match  = $sl->get('application')->getMvcEvent()->getRouteMatch();
        $helper = new AppLink();

        $helper->setRouter($sl->get($router));

        if ($match instanceof \Laminas\Router\RouteMatch) {
            $helper->setRouteMatch($match);
        }

        return $helper;
    }
}
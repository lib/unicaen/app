<?php

namespace UnicaenApp\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenApp\Service\MessageCollector;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class MessageCollectorHelperFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $helper = new MessageCollectorHelper();

        /** @var MessageCollector $messageCollectorService */
        $messageCollectorService = $container->get('MessageCollector');

        $helper->setMessageCollectorService($messageCollectorService);

        return $helper;
    }
}
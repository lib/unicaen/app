<?php

namespace UnicaenApp\View\Helper\Navigation;

use Laminas\Navigation\AbstractContainer;
use Laminas\View\Helper\Navigation\Breadcrumbs;

/**
 * Aide de vue générant le code HTML du "fil d'Ariane" (breadcrumbs) de l'application.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class FilAriane extends Breadcrumbs
{
    /**
     * Constructeur.
     */
    public function __construct()
    {
        $this
            ->setSeparator('</li><li class="breadcrumb-item">')
            ->setRenderInvisible(true)
            ->setMinDepth(0);
    }
    
    /**
     * {@inheritdoc}
     */
    public function render($container = null)
    {
        $html = $this->parentRender($container);
        if ($html === '') {
            return '';
        }

        return sprintf(<<<EOF
<nav id="breadcrumb" aria-label="breadcrumb"> 
    <ol class="breadcrumb">
        <li class="breadcrumb-item">%s</li>
    </ol>
</nav>
EOF
, $html);
    }

    /**
     * Méthode extraite pour permettre les tests unitaires.
     *
     * @param string|AbstractContainer $container
     * @return string
     */
    protected function parentRender($container = null)
    {
        return parent::render($container);
    }
}
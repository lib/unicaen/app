<?php

namespace UnicaenApp\View\Helper\Navigation;

use RecursiveIteratorIterator;
use Laminas\Navigation\Page\AbstractPage;

/**
 * Dessine le menu secondaire de l'application (vertical).
 * 
 * Au préalable, mettons-nous d'accord sur niveaux et profondeurs : 
 *    "Accueil" (niv 1, depth 0) > "Deux roues" (niv 2, depth 1) > "Vélos" (niv 3, depth 2) > "VTT" (niv 4, depth 3)
 * 
 * Les règles suivantes sont appliquées :
 * 
 * - Seules les pages visibles sont prises en compte.
 * 
 * - Si la page active est de niveau 1 (accueil), le menu est vide 
 *   (NB: les pages de niveau 2 apparaissent dans le menu principal).
 * 
 * - Si la page active est de niveau 2, seules ses pages filles éventuelles (de niveau 3)
 *   apparaissent dans le menu.
 * 
 * - Si la page active est de niveau N supérieur à 3, elle continue d'apparaître dans le menu, et
 *   ses pages filles (niveau N+1) éventuelles apparaissent dessous.
 *
 * NB : il est quand même possible d'utiliser {@see setMinDepth()} pour changer le seuil de recherche.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class MenuSecondaire extends AbstractMenu
{
    /**
     * CSS class to use for the ul element
     *
     * @var string
     */
    protected $ulClass = 'nav flex-column menu-secondaire';

    /**
     * @var bool
     */
    protected $doNotRender = false;

    /**
     * Désactive le rendu du menu secondaire.
     */
    public function doNotRender()
    {
        $this->doNotRender = true;
    }

    /**
     * {@inheritdoc}
     */
    public function render($container = null)
    {
        if ($this->doNotRender) {
            return '';
        }

        return parent::render($container);
    }

    /**
     * {@inheritdoc}
     */
    public function renderMenu($container = null, array $options = array())
    {
        $this->parseContainer($container);
        if (null === $container) {
            $container = $this->getContainer();
        }

        $options = $this->normalizeOptions($options);
        
//        echo PHP_EOL;
//        $iterator = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);
//        foreach ($iterator as $page) { /* @var $page \Laminas\Navigation\Page\Mvc */
//            echo str_repeat('  ', $iterator->getDepth()) . sprintf("%s) %s <%s> %s" . PHP_EOL, 
//                    $iterator->getDepth(),
//                    $page->get('route'),
//                    $page->get('label'),
//                    ($page->isActive(true) ? 'A' : ''));
//        }
//        echo PHP_EOL;
        
//        $prec = $this->getRenderInvisible(); // valeur initiale rétablie plus bas
//        $this->setRenderInvisible(true);
        
        // recherche de la page active à partir du niveau seuil éventuellement spécifié
        $minDepth = $this->getMinDepth() > 0 ? $this->getMinDepth() : 1;
        $found = $this->findActive($container, $minDepth, $minDepth);
        if (!$found) {
            return '';
        }
        $activePage = $found['page'];
        
        // recherche de la page active quelque soit son niveau pour déterminer la profondeur maxi utilisée plus bas
        $found = $this->findActive($container, 1);
        $maxDepth = $found['depth'] - 1;
        
//        $this->setRenderInvisible($prec);
        
        // on ne considère que le sous-menu correspondant à la page active
        $container = $activePage;
        
        // suppression des pages qu'on ne veut pas prendre en compte
        $iterator = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);
        $iterator->setMaxDepth(0);
        foreach ($iterator as $page) { /* @var $page AbstractPage */
            $isActive = $page->isActive(true);
            if (!$isActive && $page->hasChildren()) {
                $page->removePages();
            }
        }
        
//        echo PHP_EOL;
//        $iterator = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);
//        foreach ($iterator as $page) { /* @var $page \Laminas\Navigation\Page\Mvc */
//            echo str_repeat('  ', $iterator->getDepth()) . sprintf("%s <%s> %s" . PHP_EOL, 
//                    $page->get('route'),
//                    $page->get('label'),
//                    ($page->isActive(true) ? 'A' : ''));
//        }
//        echo PHP_EOL;
        
        return $this->renderNormalMenu(
                $container,
                $options['ulClass'],
                $options['indent'],
                0,
                $maxDepth,
                $options['onlyActiveBranch'],
                $options['escapeLabels'],
                $options['addClassToListItem'],
                isset($options['liActiveClass']) ? $options['liActiveClass'] : null
        );
    }
}
<?php

namespace UnicaenApp\View\Helper\Upload;

use Psr\Container\ContainerInterface;
use Laminas\Mvc\Controller\PluginManager;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class UploaderHelperFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $helper = new UploaderHelper();
        
        /** @var PluginManager $cpm */
        $cpm = $container->get('ControllerPluginManager');

        $helper->setControllerPluginManager($cpm);

        return $helper;
    }
}
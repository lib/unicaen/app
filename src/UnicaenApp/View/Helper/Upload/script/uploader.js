/**
 *
 */
$(function() {
    installUpload();
});

/**
 * Recherche tous les containers présents sur la page et
 * installe sur chacun le nécessaire pour gérer l'upload de fichier en mode AJAX.
 */
function installUpload()
{
    $(".upload-container").each(function() {
        var container = $(this);
        installUploaderOn(container);
    });
}

/**
 * Installe (si cela n'a pas déjà été fait) sur un container le nécessaire pour gérer l'upload de fichier en mode AJAX.
 *
 * @param {object} container
 */
function installUploaderOn(container)
{
    if (! $(container).hasClass("upload-container")) {
        console.error("Pour installer un uploader, le container doit avoir la classe 'upload-container'");
    }
    if (container.data('uploader-installed') === "1") {
        return;
    }

    var uploadEventFilesListed  = "upload-event-files-listed";
    var uploadEventFileDeleted  = "upload-event-file-deleted";
    var uploadEventFileUploaded = "upload-event-file-uploaded";

    var form      = container.find(".upload-form");
    var filesDiv  = container.find(".uploaded-files-div");
    var choose    = container.find(".choose-file");

    form.ajaxForm({
        success: function(responseText, statusText, xhr, form) {
            // détecte si des erreurs sont retournées
            if (responseText.errors) {
                updateUploadContainer(container, false);
                alert("Impossible de déposer le fichier!\n- " + responseText.errors.join('\n- '));
            }
            else {
                updateUploadContainer(container, true, function(){
                    $("body").trigger(uploadEventFileUploaded, [ container ]);
                });
            }
        },
        error: function() {
            alert("Oups, une erreur s'est produite pendant l'envoi de fichier! Essayez à nouveau, svp.");
        },
        beforeSubmit: function(arr, $form, options) {
            // The array of form data takes the following form:
            // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
            // interdiction du bouton d'envoi
            form.find(".upload-file").button('loading');
            // ajout d'un témoin de chargement AJAX
            if (! filesDiv.find("ul").length) {
                filesDiv.html("<ul/>");
            }
            filesDiv.find("ul").append($("<li/>").addClass("loading"));
            // return false to cancel submit
        }
    });

    // chargement initial de la liste des fichiers
    filesDiv.addClass("loading").refresh([], function() {
        filesDiv.hide().removeClass("loading").fadeIn();
        $("body").trigger(uploadEventFilesListed, [ container ]);
    });

    // écoute clic sur suppression de fichier pour faire la requête AJAX et rafraîchir la liste des fichiers
    filesDiv.on("click", ".delete-file", function(event) {
        var a = $(this);
        a.button('loading');
        $.post(a.prop('href'), [], function(data, textStatus, jqXHR) {
            a.parent("li").fadeOut();
            filesDiv.refresh({}, function(){
                $("body").trigger(uploadEventFileDeleted, [ container ]);
            });

        });
        event.preventDefault();
    });

    // affichage/masquage bouton d'envoi selon sélection de fichier
    choose.on("change", function() {
        updateUploadButton(container);
    });

    // masquage initial du bouton d'envoi
    updateUploadButton(container);

    container.data('uploader-installed', "1");
}

/**
 * Rafraîchit la liste des fichiers déposés puis quand c'est fait :
 * - réinitialise le formulaire, si demandé ;
 * - autorise le bouton "Envoyer"
 * - affiche ou masque le bouton "Envoyer" en fonction de la situation.
 *
 * @param {object} container
 * @param {boolean} clearForm
 * @param {function} trigFnc
 */
function updateUploadContainer(container, clearForm, trigFnc)
{
    var form     = container.find(".upload-form");
    var filesDiv = container.find(".uploaded-files-div");

    filesDiv.refresh([], function() {
        if (clearForm) {
            form.clearForm();
        }
        if (trigFnc) trigFnc();
        form.find(".upload-file").button('reset');
        updateUploadButton(container);
    });
}

/**
 * Affiche ou masque le bouton "Envoyer" selon qu'un fichier a été sélectionné ou non
 * via le bouton "Parcourir".
 *
 * @param {object} container
 */
function updateUploadButton(container)
{
    var browseButton = container.find(".choose-file");
    var sendBtn      = browseButton.siblings(".upload-file");

    browseButton.val() ? sendBtn.fadeIn() : sendBtn.hide();
}
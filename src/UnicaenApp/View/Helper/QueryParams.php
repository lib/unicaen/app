<?php

namespace UnicaenApp\View\Helper;

use Laminas\Http\Request;
use Laminas\View\Helper\AbstractHelper;

class QueryParams extends AbstractHelper
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Retourne le tableau des paramètres GET (query parameters).
     *
     * @return array
     */
    public function __invoke()
    {
        return $this->request->getUri()->getQueryAsArray();
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }
}
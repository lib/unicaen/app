<?php
namespace UnicaenApp\View\Helper;

use Exception;
use UnicaenApp\Traits\MessageAwareInterface;
use UnicaenApp\Traits\MessageAwareTrait;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerAwareTrait;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger as FlashMessengerPlugin;
use Laminas\View\Helper\AbstractHelper;

/**
 * Aide de vue permettant de stocker une liste de messages d'information de différentes sévérités
 * et de générer le code HTML pour les afficher (affublés d'un icône correspondant à leur sévérité).
 *
 * Possibilité d'importer les messages du FlashMessenger pour les mettre en forme de la même manière
 * (avec support du namespace, ex: 'these/info' ou 'these/*').
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Messenger extends AbstractHelper implements MessageAwareInterface, EventManagerAwareInterface
{
    const EVENT_RENDER = 'unicaen-app-messenger-render';

    use EventManagerAwareTrait;
    use MessageAwareTrait;

    const NAMESPACED_SEVERITY_SEPARATOR = '/';

    protected $uiClasses = [
        // severity   => [ alert class, icon class ]
        self::INFO    => ['info', 'info-circle'],
        self::SUCCESS => ['success', 'check-circle'],
        self::WARNING => ['warning', 'exclamation-circle'],
        self::ERROR   => ['danger', 'exclamation-triangle'],
    ];

    protected $severityOrder = [
        // severity   => order
        self::SUCCESS => 1,
        self::ERROR   => 2,
        self::WARNING => 3,
        self::INFO    => 4,
    ];

    /**
     * Activation ou non de l'affichage de l'icône
     *
     * @var bool
     */
    protected $withIcon = true;

    /**
     * @var string
     */
    protected $containerInnerTemplate = '%s';

    /**
     * @var array
     */
    protected $containerClassesToAdd = [];

    /**
     * @var FlashMessengerPlugin
     */
    protected $pluginFlashMessenger;

    /**
     * Helper entry point.
     *
     * @return static
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Retourne le code HTML généré par cette aide de vue.
     *
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->render();
        } catch (Exception $exc) {
            var_dump($exc->getMessage(), \UnicaenApp\Util::formatTraceString($exc->getTraceAsString()));
            die;
        }
    }

    /**
     * Génère le code HTML.
     *
     * @return string
     */
    protected function render()
    {
        if (!$this->hasMessages()) {
            return '';
        }

        $out = '';

        foreach ($this->getSortedMessages() as $severity => $array) {
            foreach ($array as $priority => $message) {
                if ($em = $this->getEventManager()){
                    $em->trigger(self::EVENT_RENDER, null, compact('severity','message'));
                }

                $out .= sprintf(
                    $this->getTemplate(is_string($severity) ? $severity : (is_string($priority) ? $priority : 'info')),
                    implode('<br />', (array)$message)
                );;
            }
        }

        return $out;
    }

    /**
     * Génère le code HTML d'un seul message. Pour usage ponctuel.
     *
     * @param string $message  Message à afficher
     * @param string $severity Ex: MessageAwareInterface::INFO
     *
     * @return string
     */
    public function renderMessage($message, $severity = null)
    {
        if (!$message) {
            return '';
        }

        return sprintf($this->getTemplate($severity ?: 'info'), $message);
    }

    /**
     * @return array
     */
    protected function getSortedMessages()
    {
        $messages = (array)$this->getMessages();
        $order    = $this->severityOrder;

        uksort($messages, function ($s1, $s2) use ($order) {
            if ($order[$s1] < $order[$s2]) {
                return -1;
            }
            if ($order[$s1] > $order[$s2]) {
                return 1;
            }

            return 0;
        });

        return $messages;
    }

    /**
     * Importe les messages n-1 du FlashMessenger.
     *
     * @param string $namespace
     * @return static
     */
    protected function importFlashMessages($namespace = null)
    {
        return $this->_importFromFlashMessages($currentMessages = false, $namespace);
    }

    /**
     * Importe les messages courants du FlashMessenger.
     *
     * @param string $namespace
     * @return static
     */
    protected function importCurrentFlashMessages($namespace = null)
    {
        return $this->_importFromFlashMessages($currentMessages = true, $namespace);
    }

    /**
     * @param bool $currentMessages
     * @param string|null $namespace Facultatif, ex: 'info', 'these/danger', 'these/*', 'these' (i.e. 'these/*')
     * @return static
     */
    private function _importFromFlashMessages(bool $currentMessages, string $namespace = null)
    {
        $getMethod = $currentMessages ? 'getCurrentMessagesFromNamespace' : 'getMessagesFromNamespace';

        $fm = $this->getPluginFlashMessenger();

        if ($namespace) {
            // collecte des messages dans les namespaces de la forme "namespace/sévérité"
            $array = $this->extractSeveritiesAndNamespaces($namespace);
            foreach ($array as $sev => $ns) {
                foreach ($fm->$getMethod($ns) as $message) {
                    $this->addMessage($message, $sev);
                }
                if ($currentMessages) {
                    /* Si on importe alors on nettoie pour éviter un deuxième affichage */
                    $fm->clearCurrentMessagesFromNamespace($ns);
                }
            }

            // collecte des messages dans le namespace tel quel (fonctionne aussi si le namespace est une sévérité)
            foreach ((array)$fm->$getMethod($namespace) as $message) {
                $this->addMessage($message, $namespace);
            }
            if ($currentMessages) {
                /* Si on importe alors on nettoie pour éviter un deuxième affichage */
                $fm->clearCurrentMessagesFromNamespace($namespace);
            }
        }
        else {
            foreach ((array)$fm->$getMethod('error') as $message) {
                $this->addMessage($message, self::ERROR);
            }
            foreach ((array)$fm->$getMethod('success') as $message) {
                $this->addMessage($message, self::SUCCESS);
            }
            foreach ((array)$fm->$getMethod('info') as $message) {
                $this->addMessage($message, self::INFO);
            }
            foreach ((array)$fm->$getMethod('warning') as $message) {
                $this->addMessage($message, self::WARNING);
            }

            if ($currentMessages) {
                $fm->clearCurrentMessagesFromContainer();
            }
        }

        return $this;
    }

    /**
     * @param string $namespace Ex: 'these/danger' ou 'these/*'
     * @return array 'severity' => 'namespace'
     */
    private function extractSeveritiesAndNamespaces($namespace)
    {
        // Normalisation : $namespace doit être de la forme 'namespace/severity' ou 'namespace/*'.
        // Ex: 'these/danger' ou 'these/*'
        $separatorFound = strrpos($namespace, $sep = self::NAMESPACED_SEVERITY_SEPARATOR) !== false;
        if (! $separatorFound) {
            $namespace = $namespace . '/*';
        }

        $parts = explode($sep, $namespace);
        $severity = array_pop($parts);

        // Si $namespace est de la forme 'namespace/*', cela revient à importer les namespaces
        // 'namespace/danger', 'namespace/success', 'namespace/info' et 'namespace/warning'.
        if ($severity === '*') {
            $namespacePrefix = implode(self::NAMESPACED_SEVERITY_SEPARATOR, $parts) . '/';
            $array = [
                $s = self::ERROR   => $namespacePrefix . $s,
                $s = self::SUCCESS => $namespacePrefix . $s,
                $s = self::INFO    => $namespacePrefix . $s,
                $s = self::WARNING => $namespacePrefix . $s,
            ];
        } else {
            $array = [
                $severity => $namespace,
            ];
        }

        return $array;
    }

    /**
     * Spécifie l'unique message courant au format Exception.
     *
     * @param Exception $exception Exception
     * @param string    $severity  Ex: Messenger::INFO
     *
     * @return static
     */
    public function setException(Exception $exception, $severity = self::ERROR)
    {
        $message = sprintf("<p><strong>%s</strong></p>", $exception->getMessage());
        if (($previous = $exception->getPrevious())) {
            $message .= sprintf("<p>Cause :<br />%s</p>", $previous->getMessage());
        }

        return $this->setMessages([$severity => $message]);
    }

    /**
     * @param string|null $namespace
     * @return static
     * @deprecated Utilisez plutôt setMessagesFromFlashMessengerWithNamespace($namespace)
     *                          ou setMessagesFromFlashMessengerWithNoNamespace()
     */
    public function setMessagesFromFlashMessenger($namespace = null)
    {
        $this->messages = [];
        $this->importFlashMessages($namespace);

        return $this;
    }

    /**
     * Remplace les messages existants par les messages *courants* du FlashMessenger stoqués dans le namespace spécifié.
     *
     * @param string $namespace
     * @return static
     */
    public function setMessagesFromFlashMessengerWithNamespace($namespace)
    {
        $this->messages = [];
        $this->importFlashMessages($namespace);

        return $this;
    }

    /**
     * Remplace les messages existants par les messages *courants* du FlashMessenger n'étant pas stoqués dans un namespace particulier
     * (ou pour être exact qui sont stoqués dans le namespace 'error', 'success', 'info' ou 'warning').
     *
     * @return static
     */
    public function setMessagesFromFlashMessengerWithNoNamespace()
    {
        $this->messages = [];
        $this->importFlashMessages();

        return $this;
    }

    /**
     * @param string|null $namespace
     * @return static
     * @deprecated Utilisez plutôt setCurrentMessagesFromFlashMessengerWithNamespace($namespace)
     *                          ou setCurrentMessagesFromFlashMessengerWithNoNamespace()
     */
    public function setCurrentMessagesFromFlashMessenger($namespace = null)
    {
        $this->messages = [];
        $this->importCurrentFlashMessages($namespace);

        return $this;
    }

    /**
     * Remplace les messages existants par les messages *en session* du FlashMessenger stoqués dans le namespace spécifié.
     *
     * @param string $namespace
     * @return static
     */
    public function setCurrentMessagesFromFlashMessengerWithNamespace($namespace)
    {
        $this->messages = [];
        $this->importCurrentFlashMessages($namespace);

        return $this;
    }

    /**
     * Remplace les messages existants par les messages *en session* du FlashMessenger n'étant pas stoqués dans un namespace particulier
     * (ou pour être exact qui sont stoqués dans le namespace 'error', 'success', 'info' ou 'warning').
     *
     *
     * @return static
     */
    public function setCurrentMessagesFromFlashMessengerWithNoNamespace()
    {
        $this->messages = [];
        $this->importCurrentFlashMessages();

        return $this;
    }

    /**
     * @param string|null $namespace
     * @return static
     * @deprecated Utilisez plutôt addMessagesFromFlashMessengerWithNamespace($namespace)
     *                          ou addMessagesFromFlashMessengerWithNoNamespace()
     */
    public function addMessagesFromFlashMessenger($namespace = null)
    {
        $this->importFlashMessages($namespace);

        return $this;
    }

    /**
     * Ajoute aux messages à afficher les messages *courants* du FlashMessenger stoqués dans le namespace spécifié.
     *
     * @param string $namespace
     * @return static
     */
    public function addMessagesFromFlashMessengerWithNamespace($namespace)
    {
        $this->importFlashMessages($namespace);

        return $this;
    }

    /**
     * Ajoute aux messages à afficher les messages *courants* du FlashMessenger n'étant pas stoqués dans un namespace particulier
     * (ou pour être exact qui sont stoqués dans le namespace 'error', 'success', 'info' ou 'warning').
     *
     * @return static
     */
    public function addMessagesFromFlashMessengerWithNoNamespace()
    {
        $this->importFlashMessages();

        return $this;
    }

    /**
     * @param string $namespace
     * @return static
     * @deprecated Utilisez plutôt addCurrentMessagesFromFlashMessengerWithNamespace($namespace)
     *                          ou addCurrentMessagesFromFlashMessengerWithNoNamespace()
     */
    public function addCurrentMessagesFromFlashMessenger($namespace = null)
    {
        $this->importCurrentFlashMessages($namespace);

        return $this;
    }

    /**
     * Ajoute aux messages à afficher les messages *en session* du FlashMessenger stoqués dans le namespace spécifié.
     *
     * @param string $namespace
     * @return static
     */
    public function addCurrentMessagesFromFlashMessengerWithNamespace($namespace)
    {
        $this->importCurrentFlashMessages($namespace);

        return $this;
    }

    /**
     * Ajoute aux messages à afficher les messages *en session* du FlashMessenger n'étant pas stoqués dans un namespace particulier
     * (ou pour être exact qui sont stoqués dans le namespace 'error', 'success', 'info' ou 'warning').
     *
     * @return static
     */
    public function addCurrentMessagesFromFlashMessengerWithNoNamespace()
    {
        $this->importCurrentFlashMessages();

        return $this;
    }

    /**
     * Active ou non l'affichage de l'icône.
     *
     * @param bool $withIcon <tt>true</tt> pour activer l'affichage de l'icône.
     *
     * @return static
     */
    public function withIcon($withIcon = true)
    {
        $this->withIcon = (bool)$withIcon;

        return $this;
    }

    /**
     * Retourne le motif utilisé pour générer le conteneur de chaque message à afficher.
     *
     * @param string $severity    Ex: Messenger::INFO
     * @param string $containerId Dom id éventuel à utiliser pour la div englobante
     *
     * @return string
     */
    public function getTemplate($severity, $containerId = null)
    {
        $severity = array_key_exists($severity, $this->uiClasses) ? $severity : self::INFO;

        $alertClass = $this->uiClasses[$severity][0];
        $iconClass  = $this->uiClasses[$severity][1];

        $innerTemplate = $this->containerInnerTemplate ?: '%s';
        $iconMarkup    = $this->withIcon ? "<span class=\"fas fa-$iconClass\"></span>" : null;
        $containerId   = $containerId ? sprintf('id="%s"', $containerId) : '';

        $classesToAdd = $this->containerClassesToAdd ? implode(' ', $this->containerClassesToAdd) : '';

        $template = <<<EOT
<div class="messenger alert alert-dismissible alert-$alertClass $classesToAdd" $containerId>
    $iconMarkup
    $innerTemplate
    <button type="button" class="btn-close alert-dismissible" title="Fermer cette alerte" data-bs-dismiss="alert"></button>
</div>
EOT;

        return $template . PHP_EOL;
    }

    /**
     * @param array|string $containerClassesToAdd Ex: ['alert-md']
     * @return static
     */
    public function setContainerClassesToAdd($containerClassesToAdd)
    {
        $this->containerClassesToAdd = (array) $containerClassesToAdd;

        return $this;
    }

    /**
     *
     * @param string $containerInnerTemplate
     *
     * @return static
     */
    public function setContainerInnerTemplate($containerInnerTemplate = '%s')
    {
        $this->containerInnerTemplate = $containerInnerTemplate;

        return $this;
    }

    /**
     * @param FlashMessengerPlugin $pluginFlashMessenger
     */
    public function setPluginFlashMessenger(FlashMessengerPlugin $pluginFlashMessenger)
    {
        $this->pluginFlashMessenger = $pluginFlashMessenger;
    }

    /**
     * @return FlashMessengerPlugin
     */
    protected function getPluginFlashMessenger()
    {
        return $this->pluginFlashMessenger;
    }
}
<?php

namespace UnicaenApp\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenApp\Options\ModuleOptions;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of AppInfosFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AppInfosFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options = $container->get('unicaen-app_module_options'); /* @var $options ModuleOptions */

        return new AppInfos($options->getAppInfos());
    }
}
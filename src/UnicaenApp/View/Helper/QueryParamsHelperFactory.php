<?php

namespace UnicaenApp\View\Helper;

use Psr\Container\ContainerInterface;
use Laminas\Http\Request;

class QueryParamsHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return QueryParams
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var Request $request */
        $request = $container->get('request');

        $helper = new QueryParams();
        $helper->setRequest($request);

        return $helper;
    }
}
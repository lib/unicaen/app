<?php

namespace UnicaenApp\View\Helper;

use Exception;
use UnicaenApp\Util;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue qui génère un lien permettant d'afficher/masquer un container.
 *
 * @method PhpRenderer getView()
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class ToggleDetails extends AbstractHelper
{
    /**
     * @var bool
     */
    protected static $inlineJsAppended = false;

    /**
     * @var string
     */
    protected $domid;

    /**
     * @var string
     */
    protected $detailsDivId = null;

    /**
     * @var bool
     */
    protected $rememberState = true;

    /**
     * @var string
     */
    protected $showDetailsIconClass; // ex: 'fas fa-chevron-up';

    /**
     * @var string
     */
    protected $hideDetailsIconClass; // ex: 'fas fa-chevron-down;

    /**
     * @var string
     */
    protected $showDetailsTitle = "Cliquez pour afficher les détails";

    /**
     * @var string
     */
    protected $hideDetailsTitle = "Cliquez pour masquer les détails";

    /**
     * @var string
     */
    protected $showDetailsLabel = "Afficher";

    /**
     * @var string
     */
    protected $hideDetailsLabel = "Masquer";

    /**
     * Helper entry point.
     *
     * @param string $detailsDivId  Dom id du container à afficher/masquer
     * @param bool   $rememberState Faut-il restaurer l'état de visibilité du container au chargement de la page
     *                              (mémorisé dans le localStorage du navigateur)
     * @return ToggleDetails
     */
    public function __invoke($detailsDivId, $rememberState = true)
    {
        $this->detailsDivId = $detailsDivId;
        $this->rememberState = $rememberState;
        $this->domid = "a-toggle-details-$detailsDivId";

        return $this;
    }

    /**
     * Spécifie le label (texte) du lien "afficher".
     *
     * @param string $showDetailsLabel Ex: "Afficher"
     * @return $this
     */
    public function setShowDetailsLabel($showDetailsLabel)
    {
        $this->showDetailsLabel = $showDetailsLabel;

        return $this;
    }

    /**
     * Spécifie le label (texte) du lien "masquer".
     *
     * @param string $hideDetailsLabel Ex: "Masquer"
     * @return $this
     */
    public function setHideDetailsLabel($hideDetailsLabel)
    {
        $this->hideDetailsLabel = $hideDetailsLabel;

        return $this;
    }

    /**
     * Spécifie la classe de l'icône figurant devant le lien "masquer".
     *
     * @param string $showDetailsIconClass Ex: 'fas fa-chevron-up';
     * @return ToggleDetails
     */
    public function setShowDetailsIconClass($showDetailsIconClass)
    {
        $this->showDetailsIconClass = $showDetailsIconClass;

        return $this;
    }

    /**
     * Spécifie la classe de l'icône figurant devant le lien "afficher".
     *
     * @param string $hideDetailsIconClass Ex: 'fas fa-chevron-down';
     * @return ToggleDetails
     */
    public function setHideDetailsIconClass($hideDetailsIconClass)
    {
        $this->hideDetailsIconClass = $hideDetailsIconClass;

        return $this;
    }

    /**
     * Spécifie le title du lien "afficher".
     *
     * @param string $showDetailsTitle Ex: "Cliquez pour afficher les détails"
     * @return ToggleDetails
     */
    public function setShowDetailsTitle($showDetailsTitle)
    {
        $this->showDetailsTitle = $showDetailsTitle;

        return $this;
    }

    /**
     * Spécifie le title du lien "masquer".
     *
     * @param string $hideDetailsTitle Ex: "Cliquez pour masquer les détails"
     * @return ToggleDetails
     */
    public function setHideDetailsTitle($hideDetailsTitle)
    {
        $this->hideDetailsTitle = $hideDetailsTitle;

        return $this;
    }

    /**
     * Retourne le code HTML généré par cette aide de vue.
     *
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->render();
        } catch (Exception $exc) {
            var_dump($exc->getMessage(), Util::formatTraceString($exc->getTraceAsString()));
            die;
        }
    }

    /**
     * Génère le code HTML.
     *
     * @return string
     */
    protected function render()
    {
        $icon = trim($this->showDetailsIconClass) ?
            "<span class=\"toggle-details-icon {$this->showDetailsIconClass}\" aria-hidden=\"true\"></span>" :
            "";

        $html = <<<EOS
<a href="#" id="{$this->domid}" class="toggle-details" 
   data-target="#{$this->detailsDivId}">$icon <span class="toggle-details-label">{$this->showDetailsLabel}</span></a>
EOS;

        $this->includeJs($html);

        return $html . PHP_EOL;
    }

    /**
     * Retourne le nom de l'événement jQuery déclenché (sur le 'body') lorsque le container est devenu visible.
     *
     * @return string
     */
    public function getJsEventNameWhenVisible()
    {
        return 'event-toggle-details-visible-' . $this->detailsDivId;
    }

    /**
     * Retourne le nom de l'événement jQuery déclenché (sur le 'body') lorsque le container est devenu invisible.
     *
     * @return string
     */
    public function getJsEventNameWhenInvisible()
    {
        return 'event-toggle-details-invisible-' . $this->detailsDivId;
    }

    /**
     *
     * @param string $html
     * @return ToggleDetails
     */
    protected function includeJs(&$html)
    {
        $rememberState = $this->rememberState ? 'true' : 'false';

        $js = <<<EOS
$(function() {
    var a = $("#{$this->domid}");
    var target = $(a.data('target'));
    var rememberState = $rememberState;
    var localStorageKey = 'targetIsVisible@' + a.data('target');
    var eventWhenVisible = "{$this->getJsEventNameWhenVisible()}";
    var eventWhenInvisible = "{$this->getJsEventNameWhenInvisible()}";
    a.on("click", function(e) {
        target.slideToggle(300, updateUI);
        e.preventDefault();
    });
    function updateUI() {
        var targetIsVisible = target.is(':visible');
        localStorage.setItem(localStorageKey, targetIsVisible);
        a.attr("title", targetIsVisible ? "{$this->hideDetailsTitle}" : "{$this->showDetailsTitle}");
        a.find(".toggle-details-icon").attr('class', targetIsVisible ? "toggle-details-icon {$this->hideDetailsIconClass}" : "toggle-details-icon {$this->showDetailsIconClass}");
        a.find(".toggle-details-label").html(targetIsVisible ? "{$this->hideDetailsLabel}" : "{$this->showDetailsLabel}");
        $("body").trigger(targetIsVisible ? eventWhenVisible : eventWhenInvisible, { target: target, a: a });
    }
    
    if (rememberState) {
        // state stored in localStorage as string, `toggle` needs boolean
        var targetIsVisible = localStorage.getItem(localStorageKey);
        if (targetIsVisible !== null) {
            target.toggle(targetIsVisible === 'true' ? true : false);
        }
    }

    updateUI();
});
EOS;

        $request = $this->getView()->getHelperPluginManager()->get('request');
        $isXmlHttpRequest = $request->isXmlHttpRequest();

        if ($isXmlHttpRequest) {
            // pour une requête AJAX on ne peut pas utilser le plugin "inlineScript"
            if (!static::$inlineJsAppended) {
                $html .= PHP_EOL . "<script>" . PHP_EOL . $js . PHP_EOL . "</script>";
                static::$inlineJsAppended = true;
            }
        } else {
            $this->getView()->inlineScript()->appendScript($js);
        }

        return $this;
    }
}

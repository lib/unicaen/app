<?php
namespace UnicaenApp\View\Helper;

use Laminas\View\Helper\AbstractHelper;

/**
 * Aide de vue générant le lien et les infos concernant la connexion à l'application.
 * 
 * En l'occurence, cette aide de vue ne génère rien car une application n'utilisant 
 * que le module UnicaenApp ne fournit rien permettant à l'utilisateur de se connecter.
 * 
 * Cette aide de vue existe simplement dans le but d'être surchargée par le module UnicaenUser.
 * 
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class AppConnection extends AbstractHelper
{
    /**
     * Point d'entrée.
     * 
     * @return AppConnection
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Retourne le code HTML généré par cette aide de vue.
     * 
     * @return string
     */
    public function __toString()
    {
        return '';
    }
}
<?php

namespace UnicaenApp\View\Helper;

use Psr\Container\ContainerInterface;
use Laminas\EventManager\EventManagerInterface;
use Laminas\Mvc\Controller\PluginManager;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger as FlashMessengerPlugin;

/**
 * @author Unicaen
 */
class MessengerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $messenger = new Messenger();

        /** @var FlashMessengerPlugin $flashMessengerPlugin */
        $flashMessengerPlugin = $container->get(PluginManager::class)->get('flashMessenger');
        $messenger->setPluginFlashMessenger($flashMessengerPlugin);

//        /* @var $application Application  */
//        $application = $container->get('application');
//        if ($application){
//            $messenger->setEventManager($application->getEventManager());
//        }
        /** @var EventManagerInterface $eventManager */
        $eventManager = $container->get('EventManager');
        $messenger->setEventManager($eventManager);

        return $messenger;
    }
}
<?php

namespace UnicaenApp\View\Strategy;

use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventManagerInterface;
use UnicaenApp\View\Renderer\XmlRenderer;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\ViewEvent;
use UnicaenApp\View\Model\XmlModel;

class XmlStrategy extends AbstractListenerAggregate
{

    /**
     * Multibyte character sets that will trigger a binary content-transfer-encoding
     *
     * @var array
     */
    protected $multibyteCharsets = [
        'UTF-16',
        'UTF-32',
    ];

    /**
     * @var XmlRenderer
     */
    protected $renderer;



    /**
     * Constructor
     *
     * @param  PhpRenderer $renderer
     */
    public function __construct(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }



    /**
     * {@inheritDoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RENDERER, [$this, 'selectRenderer'], $priority);
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RESPONSE, [$this, 'injectResponse'], $priority);
    }



    /**
     * Detect if we should use the XmlRenderer based on model type and/or
     * Accept header
     *
     * @param  ViewEvent $e
     *
     * @return null|JsonRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();

        if (!$model instanceof XmlModel) {
            // no XmlModel; do nothing
            return;
        }

        // XmlModel found
        return $this->renderer;
    }



    /**
     * Inject the response with the XML payload and appropriate Content-Type header
     *
     * @param  ViewEvent $e
     *
     * @return void
     */
    public function injectResponse(ViewEvent $e)
    {
        $renderer = $e->getRenderer();
        if ($renderer !== $this->renderer) {
            // Discovered renderer is not ours; do nothing
            return;
        }

        $result = $e->getResult();
        if (!is_string($result)) {
            // We don't have a string, and thus, no XML
            return;
        }

        $model = $e->getModel();
        /* @var $model XmlModel */

        if ($model instanceof XmlModel){
            $charset = $model->getCharset();

            /* On injecte l'entête XML */
            $result = '<?xml version="1.0" encoding="'.$charset.'"?>'."\n".$result;
            $e->setResult($result);
        }

        // Populate response
        $response = $e->getResponse();
        $response->setContent($result);

        if ($model instanceof XmlModel) {
            $headers = $response->getHeaders();

            $headers->addHeaderLine('content-type', 'text/xml; charset=' . $charset);
            $headers->addHeaderLine('Accept-Ranges', 'bytes');
            $headers->addHeaderLine('Content-Disposition', "attachment; filename=\"" . $model->getFilename() . "\"");
            $headers->addHeaderLine('Content-Length', strlen($result));

            if (in_array(strtoupper($charset), $this->multibyteCharsets)) {
                $headers->addHeaderLine('content-transfer-encoding', 'BINARY');
            }
        }
    }
}

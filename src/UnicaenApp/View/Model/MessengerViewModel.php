<?php

namespace UnicaenApp\View\Model;

use Laminas\View\Model\ViewModel;

/**
 * Description of MessengerViewModel
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class MessengerViewModel extends ViewModel
{
    public function __construct($variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setTemplate('unicaen-app/messenger.phtml');
    }
}
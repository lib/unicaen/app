<?php

namespace UnicaenApp\View\Model;

use Laminas\View\Model\ViewModel;


class XmlModel extends ViewModel
{

    /**
     * Character set for associated content-type
     *
     * @var string
     */
    protected $charset = 'utf-8';

    /**
     *
     * @var string
     */
    protected $filename = 'export.xml';

    /**
     * XML is usually terminal
     *
     * @var bool
     */
    protected $terminate = true;



    /**
     * Set the content-type character set
     *
     * @param  string $charset
     *
     * @return XmlStrategy
     */
    public function setCharset($charset)
    {
        $this->charset = (string)$charset;

        return $this;
    }



    /**
     * Retrieve the current character set
     *
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }



    /**
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }



    /**
     *
     * @param string $filename
     *
     * @return self
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }
}
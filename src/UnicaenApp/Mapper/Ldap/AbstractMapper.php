<?php

namespace UnicaenApp\Mapper\Ldap;

use Laminas\Ldap\Dn;
use Laminas\Ldap\Filter\AbstractFilter;
use Laminas\Ldap\Ldap;
use UnicaenApp\Exception\RuntimeException;

/**
 * Classe mère des services d'accès à l'annuaire LDAP.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
abstract class AbstractMapper
{
    /**
     * @var Ldap
     */
    protected $ldap;

    /**
     * @var array
     */
    protected $config;

    /**
     * Constructeur.
     *
     * @param Ldap  $ldap Objet d'accès à l'annuaire LDAP
     * @param array $config
     */
    public function __construct(Ldap $ldap = null, array $config = [])
    {
        $this->setLdap($ldap);
        $this->setConfig($config);
    }

    /**
     * Retourne la liste des attributs LDAP remontés dans les résultats de recherches.
     * NB: l'attribut 'dn' est forcément inclus.
     *
     * @return array e.g. array("mail", "sn", "cn")
     */
    abstract public function getAttributes();

    /**
     * Retourne l'objet d'accès à l'annuaire LDAP.
     *
     * @return Ldap
     */
    public function getLdap()
    {
        return $this->ldap;
    }

    /**
     * Spécifie l'objet d'accès à l'annuaire LDAP.
     *
     * @param Ldap $ldap
     * @return AbstractMapper
     */
    public function setLdap(Ldap $ldap = null)
    {
        $this->ldap = $ldap;

        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     * @return AbstractMapper
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param string $name
     * @param string $category
     * @return string
     */
    public function configParam($category, $name)
    {
        if (empty($this->config[$category][$name])) {
            throw new RuntimeException("Le tableau de config doit fournir une valeur valide pour le paramètre ['$category']['$name']");
        }

        return $this->config[$category][$name];
    }

    /**
     * Cherche une entrée dans l'annuaire LDAP selon un filtre puis parcourt récursivement le résultat trouvé
     * pour remplacer tout array qui ne contient qu'une seule valeur par cette valeur.
     *
     * @param string|AbstractFilter|array $filter
     * @param string                      $baseDn
     * @param array                       $attributes
     * @param string|null                 $sort
     * @param int                         $scope
     * @return array
     */
    public function searchSimplifiedEntry($filter, $baseDn = null, array $attributes = [], $sort = null, $scope = Ldap::SEARCH_SCOPE_SUB)
    {
        if (!$attributes) {
            $attributes = $this->getAttributes() ?: ['*'];
        }
        $entries = @$this->getLdap()->searchEntries($filter, $baseDn, $scope, $attributes, $sort);
        if (count($entries) > 1) {
            throw new RuntimeException("Plus d'une entrée trouvée avec ce filtre: " . $filter);
        }
        if (!$entries) {
            return null;
        }
        $entry = $entries[0];

        return self::simplifiedEntry($entry);
    }

    /**
     * Cherche des entrées dans l'annuaire LDAP selon un filtre puis parcourt récursivement les entrées trouvées
     * pour remplacer tout array qui ne contient qu'une seule valeur par cette valeur.
     *
     * @param  string|AbstractFilter|array $filter
     * @param  string|Dn|null              $basedn
     * @param  array                       $attributes
     * @param  string|null                 $sort
     * @param  integer                     $scope
     * @return array
     */
    public function searchSimplifiedEntries($filter, $basedn = null, array $attributes = [], $sort = null, $scope = Ldap::SEARCH_SCOPE_SUB)
    {
        if (!$attributes) {
            $attributes = $this->getAttributes() ?: ['*'];
        }
        $entries = $this->getLdap()->searchEntries($filter, $basedn, $scope, $attributes, $sort);
        foreach ($entries as $i => $entry) {
            $entries[$i] = self::simplifiedEntry($entry);
        }

        return $entries;
    }

    /**
     * Parcours récursivement le tableau spécifié pour remplacer tout array qui ne contient
     * qu'une seule valeur par cette valeur.
     *
     * @param array $entry
     * @param array $returnAttribs
     * @param array $omitAttribs
     * @return array Le tableau "simplifié"
     */
    static public function simplifiedEntry(array $entry, array $returnAttribs = [], array $omitAttribs = [])
    {
        $return = [];

        foreach ($entry as $attr => $value) {
            if (($returnAttribs && !in_array($attr, $returnAttribs)) || ($omitAttribs && in_array($attr, $omitAttribs))) {
                continue;
            }
            if (is_array($value)) {
                if (count($value) > 1) {
                    $return[$attr] = self::simplifiedEntry($value);
                } else {
                    $return[$attr] = count($value) === 1 ? $value[0] : [];
                }
            } else {
                $return[$attr] = $value;
            }
        }

        return $return;
    }
}

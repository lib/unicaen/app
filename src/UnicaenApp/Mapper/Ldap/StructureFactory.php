<?php

namespace UnicaenApp\Mapper\Ldap;

/**
 * @author Unicaen
 */
class StructureFactory extends AbstractMapperFactory
{
    /**
     * @var string Classe du mapper à instancier.
     */
    protected $mapperClass = Structure::class;
}

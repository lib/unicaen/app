<?php

namespace UnicaenApp\Mapper\Ldap;

/**
 * @author Unicaen
 */
class PeopleFactory extends AbstractMapperFactory
{
    /**
     * @var string Classe du mapper à instancier.
     */
    protected $mapperClass = People::class;
}

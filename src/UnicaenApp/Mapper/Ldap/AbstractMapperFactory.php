<?php

namespace UnicaenApp\Mapper\Ldap;

use Psr\Container\ContainerInterface;
use UnicaenApp\Options\ModuleOptions;
use Laminas\Ldap\Ldap;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Classe abstraite des constructeurs de mapper.
 *
 * @author Unicaen
 */
abstract class AbstractMapperFactory implements FactoryInterface
{
    /**
     * @var string Classe du mapper à instancier.
     */
    protected $mapperClass;

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ModuleOptions $ldapParams */
        $ldapParams = $container->get('unicaen-app_module_options');
        $ldapConfig = $ldapParams->getLdap();

        $className = $this->mapperClass;

        return new $className($this->createLdap($ldapConfig), $ldapConfig);
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * @param array $ldapConfig
     * @return Ldap
     * @throws \Laminas\Ldap\Exception\LdapException
     */
    protected function createLdap(array $ldapConfig)
    {
        $ldapConnectionParams = isset($ldapConfig['connection']['default']['params']) ?
            $ldapConfig['connection']['default']['params'] :
            [];

        return new Ldap($ldapConnectionParams);
    }
}

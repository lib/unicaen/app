<?php

namespace UnicaenApp\Mapper\Ldap;

/**
 * @author Unicaen
 */
class GroupFactory extends AbstractMapperFactory
{
    /**
     * @var string Classe du mapper à instancier.
     */
    protected $mapperClass = Group::class;
}

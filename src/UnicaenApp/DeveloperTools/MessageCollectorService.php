<?php

namespace UnicaenApp\DeveloperTools;

use Laminas\DeveloperTools\Collector\CollectorInterface;
use Laminas\Mvc\MvcEvent;
use UnicaenApp\Service\MessageCollectorAwareTrait;

class MessageCollectorService implements CollectorInterface
{
    use MessageCollectorAwareTrait;

    const NAME = 'messages';
    const PRIORITY = 200;

    public function getMessages(): array
    {
        return $this->messageCollector->getMessages();
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function getPriority(): int
    {
        return static::PRIORITY;
    }

    /**
     * {@inheritDoc}
     */
    public function collect(MvcEvent $mvcEvent)
    {
    }
}

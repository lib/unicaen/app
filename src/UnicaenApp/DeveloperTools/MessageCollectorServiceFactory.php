<?php

namespace UnicaenApp\DeveloperTools;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenApp\Service\MessageCollector;

class MessageCollectorServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): MessageCollectorService
    {
        $service = new MessageCollectorService();

        /** @var MessageCollector $messageCollector */
        $messageCollector = $container->get('MessageCollector');
        $service->setServiceMessageCollector($messageCollector);

        return $service;
    }

}
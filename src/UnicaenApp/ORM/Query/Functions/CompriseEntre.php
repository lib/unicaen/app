<?php

namespace UnicaenApp\ORM\Query\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class CompriseEntre extends FunctionNode
{
    public $histoCreation;
    public $histoDestruction;
    public $dateObs;

    public function getSql(SqlWalker $sqlWalker)
    {
        $sql = sprintf('comprise_entre(%s, %s',
                $this->histoCreation->dispatch($sqlWalker),
                $this->histoDestruction->dispatch($sqlWalker));
        
        if ($this->dateObs){
            $sql .= ','.$this->dateObs->dispatch($sqlWalker);
        }
        $sql .= ')';
        return $sql;
    }

    /**
     * NB: le format DQL attendu est "compriseEntre"
     * (et non pas "UNICAEN_ORACLE.COMPRISE_ENTRE").
     * 
     * @param Parser $parser
     */
    public function parse(Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->histoCreation = $parser->ArithmeticPrimary();
        if(Lexer::T_COMMA === $lexer->lookahead['type']){
            $parser->match(Lexer::T_COMMA);
            $this->histoDestruction = $parser->ArithmeticPrimary();
        }
        if(Lexer::T_COMMA === $lexer->lookahead['type']){
            $parser->match(Lexer::T_COMMA);
            $this->dateObs = $parser->ArithmeticPrimary();
        }
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
<?php

namespace UnicaenApp\ORM\Query\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Coalesce extends FunctionNode
{
    public Node $inputString;
    public Node $replacementString;

    /**
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->inputString = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->replacementString = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @throws \Doctrine\ORM\Query\AST\ASTException
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return sprintf('coalesce(%s, %s)',
                $this->inputString->dispatch($sqlWalker), 
                $this->replacementString->dispatch($sqlWalker));
    }
}
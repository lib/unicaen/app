<?php

namespace UnicaenApp\ORM\Query\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Replace extends FunctionNode
{
    public $inputString;
    public $searchString;
    public $replacementString;

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf('REPLACE(%s, %s, %s)', 
                $this->inputString->dispatch($sqlWalker), 
                $this->searchString->dispatch($sqlWalker), 
                $this->replacementString->dispatch($sqlWalker));
    }
    
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->inputString = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->searchString = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->replacementString = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
<?php

namespace UnicaenApp\ServiceManager;

use Psr\Container\ContainerInterface;

/**
 * Interface remplaçant provisoirement Laminas\ServiceManager\ServiceLocatorAwareInterface qui
 * dont l'usage déclenche un warning PHP car elle disparaît dans ZF3.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @deprecated À abandonner, svp
 */
interface ServiceLocatorAwareInterface
{
    /**
     * Set service locator
     *
     * @param ContainerInterface $serviceLocator
     * @deprecated À abandonner, svp
     */
    public function setServiceLocator(ContainerInterface $serviceLocator);

    /**
     * Get service locator
     *
     * @return ContainerInterface
     * @deprecated À abandonner, svp
     */
    public function getServiceLocator();
}

<?php

namespace UnicaenApp\ServiceManager;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\InitializerInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Initializer provisoire permettant d'injecter le service locator dans n'importe quel service,
 * pratique bannie dans ZF3.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @deprecated À abandonner, svp
 */
class ServiceLocatorAwareInitializer implements InitializerInterface
{
    /**
     * Initialize
     *
     * @param mixed                   $instance
     * @param ServiceLocatorInterface $serviceLocator
     * @deprecated À abandonner, svp
     */
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        $this->__invoke($serviceLocator, $instance);
    }

    public function __invoke(ContainerInterface $container, $instance)
    {
        if ($instance instanceof ServiceLocatorAwareInterface) {
            $instance->setServiceLocator($container);
        }
    }
}

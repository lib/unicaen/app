<?php

namespace UnicaenApp\ServiceManager;

use Psr\Container\ContainerInterface;

/**
 * Trait remplaçant provisoirement Laminas\ServiceManager\ServiceLocatorAwareTrait
 * qui a disparu dans ZF3.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @deprecated À abandonner, svp
 */
trait ServiceLocatorAwareTrait
{
    /**
     * @var ContainerInterface
     */
    protected $serviceLocator;

    /**
     * Set service locator
     *
     * @param ContainerInterface $serviceLocator
     * @deprecated À abandonner, svp
     */
    public function setServiceLocator(ContainerInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ContainerInterface
     * @deprecated À abandonner, svp
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}

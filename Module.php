<?php

namespace UnicaenApp;

use Laminas\EventManager\EventInterface;
use Laminas\Http\Request as HttpRequest;
use Laminas\ModuleManager\Feature\AutoloaderProviderInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\ControllerPluginProviderInterface;
use Laminas\ModuleManager\Feature\ServiceProviderInterface;
use Laminas\Mvc\I18n\Translator;
use Laminas\ServiceManager\ServiceManager;
use Laminas\Session\Container;
use Laminas\Session\SessionManager;
use Laminas\Validator\AbstractValidator;
use Laminas\View\Helper\Navigation;
use Laminas\View\HelperPluginManager;
use Locale;
use UnicaenApp\Controller\Plugin\MessengerPluginFactory;
use UnicaenApp\Controller\Plugin\Upload\UploaderPluginFactory;
use UnicaenApp\Mvc\Listener\MaintenanceListener;
use UnicaenApp\Mvc\Listener\ModalListener;
use UnicaenApp\Mvc\View\Http\ExceptionStrategy as HttpExceptionStrategy;
use UnicaenApp\Options\ModuleOptions;

define('__VENDOR_DIR__', dirname(dirname(__DIR__)));

/**
 * Point d'entrée du module.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Module implements
        BootstrapListenerInterface,
        ConfigProviderInterface,
        ControllerPluginProviderInterface,
        ServiceProviderInterface
{

    /**
     *
     * @return array
     * @see ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     *
     * @return array
     * @see AutoloaderProviderInterface
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Laminas\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * This method is called once the MVC bootstrapping is complete,
     * after the "loadModule.post" event, once $application->bootstrap() is called.
     *
     * @param EventInterface $e
     * @see BootstrapListenerInterface
     * @return array
     */
    public function onBootstrap(EventInterface $e) /* @var $e \Laminas\Mvc\MvcEvent */
    {
        /* @var $application \Laminas\Mvc\Application */
        $application = $e->getApplication();
        /* @var $services ServiceManager */
        $services    = $application->getServiceManager();

        $this->bootstrapSession($e);

        // la locale par défaut est celle du service de traduction
        /** @var \Laminas\I18n\Translator\Translator $translator */
        $translator = $services->get('translator');
        Locale::setDefault($translator->getLocale());

        AbstractValidator::setDefaultTranslator(new Translator($translator));

        $eventManager = $application->getEventManager();

        if ($e->getRequest() instanceof HttpRequest) {
            /** @var HttpExceptionStrategy $baseExceptionStrategy */
            $baseExceptionStrategy = $services->get('HttpExceptionStrategy');
            $exceptionStrategy = new HttpExceptionStrategy();
            $exceptionStrategy->setDisplayExceptions($baseExceptionStrategy->displayExceptions());
            $exceptionStrategy->attach($eventManager);
        }

        /* @var $vhm HelperPluginManager */
        $vhm = $services->get('ViewHelperManager');

        /* @var $nvh Navigation */
        $nvh = $vhm->get('Laminas\View\Helper\Navigation');
        $nvh->setServiceLocator($services);
        // Déclaration des plugins maison pour l'aide de vue de navigation
        $invokables = array(
            'menuPrincipal'  => 'UnicaenApp\View\Helper\Navigation\MenuPrincipal',
            'menuSecondaire' => 'UnicaenApp\View\Helper\Navigation\MenuSecondaire',
            'menuContextuel' => 'UnicaenApp\View\Helper\Navigation\MenuContextuel',
            'menuPiedDePage' => 'UnicaenApp\View\Helper\Navigation\MenuPiedDePage',
            'filAriane'      => 'UnicaenApp\View\Helper\Navigation\FilAriane',
            'plan'           => 'UnicaenApp\View\Helper\Navigation\Plan',
        );
        foreach ($invokables as $key => $value) {
            if (!$nvh->getPluginManager()->has($key)) {
                $nvh->getPluginManager()->setInvokableClass($key, $value);
            }
        }

        (new ModalListener())->attach($eventManager);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $services->get('unicaen-app_module_options');

        // détection du mode maintenance pour rendre l'appli indisponible
        $maintenanceListener = new MaintenanceListener($moduleOptions->getMaintenance());
        $maintenanceListener->attach($eventManager);

        $this->appendSessionRefreshJs($e);

        return [];
    }

    /**
     * Init session manager.
     *
     * @param \Laminas\Mvc\MvcEvent $e
     */
    public function bootstrapSession($e)
    {
        if (Util::isConsole()) {
            return;
        }

        /** @var SessionManager $sessionManager */
        $sessionManager = $e->getApplication()->getServiceManager()->get(SessionManager::class);
        Container::setDefaultManager($sessionManager);
        $sessionManager->start();

        $container = new Container('initialized');
        if (!isset($container->init)) {
            $sessionManager->regenerateId(true);
             $container->init = 1;
        }
    }



    /**
     * Ajoute au rendu de toutes les vues une ligne de Javascript (en inline)
     * appelant la fonction de rafraîchissement périodique de la session de l'utilisateur.
     *
     * @param EventInterface $e
     */
    public function appendSessionRefreshJs(EventInterface $e)
    {
        if (Util::isConsole()) {
            return;
        }

        $sm     = $e->getApplication()->getServiceManager();
        $period = $sm->get('unicaen-app_module_options')->getSessionRefreshPeriod();
        if ($period <= 0) {
            return;
        }

        $basePathHelper = $sm->get('ViewHelperManager')->get('BasePath');
        $url            = $basePathHelper() . "/refresh-session";
        $js             = "$(function() { refreshSession('$url', $period); });";

        $sm->get('ViewHelperManager')->get('inlineScript')->offsetSetScript(1001,$js);
    }

    /**
     * Expected to return \Laminas\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Laminas\ServiceManager\Config
     */
    public function getControllerPluginConfig()
    {
        return array(
            'factories' => array(
                'ldapPeopleService'    => 'UnicaenApp\Controller\Plugin\LdapPeopleServiceFactory',
                'ldapStructureService' => 'UnicaenApp\Controller\Plugin\LdapStructureServiceFactory',
                'ldapGroupService'     => 'UnicaenApp\Controller\Plugin\LdapGroupServiceFactory',
                'mail'                 => 'UnicaenApp\Controller\Plugin\MailFactory',
                'appInfos'             => 'UnicaenApp\Controller\Plugin\AppInfosFactory',
                'uploader'              => UploaderPluginFactory::class,
                'messenger'             => MessengerPluginFactory::class,
            ),
            'invokables' => array(
                'multipageForm'         => 'UnicaenApp\Controller\Plugin\MultipageFormPlugin',
                'modalInnerViewModel'   => 'UnicaenApp\Controller\Plugin\ModalInnerViewModel',
                'popoverInnerViewModel' => 'UnicaenApp\Controller\Plugin\PopoverInnerViewModel',
                'confirm'               => 'UnicaenApp\Controller\Plugin\ConfirmPlugin',
            ),
        );
    }

    /**
     *
     * @return array
     * @see ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return [];
    }
}
